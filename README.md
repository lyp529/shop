#ga 项目

## 技术框架
| 技术框架 | 版本 |
| --- | --- |
| php | 5.6.x 或以上 |
| mysql | 5.7.x |
| nodejs | 8.11+ |
| yii | 2.0.x |
| vue-cli | 3.x |

## 项目初始化

### 1 初始化 php 框架

#### 1.1 进入 `/php` 解压 vendor.zip

#### 1.2 生成yii所需的配置文件
执行命令。根据命令提示进行操作。一般选择开发环境
```
php init
```

#### 1.3 配置系统数据库。打开 `/php/common/config/main-local.php`
>如下：修改对应的配置。

```php
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=数据库名',
        'username' => '账号',
        'password' => '密码',
        'charset' => 'utf8',
    ],
```

### 2 初始化 nodejs 依赖库 

#### 2.1 切换到 `/` 目录 或 `/php` 目录。执行下面命令
>命令执行后，npm镜像将使用淘宝镜像（速度快）、安装 vue-cli 工具，以及下载 `/vue-cli` 下各模块的依赖库
```
yii node/init  [具体实现可查看NodeController.actionInit方法]
```

### 3 更新项目数据库
    
## 启动项目

### 开发环境（代码热更新）
    1. 执行命令 `vue ui` 启动vue项目管理工具
    2. 使用浏览器打开的工具，导入相关的 vue-cli 项目。
    
        如：`/vue-cli/backend`、`/vue-cli/frontend` 都是vue-cli项目
        
    3. 导入后，点击【任务】-【serve】-【运行】启动测试项目
    4. 点击项目管理工具右侧【输出】，可查看测试项目运行的地址，打开即可访问。点击【启动app】也可打开测试项目
    
### 生产环境（常规web访问）
    1. 执行命令 `yii node/build` 生成项目文件
    2. 访问 `/php` 目录下对应模块的 web 路径即可开启运行项目
    
        如：http://127.0.0.1/ga/php/backend/web
