<?php


namespace console\update;


use common\constants\C;
use common\models\db\KeyValue;
use common\models\db\UserAgent;
use common\models\db\UserMember;
use common\models\UserModel;
use yii\web\User;

/**
 * 2019年10月 更新数据的方法及
 * Class Update_1910
 * @package console\update
 */
class Update_1910 {
    /**
     * 修复会员数据。因注册时缺少了大股东、股东、总代代理信息
     */
    public static function fixUserMember() {
        $memberList = UserMember::findAll(["vendor_id" => 0]);
        foreach ($memberList as $member) {
            $agent = UserAgent::findOne($member->agent_id);
            if ($agent === null) {
                continue;
            }
            $member->vendor_id = $agent->vendor_id;
            $member->holder_id = $agent->holder_id;
            $member->super_holder_id = $agent->super_holder_id;
            $member->save();
        }
    }

    /**
     * 写入联系方式数据
     */
    public static function writeContactVo() {
        $memberContactVo = KeyValue::getMemberContactVo();
        if (empty($memberContactVo->qqs)) {
            $memberContactVo->qqs = ["1302278079", "2483303495"];
            KeyValue::saveMemberContactVo($memberContactVo);
        }
    }
}