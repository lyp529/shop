<?php

use yii\db\Migration;

/**
 * Class m190923_095838_change_notice_type_table_name
 */
class m190923_095838_change_notice_type_table_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190923_095838_change_notice_type_table_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190923_095838_change_notice_type_table_name cannot be reverted.\n";

        return false;
    }
    */
}
