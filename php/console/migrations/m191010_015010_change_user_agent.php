<?php

use yii\db\Migration;

/**
 * Class m191010_015010_change_user_agent
 */
class m191010_015010_change_user_agent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_agent","invite_code",$this->string(50)->comment("邀请码"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191010_015010_change_user_agent cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191010_015010_change_user_agent cannot be reverted.\n";

        return false;
    }
    */
}
