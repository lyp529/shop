<?php

use yii\db\Migration;

/**
 * Class m191017_112930_create_tab_earn_rank_real_offset
 */
class m191017_112930_create_tab_earn_rank_real_offset extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tab_earn_rank_real_offset` (
  `er_offset` int(11) NOT NULL COMMENT '保存每次查询tab_earn_rank_real的offset',
  `record_time` datetime NULL 
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci COMMENT='盈利排行榜配置表（真实数据）';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191017_112930_create_tab_earn_rank_real_offset cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_112930_create_tab_earn_rank_real_offset cannot be reverted.\n";

        return false;
    }
    */
}
