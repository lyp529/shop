<?php

use yii\db\Migration;

/**
 * Class m190928_040611_update_tab_schedule_field_NULL
 */
class m190928_040611_update_tab_schedule_field_NULL extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE tab_schedule MODIFY COLUMN payout_time DATETIME DEFAULT NULL COMMENT '派彩时间'");
        $this->execute("ALTER TABLE tab_schedule MODIFY COLUMN result_nums VARCHAR(10) DEFAULT NULL COMMENT '中奖号码1@2@3'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190928_040611_update_tab_schedule_field_NULL cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190928_040611_update_tab_schedule_field_NULL cannot be reverted.\n";

        return false;
    }
    */
}
