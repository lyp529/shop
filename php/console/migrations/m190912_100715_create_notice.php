<?php

use yii\db\Migration;

/**
 * Class m190912_100715_create_notice
 */
class m190912_100715_create_notice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        CREATE TABLE IF NOT EXISTS `tab_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL COMMENT '操作人员',
  `notice_type` tinyint(1) NOT NULL COMMENT '类型',
  `record_date` datetime NOT NULL COMMENT '添加时间',
  `update_date` datetime NULL COMMENT '更新时间',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='公告表' AUTO_INCREMENT=1 ;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("tab_notice");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190912_100715_create_notice cannot be reverted.\n";

        return false;
    }
    */
}
