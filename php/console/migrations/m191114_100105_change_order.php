<?php

use yii\db\Migration;

/**
 * Class m191114_100105_change_order
 */
class m191114_100105_change_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE tab_order MODIFY COLUMN express_no char(33) NULL ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191114_100105_change_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191114_100105_change_order cannot be reverted.\n";

        return false;
    }
    */
}
