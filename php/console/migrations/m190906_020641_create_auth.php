<?php

use yii\db\Migration;

/**
 * Class m190906_020641_create_auth
 */
class m190906_020641_create_auth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("auth_role", [
            "id" => $this->primaryKey(),
            "type" => $this->integer()->notNull()->defaultValue(1)->comment("角色类型。1用户 2分组"),
            "bind_id" => $this->integer()->notNull()->comment("绑定的数据表id。如用户id、分组id"),
            "record_time" => $this->dateTime()->notNull()->comment("创建时间"),
        ], "CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = '权限角色'");
        $this->createIndex("bind_id", "auth_role", "bind_id");

        $this->createTable("auth_item", [
            "id" => $this->primaryKey(),
            "role_id" => $this->integer()->notNull()->comment("角色id"),
            "item_name" => $this->string(64)->notNull()->comment("权限项目名字"),
            "record_time" => $this->dateTime()->notNull()->comment("创建时间"),
        ], "CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = '权限表'");
        $this->createIndex("role_id", "auth_item", "role_id");
        $this->createIndex("item_name", "auth_item", "item_name");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190906_020641_create_auth cannot be reverted.\n";

        return false;
    }

    
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190906_020641_create_auth cannot be reverted.\n";

        return false;
    }
    */
}
