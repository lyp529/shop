<?php

use yii\db\Migration;

/**
 * Class m191010_031435_change_field_credit
 */
class m191010_031435_change_field_credit extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE user_agent MODIFY COLUMN credit DECIMAL(10,2) NOT NULL");
        $this->EXECUTE("ALTER TABLE user_holder MODIFY COLUMN credit DECIMAL(10,2) NOT NULL");
        $this->EXECUTE("ALTER TABLE user_member MODIFY COLUMN credit DECIMAL(10,2) NOT NULL");
        $this->EXECUTE("ALTER TABLE user_super_holder MODIFY COLUMN credit DECIMAL(10,2) NOT NULL");
        $this->EXECUTE("ALTER TABLE user_vendor MODIFY COLUMN credit DECIMAL(10,2) NOT NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191010_031435_change_field_credit cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191010_031435_change_field_credit cannot be reverted.\n";

        return false;
    }
    */
}
