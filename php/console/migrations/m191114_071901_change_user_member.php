<?php

use yii\db\Migration;

/**
 * Class m191114_071901_change_user_member
 */
class m191114_071901_change_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE user_member MODIFY COLUMN integral decimal(12,2) NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191114_071901_change_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191114_071901_change_user_member cannot be reverted.\n";

        return false;
    }
    */
}
