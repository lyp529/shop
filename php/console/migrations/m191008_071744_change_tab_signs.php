<?php

use yii\db\Migration;

/**
 * Class m191008_071744_change_tab_signs
 */
class m191008_071744_change_tab_signs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_member", "last_sign_date", $this->date()->comment("签到时间"));
        $this->dropTable("tab_sign");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191008_071744_change_tab_signs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_071744_change_tab_signs cannot be reverted.\n";

        return false;
    }
    */
}
