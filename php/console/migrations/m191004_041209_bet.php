<?php

use yii\db\Migration;

/**
 * Class m191004_041209_bet
 */
class m191004_041209_bet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `tab_bet` ADD `agent_id` INT NOT NULL AFTER `member_id` ,
ADD `vendor_id` INT NOT NULL AFTER `agent_id` ,
ADD `holder_id` INT NOT NULL AFTER `vendor_id` ,
ADD `super_holder_id` INT NOT NULL AFTER `holder_id`");
        $this->execute("select 1;ALTER TABLE `tab_bet` ADD `group_num` INT NOT NULL COMMENT '分组的数量' AFTER `unit`");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191004_041209_bet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191004_041209_bet cannot be reverted.\n";

        return false;
    }
    */
}
