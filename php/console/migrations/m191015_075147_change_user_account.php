<?php

use yii\db\Migration;

/**
 * Class m191015_075147_change_user_account
 */
class m191015_075147_change_user_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn("user_account", "login_date", "login_time");
        $this->addColumn("user_account", "last_login_time", $this->dateTime()->comment("上次登录时间")->after("login_ip"));
        $this->addColumn("user_account", "last_login_ip", $this->string(100)->comment("上次登录IP")->after("last_login_time"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191015_075147_change_user_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_075147_change_user_account cannot be reverted.\n";

        return false;
    }
    */
}
