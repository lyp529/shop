<?php

use yii\db\Migration;

/**
 * Class m190929_114221__debug_log
 */
class m190929_114221__debug_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `_debug_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_type` varchar(20) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `record_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `log_type` (`log_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据库日志表' ;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190929_114221__debug_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_114221__debug_log cannot be reverted.\n";

        return false;
    }
    */
}
