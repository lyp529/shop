<?php

use yii\db\Migration;

/**
 * Class m190923_025107_tab_change_tab_order_wallet
 */
class m190923_025107_tab_change_tab_order_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("tab_order_wallet", "order_sub_type", $this->integer()->notNull()->comment("存取款方式")->after("order_type"));
        $this->addColumn("tab_order_wallet", "bank_no", $this->string()->comment("银行卡编号")->after("pay_type"));
        $this->addColumn("tab_order_wallet", "bank_name", $this->string()->comment("开户姓名")->after("bank_no"));
        $this->addColumn("tab_order_wallet", "remark", $this->string(500)->comment("备注")->after("bank_name"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190923_025107_tab_change_tab_order_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190923_025107_tab_change_tab_order_wallet cannot be reverted.\n";

        return false;
    }
    */
}
