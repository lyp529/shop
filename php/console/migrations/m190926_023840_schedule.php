<?php

use yii\db\Migration;

/**
 * Class m190926_023840_schedule
 */
class m190926_023840_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `tab_schedule` ADD UNIQUE (
`kind` ,
`period`
);");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190926_023840_schedule cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190926_023840_schedule cannot be reverted.\n";

        return false;
    }
    */
}
