<?php

use yii\db\Migration;

/**
 * Class m191014_074641_change_tab_earnings_ranking
 */
class m191014_074641_change_tab_earnings_ranking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("tab_earnings_ranking");

        $this->execute("CREATE TABLE `tab_earnings_ranking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '中奖用户名',
  `er_type` int(11) NOT NULL COMMENT '中奖玩法',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '中奖金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci AUTO_INCREMENT=1;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191014_074641_change_tab_earnings_ranking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_074641_change_tab_earnings_ranking cannot be reverted.\n";

        return false;
    }
    */
}
