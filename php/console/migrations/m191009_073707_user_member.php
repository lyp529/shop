<?php

use yii\db\Migration;

/**
 * Class m191009_073707_user_member
 */
class m191009_073707_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `user_member` ADD `last_withdraw_date` DATE NULL COMMENT '最近提款时间',
ADD `last_withdraw_times` INT NOT NULL DEFAULT '0' COMMENT '最近提款次数'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191009_073707_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191009_073707_user_member cannot be reverted.\n";

        return false;
    }
    */
}
