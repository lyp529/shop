<?php

use yii\db\Migration;

/**
 * Class m190907_093734_change_tab_user
 */
class m190907_093734_change_tab_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("tab_user","is_admin",$this->tinyInteger()->notNull()->defaultValue(0)->comment("是否是超级管理员")->after('account_type'));
        $this->dropColumn("tab_user","account_type");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("tab_user","is_admin");

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190907_093734_change_tab_user cannot be reverted.\n";

        return false;
    }
    */
}
