<?php

use yii\db\Migration;

/**
 * Class m190927_072158_change_tab_bet
 */
class m190927_072158_change_tab_bet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("tab_bet", "bet_amount", $this->decimal(10, 2)->comment("用户下注金额"));
        $this->addColumn("tab_bet", "bet_date", $this->date()->comment("下注日期"));
        $this->createIndex("bet_date", "tab_bet", "bet_date");
        $this->addColumn("tab_bet", "update_time", $this->dateTime()->comment("数据更新时间"));
        $this->addColumn("tab_bet", "record_time", $this->dateTime()->comment("数据创建时间"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190927_072158_change_tab_bet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_072158_change_tab_bet cannot be reverted.\n";

        return false;
    }
    */
}
