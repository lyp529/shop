<?php

use yii\db\Migration;

/**
 * Class m191010_021510_change_tab_order_wallet
 */
class m191010_021510_change_tab_order_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn("tab_order_wallet", "bank_name", "bank_username");
        $this->renameColumn("tab_order_wallet", "bank_no", "bank_account");
        $this->addColumn("tab_order_wallet", "bank_name", $this->string(50)->comment("银行名字。如：中国工商银行")->after("pay_type"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191010_021510_change_tab_order_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191010_021510_change_tab_order_wallet cannot be reverted.\n";

        return false;
    }
    */
}
