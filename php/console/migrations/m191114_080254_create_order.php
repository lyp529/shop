<?php

use yii\db\Migration;

/**
 * Class m191114_080254_create_order
 */
class m191114_080254_create_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tab_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `admin_id` int(11) DEFAULT NULL COMMENT '操作人员ID',
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `order_number` char(33) COLLATE utf8_german2_ci NOT NULL COMMENT '订单编号',
  `express_no` char(33) COLLATE utf8_german2_ci NOT NULL COMMENT '快递编号',
  `order_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:待发货,2:已发货',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `integral` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单所需积分',
  `record_date` datetime NOT NULL COMMENT '添加时间',
  `update_date` datetime NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `订单编号` (`order_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci COMMENT='订单表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191114_080254_create_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191114_080254_create_order cannot be reverted.\n";

        return false;
    }
    */
}
