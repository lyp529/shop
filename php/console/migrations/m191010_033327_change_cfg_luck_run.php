<?php

use yii\db\Migration;

/**
 * Class m191010_033327_change_cfg_luck_run
 */
class m191010_033327_change_cfg_luck_run extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE cfg_luck_run MODIFY COLUMN reward_value DECIMAL(10,2) NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191010_033327_change_cfg_luck_run cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191010_033327_change_cfg_luck_run cannot be reverted.\n";

        return false;
    }
    */
}
