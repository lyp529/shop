<?php

use yii\db\Migration;

/**
 * Class m190925_022916_change_tab_schedule
 */
class m190925_022916_change_tab_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn("tab_schedule", "payout_date", "payout_time");
        $this->alterColumn("tab_schedule", "period", $this->string(20)->comment("期数"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190925_022916_change_tab_schedule cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190925_022916_change_tab_schedule cannot be reverted.\n";

        return false;
    }
    */
}
