<?php

use yii\db\Migration;

/**
 * Class m191021_034247_order_wallet
 */
class m191021_034247_order_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->execute("select 1;ALTER TABLE `tab_order_wallet` ADD `agent_id` INT NOT NULL AFTER `user_id` ,
ADD `vendor_id` INT NOT NULL AFTER `agent_id` ,
ADD `holder_id` INT NOT NULL AFTER `vendor_id` ,
ADD `super_holder_id` INT NOT NULL AFTER `holder_id`
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m191021_034247_order_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_034247_order_wallet cannot be reverted.\n";

        return false;
    }
    */
}
