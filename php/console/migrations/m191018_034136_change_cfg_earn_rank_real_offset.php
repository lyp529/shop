<?php

use yii\db\Migration;

/**
 * Class m191018_034136_change_cfg_earn_rank_real_offset
 */
class m191018_034136_change_cfg_earn_rank_real_offset extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("cfg_earn_rank_real_offset");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191018_034136_change_cfg_earn_rank_real_offset cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191018_034136_change_cfg_earn_rank_real_offset cannot be reverted.\n";

        return false;
    }
    */
}
