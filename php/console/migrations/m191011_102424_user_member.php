<?php

use yii\db\Migration;

/**
 * Class m191011_102424_user_member
 */
class m191011_102424_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->execute("select 1; ALTER TABLE `user_member` ADD `total_pay` INT( 11 ) NOT NULL DEFAULT '0' COMMENT '累计充值',
ADD `total_bet` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0' COMMENT '累计下注(消费-达100%后一起清0)'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m191011_102424_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191011_102424_user_member cannot be reverted.\n";

        return false;
    }
    */
}
