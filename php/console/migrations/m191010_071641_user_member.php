<?php

use yii\db\Migration;

/**
 * Class m191010_071641_user_member
 */
class m191010_071641_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `user_member` ADD `bank_id` INT NULL ,
ADD `bank_name` VARCHAR( 50 ) NULL ,
ADD `bank_account` VARCHAR( 50 ) NULL ,
ADD `bank_username` VARCHAR( 50 ) NULL ,
ADD `bank_email` VARCHAR( 100 ) NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191010_071641_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191010_071641_user_member cannot be reverted.\n";

        return false;
    }
    */
}
