<?php

use yii\db\Migration;

/**
 * Class m190930_032055_cash_record
 */
class m190930_032055_cash_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("DROP TABLE IF EXISTS `tab_cash_record`;
CREATE TABLE IF NOT EXISTS `tab_cash_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `record_no` varchar(100) NOT NULL COMMENT '单号-不同功能代表不同单呈',
  `clog_type` int(11) NOT NULL COMMENT '帐变类型',
  `clog_type_label` varchar(200) NOT NULL COMMENT '帐户日志类型名称',
  `params` varchar(200) NOT NULL COMMENT '抽象类型',
  `amount` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注(部分备注由类型,注单编号,内容自动生成)',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `record_time` datetime NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`),
  KEY `user_type` (`user_type`,`user_id`,`clog_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='现金帐变记录表' AUTO_INCREMENT=1 ;");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190930_032055_cash_record cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_032055_cash_record cannot be reverted.\n";

        return false;
    }
    */
}
