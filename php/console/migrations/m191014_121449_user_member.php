<?php

use yii\db\Migration;

/**
 * Class m191014_121449_user_member
 */
class m191014_121449_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `user_member` ADD `before_date` DATETIME NULL COMMENT '上次记录之前信息的时间' AFTER `before_credit`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191014_121449_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_121449_user_member cannot be reverted.\n";

        return false;
    }
    */
}
