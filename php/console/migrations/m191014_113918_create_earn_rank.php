<?php

use yii\db\Migration;

/**
 * Class m191014_113918_create_earn_rank
 */
class m191014_113918_create_earn_rank extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("tab_earnings_ranking");
        $this->dropTable("cfg_earnings_ranking");

        $this->execute("CREATE TABLE `tab_earn_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '中奖用户名',
  `er_type` int(11) NOT NULL COMMENT '中奖玩法',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '中奖金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci AUTO_INCREMENT=1 COMMENT='盈利排行榜数据表';");

        $this->execute("CREATE TABLE `cfg_earn_rank_offset` (
  `er_offset` int(11) NOT NULL COMMENT '保存每次查询tab_earnings_ranking的offset'
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci COMMENT='盈利排行榜配置表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191014_113918_create_earn_rank cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_113918_create_earn_rank cannot be reverted.\n";

        return false;
    }
    */
}
