<?php

use yii\db\Migration;

/**
 * Class m191019_013004_log_operate
 */
class m191019_013004_log_operate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1; RENAME TABLE log_operation` TO log_operate`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191019_013004_log_operate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191019_013004_log_operate cannot be reverted.\n";

        return false;
    }
    */
}
