<?php

use yii\db\Migration;

/**
 * Class m191014_101916_create_cfg_earnings_ranking
 */
class m191014_101916_create_cfg_earnings_ranking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `cfg_earnings_ranking` (
  `er_offset` int(11) NOT NULL COMMENT '保存每次查询tab_earnings_ranking的offset'
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci COMMENT='盈利排行榜配置表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191014_101916_create_cfg_earnings_ranking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_101916_create_cfg_earnings_ranking cannot be reverted.\n";

        return false;
    }
    */
}
