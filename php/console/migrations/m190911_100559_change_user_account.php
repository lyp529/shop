<?php

use yii\db\Migration;

/**
 * Class m190911_100559_change_user_account
 */
class m190911_100559_change_user_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn("user_account", "record_date", "record_time");
        $this->renameColumn("user_account", "modify_date", "update_time");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190911_100559_change_user_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190911_100559_change_user_account cannot be reverted.\n";

        return false;
    }
    */
}
