<?php

use yii\db\Migration;

/**
 * Class m191014_093734_tab_schedule
 */
class m191014_093734_tab_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->execute("select 1;ALTER TABLE `tab_schedule` ADD `predict_nums` VARCHAR( 100 ) NULL COMMENT '预设结果' AFTER `result_nums`");
        $this->execute("select 1;ALTER TABLE `tab_schedule` DROP INDEX `kind_2`");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m191014_093734_tab_schedule cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_093734_tab_schedule cannot be reverted.\n";

        return false;
    }
    */
}
