<?php

use yii\db\Migration;

/**
 * Class m191016_085523_change_tab_earn_rank
 */
class m191016_085523_change_tab_earn_rank extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("TRUNCATE TABLE tab_earn_rank");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191016_085523_change_tab_earn_rank cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191016_085523_change_tab_earn_rank cannot be reverted.\n";

        return false;
    }
    */
}
