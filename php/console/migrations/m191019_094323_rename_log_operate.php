<?php

use yii\db\Migration;

/**
 * Class m191019_094323_rename_log_operate
 */
class m191019_094323_rename_log_operate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable("log_operation", "log_operate");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191019_094323_rename_log_operate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191019_094323_rename_log_operate cannot be reverted.\n";

        return false;
    }
    */
}
