<?php

use yii\db\Migration;

/**
 * Class m191023_130732_chang_log_login
 */
class m191023_130732_chang_log_login extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("log_login", "username", $this->string(100)->comment("用户名")->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191023_130732_chang_log_login cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191023_130732_chang_log_login cannot be reverted.\n";

        return false;
    }
    */
}
