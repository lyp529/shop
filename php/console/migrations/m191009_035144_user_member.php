<?php

use yii\db\Migration;

/**
 * Class m191009_035144_user_member
 */
class m191009_035144_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191009_035144_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191009_035144_user_member cannot be reverted.\n";

        return false;
    }
    */
}
