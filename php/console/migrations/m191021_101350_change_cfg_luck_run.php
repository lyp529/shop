<?php

use yii\db\Migration;

/**
 * Class m191021_101350_change_cfg_luck_run
 */
class m191021_101350_change_cfg_luck_run extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn("cfg_luck_run","active");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191021_101350_change_cfg_luck_run cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_101350_change_cfg_luck_run cannot be reverted.\n";

        return false;
    }
    */
}
