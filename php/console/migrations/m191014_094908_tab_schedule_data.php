<?php

use yii\db\Migration;

/**
 * Class m191014_094908_tab_schedule_data
 */
class m191014_094908_tab_schedule_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("REPLACE INTO `cfg_kind` (`kind`, `kind_name`, `start_time`, `end_time`, `minute`, `periods`, `active`) VALUES
(103, '柏林五分彩', '00:00:00', '23:55:00', 5, 288, 1);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191014_094908_tab_schedule_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_094908_tab_schedule_data cannot be reverted.\n";

        return false;
    }
    */
}
