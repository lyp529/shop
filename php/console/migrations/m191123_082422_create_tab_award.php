<?php

use yii\db\Migration;

/**
 * Class m191123_082422_create_tab_award
 */
class m191123_082422_create_tab_award extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("tab_award");

        $this->execute("CREATE TABLE `tab_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `key` varchar(255) NOT NULL COMMENT '类型',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci AUTO_INCREMENT=1 COMMENT='奖励中心';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191123_082422_create_tab_award cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_082422_create_tab_award cannot be reverted.\n";

        return false;
    }
    */
}
