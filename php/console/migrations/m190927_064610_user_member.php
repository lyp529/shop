<?php

use yii\db\Migration;

/**
 * Class m190927_064610_user_member
 */
class m190927_064610_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `user_member` ADD `last_back_date` DATE NULL COMMENT '最近返还下单金额日' AFTER `credit`");
        $this->execute("select 1;ALTER TABLE `user_member` ADD `vip_level` INT( 1 ) NOT NULL DEFAULT '0' COMMENT 'VIP等级' AFTER `name`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190927_064610_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_064610_user_member cannot be reverted.\n";

        return false;
    }
    */
}
