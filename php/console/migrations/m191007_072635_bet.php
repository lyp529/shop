<?php

use yii\db\Migration;

/**
 * Class m191007_072635_bet
 */
class m191007_072635_bet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `tab_bet` ADD `username` VARCHAR( 100 ) NOT NULL COMMENT '用户名' AFTER `schedule_id` ,
ADD INDEX ( username )");
        $this->execute("select 1;ALTER TABLE `tab_bet` ADD `bet_balance` DECIMAL( 8, 2 ) NOT NULL COMMENT '投注后的余额' AFTER `bet_date`");
        $this->execute("select 1;ALTER TABLE `user_admin` CHANGE `user_name` `username` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '重复数据-便于查看'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191007_072635_bet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191007_072635_bet cannot be reverted.\n";

        return false;
    }
    */
}
