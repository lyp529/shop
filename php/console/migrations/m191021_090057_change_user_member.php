<?php

use yii\db\Migration;

/**
 * Class m191021_090057_change_user_member
 */
class m191021_090057_change_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_member", "place_order_rebated", $this->decimal(12,2)->comment("昨天下单返利")->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191021_090057_change_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_090057_change_user_member cannot be reverted.\n";

        return false;
    }
    */
}
