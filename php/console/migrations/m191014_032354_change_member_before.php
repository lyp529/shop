<?php

use yii\db\Migration;

/**
 * Class m191014_032354_change_member_before
 */
class m191014_032354_change_member_before extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_member","before_credit",$this->decimal(10,2)->comment("昨日账户余额"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191014_032354_change_member_before cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_032354_change_member_before cannot be reverted.\n";

        return false;
    }
    */
}
