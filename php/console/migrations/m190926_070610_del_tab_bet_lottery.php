<?php

use yii\db\Migration;

/**
 * Class m190926_070610_del_tab_bet_lottery
 */
class m190926_070610_del_tab_bet_lottery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("tab_bet_order");
        $this->dropTable("tab_lottery");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190926_070610_del_tab_bet_lottery cannot be reverted.\n";

        return false;
    }
    */
}
