<?php

use yii\db\Migration;

/**
 * Class m191015_063801_insert_cfg_luck_run
 */
class m191015_063801_insert_cfg_luck_run extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE cfg_luck_run MODIFY COLUMN reward_value INT(11) NULL");

        $this->execute("INSERT INTO cfg_luck_run(id,reward_type,info,rate,active,reward_value) VALUE(1,1,'未中奖',77,1,0),(2,2,'元',3,1,5),(3,2,'元',2,1,10),(4,2,'元',1,1,20),(5,3,'积分',5,1,500),(6,3,'积分',5,1,800),(7,3,'积分',4,1,1000),(8,3,'积分',3,1,1500)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191015_063801_insert_cfg_luck_run cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_063801_insert_cfg_luck_run cannot be reverted.\n";

        return false;
    }
    */
}
