<?php

use yii\db\Migration;

/**
 * Class m191011_101415_change_member_vipdate
 */
class m191011_101415_change_member_vipdate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_member","before_vip_level",$this->integer(1)->comment("昨日vip等级"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191011_101415_change_member_vipdate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191011_101415_change_member_vipdate cannot be reverted.\n";

        return false;
    }
    */
}
