<?php

use yii\db\Migration;

/**
 * Class m190919_041201_tab_signin
 */
class m190919_041201_tab_signin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tab_sign',[
            'id' => $this->primaryKey(),
            'user_id' => $this->string(50)->notNull()->comment("用户id"),
            'sign_datetime' => $this->date()->notNull()->comment("签到时间"),
            'record_time' => $this->dateTime()->notNull()->comment("创建时间"),
        ],"CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = '签到列表'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190919_041201_tab_signin cannot be reverted.\n";

        return false;
    }
    */
}
