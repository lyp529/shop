<?php

use yii\db\Migration;

/**
 * Class m191015_113819_insert_cfg_luck_run
 */
class m191015_113819_insert_cfg_luck_run extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("DELETE FROM cfg_luck_run ");
        $this->execute("insert  into `cfg_luck_run` values (1,1,'谢谢参与',72,1,0),(2,2,'现金',7,1,5),(3,2,'现金',6,1,10),(4,2,'现金',5,1,18),(5,2,'现金',4,1,28),(6,2,'现金',3,1,38),(7,2,'现金',2,1,88),(8,2,'现金',1,1,8888)");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191015_113819_insert_cfg_luck_run cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_113819_insert_cfg_luck_run cannot be reverted.\n";

        return false;
    }
    */
}
