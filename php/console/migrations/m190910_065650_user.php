<?php

use yii\db\Migration;

/**
 * Class m190910_065650_user
 */
class m190910_065650_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(1) NOT NULL COMMENT '用户类型',
  `user_type_name` varchar(20) NOT NULL COMMENT '用户类型名称',
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(100) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `record_date` datetime NOT NULL,
  `modify_date` datetime DEFAULT NULL,
  `modify_user_type` int(11) DEFAULT NULL,
  `modify_user_id` int(11) NOT NULL,
  `modify_username` varchar(100) DEFAULT NULL,
  `modify_ip` varchar(30) DEFAULT NULL,
  `session_id` varchar(100) DEFAULT NULL,
  `login_fail_times` int(11) NOT NULL COMMENT '登录失败次数',
  `lock` int(11) NOT NULL COMMENT '是否锁定',
  `login_date` datetime DEFAULT NULL COMMENT '最近登录时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最近登录ip',
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`modify_user_type`,`modify_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户帐号表' AUTO_INCREMENT=1 ;
        ");
        $this->execute("CREATE TABLE IF NOT EXISTS `user_admin` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL COMMENT '重复数据-便于查看',
  `name` varchar(100) NOT NULL COMMENT '重复数据-便于查看',
  `is_admin` int(11) NOT NULL COMMENT '是否超级管理员',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';");

        $this->execute("CREATE TABLE IF NOT EXISTS `user_agent` (
  `user_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL,
  `fraction_vendor` decimal(3,2) NOT NULL,
  `fration_max` decimal(3,2) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理表';");

        $this->execute("CREATE TABLE IF NOT EXISTS `user_holder` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `fraction_max` double NOT NULL COMMENT '股东占成上限',
  `fraction_super_holder` double NOT NULL COMMENT '大股东占成',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='股东表';");

        $this->execute("CREATE TABLE IF NOT EXISTS `user_member` (
  `user_id` int(11) NOT NULL,
  `is_direct` int(11) NOT NULL COMMENT '是否直属会员',
  `direct_user_type` int(11) NOT NULL COMMENT '直属上线类型',
  `direct_user_id` int(11) NOT NULL COMMENT '直属上线的ID',
  `super_holder_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员表';");

        $this->execute("CREATE TABLE IF NOT EXISTS `user_super_holder` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL,
  `commission_plan` int(11) NOT NULL,
  `company_report` int(11) NOT NULL COMMENT '是否可查看公司报表',
  `include_below` int(11) NOT NULL COMMENT '是否包底',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='大股东表';");

        $this->execute("CREATE TABLE IF NOT EXISTS `user_vendor` (
  `user_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL,
  `fraction_holder` decimal(3,2) NOT NULL COMMENT '股东占成',
  `fraction_max` double(3,2) NOT NULL COMMENT '总代理占成上限',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='总代理表';");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190910_065650_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190910_065650_user cannot be reverted.\n";

        return false;
    }
    */
}
