<?php

use yii\db\Migration;

/**
 * Class m191123_123628_create_tab_order
 */
class m191123_123628_create_tab_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE tab_order ADD goods_name VARCHAR(255) NOT NULL COMMENT '商品名'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191123_123628_create_tab_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_123628_create_tab_order cannot be reverted.\n";

        return false;
    }
    */
}
