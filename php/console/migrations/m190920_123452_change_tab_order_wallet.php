<?php

use yii\db\Migration;

/**
 * Class m190920_123452_change_tab_order_wallet
 */
class m190920_123452_change_tab_order_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn("tab_order_wallet", "order_number", "order_no");
        $this->addColumn("tab_order_wallet", "update_time", $this->dateTime());
        $this->addColumn("tab_order_wallet", "record_time", $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190920_123452_change_tab_order_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190920_123452_change_tab_order_wallet cannot be reverted.\n";

        return false;
    }
    */
}
