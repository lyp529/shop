<?php

use yii\db\Migration;

/**
 * Class m191014_133226_decimal
 */
class m191014_133226_decimal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->execute("ALTER TABLE `tab_bet` CHANGE `bet_balance` `bet_balance` DECIMAL( 12, 2 ) NOT NULL COMMENT '投注后的余额'");
        $this->execute("ALTER TABLE `tab_bet` CHANGE `bet_result` `bet_result` DECIMAL( 12, 2 ) NOT NULL COMMENT '派彩结果'");
        $this->execute("ALTER TABLE `tab_bet` CHANGE `bet_amount` `bet_amount` DECIMAL( 12, 2 ) NULL DEFAULT NULL COMMENT '用户下注金额'");
        $this->execute("ALTER TABLE `user_member` CHANGE `credit` `credit` DECIMAL( 12, 2 ) NOT NULL");
        $this->execute("ALTER TABLE `user_member` CHANGE `total_bet` `total_bet` DECIMAL( 12, 2 ) NOT NULL DEFAULT '0.00' COMMENT '累计下注(消费)'");
        $this->execute("ALTER TABLE `user_member` CHANGE `before_credit` `before_credit` DECIMAL( 12, 2 ) NULL DEFAULT NULL COMMENT '昨日账户余额'");
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m191014_133226_decimal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_133226_decimal cannot be reverted.\n";

        return false;
    }
    */
}
