<?php

use yii\db\Migration;

/**
 * Class m191201_051211_change_tab_cash_rewards
 */
class m191201_051211_change_tab_cash_rewards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE tab_cash_rewards  MODIFY COLUMN value DECIMAL(10,3) NOT NULL;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191201_051211_change_tab_cash_rewards cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191201_051211_change_tab_cash_rewards cannot be reverted.\n";

        return false;
    }
    */
}
