<?php

use yii\db\Migration;

/**
 * Class m191015_114402_change_cfg_earn_rank_offset
 */
class m191015_114402_change_cfg_earn_rank_offset extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("cfg_earn_rank_offset", "record_time", $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191015_114402_change_cfg_earn_rank_offset cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_114402_change_cfg_earn_rank_offset cannot be reverted.\n";

        return false;
    }
    */
}
