<?php

use yii\db\Migration;

/**
 * Class m190927_080708_change_del_bet_lottery
 */
class m190927_080708_change_del_bet_lottery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("tab_bet_order");
        $this->dropTable("tab_lottery");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("tab_bet_order");
        $this->dropTable("tab_lottery");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_080708_change_del_bet_lottery cannot be reverted.\n";

        return false;
    }
    */
}
