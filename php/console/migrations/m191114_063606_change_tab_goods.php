<?php

use yii\db\Migration;

/**
 * Class m191114_063606_change_tab_goods
 */
class m191114_063606_change_tab_goods extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("tab_goods");

        $this->execute("CREATE TABLE `tab_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credits` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '兑换积分',
  `bonus_points` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '奖励分',
  `name` varchar(255) NOT NULL COMMENT '商品名称',
  `content` text NOT NULL COMMENT '描述',
  `images` varchar(255) NULL COMMENT '图片',
  `record_date` datetime NOT NULL COMMENT '添加时间',
  `update_date` datetime NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci AUTO_INCREMENT=1 COMMENT='商品表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191114_063606_change_tab_goods cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191114_063606_change_tab_goods cannot be reverted.\n";

        return false;
    }
    */
}
