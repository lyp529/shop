<?php

use yii\db\Migration;

/**
 * Class m191009_041127_change_tab_cash_record
 */
class m191009_041127_change_tab_cash_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191009_041127_change_tab_cash_record cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191009_041127_change_tab_cash_record cannot be reverted.\n";

        return false;
    }
    */
}
