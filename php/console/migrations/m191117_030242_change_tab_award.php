<?php

use yii\db\Migration;

/**
 * Class m191117_030242_change_tab_award
 */
class m191117_030242_change_tab_award extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191117_030242_change_tab_award cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191117_030242_change_tab_award cannot be reverted.\n";

        return false;
    }
    */
}
