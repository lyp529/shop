<?php

use yii\db\Migration;

/**
 * Class m191021_025757_change_user_account
 */
class m191021_025757_change_user_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("user_account", "session_dead_time", $this->dateTime()->comment("session有效截止时间。用于判断会员是否登录")->after("session_id"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191021_025757_change_user_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_025757_change_user_account cannot be reverted.\n";

        return false;
    }
    */
}
