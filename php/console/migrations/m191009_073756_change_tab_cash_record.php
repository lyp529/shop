<?php

use yii\db\Migration;

/**
 * Class m191009_073756_change_tab_cash_record
 */
class m191009_073756_change_tab_cash_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE tab_cash_record MODIFY COLUMN record_no VARCHAR(100) DEFAULT NULL COMMENT '单号-不同功能代表不同单呈'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191009_073756_change_tab_cash_record cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191009_073756_change_tab_cash_record cannot be reverted.\n";

        return false;
    }
    */
}
