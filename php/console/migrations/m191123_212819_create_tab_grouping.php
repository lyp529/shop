<?php

use yii\db\Migration;

/**
 * Class m191123_212819_create_tab_grouping
 */
class m191123_212819_create_tab_grouping extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->execute("CREATE TABLE `tab_grouping` (
  `user_id` int(11) NOT NULL,
  `a_array` varchar(255) NOT NULL COMMENT 'A组',
  `b_array` varchar(255) NOT NULL COMMENT 'B组',
  `self_array` varchar(255) NOT NULL COMMENT '自己所在组',
  UNIQUE KEY `用户id` (`user_id`)
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci AUTO_INCREMENT=1 COMMENT='AB组';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191123_212819_create_tab_grouping cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_212819_create_tab_grouping cannot be reverted.\n";

        return false;
    }
    */
}
