<?php

use yii\db\Migration;

/**
 * Class m190929_130629_create_cfg_luck_run
 */
class m190929_130629_create_cfg_luck_run extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `cfg_luck_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_type` int(11) NOT NULL,
  `info` varchar(100) NOT NULL,
  `rate` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='积分轮盘配置表' AUTO_INCREMENT=1;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190929_130629_create_cfg_luck_run cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_130629_create_cfg_luck_run cannot be reverted.\n";

        return false;
    }
    */
}
