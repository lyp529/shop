<?php

use yii\db\Migration;

/**
 * Class m191114_072345_create_cash_rewards
 */
class m191114_072345_create_cash_rewards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tab_cash_rewards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL COMMENT '类型',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '值',
  `condition` int(11) NOT NULL DEFAULT '0' COMMENT '条件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 collate = utf8mb4_unicode_ci AUTO_INCREMENT=1 COMMENT='兑换奖励表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191114_072345_create_cash_rewards cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191114_072345_create_cash_rewards cannot be reverted.\n";

        return false;
    }
    */
}
