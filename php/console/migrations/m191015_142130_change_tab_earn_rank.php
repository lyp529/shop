<?php

use yii\db\Migration;

/**
 * Class m191015_142130_change_tab_earn_rank
 */
class m191015_142130_change_tab_earn_rank extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("UPDATE tab_earn_rank SET er_type = 103");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191015_142130_change_tab_earn_rank cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191015_142130_change_tab_earn_rank cannot be reverted.\n";

        return false;
    }
    */
}
