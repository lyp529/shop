<?php

use yii\db\Migration;

/**
 * Class m190919_115152_create_order_wallet
 */
class m190919_115152_create_order_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tab_order_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `admin_id` int(11) DEFAULT NULL COMMENT '操作人员ID',
  `order_number` char(33) COLLATE utf8_german2_ci NOT NULL COMMENT '订单编号',
  `order_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:存款,2:取款',
  `order_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:待处理,2:已完成,3:已取消,4:已失败',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `finish_time` datetime DEFAULT NULL COMMENT '完成支付时间',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `pay_type` smallint(2) NOT NULL COMMENT '支付通道',
  PRIMARY KEY (`id`),
  UNIQUE KEY `订单编号` (`order_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci COMMENT='钱包订单表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("tab_order_wallet");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190919_115152_create_order_wallet cannot be reverted.\n";

        return false;
    }
    */
}
