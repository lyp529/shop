<?php

use yii\db\Migration;

/**
 * Class m190920_064956_betting_record
 */
class m190920_064956_betting_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("tab_lottery",[
            'id' => $this->primaryKey(),
            'status' => $this->integer(10)->notNull()->comment("状态：1未出结果 2已出结果"),
            'trem' => $this->string(50)->notNull()->comment("期数"),
            'result' => $this->string(50)->notNull()->comment("开奖结果"),
            'open_time' => $this->dateTime()->notNull()->comment("开奖时间"),
            'update_time' => $this->dateTime()->notNull()->comment("更新时间"),
            'record_time' => $this->dateTime()->notNull()->comment("创建时间"),
        ],"CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = '开奖结果记录'");


        $this->createTable("tab_bet_order",[
            'id' => $this->primaryKey(),
            'lottery_id' => $this->integer(10)->notNull()->comment('投注结果ID'),
            'user_id' => $this->string(50)->notNull()->comment("用户ID"),
            'status' => $this->integer(10)->notNull()->comment("状态：1待支付 2等待结果 3完成"),
            'result' => $this->text()->comment("投注记录"),
            'num_notes' => $this->integer(10)->notNull()->comment("注(倍数)"),
            'pay_ment' => $this->float(11)->notNull()->comment("实付金额"),
            'order_no' => $this->string(50)->notNull()->comment("投注订单号"),
            'pay_time' => $this->datetime()->notNull()->comment("支付时间"),
            'update_time' => $this->datetime()->notNull()->comment("更新时间"),
            'record_time' => $this->datetime()->notNull()->comment("创建时间"),
        ],"CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = '投注记录表'");
        $this->createIndex("lottery_id", "tab_bet_order", "lottery_id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("ab_bet_order");
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190920_064956_betting_record cannot be reverted.\n";

        return false;
    }
    */
}
