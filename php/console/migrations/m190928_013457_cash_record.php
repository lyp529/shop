<?php

use yii\db\Migration;

/**
 * Class m190928_013457_cash_record
 */
class m190928_013457_cash_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `tab_cash_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cash_type` int(11) NOT NULL COMMENT '帐变类型',
  `bet_no` varchar(100) DEFAULT NULL COMMENT '注单编号(注单类的帐户变才有值)',
  `content` varchar(200) NOT NULL COMMENT '抽象类型',
  `amount` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `remark` varchar(500) NOT NULL COMMENT '备注(部分备注由类型,注单编号,内容自动生成)',
  PRIMARY KEY (`id`),
  KEY `user_type` (`user_type`,`user_id`,`cash_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='现金帐变记录表' AUTO_INCREMENT=1 ;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190928_013457_cash_record cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190928_013457_cash_record cannot be reverted.\n";

        return false;
    }
    */
}
