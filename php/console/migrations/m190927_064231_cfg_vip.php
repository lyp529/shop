<?php

use yii\db\Migration;

/**
 * Class m190927_064231_cfg_vip
 */
class m190927_064231_cfg_vip extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `cfg_vip` (
  `level` int(11) NOT NULL COMMENT 'VIP等级',
  `name` varchar(50) NOT NULL COMMENT 'VIP名称',
  `amount` int(11) NOT NULL COMMENT '金额',
  `amount_desc` varchar(100) NOT NULL COMMENT '金额描述',
  `withdraw_times` int(11) NOT NULL COMMENT '每日可提现次数',
  `single_limit` int(11) NOT NULL COMMENT '单笔限额',
  `back_percent` int(11) NOT NULL COMMENT '每天下单金额返回百分比',
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='VIP等级配置';");

        $this->execute("REPLACE INTO `cfg_vip` (`level`, `name`, `amount`, `amount_desc`, `withdraw_times`, `single_limit`, `back_percent`) VALUES
(0, '青铜会员', 0, '5万以下', 2, 5000, 0),
(1, '白银会员', 50000, '5万', 2, 5000, 0),
(2, '黄金会员', 150000, '15万', 2, 10000, 1),
(3, '铂金会员', 300000, '30万', 2, 20000, 2),
(4, '钻石会员', 500000, '50万', 2, 40000, 4),
(5, '皇冠会员', 1000000, '100万', 2, 100000, 8);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190927_064231_cfg_vip cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190927_064231_cfg_vip cannot be reverted.\n";

        return false;
    }
    */
}
