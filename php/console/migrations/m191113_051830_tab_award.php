<?php

use yii\db\Migration;

/**
 * Class m191113_051830_tab_award
 */
class m191113_051830_tab_award extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->execute("
create table tab_award(
  `team` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '团队',
  `logistics` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '物流',
  `director` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '主管',
  `manager` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '经理',
  `majordomo` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总监',
  `chairman` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '董事'
) comment '奖励中心' engine = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191113_051830_tab_award cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191113_051830_tab_award cannot be reverted.\n";

        return false;
    }
    */
}
