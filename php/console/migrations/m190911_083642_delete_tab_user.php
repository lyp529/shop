<?php

use yii\db\Migration;

/**
 * Class m190911_083642_delete_tab_user
 */
class m190911_083642_delete_tab_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable("tab_user");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190911_083642_delete_tab_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190911_083642_delete_tab_user cannot be reverted.\n";

        return false;
    }
    */
}
