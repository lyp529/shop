<?php

use yii\db\Migration;

/**
 * Class m190924_161243_table
 */
class m190924_161243_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `cfg_kind` (
  `kind` int(11) NOT NULL,
  `kind_name` varchar(100) NOT NULL,
  `start_time` time NOT NULL COMMENT '每天开始时间',
  `end_time` time NOT NULL COMMENT '每天结束时间',
  `minute` int(11) NOT NULL COMMENT '每期多少分钟',
  `periods` int(11) NOT NULL COMMENT '每天总期数',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT '有效无效',
  PRIMARY KEY (`kind`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='彩票类型配置表';");

        $this->execute("
INSERT INTO `cfg_kind` (`kind`, `kind_name`, `start_time`, `end_time`, `minute`, `periods`, `active`) VALUES
(101, '重庆时时彩', '00:30:00', '23:50:00', 20, 60, 1),
(102, '曼谷时时彩', '00:00:00', '23:55:00', 5, 288, 1),
(201, '广东11选5', '09:30:00', '23:50:00', 20, 142, 1),
(202, '江西11选5', '09:30:00', '23:50:00', 20, 142, 1),
(301, '江苏快3', '08:50:00', '22:10:00', 20, 41, 1),
(302, '北京快3', '09:20:00', '23:40:00', 20, 44, 1),
(303, '曼谷快3', '09:00:00', '22:00:00', 5, 157, 1);");


        $this->execute("
CREATE TABLE IF NOT EXISTS `tab_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(2) NOT NULL COMMENT '彩票类型',
  `kind_name` varchar(100) DEFAULT NULL COMMENT '类型名称-重复数据方便查看',
  `open_time` datetime NOT NULL COMMENT '开赛时间',
  `period` int(10) NOT NULL COMMENT '期号',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0未开奖 1已开奖',
  `active` int(1) NOT NULL COMMENT '是否开启',
  `record_time` datetime NOT NULL COMMENT '新增时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `result_nums` varchar(10) NOT NULL COMMENT '中奖号码1@2@3',
  `payout_date` datetime NOT NULL COMMENT '派彩时间',
  PRIMARY KEY (`id`),
  KEY `active` (`active`),
  KEY `game_type` (`kind`),
  KEY `open_time` (`open_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='彩票赛事表' AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `tab_bet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '注单ID',
  `bet_no` varchar(100) NOT NULL COMMENT '注单编号-uniqid',
  `schedule_id` int(11) NOT NULL COMMENT '场次ID',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `bet_type` varchar(100) NOT NULL COMMENT '下注类型',
  `bet_type_name` varchar(200) NOT NULL COMMENT '下注类型名称',
  `bet_content` varchar(1000) NOT NULL COMMENT '下注内容',
  `multiple` int(11) NOT NULL COMMENT '倍数',
  `unit` tinyint(1) NOT NULL COMMENT '单位 1元 2角',
  `bet_amount` int(11) NOT NULL COMMENT '下注金额',
  `bet_result` decimal(10,2) NOT NULL COMMENT '派彩结果',
  PRIMARY KEY (`id`),
  KEY `bet_no` (`bet_no`,`schedule_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='注单表' AUTO_INCREMENT=1 ;");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190924_161243_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190924_161243_table cannot be reverted.\n";

        return false;
    }
    */
}
