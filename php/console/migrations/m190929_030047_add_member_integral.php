<?php

use yii\db\Migration;

/**
 * Class m190929_030047_add_member_integral
 */
class m190929_030047_add_member_integral extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE user_member ADD integral INT(11) DEFAULT 0 NULL COMMENT '积分'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190929_030047_add_member_integral cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_030047_add_member_integral cannot be reverted.\n";

        return false;
    }
    */
}
