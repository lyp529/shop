<?php

use yii\db\Migration;

/**
 * Class m190918_012257_create_tab_cron
 */
class m190918_012257_create_tab_cron extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
create table log_cron_record(
    id          int auto_increment primary key,
    cron_id     int            not null comment '任务id',
    reason      tinyint(3)     not null comment '运行结果。1成功 -1出错',
    use_second  decimal(10, 4) null comment '用时秒数',
    record_time int            not null comment '记录时间'
) comment '记录定时任务运行的状态' engine = InnoDB collate = utf8mb4_unicode_ci;");

        $this->execute("
create table tab_cron (
    id            int auto_increment primary key,
    name          varchar(50)          not null comment '定时任务名称',
    route         varchar(50)          not null comment '定时任务路由',
    cron_time     varchar(50)          not null comment '运行时间。crontab格式',
    status        tinyint(1)           not null comment '任务状态。1运行中 2运行结束',
    last_run_time datetime             not null comment '上次运行时间',
    next_run_time datetime             not null comment '下次运行时间',
    active        tinyint(1) default 1 not null comment '启用状态。1启用 0关闭',
    update_time   datetime             not null comment '任务配置更新时间',
    record_time   datetime             not null comment '任务配置创建时间',
    constraint name unique (name)
) comment '定时任务配置' engine = InnoDB collate = utf8mb4_unicode_ci;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("log_cron_record");
        $this->dropTable("tab_cron");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190918_012257_create_tab_cron cannot be reverted.\n";

        return false;
    }
    */
}
