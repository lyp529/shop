<?php

use yii\db\Migration;

/**
 * Class m191011_081338_create_tab_key_value
 */
class m191011_081338_create_tab_key_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("tab_key_value", [
            "key" => $this->string(255)->unique()->comment("存储数据的key"),
            "value" => $this->text()->comment("存储内容"),
        ], "CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = 'key value 存储引擎'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("tab_key_value");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191011_081338_create_tab_key_value cannot be reverted.\n";

        return false;
    }
    */
}
