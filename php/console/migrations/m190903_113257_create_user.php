<?php

use yii\db\Migration;

/**
 * Class m190903_113257_create_user
 */
class m190903_113257_create_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tab_user',[
            "id" => $this->primaryKey(),
            "account" => $this->string(50)->notNull()->comment("登录名"),
            "password" => $this->string(64)->notNull()->comment("密码"),
            "name" => $this->string(50)->notNull()->comment("用户名"),
            "account_type" => $this->integer(11)->notNull()->comment('用户类型。1管理员 2代理'),
            "update_time" => $this->dateTime()->notNull()->comment("更新时间"),
            "record_time" => $this->dateTime()->notNull()->comment("创建时间"),
            ],"CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB COMMENT = '用户列表'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("tab_user");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190903_113257_create_user cannot be reverted.\n";

        return false;
    }
    */
}
