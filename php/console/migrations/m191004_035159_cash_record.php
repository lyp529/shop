<?php

use yii\db\Migration;

/**
 * Class m191004_035159_cash_record
 */
class m191004_035159_cash_record extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `tab_cash_record` DROP `user_type`");
        $this->execute("select 1;ALTER TABLE `tab_cash_record` CHANGE `user_id` `member_id` INT( 11 ) NOT NULL");
        $this->execute("select 1;ALTER TABLE `tab_cash_record` ADD `agent_id` INT NOT NULL AFTER `member_id` ,
ADD `vendor_id` INT NOT NULL AFTER `agent_id` ,
ADD `holder_id` INT NOT NULL AFTER `vendor_id` ,
ADD `super_holder_id` INT NOT NULL AFTER `holder_id`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191004_035159_cash_record cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191004_035159_cash_record cannot be reverted.\n";

        return false;
    }
    */
}
