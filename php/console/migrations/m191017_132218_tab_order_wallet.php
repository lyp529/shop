<?php

use yii\db\Migration;

/**
 * Class m191017_132218_tab_order_wallet
 */
class m191017_132218_tab_order_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `tab_order_wallet` ADD `pay_username` VARCHAR( 200 ) NULL COMMENT '支付用户信息' AFTER `pay_type`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191017_132218_tab_order_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_132218_tab_order_wallet cannot be reverted.\n";

        return false;
    }
    */
}
