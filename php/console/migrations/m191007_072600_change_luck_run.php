<?php

use yii\db\Migration;

/**
 * Class m191007_072600_change_luck_run
 */
class m191007_072600_change_luck_run extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->EXECUTE("ALTER TABLE cfg_luck_run ADD reward_value INT(11) DEFAULT 0 NULL COMMENT '奖励值'");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191007_072600_change_luck_run cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191007_072600_change_luck_run cannot be reverted.\n";

        return false;
    }
    */
}
