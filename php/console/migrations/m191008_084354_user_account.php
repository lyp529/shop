<?php

use yii\db\Migration;

/**
 * Class m191008_084354_user_account
 */
class m191008_084354_user_account extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("select 1;ALTER TABLE `user_account` ADD `password2` VARCHAR( 500 ) NULL COMMENT '资金密码' AFTER `password`");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191008_084354_user_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191008_084354_user_account cannot be reverted.\n";

        return false;
    }
    */
}
