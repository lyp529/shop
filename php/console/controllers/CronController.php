<?php
/**
 * Created by PhpStorm.
 * User: 11054
 * Date: 2018/9/22
 * Time: 15:55
 */

namespace console\controllers;

use common\constants\C;
use common\exceptions\ForeseeableException;
use common\models\db\Cron;
use common\models\db\CronRecord;
use common\utils\CronParserUtil;
use common\utils\FileUtil;
use common\utils\TimeUtil;
use Yii;
use yii\console\ExitCode;
use yii\db\Exception;

/**
 * 通用自动运行
 * Class CronController
 */
class CronController extends BaseConsoleController
{
    public $defaultAction = "run";

    /**
     * 定时任务入口
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionRun() {
        /** @var Cron[] $cronList */
        $cronList = Cron::find()->where(["active" => C::TRUE, "status" => C::CRON_STATUS_FINISHED])->andWhere(["<=", "next_run_time", TimeUtil::now()])->all();

        $pool = []; // 进程池

        $startSecond = microtime(true);

        foreach ($cronList as $cron) {
            $cron->status = C::CRON_STATUS_RUNNING;
            $cron->save();

            $pool[] = proc_open("php " . FileUtil::getPhpDir() . "/yii $cron->route", [], $pipe);
        }

        // 回收子进程
        while (count($pool)) {
            foreach ($pool as $index => $result) {
                $status = proc_get_status($result);
                if ($status['running'] == FALSE) {
                    $now = TimeUtil::now();
                    proc_close($result);
                    unset($pool[$index]);

                    // 修改任务状态
                    $cron = $cronList[$index];
                    $cron->status = C::CRON_STATUS_FINISHED;
                    $cron->last_run_time = $now;
                    $cron->next_run_time = $cron->getNextRunTime();
                    $cron->update_time = $now;

                    $endSecond = microtime(true);
                    $useTime = $endSecond - $startSecond;
                    $cronRecord = new CronRecord();
                    $cronRecord->cron_id = $cron->id;
                    $cronRecord->reason = C::CRON_REASON_SUCCESS;
                    $cronRecord->use_second = $useTime;
                    $cronRecord->record_time = strtotime($now);

                    // 任务出错
                    if ($status['exitcode'] !== ExitCode::OK) {
                        $cronRecord->reason = C::CRON_REASON_ERROR;
                    }

                    $cron->save();
                    if ($cronRecord->insert() === false) {
                        echo $cronRecord->getFirstError();
                    }
                }
            }

            usleep(100);
        }
    }

    /**
     * 初始化定时任务配置
     * yii cron/init
     * 定时任务：
     *      name        唯一名字（一般不能更改）
     *      route       运行的路由
     *      cronTime    运行的周期（和crontab配置相同）
     * @throws \Exception
     */
    public function actionInit() {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $nowTime = TimeUtil::now();

            /** @var Cron[] $cronList 需要启动的定时任务 */
            // 注意：名字是任务的唯一标识。所以不能修改
            $cronList = [
                Cron::initBase("记录玩家0点时的信息", "cron-do/mark-user-data", "0 0 * * *"),
                Cron::initBase("自动退水", "cron-do/bet-back-daily", "1 0 * * *"),
                Cron::initBase("清理垃圾数据", "cron-do/clear-trash", "0 5 * * *"),
                Cron::initBase("自动处理私彩数据", "cron-do/ssc-personal", "* * * * *"),
                Cron::initBase("自动处理重庆时时彩", "cron-do/ssc-cq", "* * * * *"),
                Cron::initBase("刷新在线人数", "cron-do/refresh-online-num", "* * * * *"),
            ];
            echo "======== 定时任务 开始更新 ========" . PHP_EOL . PHP_EOL;
            echo "=> 关闭所有定时任务" . PHP_EOL;
            Cron::updateAll(["active" => C::FALSE]);

            foreach ($cronList as $newCron) {
                if (!CronParserUtil::check($newCron->cron_time)) {
                    throw new Exception("不支持这种 cron time 格式：" . $newCron->cron_time);
                }

                $cron = Cron::findOne(["name" => $newCron->name]);
                if ($cron === null) {// 没有存在过的定时任务
                    $cron = $newCron;
                    $cron->status = C::CRON_STATUS_FINISHED;
                    $cron->last_run_time = $nowTime;
                    $cron->next_run_time = $nowTime;
                    $cron->record_time = $nowTime;
                    echo "=> 新增 [" . $cron->getDetailName() . "]" . PHP_EOL;
                } else {// 已存在的定时任务，进行更新操作
                    if ($cron->route === $newCron->route && $cron->cron_time === $newCron->cron_time) {
                        echo "=> 启动 [" . $cron->getDetailName() . "]" . PHP_EOL;
                    } else {
                        $cron->route = $newCron->route;
                        $cron->cron_time = $newCron->cron_time;
                        $cron->next_run_time = $cron->getNextRunTime();
                        echo "=> 更新 [" . $cron->getDetailName() . "]" . PHP_EOL;
                    }
                }
                $cron->active = C::TRUE;
                $cron->update_time = $nowTime;
                if ($cron->save() === false) {
                    throw new ForeseeableException("定时任务更新失败：" . $cron->getFirstError());
                }
                usleep(100000);
            }
            echo PHP_EOL . "======== 定时任务 更新完成 ========" . PHP_EOL;

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
