<?php
/**
 * Created by PhpStorm.
 * User: 11054
 * Date: 2018/9/22
 * Time: 15:55
 */

namespace console\controllers;

use common\constants\C;
use common\exceptions\SystemException;
use common\services\ScheduleService;
use common\services\SM;
use common\utils\ApiUtil;

/**
 * 通用自动运行
 * Class CronController
 */
class CronDoController extends BaseConsoleController
{



    /**
     * 处理每天退水返还
     */
    public function actionBetBackDaily() {
        SM::getUserService()->betBackDaily();
    }


    /**
     * 处理记录玩家每天0点时的信息
     * yii cron-do/mark-user-data
     * @throws \yii\db\Exception
     */
    public function actionMarkUserData() {
        SM::getUserService()->markUserBeforeData();

    }

    /**
     * 抓取重庆时时彩记录并保存派彩
     * 在根目录运行命令: yii cron-do/ssc-cq (以后可能会放到ssh,每同秒运行一次)
     * @throws \Throwable
     */
    public function actionSscCq() {
        // 自动生成重庆数据
        SM::getScheduleService()->generateOfficialSchedule(C::KIND_SSC_CHONGQING);

        //SscDataVo 时时彩取网数据
        try{
            $sccDataVo = ApiUtil::getSscDataVo();
            SM::getScheduleService()->updateSSCResult($sccDataVo);
        }catch ( \Exception $exception){
        }

        //SscDataVo2 时时彩取网数据
        try{
            $sccDataVo = ApiUtil::getSscDataVo2();
            SM::getScheduleService()->updateSSCResult($sccDataVo);
        }catch ( \Exception $exception){
        }

        // 保存完结果后进行自动派彩
        SM::getPayoutService()->autoPayout(C::KIND_SSC_CHONGQING);
    }



    /**
     * 生成私彩数据
     * yii cron-do/ssc-personal
     * @throws SystemException
     */
    public function actionSscPersonal() {
        $personalKinds = [C::KIND_SSC_BERLIN];
        foreach ($personalKinds as $kind) {
            SM::getScheduleService()->generatePersonSchedule($kind);
            SM::getScheduleService()->openPersonalSchedule($kind);
            SM::getPayoutService()->autoPayout($kind);
        }
    }

    /**
     * 刷新在线人数
     */
    public function actionRefreshOnlineNum() {
        SM::getSystemService()->refreshOnlineNum();
    }

    /**
     * 清理垃圾数据
     */
    public function actionClearTrash() {
        SM::getSystemService()->clearTrash();
    }
}
