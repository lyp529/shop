<?php


namespace console\controllers;


use common\utils\FileUtil;
use common\utils\StringUtil;
use common\utils\YiiUtil;
use console\update\Update_1910;
use Yii;
use yii\helpers\Console;

/**
 * Class UpdateController 更新项目使用的控制器
 * @package console\controllers
 */
class UpdateController extends BaseConsoleController {
    /**
     * @var string 默认动作
     */
    public $defaultAction = "one";
    /**
     * @var bool 是否更新了 SVN
     */
    public $isSvnUp = false;


    /**
     * 一键更新系统的命令
     */
    public function actionOne() {
        // 更新svn
        $this->actionSvn();

        // 更新后端
        Console::output("PHP更新 开始...");
        $this->actionPhp();
        Console::output("PHP更新 完成");

        // 更新前端
        Console::output("nodejs更新 开始...");
        $this->actionNode();
        Console::output("nodejs更新 完成");
    }

    /**
     * 数据升级
     */
    public function actionData() {
        Update_1910::fixUserMember();
        Update_1910::writeContactVo();
    }

    /**
     * 更新php模块
     */
    public function actionPhp() {
        // 更新svn
        $this->actionSvn();

        chdir(FileUtil::getPhpDir());
        system("php yii migrate/up --interactive=0");   // 更新数据库
        $this->actionData();  // 数据升级

        // 更新定时任务
        chdir(FileUtil::getPhpDir());
        system("php yii cron/init");
    }

    /**
     * 更新nodejs模块
     */
    public function actionNode() {
        // 更新svn
        $this->actionSvn();

        chdir(FileUtil::getPhpDir());
        system("php yii node/update-all");  // 更新更新 node 项目
        system("php yii node/build-all");   // 生成 node 项目
    }

    public function actionSvn() {
        if ($this->isSvnUp) {
            return;
        }
        chdir(FileUtil::getRootDir());

        $SVN_BIN = Yii::$app->params["SVN_BIN"];
        system("{$SVN_BIN} up");

        $this->isSvnUp = true;
    }

    /**
     * 更新表格模型
     *
     * GII gii/model 命令帮助
     *
     *   --appconfig: string
     *   custom application configuration file path.
     *   If not set, default application configuration is used.
     *
     *   --baseClass: string (defaults to 'yii\db\ActiveRecord')
     *   This is the base class of the new ActiveRecord class. It should be a fully
     *   qualified namespaced class name.
     *
     *   --color: boolean, 0 or 1
     *   whether to enable ANSI color in the output.
     *   If not set, ANSI color will only be enabled for terminals that support it.
     *
     *   --db: string (defaults to 'db')
     *   This is the ID of the DB application component.
     *
     *   --enableI18N: boolean, 0 or 1 (defaults to 0)
     *   This indicates whether the generator should generate strings using Yii::t()
     *   method. Set this to true if you are planning to make your application
     *   translatable.
     *
     *   --generateLabelsFromComments: boolean, 0 or 1 (defaults to 0)
     *   This indicates whether the generator should generate attribute labels by
     *   using the comments of the corresponding DB columns.
     *
     *   --generateQuery: boolean, 0 or 1 (defaults to 0)
     *   This indicates whether to generate ActiveQuery for the ActiveRecord class.
     *
     *   --generateRelations: string (defaults to 'all')
     *   This indicates whether the generator should generate relations based on
     *   foreign key constraints it detects in the database. Note that if your
     *   database contains too many tables, you may want to uncheck this option to
     *   accelerate the code generation process.
     *
     *   --generateRelationsFromCurrentSchema: boolean, 0 or 1 (defaults to 1)
     *   This indicates whether the generator should generate relations from current
     *   schema or from all available schemas.
     *
     *   --help, -h: boolean, 0 or 1
     *   whether to display help information about current command.
     *
     *   --interactive: boolean, 0 or 1 (defaults to 1)
     *   whether to run the command interactively.
     *
     *   --messageCategory: string (defaults to 'app')
     *   This is the category used by Yii::t() in case you enable I18N.
     *
     *   --modelClass: string
     *   This is the name of the ActiveRecord class to be generated. The class name
     *   should not contain the namespace part as it is specified in "Namespace".
     *   You do not need to specify the class name if "Table Name" ends with
     *   asterisk, in which case multiple ActiveRecord classes will be generated.
     *
     *   --ns: string (defaults to 'app\models')
     *   This is the namespace of the ActiveRecord class to be generated, e.g.,
     *   app\models
     *
     *   --overwrite: boolean, 0 or 1 (defaults to 0)
     *   whether to overwrite all existing code files when in non-interactive mode.
     *   Defaults to false, meaning none of the existing code files will be overwritten.
     *   This option is used only when `--interactive=0`.
     *
     *   --queryBaseClass: string (defaults to 'yii\db\ActiveQuery')
     *   This is the base class of the new ActiveQuery class. It should be a fully
     *   qualified namespaced class name.
     *
     *   --queryClass: string
     *   This is the name of the ActiveQuery class to be generated. The class name
     *   should not contain the namespace part as it is specified in "ActiveQuery
     *   Namespace". You do not need to specify the class name if "Table Name" ends
     *   with asterisk, in which case multiple ActiveQuery classes will be
     *   generated.
     *
     *   --queryNs: string (defaults to 'app\models')
     *   This is the namespace of the ActiveQuery class to be generated, e.g.,
     *   app\models
     *
     *   --singularize: boolean, 0 or 1 (defaults to 0)
     *   This indicates whether the generated class names should be singularized.
     *   For example, table names like some_tables will have class names SomeTable.
     *
     *   --standardizeCapitals: boolean, 0 or 1 (defaults to 0)
     *   This indicates whether the generated class names should have standardized
     *   capitals. For example, table names like SOME_TABLE or Other_Table will have
     *   class names SomeTable and OtherTable, respectively. If not checked, the
     *   same tables will have class names SOMETABLE and OtherTable instead.
     *
     *   --tableName (required): string
     *   This is the name of the DB table that the new ActiveRecord class is
     *   associated with, e.g. post. The table name may consist of the DB schema
     *   part if needed, e.g. public.post. The table name may end with asterisk to
     *   match multiple table names, e.g. tbl_* will match tables who name starts
     *   with tbl_. In this case, multiple ActiveRecord classes will be generated,
     *   one for each matching table name; and the class names will be generated
     *   from the matching characters. For example, table tbl_post will generate
     *   Post class.
     *
     *   --template: string (defaults to 'default')
     *
     *   --useSchemaName: boolean, 0 or 1 (defaults to 1)
     *   This indicates whether to include the schema name in the ActiveRecord class
     *   when it's auto generated. Only non default schema would be used.
     *
     *   --useTablePrefix: boolean, 0 or 1 (defaults to 0)
     *   This indicates whether the table name returned by the generated
     *   ActiveRecord class should consider the tablePrefix setting of the DB
     *   connection. For example, if the table name is tbl_post and
     *   tablePrefix=tbl_, the ActiveRecord class will return the table name as
     *   {{%post}}.
     */
    public function actionTable() {
        if (!YII_ENV_DEV) {
            Console::output("只能在开发环境下使用");
            return;
        }

        $tables = func_get_args();
        if (empty($tables)) {
            Console::output("请输入表名");
            exit();
        }

        $config = [
            "baseClass" => "common\\models\\table\\BaseTable", // 继承的类
            "ns" => "common\\models\\table", // 命名空间
            "generateLabelsFromComments" => 1, // 使用数据表注释生成 labels
            "overwrite" => 1, // 是否覆盖
            "interactive" => 0, // 命令行是否交互（如果交互需要用户输入是否继续等操作）
        ];

        foreach ($tables as $table) {
            $modelName = "Table" . StringUtil::underlineToHump($table);

            // 生成数据库文件
            $cmd = "php yii gii/model --tableName=" . $table . " --modelClass=" . $modelName;
            foreach ($config as $key => $value) {
                $cmd .= " --" . $key . "=" . $value;
            }
            system($cmd);

            // 生成继承文件（没有就生成，不会覆盖）
            $className = $this->getClassName($table);
            $classTemplate = $this->getClassTemplate($className, $modelName);
            $dbDir = FileUtil::getPhpDir() . "/common/models/db";
            $filename = $dbDir . "/" . $className . ".php";
            if (!file_exists($filename)) {
                file_put_contents($filename, $classTemplate);
            }

            // 添加到svn
            $tableFilename = FileUtil::getPhpDir() . "/common/models/table/" . $modelName . ".php";
            $output = [];
            exec("svn add " . $tableFilename . " | php yii base-console/empty", $output);
            exec("svn add " . $filename . " | php yii base-console/empty", $output);
        }
    }

    /**
     * @param $className
     * @param $moduleName
     * @return string
     */
    private function getClassTemplate($className, $moduleName) {
        $template = <<<PHP
<?php
namespace common\\models\\db;

use common\\models\\table\\{MODEL_NAME};

/**
 * 数据表 {MODEL_NAME} 的方法扩展 
 */
class {CLASS_NAME} extends {MODEL_NAME} 
{
    
}
PHP;
        $template = str_replace("{CLASS_NAME}", $className, $template);
        $template = str_replace("{MODEL_NAME}", $moduleName, $template);
        return $template;
    }

    /**
     * 根据表名获取类名
     * @param $tableName
     * @return string
     */
    private function getClassName($tableName) {
        /** @var string[] $nameMap 表名map 存在的又自定义类名的表名，将优先使用改名字。如果不存在，则使用通用配置 */
        $nameMap = [
            "log_cron_record" => "CronRecord",
        ];

        if (isset($nameMap[$tableName])) {
            return $nameMap[$tableName];
        }
        /** @var string[] $excludePrefix 去除的前缀 */
        $excludePrefix = ["tab_", "cfg_"];

        foreach ($excludePrefix as $prefix) {
            if (StringUtil::startsWith($tableName, $prefix)) {
                StringUtil::removePrefix($tableName, $prefix);
                break;
            }
        }

        return StringUtil::underlineToHump($tableName);
    }
}
