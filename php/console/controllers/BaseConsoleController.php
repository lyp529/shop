<?php


namespace console\controllers;


use common\utils\FileUtil;
use common\utils\SystemUtil;
use yii\console\Controller;

/**
 * Class BaseConsoleController 基础命令行控制器
 * @package console\controllers
 */
class BaseConsoleController extends Controller {
    public function beforeAction($action) {
        if (SystemUtil::isWin()) {
            // 切换到 utf8 编码
            system('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    /**
     * 不抓输出流信息，并且屏蔽
     */
    public function actionEmpty() {

    }

    /**
     * 获取yii脚本路径
     */
    public function getYiiScript() {
        return FileUtil::getPhpDir() . "/yii";
    }
}
