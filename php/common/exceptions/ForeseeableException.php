<?php


namespace common\exceptions;


use yii\base\UserException;

/**
 * Class ForeseeableException 控制器可不抓的异常
 * @package common\exceptions
 */
class ForeseeableException extends UserException {

}