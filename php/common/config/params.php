<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    // 文件上传的配置
    'UPLOAD_FILE' => [
        // 文件上传根地址
        'UPLOAD_URL' => 'http://127.0.0.1/pay/backend/web/index.php/file/upload',
        // 文件web访问根地址（获取地址）
        'WEB_URL' => 'http://127.0.0.1/pay/upload',
        // 系统路径
        'SYSTEM_PATH' => dirname(dirname(__DIR__)) . "/upload",
    ],
    'PARTNER_CODE' => 'PT',
    'PARTNER_CONFIG' => [
        'PT'=>[
            'ID'=>1,
            'KEY'=>'a888888888888888',
            'IV'=>'b888888888888888',
            'NOTIFY'=>'http://127.0.0.1/pay/notify_aes', //回调地址
        ],
        'SPJ'=>[
            'ID'=>101,
            'KEY'=>'a888888888888888',
            'IV'=>'b888888888888888',
            'NOTIFY'=>'http://www.spj168.vip/pay/notify_aes', //回调地址
        ]
    ],
    'CHONGQIN_SSC_URL'=>'https://kjh.55128.cn/history_chongqingssc.aspx',
    'CHONGQIN_SSC_URL_NEW'=>'https://www.cjcp.com.cn/kaijiang/cqssc/',
    'PAY_API_URL' => 'http://61.56.84.139/pay/api/web/index.php/api',
    'PAY_PLAT_URL' => 'http://61.56.84.139/pay/payment/web/',
    'REFERRAL_LINKS' => 'http://61.56.84.139/ga/php/frontend/web/',  // 推广链接
];
