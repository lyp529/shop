<?php
/**
 * 系统级定义变量
 */
define('IS_WIN', strstr(PHP_OS, 'WIN') ? 1 : 0);
//定义DLogger常规等级
define('LOG_LEVEL_DEBUG', 1);
define('LOG_LEVEL_INFO', 2);
define('LOG_LEVEL_WARNING', 3);
define('LOG_LEVEL_ERROR', 4);

define('LOG_LEVEL', LOG_LEVEL_DEBUG);
