<?php

use common\constants\P;
use common\models\db\BaseDB;

/**
 * 全局函数定义-尽量少用
 */

//打印变量或模型
function dv($vars, $label = '', $return = false) {
    if (is_object($vars) && method_exists($vars, 'toArray')) {
        $vars = $vars->toArray();
    }
    if (ini_get('html_errors')) {
        $content = "<pre>\n";
        if ($label != '') {
            $content .= "<strong>{$label} :</strong>\n";
        }
        $content .= htmlspecialchars(print_r($vars, true));
        $content .= "\n</pre>\n";
    } else {
        $content = $label . " :\n" . print_r($vars, true);
    }
    if ($return) {
        return $content;
    }
    echo $content;
    return null;
}

/**
 * 信用额显示方式
 * @param $amount
 * @return string 0的话变会成0.00, 格式化后的数不能再用于运算,只能显示在界面上
 */
function credit_format($amount) {
    return number_format($amount, 2);
}



//// 排列
//function arrangement($a, $len) {
//    $r = array();
//
//    $n = count($a);
//    if ($len <= 0 || $len > $n) {
//        return $r;
//    }
//
//    for ($i = 0; $i < $n; $i++) {
//        $b = $a;
//        $t = array_splice($b, $i, 1);
//        if ($len == 1) {
//            $r[] = $t;
//        } else {
//            $c = arrangement($b, $len - 1);
//            foreach ($c as $v) {
//                $r[] = array_merge($t, $v);
//            }
//        }
//    }
//
//    return $r;
//}
//
//// 组合
//function combination($a, $len) {
//    $r = array();
//
//    $n = count($a);
//    if ($len <= 0 || $len > $n) {
//        return $r;
//    }
//
//    for ($i = 0; $i < $n; $i++) {
//        $t = array($a[$i]);
//        if ($len == 1) {
//            $r[] = $t;
//        } else {
//            $b = array_slice($a, $i + 1);
//            $c = combination($b, $len - 1);
//            foreach ($c as $v) {
//                $r[] = array_merge($t, $v);
//            }
//        }
//    }
//
//    return $r;
//}


//================================= 时时彩排列组合算法 ======================================
/**
 * 获取固定位置的星级玩法 (适用于五星玩法,四星:前四后四,三星:前三中三后三,两星直选:前二后二)
 * @param $arr
 * @param $len
 * @return array
 */
//function getFixGroup($arr, $len, $explode = true) {
//    if (count($arr) != $len) {
//        die("固定位置:" . count($arr) . "!=" . $len);
//    }
//    if (!is_array($arr[0])) {
//        $arr[0] = [$arr[0]];
//    }
//    if ($len == 1) {
//        return $arr[0];
//    }
//    $tempArr = $arr;
//    unset($tempArr[0]);
//    $returnArr = [];
//    $len2 = count($arr);
//    $ret = getFixGroup(array_values($tempArr), ($len - 1));
//    foreach ($arr[$len2 - $len] as $alv) {
//        foreach ($ret as $rv) {
//            if (is_array($rv) && $explode) {
//                array_unshift($rv, $alv);
//                $returnArr[] = array_values($rv);
//            } else {
//                $returnArr[] = [$alv, $rv];
//            }
//        }
//    }
//    return $returnArr;
//}

/**
 * 最普通的组合 (适用于五星组选120,三星[前中后]三组六 四星组选24,组选6[两个二重号],二星组选)
 * @param $arr
 * @param $len
 * @param array $t
 * @return array
 */
//function getCombineGroup($arr, $len) {
//    sort($arr);
////    if ($len == 0) {
////        return [$t];
////    }
////    $result = [];
////    for ($i = 0, $l = count($arr); $i <= $l - $len; $i++) {
////        $tmp = getCombineGroup(array_slice($arr, $i + 1, $l, false), $len - 1, array_merge($t, [$arr[$i]]));
////        $result = array_merge($result, $tmp);
////    }
//    return combination($arr, $len);
//}
//
//
///**
// * 获取组选分组 (适用于五星：组选60,组选30,组选20,组选10,组选5)
// */
//function getZXGroup($type, $arrA, $arrB) {
//    if ($type == P::SC_FIVE_60) { //五星: 1个二重号+3个单号
//        $lenA = 1;
//        $lenB = 3;
//    } elseif ($type == P::SC_FIVE_30) {//五星: 2个二重号+1个单号
//        $lenA = 2;
//        $lenB = 1;
//    } elseif ($type == P::SC_FIVE_20) {//五星: 1个三重号+2个单号
//        $lenA = 1;
//        $lenB = 2;
//    } elseif ($type == P::SC_FIVE_10) {//五星: 1个三重号+1个二重号
//        $lenA = 1;
//        $lenB = 1;
//    } elseif ($type == P::SC_FIVE_5) { //五星: 1个四重号+1个单号
//        $lenA = 1;
//        $lenB = 1;
//    } elseif ($type == P::SC_FOUR_12) { //四星: 1个二重号+2个单号
//        $lenA = 1;
//        $lenB = 2;
//    } elseif ($type == P::SC_FOUR_4) { //四星: 1个三重号+1个单号
//        $lenA = 1;
//        $lenB = 1;
//    } else {
//        die("error type getZXGroup():" . $type);
//    }
//    $groupA = combination($arrA, $lenA);
//    $groupB = combination($arrB, $lenB);
//    $cells = getFixGroup([$groupA, $groupB], 2, false);
//
//    //去掉组选,两组都包含的
//    foreach ($cells as $k => $cell) {
//        foreach ($cell[0] as $v) {
//            if (in_array($v, $cell[1])) {
//                unset($cells[$k]);
//            }
//        }
//    }
//    return $cells;
//
//}


//=================================END 时时彩排列组合算法 ======================================
