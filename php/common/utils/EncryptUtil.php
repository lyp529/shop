<?php

namespace common\utils;


/**
 * 加密解密工具
 * @package common\utils
 */
class EncryptUtil {
    private static $id_plus = 168; //ID转换量

    /**
     * 简单的ID加密(短)
     * @param $id
     * @return string
     */
    public static function encryptShortId($id) {
        return strrev(base64_encode($id + self::$id_plus));
    }

    /**
     * 简单的ID解密(短)
     * @param $id_str
     * @return string
     */
    public static function decryptShortId($id_str) {
        $id = base64_decode(strrev($id_str));
        return $id - self::$id_plus;
    }
}

