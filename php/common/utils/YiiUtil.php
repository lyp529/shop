<?php


namespace common\utils;

use common\exceptions\SystemException;
use Yii;

/**
 * Class YiiUtil yii 工具
 * @package common\utils
 */
class YiiUtil
{

    /**
     * @param $sessionKey
     * @param null $sessionValue
     * @return mixed
     */
    public static function session($sessionKey, $sessionValue = null) {
        return $sessionValue === null ? Yii::$app->getSession()->get($sessionKey) : Yii::$app->getSession()->set($sessionKey, $sessionValue);
    }

    /**
     * 销毁 SESSION
     */
    public static function destroySession() {
        return Yii::$app->session->destroy();
    }

    /**
     * 获取客户端ip地址
     */
    public static function getUserIP() {
        return Yii::$app->request->getUserIP();
    }

    /**
     * @throws SystemException
     */
    public static function getSessionId() {
        try {
            return Yii::$app->getSession()->getId();
        } catch (\Exception $e) {
            throw new SystemException($e->getMessage());
        }
    }

    /**
     * 设置缓存
     * @param string $key
     * @param mixed $value
     * @param int $duration 缓存时间。单位：秒
     */
    public static function setCache($key, $value, $duration = 86400) {
        Yii::$app->cache->set($key, $value, $duration);
    }

    /**
     * 获取缓存
     * @param $key
     * @return mixed
     */
    public static function getCache($key) {
        return Yii::$app->cache->get($key);
    }

    /**
     * 删除缓存
     * @param $key
     */
    public static function delCache($key) {
        Yii::$app->cache->delete($key);
    }

}
