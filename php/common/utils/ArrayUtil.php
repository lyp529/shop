<?php

namespace common\utils;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * 数组转换处理类
 * @package common\utils
 */
class ArrayUtil extends ArrayHelper
{
    /**
     * 往数组里面增加值
     * @param $arr
     * @param $key
     * @param $value
     */
    public static function addArrayValue(&$arr, $key, $value)
    {
        if (isset($arr[$key])) {
            $arr[$key] += $value;
        } else {
            $arr[$key] = $value;
        }

    }

    /**
     * 把数组里所有值转为整形
     * @param array $array
     * @return int[]
     * @see ArrayUtil::valuesToNumber() 相似的方法
     */
    public static function valuesToInt(array &$array)
    {
        foreach ($array as $key => $value) {
            if (is_numeric($value)) {
                $array[$key] = (int)$value;
            } else if (is_array($value)) {
                $array[$key] = self::valuesToInt($value);
            }
        }
        return $array;
    }

    /**
     * 把数组中所有的数字的值转为数字（double）
     * @param $array
     * @return array
     */
    public static function valuesToNumber(&$array)
    {
        foreach ($array as $key => $value) {
            if (is_numeric($value)) {
                $array[$key] = (double)$value;
            } else if (is_array($value)) {
                $array[$key] = self::valuesToNumber($value);
            }
        }
        return $array;
    }

    /**
     * 判断数据是否设置，并返回默认值
     * @param array $arr 数组
     * @param string|int $key 数组key
     * @param mixed $default 数组中没有key时返回的默认值
     * @return mixed
     */
    public static function issetDefault(&$arr, $key, $default)
    {

        return is_array($arr) && isset($arr[$key]) ? $arr[$key] : $default;
    }

    /**
     * 删除数组的一个值
     * @param $arr
     * @param $element
     */
    public static function removeByElement(&$arr, $element)
    {
        if (!is_array($element)) {
            $element = [$element];
        }
        //关联数组,保留原来的键
        if (ArrayHelper::isAssociative($arr)) {
            foreach ($arr as $key => $value) {
                if (in_array($value, $element)) {
                    unset($arr[$key]);
                }
            }
        } else {
            $newArr = [];
            foreach ($arr as $key => $value) {
                if (!in_array($value, $element)) {
                    $newArr[] = $value;
                }
            }
            $arr = $newArr;
        }
    }

    /**
     * 获取索引数组的json字符串 防止从0开始的数组转为 ["M1","M2"] => {"0"=>"M1","1"=>"M2"}
     * @param array $arr 索引数组
     * @return false|mixed|string
     */
    public static function getIndexArrayJson($arr)
    {
        $randKey = "__key__" . mt_rand(1000000, 9999999);
        $arr[$randKey] = $randKey;
        $str = Json::encode($arr);
        $str = str_replace(",\"$randKey\":\"$randKey\"", "", $str);
        return $str;
    }

    /**
     * 把数组中所有的 key 的类型转为字符串（递归）
     * @param $arr
     * @return array
     */
    public static function keyToString(array $arr)
    {
        $retArr = [];
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                $value = ArrayUtil::keyToString($value);
            }
            $retArr[strval($key)] = $value;
        }
        return $retArr;
    }

    /**
     * 相个数组相等
     * @param array $arr1
     * @param array $arr2
     * @return bool
     */
    public static function isEqual($arr1, $arr2)
    {
        sort($arr1);
        sort($arr2);
        return $arr1 == $arr2;
    }

    /**
     * @param array $arr 把数组转为关系数组
     */
    public static function assoc(array &$arr)
    {
        foreach ($arr as $key => $value) {
            $key = (string)$key;
            if (is_array($value)) {
                self::assoc($value);
            }
            $arr[$key] = $value;
        }
    }

    /**
     * @deprecated 190410 by guorz 推荐使用下面方法代替
     * @see ArrayUtil::multisort() 排序方法（继承至 ArrayHelper ）
     *
     * 数组内字段重新排序数组
     * @param array|Model[] $arr 需要排序的数组。支持对象数组
     * @param string $prop 用于排序比较的 字段 或 属性
     * @param int $sort
     */
    public static function sortByKey(array &$arr, $prop, $sort = SORT_ASC)
    {
        if (self::isAssociative($arr)) {
            // 是映射数组（即map）。放弃对这种数组进行排序
            return;
        }

        $len = count($arr);
        $isObject = false;
        if ($len >= 1) {
            $row = $arr[0];
            $isObject = is_object($row);
        }
        for ($i = 0; $i < $len; $i++) {
            for ($j = 0; $j < $len; $j++) {
                $row1 = $arr[$i];
                $row2 = $arr[$j];

                $isSwap = false;

                if ($sort === SORT_DESC) {
                    if ($isObject) {
                        if ($row1->$prop > $row2->$prop) {
                            $isSwap = true;
                        }
                    } else {
                        if ($row1[$prop] > $row2[$prop]) {
                            $isSwap = true;
                        }
                    }
                } else if ($sort === SORT_ASC) {
                    if ($isObject) {
                        if ($row1->$prop < $row2->$prop) {
                            $isSwap = true;
                        }
                    } else {
                        if ($row1[$prop] < $row2[$prop]) {
                            $isSwap = true;
                        }
                    }
                }

                if ($isSwap) {
                    $tmp = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $tmp;
                }
            }
        }
    }

    /**
     * 判断数据是否在数组中。支持批量判断
     * 非恒等判断。即无论 $item 的值是字符串或数字都可以判定为在数组中
     * @param array|int|string $item
     * @param array $array
     * @return bool
     */
    public static function inArray($item, array $array)
    {
        if (is_array($item)) {
            $flipArray = array_flip($array);
            foreach ($item as $value) {
                if (isset($flipArray[$value])) {
                    return true;
                }
            }
            return false;
        } else {
            return in_array($item, $array);
        }
    }

    /**
     * 二维数组排序
     * @param array $array 数组
     * @param string $sortColumn 排序字段
     * @param string $sort 排序方式:SORT_ASC,SORT_DESC
     * @param bool $isReturnObj 数组值是否返回对象
     * @return array
     */
    public static function arraySort(array $array, $sortColumn, $sort, $isReturnObj = false)
    {
        foreach ($array as $key => $value) {
            is_object($value) && $array[$key] = json_decode(json_encode($value), true);
        }

        array_multisort(array_column($array, $sortColumn), $sort, $array);

        if ($isReturnObj) {
            foreach ($array as $key => $value) {
                $array[$key] = json_decode(json_encode($value));
            }
        }
        return $array;
    }
}
