<?php
/**
 * Created by PhpStorm.
 * User: ewing
 * Date: 2019-08-07
 * Time: 15:03
 */

namespace common\utils;


class HttpUtil
{
    public static function str_get_html($str) {
        require_once dirname(__DIR__) . '/3rd/simple_html_dom.php';
        return str_get_html($str);
    }

    public static function file_get_html($str) {
        require_once dirname(__DIR__) . '/3rd/simple_html_dom.php';
        return file_get_html($str);
    }


}
