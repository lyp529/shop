<?php

namespace common\utils;

use yii\helpers\StringHelper;

/**
 * 字符串处理类 继承于 StringHelper 的方法
 * @package common\utils
 */
class StringUtil extends StringHelper
{
    /**
     * 将 \n " ' 等字符转义成 \\n \" \'
     * 用于在 view 上 echo 字符串
     * @param $str
     * @return string
     */
    public static function escape($str) {
        $str = str_replace("\n", "\\n", $str);
        $str = str_replace("\"", "\\\"", $str);
        $str = str_replace("'", "\\'", $str);
        return $str;
    }

    /**
     * json含中文处理方法,如果
     * @param array('1'=>array('中文')) 转为 {"1":{"中文"}}
     * @return string
     */
    public static function utf8_json_encode($arr) {
        if (!function_exists('cell_urlencode')) {
            function cell_urlencode(&$arr) {
                foreach ($arr as $k => $v) {
                    if (is_array($v)) {
                        cell_urlencode($arr [$k]);
                    } else {
                        $arr [$k] = urlencode($v);
                    }
                }
            }
        }
        cell_urlencode($arr);
        return urldecode(json_encode($arr));
    }

    /**
     * 下划线转驼峰命名。如：  tab_user => TabUser
     * @param $name
     * @param bool $firstCharUp 首字母是否大写。默认：是
     * @return string
     */
    public static function underlineToHump($name, $firstCharUp = true) {
        if (empty($name)) {
            return "";
        }

        $words = explode("_", $name);
        foreach ($words as &$word) {
            if (empty($word)) {
                continue;
            }
            $word[0] = strtoupper($word[0]);
        }
        if (!$firstCharUp && !empty($words)) {
            $words[0] = strtolower($words[0]);
        }
        return implode("", $words);
    }

    /**
     * 移除前缀
     * @param string $string
     * @param string $prefix
     */
    public static function removePrefix(&$string, $prefix) {
        if (StringUtil::startsWith($string, $prefix)) {
            $string = substr($string, strlen($prefix) - 1);
        }
    }

    /**
     * 包含
     * @param string $string 字符串
     * @param string $needle 要查找的
     * @return bool
     */
    public static function contains($string, $needle) {
        return strpos($string, $needle) !== false;
    }

    /**
     *  截取保留 两位小数，且不四舍五入
     * @param $str
     * @return bool|string
     */
    public static function intercept($str){
        $twoDecimal =  substr(sprintf("%.3f",$str),0,-1);;
        return $twoDecimal;
    }
}
