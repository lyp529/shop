<?php
/**
 * Created by PhpStorm.
 * User: ewing
 * Date: 2019-04-04
 * Time: 21:30
 */

namespace common\utils;

//设置DLogger日志等级
use common\models\db\DebugLog;
use yii\helpers\Json;


/**
 * 数据库记录类
 * Class DLogger
 */
class DLogger
{


    //需要记录的信息
    private static $infoQueue = [];


    /**
     * 特别记录日志
     */
    //public static function specialLog($table, $data) {
    //}

    /**
     * 常规调试日志
     * @param $content
     * @param string $title
     */
    public static function debug($content, $title = '') {
        if (LOG_LEVEL <= LOG_LEVEL_DEBUG) {
            self::base_log('debug', $content, $title);
        }
    }

    /**
     * 常规显示日志
     * @param $content
     * @param string $title
     */
    public static function info($content, $title = '') {
        if (LOG_LEVEL <= LOG_LEVEL_INFO) {
            self::base_log('notice', $content, $title);
        }

    }

    /**
     * 常规警告日志
     * @param $content
     * @param string $title
     */
    public static function warning($content, $title = '') {
        if (LOG_LEVEL <= LOG_LEVEL_WARNING) {
            self::base_log('warning', $content, $title);
        }

    }

    /**
     * 常规错误日志
     * @param $content
     * @param string $title
     */
    public static function error($content, $title = '') {
        if (LOG_LEVEL <= LOG_LEVEL_ERROR) {
            self::base_log('error', $content, $title);
        }
    }


    /**
     * 基础日志调用
     * @param $log_type
     * @param $content
     * @param $title
     */
    private static function base_log($log_type, $content, $title) {
        if (is_array($content)) {
            if (isset($content[0])) {
                $str = '';
                foreach ($content as $v) {
                    $str .= $v . "\n";
                }
                $content = $str;
            } else {
                $content = Json::encode($content);
            }
        }
        $d = new DebugLog();
        $d->log_type = $log_type;
        $d->title = $title ? $title : "无";
        $d->content = $content;
        $d->record_date = TimeUtil::now();
        $flag = $d->save();
        if (IS_WIN && $flag == false) {
            echo $d->getFirstError();
            // echo "DLogger:$log_type  content >>> $content<br>\n";
        }
    }

    /**
     * 增加到信息队列
     * @param string $key 标识或内容
     * @param string $info 内容(如果是null时$key就是内容)
     */
    public static function add($key, $info = null) {
        if (is_array($info)) {
            $info = Json::encode($info);
        }
        if ($info == null) {
            self::$infoQueue[] = $key;
        } else {
            self::$infoQueue[$key] = $info;
        }
    }

    /**
     * 获取队列信息
     * @param bool $toStr
     * @return array|string
     */
    public static function getQueue($toStr = false) {
        return $toStr ? Json::encode(self::$infoQueue) : self::$infoQueue;
    }

    /**
     * 显示队列信息
     */
    public static function showQueue() {
        dv(self::$infoQueue, 'DLogger::$infoQueue');
    }

    /**
     * 记录队列
     * @param string $title 日志的标题
     * @param int $logType
     */
    public static function flush($title = '', $logType = LOG_LEVEL_DEBUG) {
        $arr = self::getQueue(false);
        $str = '';
        foreach ($arr as $k => $v) {
            $str .= $k . ' => ' . $v . "\n";
        }
        if ($logType == LOG_LEVEL_DEBUG) {
            self::debug($str, $title ? $title : 'LogQueue');
        } else {
            self::info($str, $title ? $title : 'LogQueue');
        }
    }


}
