<?php


namespace common\utils\game;


use common\constants\L;
use common\models\db\CashRecord;
use common\models\db\LogGeneral;
use common\models\db\LogLogin;
use common\models\db\LogOperate;
use common\utils\DLogger;
use common\utils\TimeUtil;
use common\utils\YiiUtil;
use Yii;
use yii\helpers\Json;
use yii\web\User;

/**
 * 基础日志工具 (建议所有方法均为 protected )
 */
class BaseLogUtil
{
    /**
     * 登录日志
     * @param $loginType
     * @param $userType
     * @param $userId
     * @param $username
     */
    protected static function loginLog($loginType, $userType, $userId, $username) {
        $ip = Yii::$app->request->userIP;
        $logLogin = new LogLogin();
        $logLogin->ip = $ip;
        $logLogin->login_type = $loginType;
        $logLogin->user_id = $userId;
        $logLogin->user_type = $userType;
        $logLogin->username = $username;
        if ($logLogin->save() === false) {
            Yii::error("登录日志插入失败。" . $logLogin->getFirstError());
        }
    }

    /**
     * 帐变日志
     * @param $logType
     * @param $memberId
     * @param $agentId
     * @param $vendorId
     * @param $holderId
     * @param $superHolderId
     * @param $username
     * @param $amount
     * @param $balance
     * @param $record_no
     * @param array $params
     * @param string $remark
     */
    protected static function cashLog($logType, $memberId, $agentId, $vendorId, $holderId, $superHolderId, $username, $amount, $balance, $record_no, array $params = [], $remark = '') {
        $cashRecord = new CashRecord();
        $cashRecord->member_id = $memberId;
        $cashRecord->agent_id = $agentId;
        $cashRecord->vendor_id = $vendorId;
        $cashRecord->holder_id = $holderId;
        $cashRecord->super_holder_id = $superHolderId;
        $cashRecord->username = $username;
        $cashRecord->clog_type = $logType;
        $cashRecord->clog_type_label = L::getLabel($logType);
        $cashRecord->params = Json::encode($params);
        $cashRecord->amount = $amount;
        $cashRecord->balance = $balance;
        $cashRecord->record_no = $record_no;
        $cashRecord->remark = $remark;
        if ($cashRecord->save() === false) {
            DLogger::error("帐变日志插入失败:" . $cashRecord->getFirstError());
        }
    }


    /**
     * 操作日志
     * @param $logType
     * @param $params
     * @param null $newData
     * @param null $oldData
     */
    protected static function operateLog($logType, $params, $newData = null, $oldData = null) {
        $logOperate = new LogOperate();
        $logOperate->user_type = UserUtil::getUserType();
        $logOperate->user_id = UserUtil::getUserId();
        $logOperate->ip = YiiUtil::getUserIP();
        $logOperate->user_name = UserUtil::getUsername();
        $logOperate->log_type = $logType;
        $logOperate->log_label = L::getLabel($logType);
        $logOperate->params = Json::encode($params);
        $oldData && $logOperate->old_data = Json::encode($oldData);
        $newData && $logOperate->new_data = Json::encode($newData);
        $logOperate->data_class = is_object($newData) ? get_class($newData) : "";
        if ($logOperate->save() === false) {
            DLogger::error("操作日志插入失败:" . $logOperate->getFirstError());
        }
    }

    /**
     * 一般日志
     */
    protected static function generalLog($logType, $params, $remark) {
        $logGeneral = new LogGeneral();
        $logGeneral->log_type = $logType;
        $logGeneral->log_label = L::getLabel($logType);
        $logGeneral->params = $params;
        $logGeneral->remark = $remark;
        $logGeneral->save();

    }


}
