<?php


namespace common\utils\game;


use common\constants\C;
use common\constants\L;
use common\models\UserModel;
use common\utils\DLogger;

/**
 * 基础日志工具
 */
class LogUtil extends BaseLogUtil
{

    /**
     * 通用日志类型
     * @param $logType
     * @param $param
     */
//    public static function log($logType, array $param) {
//
//
//    }

    /**
     * 通用帐变日志类型
     * @param int $logType 日志类型
     * @param UserModel $user 会员用户模型
     * @param int $amount 账变金额。如果【减少】则使用【负数】
     * @param string $orderNo 选填，相关订单编号
     * @param array $params 选题，参数
     * @param string $remark 选填，备注
     */
    public static function cLog($logType, UserModel $user, $amount, $orderNo = '', $params = [], $remark = '') {
        if (!L::isCashLogType($logType)) {
            DLogger::error("日志类型不是账变日志类型：" . $logType);
            return;
        }
        self::cashLog($logType, $user->memberId, $user->agentId, $user->vendorId, $user->holderId, $user->superHolderId, $user->username, $amount, $user->getCredit(), $orderNo, $params, $remark);
    }

    /**
     * 操作日志
     * @param int $logType L::P
     * @param array $params 抽象参数
     * @param null $newData 操作后的对象
     * @param null $oldData 操作前的对象
     */
    public static function pLog($logType, $params, $newData = null, $oldData = null) {
        if (!L::isOperateLogType($logType)) {
            DLogger::error("日志类型不是操作日志类型：" . $logType);
            return;
        }
        self::operateLog($logType, $params, $newData, $oldData);
    }

    /**
     * 通用日志
     * @param $logType
     * @param $params
     * @param string $remark
     */
    public static function gLog($logType, $params, $remark = "") {
        if (!L::isGeneralLogType($logType)) {
            DLogger::error("日志类型不是通用日志类型：" . $logType);
            return;
        }
        self::generalLog($logType, $params, $remark);
    }

    /**
     * 用户充值成功日志
     * @param UserModel $user
     * @param int $amount 充值金额
     * @param int $order_no 充值订单号
     */
    public static function userPayLog($user, $amount, $order_no) {
        self::cashLog(L::CLOG_USER_PAY, $user->userType, $user->userId, $user->username, $amount, $user->getCredit(), $order_no);
    }


    /**
     * 登录日志
     * @param $logType
     * @param $userType
     * @param $userId
     * @param $username
     * @throws \Throwable
     */
    public static function loginLog($logType, $userType, $userId, $username) {
        if ($logType == L::LOG_LOGIN || $logType == L::LOG_LOGOUT) {
            $loginType = ($logType == L::LOG_LOGIN) ? C::LOGIN_TYPE_LOGIN : C::LOGIN_TYPE_LOGOUT;
            parent::loginLog($loginType, $userType, $userId, $username);
        }
    }


}
