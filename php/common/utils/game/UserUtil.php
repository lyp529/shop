<?php


namespace common\utils\game;


use common\constants\C;
use common\models\UserModel;
use common\utils\YiiUtil;

/**
 * 用户工具
 */
class UserUtil
{
    /**
     * @var UserModel 当前登录的用户
     */
    private static $loginUser = null;

    /**
     * 获取当前登录的用户
     * @return UserModel
     */
    public static function getUser() {
        if (self::$loginUser == null) {
            self::$loginUser = UserModel::getUser(self::getUserType(), self::getUserId());
        }
        return self::$loginUser;
    }

    /**
     * 获取登录的用户ID
     * @return int
     */
    public static function getUserId() {
        return YiiUtil::session(C::SESSION_KEY_USER_ID);
    }

    /**
     * 获取登录的用户类型
     * @return int
     */
    public static function getUserType() {
        return YiiUtil::session(C::SESSION_KEY_USER_TYPE);
    }

    /**
     * 获取登录的用户名
     * @return int
     */
    public static function getUsername() {
        return YiiUtil::session(C::SESSION_KEY_USERNAME);
    }

    /**
     * 判断当前是否有用户登录
     */
    public static function hasLoginUser() {
        $userId = UserUtil::getUserId();
        $userType = UserUtil::getUserType();
        return !empty($userId) && !empty($userType);
    }

    /**
     * 获取登录用户的查询条件(查出自己下线的记录)
     * @return string
     */
    public static function getUserQuery() {
        if (self::getUserType() == C::USER_ADMIN) {
            return '';
        }
        return self::getIdField() . "=" . self::getUserId();
    }


    /**
     * 是否管理员
     */
    public static function isAdmin() {
        return self::getUserType() == C::USER_ADMIN;
    }


    /**
     * 获取ID字段的值查询
     * @param $userType
     * @param $userId
     * @return string
     */
    public static function getIdFieldQuery($userType, $userId) {
        return self::getIdField($userType) . '=' . $userId;
    }


    /**
     * 权限类型获取ID字段
     * @param $userType
     * @return string|null
     */
    public static function getIdField($userType = null) {
        if ($userType == null) {
            $userType = self::getUserType();
        }
        $idField = null;
        if ($userType == C::USER_SOLDER) {
            $idField = 'super_holder_id';
        } elseif ($userType == C::USER_HOLDER) {
            $idField = 'holder_id';
        } elseif ($userType == C::USER_VENDOR) {
            $idField = 'vendor_id';
        } elseif ($userType == C::USER_AGENT) {
            $idField = 'agent_id';
        } elseif ($userType == C::USER_MEMBER) {
            $idField = 'member_id';
        }
        return $idField;
    }

}
