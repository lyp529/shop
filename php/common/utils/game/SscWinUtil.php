<?php


namespace common\utils\game;


use common\constants\P;
use common\utils\CalUtil;

/**
 * Class 时时彩 算法工具
 * @remark 单元测试见 TestController.sscUnitTest
 * @package common\utils
 */
class SscWinUtil extends SscUtil
{
    private static $debug = (false && IS_WIN);

    /**
     * 获取分组结果
     * @param string $type 类型
     * @param array $data 下注数据
     * @param array $result 5位时时彩结果
     * @return int 赢输倍数
     */
    public static function getGroupWin($type, $data, $result) {
        $win = null;
        $groups = self::getGroups($type, $data);
        if (self::$debug) {
            echo "共有分组:" . count($groups) . "<br>";
            dv($groups);
        }
        switch ($type) {
            case P::SC_FIVE:
            case P::SC_FOUR_Q4:
            case P::SC_FOUR_H4:
            case P::SC_THREE_Q3:
            case P::SC_THREE_Z3:
            case P::SC_THREE_H3:
            case P::SC_TWO_Q2:
            case P::SC_TWO_H2:
                $win = self::getFixWin($type, $groups, $result);
                break;
            //---------------------- 普通组合类型 ------------------------
            case P::SC_FIVE_120:
            case P::SC_FOUR_24:
                $win = self::getCombWin($type, $groups, $result);
                break;
            case P::SC_FIVE_DWD:
                $win = self::getDwdWin($type, $groups, $result);
                break;
        }
        if ($win === null) {
            die("此类型玩法派彩未实现:" . $type);
        }
        return $win;
    }

    /**
     * 普通的组合
     * @param $type
     * @param array $groups
     * @param array $result
     * @return int|null
     */
    private static function getCombWin($type, array $groups, array $result) {
        $win = $winCount = 0;
        $result = self::filterResult($type, $result);
        sort($result);
        $odds = self::getOdds($type);
        foreach ($groups as $group) {
            sort($group);
            if ($group == $result) {
                $win += $odds;
                $winCount++;
            }
        }
        if (self::$debug) {
            echo "共赢组数:" . $winCount . " - 结果:$win<br>";
        }
        return $win;
    }


    /**
     * 获取定位胆结果
     * @param array $data
     * @param array $result
     * @return int|null
     */
    private static function getDwdWin($type, array $groups, array $result) {
//        dv($result);
//        dv($groups);
        $win = 0;
        $winCount = 0;
        $odds = self::getOdds($type);
        foreach ($groups as $index => $group) {
            foreach ($group as $v) {
                if ($v[0] == $result[$index]) {
                    $win += $odds;
                    $winCount++;

                }
            }
        }
        if (self::$debug) {
            echo "共赢组数:" . $winCount . " - 结果:$win<br>";
        }
        return $win;
    }


    /**
     * 计算固定类型的
     */
    private static function getFixWin($type, $groups, $result) {
        if (self::$debug) {
            echo "pass getFixWin<br>";
        }
        $win = 0;
        $result = self::filterResult($type, $result);
        $winCount = 0;
        $odds = self::getOdds($type);
        foreach ($groups as $group) {
            if ($group === $result) {
                $win += $odds;
                $winCount++;
            }
        }
        if (self::$debug) {
            echo "共赢组数:" . $winCount . "<br>";
        }
        return $win;
    }

    /**
     * 过漏成我需要的结果
     * @param $type
     * @return
     */
    private static function filterResult($type, $result) {
        if ($type == P::SC_FOUR_Q4) {
            $result = [$result[0], $result[1], $result[2], $result[3]];
        } elseif ($type == P::SC_FOUR_H4 || $type == P::SC_FOUR_24) {
            $result = [$result[1], $result[2], $result[3], $result[4]];
        } elseif ($type == P::SC_THREE_Q3) {
            $result = [$result[0], $result[1], $result[2]];
        } elseif ($type == P::SC_THREE_Z3) {
            $result = [$result[1], $result[2], $result[3]];
        } elseif ($type == P::SC_THREE_H3) {
            $result = [$result[2], $result[3], $result[4]];
        } elseif ($type == P::SC_TWO_Q2) {
            $result = [$result[0], $result[1]];
        } elseif ($type == P::SC_TWO_H2) {
            $result = [$result[3], $result[4]];
        }
        return $result;
    }


    /**
     * 获取赔率
     */
    public static function getOdds($type) {
        $odds = null;
        switch ($type) {
            case P::SC_FIVE: //五星
                $odds = 180000;
                break;
            case P::SC_FOUR_Q4://四星
            case P::SC_FOUR_H4:
                $odds = 18000;
                break;
            case P::SC_FIVE_5:
                $odds = 36000;
                break;
            case P::SC_FIVE_10:
                $odds = 18000;
                break;
            case P::SC_FIVE_20:
                $odds = 9000;
                break;
            case P::SC_FIVE_30:
                $odds = 6000;
                break;
            case P::SC_FIVE_60:
                $odds = 3000;
                break;
            case P::SC_FIVE_120://五星120
                $odds = 1500;
                break;
            case P::SC_THREE_Q3:// 三星
            case P::SC_THREE_Z3:
            case P::SC_THREE_H3:
                $odds = 1800;
                break;
            case P::SC_THREE_Q3Z3:
            case P::SC_THREE_Z3Z3:
            case P::SC_THREE_H3Z3:
                $odds = 600;
                break;
            case P::SC_THREE_Q3Z6:
            case P::SC_THREE_Z3Z6:
            case P::SC_THREE_H3Z6:
                $odds = 300;
                break;
            case P::SC_TWO_Q2: //二星
            case P::SC_TWO_H2:
                $odds = 180;
                break;
            case P::SC_TWO_Q2ZX:
            case P::SC_TWO_H2ZX:
                $odds = 90;
                break;
            case P::SC_FIVE_DWD://五星定位胆
                $odds = 18;
                break;
            case P::SC_FOUR_24: //四星24
                $odds = 750;
                break;
            case P::SC_FOUR_12:
                $odds = 1500;
                break;
            case P::SC_FOUR_6:
                $odds = 3000;
                break;
            case P::SC_FOUR_4:
                $odds = 4500;
                break;
            case P::SC_BDD_Q31M:
            case P::SC_BDD_Z31M:
            case P::SC_BDD_H31M:
                $odds = 6.6;
                break;
            case P::SC_BDD_Q32M:
            case P::SC_BDD_H32M:
                $odds = 33;
                break;
            case P::SC_BDD_4X1M:
                $odds = 4.7;
                break;
            case P::SC_BDD_4X2M:
                $odds = 18.42;
                break;
            case P::SC_BDD_5X2M:
                $odds = 12.07;
                break;
            case P::SC_BDD_5X3M:
                $odds = 41;
                break;
        }
        if ($odds === null) {
            die(__CLASS__ . "." . __FUNCTION__ . " error odds:" . $type);
        }
        if (self::$debug) {
            echo "$type 返回赔率 $odds<br>";
        }
        return $odds;
    }


}
