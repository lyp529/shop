<?php


namespace common\utils\game;


use common\constants\P;
use common\utils\CalUtil;

/**
 * Class 时时彩 算法工具
 * @remark 单元测试见 TestController.sscUnitTest
 * @package common\utils
 */
class SscUtil
{
    /**
     * 检查数据值
     * @param $type
     * @param $data
     * @return bool true通过 false不通过
     */
    public static function checkData($type, $data) {
        //---------------------- 固定位置组合类型 ------------------------
        $flag = false;
        switch ($type) {
            case P::SC_FIVE:
            case P::SC_FOUR_Q4:
            case P::SC_FOUR_H4:
            case P::SC_THREE_Q3:
            case P::SC_THREE_Z3:
            case P::SC_THREE_H3:
            case P::SC_TWO_Q2:
            case P::SC_TWO_H2:
                $flag = self::checkFixGroup($data, self::getLen($type));
                break;
            //---------------------- 普通组合类型 ------------------------
            case P::SC_FIVE_120:
            case P::SC_FOUR_24:
            case P::SC_THREE_Q3Z6:
            case P::SC_THREE_Z3Z6:
            case P::SC_THREE_H3Z6:
            case P::SC_THREE_Q3Z3:
            case P::SC_THREE_Z3Z3:
            case P::SC_TWO_Q2ZX:
            case P::SC_TWO_H2ZX:
            case P::SC_BDD_Q31M:
            case P::SC_BDD_Z31M:
            case P::SC_BDD_H31M:
            case P::SC_BDD_Q32M:
            case P::SC_BDD_H32M:
            case P::SC_BDD_4X1M:
            case P::SC_BDD_4X2M:
            case P::SC_BDD_5X2M:
            case P::SC_BDD_5X3M:
                $flag = count($data) == 1 && is_array($data[0]);
                break;
            //---------------------- 组选类型 ------------------------
            case P::SC_FIVE_60://五星组选60: 1个二重号+3个单号
            case P::SC_FIVE_30://五星组选30: 2个二重号+1个单号
            case P::SC_FIVE_20://五星组选20: 1个三重号+2个单号
            case P::SC_FOUR_12://四星组选12: 1个二重号+2个单号
            case P::SC_FIVE_10://五星组选10: 1个三重号+1个二重号
            case P::SC_FIVE_5: //五星组选5: 1个四重号+1个单号
            case P::SC_FOUR_4: //四星组选4: 1个三重号+1个单号
                $lens = self::getLenPair($type);
                $flag = count($data) == 2 && is_array($data[0]) && is_array($data[1]) && count($data[0]) >= $lens[0] && count($data[1]) >= $lens[1];
                break;
            case P::SC_FIVE_DWD:
                $flag = count($data) == 5 && (!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]));
                break;
        }
        return $flag;
    }


    /**
     * 获取所有时时彩玩法的分组列表
     * @param $type
     * @param $data
     * @return array
     */
    public static function getGroups($type, $data) {
        $groups = [];
        switch ($type) {
            //---------------------- 固定位置组合类型 ------------------------
            case P::SC_FIVE:
            case P::SC_FOUR_Q4:
            case P::SC_FOUR_H4:
            case P::SC_THREE_Q3:
            case P::SC_THREE_Z3:
            case P::SC_THREE_H3:
            case P::SC_TWO_Q2:
            case P::SC_TWO_H2:
                $groups = self::getFixGroup($data, self::getLen($type));
                break;
            //---------------------- 普通组合类型 ------------------------
            case P::SC_FIVE_120:
            case P::SC_FOUR_24:
            case P::SC_THREE_Q3Z6:
            case P::SC_THREE_Z3Z6:
            case P::SC_THREE_H3Z6:
            case P::SC_THREE_Q3Z3:
            case P::SC_THREE_Z3Z3:
            case P::SC_TWO_Q2ZX:
            case P::SC_TWO_H2ZX:
            case P::SC_BDD_Q31M:
            case P::SC_BDD_Z31M:
            case P::SC_BDD_H31M:
            case P::SC_BDD_Q32M:
            case P::SC_BDD_H32M:
            case P::SC_BDD_4X1M:
            case P::SC_BDD_4X2M:
            case P::SC_BDD_5X2M:
            case P::SC_BDD_5X3M:
                if (is_array($data[0])) { //多存了一层数组
                    $data = $data[0];
                }
                $groups = CalUtil::comb($data, self::getLen($type));
                break;
            //---------------------- 组选类型 ------------------------
            case P::SC_FIVE_60://五星组选60: 1个二重号+3个单号
            case P::SC_FIVE_30://五星组选30: 2个二重号+1个单号
            case P::SC_FIVE_20://五星组选20: 1个三重号+2个单号
            case P::SC_FOUR_12://四星组选12: 1个二重号+2个单号
            case P::SC_FIVE_10://五星组选10: 1个三重号+1个二重号
            case P::SC_FIVE_5: //五星组选5: 1个四重号+1个单号
            case P::SC_FOUR_4: //四星组选4: 1个三重号+1个单号
                if (count($data) != 2 || !is_array($data[0]) || !is_array($data[1])) {
                    die(__CLASS__ . '.' . __FUNCTION__ . " error:组选必须两组数");
                }
                $groups = self::getZXGroup($data[0], $data[1], self::getLenPair($type));
                break;
            case  P::SC_FIVE_DWD: //定位胆
                $groups = self::getDWDGroup($data);
                break;
        }
        return $groups;
    }

    /**
     * 获取组数
     * @param $type
     * @param $data
     * @return int
     */
    public static function getGroupNum($type, $data) {
        $groups = self::getGroups($type, $data);
        $count = 0;
        if ($type == P::SC_FIVE_DWD) {
            foreach ($groups as $group) {
                $count += count($group);
            }
        } else {
            $count = count($groups);
        }
        return $count;
    }

    /**
     * 每种类型的长度
     * @param $type
     * @return int
     */
    private static function getLen($type) {
        $len = -1;
        switch ($type) {
            case P::SC_FIVE:
            case P::SC_FIVE_120:
                $len = 5;
                break;
            case P::SC_FOUR_Q4:
            case P::SC_FOUR_H4:
            case P::SC_FOUR_24:
                $len = 4;
                break;
            case P::SC_THREE_Q3:
            case P::SC_THREE_Z3:
            case P::SC_THREE_H3:
            case P::SC_THREE_Q3Z6:
            case P::SC_THREE_Z3Z6:
            case P::SC_THREE_H3Z6:
            case P::SC_BDD_5X3M:
                $len = 3;
                break;
            case P::SC_TWO_Q2:
            case P::SC_TWO_H2:
            case P::SC_TWO_Q2ZX:
            case P::SC_TWO_H2ZX:
            case P::SC_BDD_Q32M:
            case P::SC_BDD_H32M:
            case P::SC_BDD_4X2M:
            case P::SC_BDD_5X2M:
            case P::SC_THREE_Q3Z3:
            case P::SC_THREE_Z3Z3:
            case P::SC_THREE_H3Z3:
                $len = 2;
                break;
            case P::SC_BDD_Q31M:
            case P::SC_BDD_Z31M;
            case P::SC_BDD_H31M;
            case P::SC_BDD_4X1M;
                $len = 1;
                break;
        }
        if ($len == -1) {
            die('error getLen()' . $type);
        }
        return $len;
    }

    /**
     * 组选类型的前后长度
     */
    private static function getLenPair($type) {
        $lens = [];
        switch ($type) {
            case P::SC_FIVE_60://五星组选60: 1个二重号+3个单号
                $lens = [1, 3];
                break;
            case P::SC_FIVE_30://五星组选30: 2个二重号+1个单号
                $lens = [2, 1];
                break;
            case P::SC_FIVE_20://五星组选20: 1个三重号+2个单号
            case P::SC_FOUR_12://四星组选12: 1个二重号+2个单号
                $lens = [1, 2];
                break;
            case P::SC_FIVE_10://五星组选10: 1个三重号+1个二重号
            case P::SC_FIVE_5: //五星组选5: 1个四重号+1个单号
            case P::SC_FOUR_4: //四星组选4: 1个三重号+1个单号
                $lens = [1, 1];
                break;
        }
        if (empty($lens)) {
            die('error getLenPair()' . $type);
        }
        return $lens;
    }

    /**
     * 检测固定类型的数据是否正确
     * @param $arr
     * @param $len
     * @return bool true正确 false不正确
     */
    private static function checkFixGroup($arr, $len) {
        return count($arr) == $len;
    }

    /**
     * 获取固定位置的星级玩法 (适用于五星玩法,四星:前四后四,三星:前三中三后三,两星直选:前二后二)
     * @param $arr
     * @param $len
     * @return array
     */
    private static function getFixGroup($arr, $len, $explode = true) {
        if (count($arr) != $len) {
            die("固定位置:" . count($arr) . "!=" . $len);
        }
        if (!is_array($arr[0])) {
            $arr[0] = [$arr[0]];
        }
        if ($len == 1) {
            return $arr[0];
        }
        $tempArr = $arr;
        unset($tempArr[0]);
        $returnArr = [];
        $len2 = count($arr);
        $ret = self::getFixGroup(array_values($tempArr), ($len - 1));
        foreach ($arr[$len2 - $len] as $alv) {
            foreach ($ret as $rv) {
                if (is_array($rv) && $explode) {
                    array_unshift($rv, $alv);
                    $returnArr[] = array_values($rv);
                } else {
                    $returnArr[] = [$alv, $rv];
                }
            }
        }
        return $returnArr;
    }


    /**
     * 获取组选分组 (适用于五星：组选60,组选30,组选20,组选10,组选5)
     */
    private static function getZXGroup($arrA, $arrB, $lens) {
        $groupA = CalUtil::comb($arrA, $lens[0]);
        $groupB = CalUtil::comb($arrB, $lens[1]);
        $cells = self::getFixGroup([$groupA, $groupB], 2, false);

        //去掉组选,两组都包含的
        foreach ($cells as $k => $cell) {
            foreach ($cell[0] as $v) {
                if (in_array($v, $cell[1])) {
                    unset($cells[$k]);
                }
            }
        }
        return $cells;
    }

    /**
     * 获取定位胆分组
     * e.g. getDWDGroup([[1,2],[3,4],[5,6],[7,8,9],[1,2]])
     */
    private static function getDWDGroup($arr) {
        if (count($arr) != 5) {
            die('getDWDGroup() 必须5组');
        }
        return [CalUtil::comb($arr[0], 1), CalUtil::comb($arr[1], 1), CalUtil::comb($arr[2], 1), CalUtil::comb($arr[3], 1), CalUtil::comb($arr[4], 1)];
    }


}
