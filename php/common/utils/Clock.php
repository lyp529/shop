<?php

namespace fx\utils;
/**
 * 程序运行计时类
 */
class Clock
{
    private static $TIME_START;

    /**
     * 计时开始
     */
    public static function start() {
        $arr = explode(" ", microtime());
        self::$TIME_START = $arr [1] . substr($arr [0], 1);
    }

    /**
     * 获取程序从开始至运行到此处时间
     * @param bool $show 是否显示
     * @return string 保留三位小数时间
     */
    public static function runtime($show = false) {
        $TIME_STOP = explode(" ", microtime());
        $TIME_STOP = $TIME_STOP [1] . substr($TIME_STOP [0], 1);
        $sTime = number_format($TIME_STOP - self::$TIME_START, 3);
        if ($show) {
            echo " <Hr>runtime=$sTime";
        }
        return $sTime;
    }
}
