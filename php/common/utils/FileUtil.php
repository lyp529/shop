<?php


namespace common\utils;


use Yii;

/**
 * Class FileUtil 文件工具。包括获取路径
 * @package common\utils
 */
class FileUtil {
    /**
     * 获取项目根路径 到 ga 的路径
     * @return string
     */
    public static function getRootDir() {
        return dirname(dirname(dirname(__DIR__)));
    }

    /**
     * 获取项目根路径
     */
    public static function getPhpDir() {
        return FileUtil::getRootDir() . "/php";
    }

    /**
     * 获取 console 模块的根路径
     */
    public static function getConsoleDir() {
        return FileUtil::getPhpDir() . "/console";
    }

    /**
     * 获取后台路径
     */
    public static function getBackendDir() {
        return FileUtil::getPhpDir() . "/backend";
    }

    /**
     * 前端（会员段模块）
     * @return string
     */
    public static function getFrontendDir() {
        return FileUtil::getPhpDir() . "/frontend";
    }

    /**
     * 获取 vue-cli 的路径
     */
    public static function getVueCliDir() {
        return FileUtil::getRootDir() . "/vue-cli";
    }

    /**
     * 获取上传文件的路径
     * @return string
     */
    public static function getUploadSystemDir() {
        return Yii::$app->params["UPLOAD_FILE"]["SYSTEM_PATH"];
    }

    /**
     * 获取文件web访问路径
     * @return string
     */
    public static function getWebUrl() {
        return Yii::$app->params["UPLOAD_FILE"]["WEB_URL"];
    }

    /**
     * 复制文件（文件夹）
     * @param $source
     * @param $dest
     */
    public static function copy($source, $dest) {
        if (is_dir($source)) {
            if (!file_exists($dest)) {
                mkdir($dest, 0755, true);
            }
            $filenames = scandir($source);
            foreach ($filenames as $filename) {
                if (in_array($filename, [".", ".."])) {
                    continue;
                }
                $sourceFilename = $source . "/" . $filename;
                $destFilename = $dest . "/" . $filename;
                self::copy($sourceFilename, $destFilename);
            }
        } else {
            copy($source, $dest);
        }
    }

    /**
     * 删除文件（文件夹）
     * @param $absFilename
     */
    public static function delete($absFilename) {
        if (!file_exists($absFilename)) {
            return;
        }
        if (is_dir($absFilename)) {
            $filenames = scandir($absFilename);
            foreach ($filenames as $filename) {
                if (in_array($filename, [".", ".."])) {
                    continue;
                }
                $subAbsFilename = $absFilename . "/" . $filename;
                self::delete($subAbsFilename);
            }
        } else {
            unlink($absFilename);
        }
    }

    /**
     * 字节数自动提升代为为：KB MB GB
     * @param int $byte 字节数
     * @return string
     */
    public static function byteAutoUpUnit($byte)
    {
        /** @var array $unitLevel 单位等级配置 */
        $unitLevel = ["B", "KB", "MB", "GB"];

        $num = $byte;
        $loopTime = count($unitLevel) - 1;
        for ($level = 0; $level < $loopTime; ++$level) {
            if ($num < 1000) {
                break;
            } else {
                $num = round($num / 1024, 3);
            }
        }

        return $num . $unitLevel[$level];
    }
}