<?php

namespace common\utils;


/**
 * Class TimeUtil 时间工具
 * @package common\utils
 */
class TimeUtil
{

    /**
     * @return string 当前时间
     */
    public static function now() {
        return date("Y-m-d H:i:s");
    }

    /**
     * 获取当前时间前多少秒
     * @param $seconds
     * @return string Y-m-d H:i:s
     */
    public static function getNowBefore($seconds) {
        return self::getTimeBeforeSecond(self::now(), $seconds);
    }

    /**
     * 获取当前时间后多少秒
     * @param $seconds
     * @return string  Y-m-d H:i:s
     */
    public static function getNowAfter($seconds) {
        return self::getTimeAfterSecond(self::now(), $seconds);
    }

    /**
     * @deprecated 推荐使用 now() 代替
     * @see TimeUtil::now()
     * @return string 当前时间
     */
    public static function getCurrTime() {
        return date("Y-m-d H:i:s");
    }

    /**
     * 获取今天 Y-m-d
     */
    public static function today() {
        return self::getCurrDate();
    }

    /**
     * 将两个值设为今天的开始与结束
     * @param $startTime
     * @param $endTime
     */
    public static function setToday(&$startTime, &$endTime) {
        $startTime = self::today() . ' 00:00:00';
        $endTime = self::today() . ' 23:59:59';
    }

    /**
     * 获取本月。如： "2019-02"
     */
    public static function thisMonth() {
        return date("Y-m");
    }

    /**
     * @return string 当前日期
     */
    public static function getCurrDate() {
        return date("Y-m-d");
    }

    /**
     * @return string 昨天 Y-m-d
     */
    public static function yesterday() {
        return self::getDateBeforeDays(1);
    }

    /**
     * 将 20190801102643 格式的字符串转为日期格式
     */
    public static function getTimeByStr($str) {
        $y = substr($str, 0, 4);
        $m = substr($str, 4, 2);
        $d = substr($str, 6, 2);
        $h = substr($str, 8, 2);
        $i = substr($str, 10, 2);
        $s = substr($str, 12, 2);
        return $y . '-' . $m . '-' . $d . ' ' . $h . ':' . $i . ':' . $s;
    }

    /**
     * 年月日
     * 如20180901
     * @param $datetime
     * @return string
     */
    public static function getYearMD($datetime = null) {
        if ($datetime === null) {
            $datetime = self::now();
        }
        $year = self::getYear($datetime);
        $month = self::getMonth($datetime);
        $day = self::getDay($datetime);
        return $year . $month . $day;
    }

    /**
     * 获取日期的年月
     * @param $date
     * @return string
     */
    public static function getYearMonth($date) {
        return date("Y-m", strtotime($date));
    }

    /**
     * 获取日期的年月日
     * @param $date
     * @return false|string
     */
    public static function getYearMonthDay($date) {
        return date("Y-m-d", strtotime($date));
    }

    /**
     * 获取一定天数之前的日志
     * @param int $days 多少天
     * @return string 日期
     */
    public static function getDateBeforeDays($days) {
        return date("Y-m-d", time() - 86400 * $days);
    }

    /**
     * 获取当天开始时间
     * @param string $dateOrTime 当天的日期或时间
     * @return string
     */
    public static function getDateStartTime($dateOrTime) {
        return self::timestampToTime(strtotime(self::timeToDate($dateOrTime)));
    }

    /**
     * 获取当天结束时间
     * @param string $dateOrTime 当天的日期或时间
     * @return string
     */
    public static function getDateEndTime($dateOrTime) {
        $tomorrow = self::getNextDay($dateOrTime);
        $timestamp = strtotime($tomorrow) - 1;
        return self::timestampToTime($timestamp);
    }

    /**
     * @return string 获取本月1日的日期
     */
    public static function getThisMonthFirstDate() {
        return date("Y-m", time()) . "-01";
    }

    /**
     * @return string 本月最后一天日期
     */
    public static function getThisMonthEndDate() {
        return self::timestampToDate(strtotime(self::getThisMonthEndTime()));
    }

    /**
     * @return string 获取本月第一天0点时间
     */
    public static function getThisMonthStartTime() {
        return self::timestampToTime(strtotime(self::getThisMonthFirstDate()));
    }

    /**
     * @return string 获取本月结束的时间
     */
    public static function getThisMonthEndTime() {
        return self::timestampToTime(strtotime(self::getNextMonth(self::today())) - 1);
    }

    /**
     * @return string 上月开始时间
     */
    public static function getPrevMonthStartTime() {
        $prevMonth = self::getPrevMonth(self::today());
        return self::timestampToTime(strtotime($prevMonth));
    }

    /**
     * @return string 上月结束时间
     */
    public static function getPrevMonthEndTime() {
        return self::timestampToTime(strtotime(self::getThisMonthStartTime()) - 1);
    }

    /**
     * @return string 获取本年第一天日期
     */
    public static function getThisYearFirstDate() {
        return date("Y") . "-01-01";
    }

    /**
     * 获取今年的第一个月
     */
    public static function getThisYearFirstMonth() {
        return date("Y") . "-01";
    }

    /**
     * @return string 本周周一的日期
     */
    public static function getThisWeekFirstDate() {
        return date('Y-m-d', (time() - ((date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600));
    }

    /**
     * @return string 上周周一的日期
     */
    public static function getPrevWeekFirstDate() {
        return self::timestampToDate(strtotime(self::getThisWeekFirstDate()) - 7 * 86400);
    }

    /**
     * @return string 上周周日日期
     */
    public static function getPrevWeekEndDate() {
        return self::timestampToDate(strtotime(self::getThisWeekFirstDate()) - 86400);
    }

    /**
     * @param int $days 获取多少天之后的日期
     * @param null|string $date 给定的当前日期
     * @return string
     */
    public static function getDateAfterDays($days, $date = null) {
        $time = time();
        if ($date !== null) {
            $time = strtotime($date);
        }

        return date("Y-m-d", $time + 86400 * $days);
    }

    /**
     * 获取某月天数
     * @param $month
     * @return false|string
     */
    public static function getDayCount($month) {
        return date("t", strtotime($month));
    }

    /**
     * 获取某年天数
     * @param $year
     * @return false|int|string
     */
    public static function getYearDays($year) {
        return date("z", mktime(23, 59, 59, 12, 31, $year)) + 1;
    }

    /**
     * 获取指定日期月份
     * @param $day
     * @return false|string
     */
    public static function getMonth($day) {
        return date("m", strtotime($day));
    }

    /**
     * 获取月份的起始时间
     * @param string $month 如："2019-01" 或 "2019-01-01"
     * @return string 如："2019-01-01 00:00:00"
     */
    public static function getMonthStartTime($month) {
        return self::timestampToTime(strtotime(date("Y-m", strtotime($month))));
    }

    /**
     * 获取月份的结束时间
     * @param string $month 如："2019-01" 或 "2019-01-01"
     * @return string 如："2019-01-31 23:59:59"
     */
    public static function getMonthEndTime($month) {
        $monthEndTimestamp = strtotime(self::getNextMonthFirstDate($month)) - 1;
        return self::timestampToTime($monthEndTimestamp);
    }

    /**
     * 获取月份开始日期
     * @param $month
     * @return false|string
     */
    public static function getMonthStartDate($month) {
        return self::timeToDate(self::getMonthStartTime($month));
    }

    /**
     * 获取月份最后一天的日期
     * @param $month
     * @return false|string
     */
    public static function getMonthEndDate($month) {
        return self::timeToDate(self::getMonthEndTime($month));
    }

    /**
     * 获取一段时间内的月份数组
     * @param $start
     * @param $end
     * @return array
     */
    public static function getMonthsDuring($start, $end) {
        $startDate = self::getMonthStartDate($start);
        $endDate = self::getMonthEndDate($end);
        $endTimeStamp = strtotime($endDate);
        $months = [];
        for ($i = 0; $i < 2000; $i++) {
            $timeStamp = strtotime($startDate) + $i * 86400;
            if ($timeStamp >= $endTimeStamp) {
                break;
            }
            $month = date('Y-m', $timeStamp);
            if (!in_array($month, $months)) {
                $months[] = $month;
            }
        }
        return $months;
    }

    /**
     * 获取指定日期年份
     * @param $day
     * @return false|string
     */
    public static function getYear($day) {
        return date("Y", strtotime($day));
    }


    /**
     * 获取指定年份的开始秒
     * @param $date
     * @return string
     */
    public static function getYearFirstTime($date) {
        if (strlen($date) == 4) {
            $y = $date;
        } else {
            $y = date("Y", strtotime($date));
        }
        return $y . '-01-01 00:00:00';
    }

    /**
     * 获取指定年份的最后秒
     * @param $date
     * @return string
     */
    public static function getYearLastTime($date) {
        if (strlen($date) == 4) {
            $y = $date;
        } else {
            $y = date("Y", strtotime($date));
        }
        return $y . '-12-31 23:59:59';
    }


    /**
     * 获取年月日的日数
     * @param $day
     * @return false|string
     */
    public static function getDay($day) {
        return date("d", strtotime($day));
    }

    /**
     * 获取上一个月
     * @param $date
     * @return false|string
     */
    public static function getPrevMonth($date) {
        return date("Y-m", strtotime('-1 month', strtotime($date)));
    }

    /**
     * 获取下一个月 格式：YYYY-mm
     * @param $date
     * @return false|string
     */
    public static function getNextMonth($date) {
        return date("Y-m", strtotime('+1 month', strtotime($date)));
    }

    /**
     * 获取时间的小时数。如：20、09
     * @param string $time
     * @return string
     */
    public static function getHour($time) {
        return date("H", strtotime($time));
    }

    /**
     * 获取时间的分钟数。如：00, 15
     * @param string $time
     * @return string
     */
    public static function getMinute($time) {
        return date("i", strtotime($time));
    }

    /**
     * 获取时间的小时和分钟。如：10:00 12:15
     * @param string $time
     * @return string
     */
    public static function getHourMiter($time) {
        return date("H:i", strtotime($time));
    }

    /**
     * 获取月日。如：04-01 10-31
     * @param $date
     * @return string
     */
    public static function getMonthDate($date) {
        return date("m-d", strtotime($date));
    }

    /**
     * 获取月日时分。如：01-01 10:00
     * @param $datetime
     * @return string
     */
    public static function getMonthDateHourMinute($datetime) {
        return date("m-d H:i", strtotime($datetime));
    }

    /**
     * 获取月日时分秒。如：01-01 10:00:00
     * @param $datetime
     * @return string
     */
    public static function getMonthDateHourMinuteSecs($datetime) {
        return date("m-d H:i:s", strtotime($datetime));
    }


    /**
     * 获取下几个月的第一天
     * @param $date
     * @param int $count
     * @return false|string
     */
    public static function getNextMonthFirstDate($date, $count = 1) {
        $ym = date('Y-m', strtotime($date));
        list($y, $m) = explode("-", $ym);
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                if ($m == 12) {
                    $y++;
                    $m = 1;
                } else {
                    $m++;
                }
            }
        } else {
            for ($i = 0; $i < abs($count); $i++) {
                if ($m == 1) {
                    $y--;
                    $m = 12;
                } else {
                    $m--;
                }
            }
        }
        return $y . '-' . (strlen($m) == 1 ? '0' . $m : $m) . '-01';
    }

    /**
     * 把时间转为日期
     * @param string $time 时间
     * @return false|string
     */
    public static function timeToDate($time) {
        return date("Y-m-d", strtotime($time));
    }

    /**
     * 获取上一天的日期
     * @param $date
     * @return false|string
     */
    public static function getPrevDay($date) {
        return date("Y-m-d", strtotime($date) - 86400);
    }

    /**
     * 获取下一天的日期
     * @param string $date 日期
     * @return false|string 下一天的日期
     */
    public static function getNextDay($date) {
        return date("Y-m-d", strtotime($date) + 86400);
    }

    /**
     * 获取多少年前的日期
     * @param string $date 原日期
     * @param int $years 多少年前。默认：1
     * @return string
     */
    public static function getPrevYearDate($date, $years = 1) {
        return date("Y-m-d", strtotime($date . " -{$years} year"));
    }

    /**
     * 计算两个日期相差的日期天数
     * @param $dateOne
     * @param $dateTwo
     * @return int
     */
    public static function getTwoDayNum($dateOne, $dateTwo) {
        return (int)abs(floor((strtotime($dateTwo) - strtotime($dateOne)) / 86400));
    }

    /**
     * 把时间转为分钟的格式。如："2018-01-01 08:00:00" 转成 "08:00"
     * @param string $datetime
     * @return string
     */
    public static function timeToMinute($datetime) {
        return date("H:i", strtotime($datetime));
    }

    /**
     * 时间戳转时间
     * @param int $timestamp 单位：秒
     * @return string
     */
    public static function timestampToTime($timestamp) {
        return date("Y-m-d H:i:s", $timestamp);
    }

    /**
     * 时间戳转日期
     * @param $timestamp
     * @return false|string
     */
    public static function timestampToDate($timestamp) {
        return date("Y-m-d", $timestamp);
    }

    /**
     * 把时间转为完整的时间。主要处理 "2018-01-01 10:00" 这种数据
     * @param string $time 时间字符串
     * @return string
     */
    public static function fullTime($time) {
        return self::timestampToTime(strtotime($time));
    }

    /**
     * 获取今天零点
     * @return string
     */
    public static function getTodayStart() {
        $time = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        return date("Y-m-d H:i:s", $time);
    }

    /**
     * 获取今天结束
     * @return false|int
     */
    public static function getTodayEnd() {
        $time = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
        return date("Y-m-d H:i:s", $time);
    }

    /**
     * 获取身份证的出生日期
     * @param $idCard
     * @return bool
     */
    public static function getIdCardBornDate($idCard) {
        if (strlen($idCard) !== 18) { // 身份证合法性验证
            return false;
        }
        $year = substr($idCard, 6, 4);
        $month = substr($idCard, 10, 2);
        $day = substr($idCard, 12, 2);

        return $year . "-" . $month . "-" . $day;
    }

    /**
     * 通过出生日期获取年龄
     * @param $bornDate
     * @return false|int|string
     */
    public static function getAge($bornDate) {
        $bornYear = self::getYear($bornDate);
        $bornMonth = self::getMonth($bornDate);
        $bornDay = self::getDay($bornDate);

        $baseAge = self::getYear(self::now()) - $bornYear;
        if ($bornMonth > date("m")) {
            $baseAge -= 1;
        } else if ($bornMonth == date("m") || $bornDay > date("d")) {
            $baseAge -= 1;
        }
        return $baseAge;
    }

    /**
     * 将日期转为通用的格式。
     * 如："2000.1.1"、"2000/1/1" 等。将转为："2000-01-01"
     * @param $date
     * @return string
     */
    public static function formatDate($date) {
        $matchArr = [];
        // excel 表格时间数据类型数据兼容。excel时间数据类型 2001/2/3，导入后会变成： 02-03-01
        if (preg_match("/^([0-9]{2,2})-([0-9]{2,2})-([0-9]{2,2})/i", $date, $matchArr) > 0) {
            $year = $matchArr[3];
            $month = $matchArr[1];
            $day = $matchArr[2];

            $date = $year . "-" . $month . "-" . $day;
        }
        $date = str_replace([".", "/", "\\"], "-", $date);

        return self::timestampToDate(strtotime($date));
    }

    /**
     * 获取两个时间相差的分钟数
     * @param $time1
     * @param $time2
     * @return int 大于等于0的整数
     */
    public static function getTwoTimeMin($time1, $time2) {
        return floor(TimeUtil::getTwoTimeSecs($time1, $time2) / 60);
    }

    /**
     * 获取连个时间相差的秒数
     * @param $time1
     * @param $time2
     * @return float
     */
    public static function getTwoTimeSecs($time1, $time2) {
        return floor(abs(strtotime($time1) - strtotime($time2)));
    }

    /**
     * 获取多少秒前的时间
     * @param $time
     * @param $second
     * @return string
     */
    public static function getTimeBeforeSecond($time, $second) {
        $timestamp = strtotime($time) - $second;
        return TimeUtil::timestampToTime($timestamp);
    }

    /**
     * 获取多少秒后的时间
     * @param $time
     * @param $second
     * @return string
     */
    public static function getTimeAfterSecond($time, $second) {
        $timestamp = strtotime($time) + $second;
        return TimeUtil::timestampToTime($timestamp);
    }
}
