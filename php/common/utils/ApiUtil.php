<?php
/**
 * Created by PhpStorm.
 * User: ewing
 * Date: 2019-07-26
 * Time: 17:10
 */

namespace common\utils;

use common\classes\Aes;
use common\models\vo\http\SscCellVo;
use common\models\vo\http\SscDataVo;
use Yii;

/**
 * API工具
 * Class ApiUtil
 */
class ApiUtil
{
    public static function getPartnerCode() {
        return Yii::$app->params['PARTNER_CODE'];
    }

    /**
     * 获取支付API网址
     */
    public static function getPayApiUrl() {
        return Yii::$app->params['PAY_API_URL'];
    }


    #====================================== 旧方法 =======================================

    /**
     * 获取第三方公司配置信息
     * @param $partnerCode
     * @return mixed
     */
    private static function getPartnerData($partnerCode) {
        return Yii::$app->params['PARTNER_CONFIG'][$partnerCode];
    }

    /**
     * 根据第三方标志获取第三方ID
     * @param $partnerCode
     * @return mixed
     */
    public static function getPartnerId($partnerCode) {
        return self::getPartnerData($partnerCode)['ID'];
    }

    /**
     * 根据第三方标志获取回调地址
     * @param $partnerCode
     * @return string $url
     */
    public static function getPartnerNotifyUrl($partnerCode) {
        return self::getPartnerData($partnerCode)['NOTIFY'];
    }


    /**
     * 根据第三方ID获取第三方标志
     * @param $partnerId
     * @return string
     */
    public static function getPartnerCodeById($partnerId) {
        foreach (Yii::$app->params['PARTNER_CONFIG'] as $k => $v) {
            if ($v['ID'] == $partnerId) {
                return $k;
            }
        }
    }

    /**
     * 通过合作公司标志获取密钥
     * @param string $partnerCode
     * @return
     */
    public static function getPartnerKeyByCode($partnerCode) {
        $key = self::getPartnerData($partnerCode)['KEY'];
        if (strlen($key) != 16) {
            die ('PARTNER_CODE_KEY strlen must be 16.');
        }
        return $key;
    }

    /**
     * 通过合作公司标志获取密钥
     * @param string $partnerCode
     * @return
     */
    public static function getPartnerIvByCode($partnerCode) {
        $iv = self::getPartnerData($partnerCode)['IV'];
        if (strlen($iv) != 16) {
            die ('PARTNER_IV strlen must be 16.');
        }
        return $iv;
    }

    /**
     * 不定参数
     * @param $partnerCode
     * @param $params
     * @return string
     */
    public static function makeHash($partnerCode, $params) {
        $key = ApiUtil::getPartnerKeyByCode($partnerCode);
        $iv = ApiUtil::getPartnerIvByCode($partnerCode);
        $aes = new Aes ($key, $iv);
        $hash_source = '';
        foreach ($params as $v) {
            $hash_source .= "|" . $v;
        }
        $hash_source = substr($hash_source, 1);
        return urlencode($aes->encrypt($hash_source));
    }

    /**
     * 解析 hash 的值
     * @param $hash
     * @return array
     */
    public static function decodeHash($hash) {
        $partnerCode = ApiUtil::getPartnerCode();
        $key = ApiUtil::getPartnerKeyByCode($partnerCode);
        $iv = ApiUtil::getPartnerIvByCode($partnerCode);
        $aes = new Aes ($key, $iv);
        return explode("|", $aes->decrypt($hash));
    }

    /**
     * 通知第三方公司已经确认了支付
     * @param $partnerId
     * @param $url
     * @param $params
     * @return array
     */
    public static function notifyPartner($partnerId, $url, $params) {
        $partnerCode = self::getPartnerCodeById($partnerId);
        $hash = self::makeHash($partnerCode, $params);
        $url .= "?Hash=" . $hash;
        list($content) = CurlUtil::httpsRequest($url);
        return [$content, $url];
    }


    /**
     * 获取下次需要通知的时间
     * @param $lastNotifyTime
     * @param $count
     * @return int
     */
    public static function getNextNotifyTime($lastNotifyTime, $count) {
        if ($count <= 3) {
            $second = 30;
        } elseif ($count <= 5) {
            $second = 60;
        } elseif ($count <= 6) {
            $second = 300;
        } elseif ($count <= 7) {
            $second = 900;
        } elseif ($count <= 8) {
            $second = 3600;
        } elseif ($count <= 9) {
            $second = 86400;
        } else {
            $second = 3 * 86400;
        }
        // echo "---<br>$count 上次通知时间:".$lastNotifyTime.",需要增加秒数:$second 下次日期:".date('Y-m-d H:i:s',strtotime($lastNotifyTime) + $second)."\n";
        return $lastNotifyTime ? strtotime($lastNotifyTime) + $second : time();

    }


    /**
     * 获取时时彩结果VO
     * @return SscDataVo
     */
    public static function getSscDataVo() {
        $url = Yii::$app->params['CHONGQIN_SSC_URL'];
        $html = HttpUtil::file_get_html($url);
        $result = '';
        $temp = $html->find(".kaij-cartoon span");
        foreach ($temp as $item) {
            $result .= $item->innertext;
        }

        $vo = new SscDataVo();
        $vo->lastPeriod = $html->find('.kaij-qs', 0)->innertext;
        $vo->lastResult = $result;
        $vo->nextPeriod = $html->find('.kaij-qsnext', 0)->innertext;

        $i = 0;
        $day = $hour = $minute = $second = '';
        foreach ($html->find('.open span') as $temp) {
            if ($i == 0) $day = $temp->innertext;
            if ($i == 2) $hour = $temp->innertext;
            if ($i == 4) $minute = $temp->innertext;
            if ($i == 6) $second = $temp->innertext;
            $i++;
        }
        $vo->nextPeriodCountDown = [$day, $hour, $minute, $second];
        $vo->nextOpenTime = $html->find('#nextOpentime', 0)->value;

        $historyCellVos = [];
        foreach ($html->find("tbody tr") as $item) {
            $found = false;
            $i = 0;
            $result = '';
            $oneResult = new SscCellVo();
            foreach ($item->find('td') as $v) {
                $value = $v->innertext;
                if ($i == 0) {
                    $oneResult->openTime = $value;
                } elseif ($i == 1) {
                    $oneResult->period = $value;
                } elseif ($i == 2) {
                    foreach ($v->find('span') as $vv) {
                        $result .= $vv->innertext;
                    }
                    $oneResult->result = $result;
                } elseif ($i == 3) {
                    $oneResult->sumA = $value;
                } elseif ($i == 4) {
                    $oneResult->sumB = $value;
                } elseif ($i == 5) {
                    $oneResult->rate = trim($value);
                } elseif ($i == 6) {
                    $oneResult->rateType = $value;
                } elseif ($i == 7) {
                    $oneResult->spacing = $value;
                }
                $found = true;
                $i++;
            }
            if ($found) {
                //var_dump($oneResult);die;
                $historyCellVos[] = $oneResult;
            }
        }
        $vo->historyCellVos = $historyCellVos;
        return $vo;
    }

    /**
     * 新获取时时彩结果VO
     * @return SscDataVo
     */
    public static function getSscDataVo2() {
        $url = Yii::$app->params['CHONGQIN_SSC_URL_NEW'];
        $html = HttpUtil::file_get_html($url);
        $result = '';
        $temp = $html->find(".kj_num img");
        foreach ($temp as $item) {
            $result = self::kjresult($item->src,$result);
        }
        $vo = new SscDataVo();
        $vo->lastPeriod = $html->find('.info_list dl dd p', 0)->innertext;
        preg_match('/\d+/',$vo->lastPeriod,$arr)?$vo->lastPeriod=$arr[0]:0;
        $vo->lastResult = $result;
        $date=substr($vo->lastPeriod,0,8);
        $stage=substr($vo->lastPeriod,-3);
        $time = strtotime($date);
        if($stage=='059'){
            $vo->nextPeriod = (date('Ymd',strtotime($date)+86400)).'001';
        }else{
            $stage=(int)$stage+1;
            $vo->nextPeriod=$stage<10?$date.'00'.(string)$stage:$date.'0'.(string)$stage;
        }

//        $i = 0;
//        $day = $hour = $minute = $second = '';
//        foreach ($html->find('.open span') as $temp) {
//            if ($i == 0) $day = $temp->innertext;
//            if ($i == 2) $hour = $temp->innertext;
//            if ($i == 4) $minute = $temp->innertext;
//            if ($i == 6) $second = $temp->innertext;
//            $i++;
//        }
//        $vo->nextPeriodCountDown = [$day, $hour, $minute, $second];
//        $vo->nextOpenTime = $html->find('#nextOpentime', 0)->value;

        $historyCellVos = [];
        $j=0;
        foreach ($html->find("tbody tr") as $item) {
                $found = false;
                $i = 0;
                $result = '';
                $oneResult = new SscCellVo();
                foreach ($item->find('td') as $v) {
                    $value = $v->innertext;
                    if ($i == 0) {
                        $oneResult->period = $value;
                    } elseif ($i == 1) {
                        $oneResult->openTime = $value;
                    } elseif ($i == 2) {
                        foreach ($v->children as $vv) {
                            $result = self::kjresult($vv->src, $result);
                        }
                        $oneResult->result = $result;
                    }
                    $found = true;
                    $i++;
                }
                if($j==1||$j==3||$j==5||$j==7||$j==9){
                    if ($found) {
                        //var_dump($oneResult);die;
                        $historyCellVos[] = $oneResult;
                    }
                }
                $j++;
        }
        $vo->historyCellVos = $historyCellVos;
        return $vo;
    }

    /**
     * @param $img_src 图片路径
     * @param $result 开奖结果
     * @return string
     */
    public static function kjresult($img_src,$result){
        switch ($img_src){
            case 'js/kj_js_css/img/201808200329022908.png':
                $result .= 1;
                break;
            case 'js/kj_js_css/img/201808200329023852.png':
                $result .= 2;
                break;
            case 'js/kj_js_css/img/201808200329024582.png':
                $result .= 3;
                break;
            case 'js/kj_js_css/img/201808200329025833.png':
                $result .= 4;
                break;
            case 'js/kj_js_css/img/201808200329026188.png':
                $result .= 5;
                break;
            case 'js/kj_js_css/img/201808200329027351.png':
                $result .= 6;
                break;
            case 'js/kj_js_css/img/201808200329028495.png':
                $result .= 7;
                break;
            case 'js/kj_js_css/img/201808200329029838.png':
                $result .= 8;
                break;
            case 'js/kj_js_css/img/2018082003290210496.png':
                $result .= 9;
                break;
            case 'js/kj_js_css/img/201808200329021430.png':
                $result .= 0;
                break;
            default:
                $result .= '';
        }
        return $result;
    }

}
