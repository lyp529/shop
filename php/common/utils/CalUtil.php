<?php


namespace common\utils;


/**
 * Class CalUtil 算法工具
 * @package common\utils
 */
class CalUtil
{
    // 阶乘
    public static function factorial($n) {
        return array_product(range(1, $n));
    }

    // 排列数
    public static function A($n, $m) {
        return self::factorial($n) / self::factorial($n - $m);
    }

    // 组合数
    public static function C($n, $m) {
        return self::A($n, $m) / self::factorial($m);
    }

    // 排列
    public static function arrange($a, $len) {
        $r = array();

        $n = count($a);
        if ($len <= 0 || $len > $n) {
            return $r;
        }

        for ($i = 0; $i < $n; $i++) {
            $b = $a;
            $t = array_splice($b, $i, 1);
            if ($len == 1) {
                $r[] = $t;
            } else {
                $c = self::arrange($b, $len - 1);
                foreach ($c as $v) {
                    $r[] = array_merge($t, $v);
                }
            }
        }

        return $r;
    }

    // 组合
    public static function comb($a, $len) {
        $r = array();

        $n = count($a);
        if ($len <= 0 || $len > $n) {
            return $r;
        }

        for ($i = 0; $i < $n; $i++) {
            $t = array($a[$i]);
            if ($len == 1) {
                $r[] = $t;
            } else {
                $b = array_slice($a, $i + 1);
                $c = self::comb($b, $len - 1);
                foreach ($c as $v) {
                    $r[] = array_merge($t, $v);
                }
            }
        }
        return $r;
    }

}
