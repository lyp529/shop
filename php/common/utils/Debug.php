<?php
/**
 * Created by PhpStorm.
 * User: 11054
 * Date: 2018/7/3
 * Time: 16:39
 */

namespace common\utils;

use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * 调试处理工具
 * Class QueryUtil
 * @package common\utils
 */
class Debug extends VarDumper
{
    /**
     * 获取最后一条查询的sql
     * @param $query ActiveQuery
     */
    public static function showLastSql($query) {
        echo "showLastSql --->" . $query->createCommand()->getRawSql() . "<br>\n";
    }

    /**
     * @return bool 是否是测试环境
     */
    public static function isDebug() {
        return YII_DEBUG;
    }
}
