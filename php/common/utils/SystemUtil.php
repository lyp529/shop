<?php


namespace common\utils;


use common\constants\C;

/**
 * Class SystemUtil 系统、环境工具
 * @package common\utils
 */
class SystemUtil
{
    /**
     * 判断当前运行环境是不是windows
     * @return bool
     */
    public static function isWin() {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    /**
     * 判断是否管理后台的网址
     * @reamrk 真网才生效
     */
    public static function isAdminSite() {
        return StringUtil::contains($_SERVER['HTTP_HOST'], C::ADMIN_SITE_SIGN);
    }

    /**
     * 判断是否管理后台的网址
     * @reamrk 真网才生效
     */
    public static function isAgentSite() {
        return StringUtil::contains($_SERVER['HTTP_HOST'], C::AGENT_SITE_SIGN);
    }

}
