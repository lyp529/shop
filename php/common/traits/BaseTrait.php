<?php

namespace common\traits;

use common\exceptions\SystemException;
use common\utils\StringUtil;
use common\exceptions\ForeseeableException;
use ReflectionClass;

trait BaseTrait
{
    private static $varMap = [];

    /**
     * 确认常量存在
     * @param $value
     * @throws ForeseeableException
     */
    public static function ensureConstValue($value)
    {
        if (YII_DEBUG) {
            try {
                $class = new ReflectionClass(__CLASS__);
                $constMap = $class->getConstants(); //返回所有常量名和值
                $flag = in_array($value, array_values($constMap));
                if (!$flag) {
                    throw new ForeseeableException(__CLASS__ . " 不存在常量值:" . $value);
                }
            } catch (\ReflectionException $e) {
                echo $e;
            }
        }
    }

    /**
     * 确认为每个常量都定义了label
     * @throws ForeseeableException
     */
    public static function ensureLabelDefined()
    {
        if (YII_DEBUG) {
            try {
                $class = new ReflectionClass(__CLASS__);
                if (!$class->hasMethod("labels")) {
                    throw new ForeseeableException(__CLASS__ . "中必须存在方法 labels()");
                } else {
                    $instance = $class->newInstanceArgs();
                    $method = $class->getMethod('labels');
                    $labels = $method->invoke($instance);
                    $constMap = $class->getConstants(); //返回所有常量名和值
                    foreach ($constMap as $k => $v) {
                        if (!isset($labels[$v])) {
                            throw new ForeseeableException(__CLASS__ . "::labels 中未定义" . self::getPropertyName($v) . "的label");
                        }
                    }
                }
            } catch (\ReflectionException $e) {
                echo $e;
            }
        }
    }

    /**
     * 确认一个常量类中的值唯一,用以防止定义的值相同引起数据错乱
     * @param string $prefix 常量前缀
     * @throws SystemException
     */
    public static function ensureUniqueCheck($prefix = "")
    {
        if (YII_DEBUG) {
            try {
                $class = new ReflectionClass(__CLASS__);
                $constMap = $class->getConstants(); //返回所有常量名和值
                $existsValues = [];
                foreach ($constMap as $k => $v) {
                    if (!empty($prefix) && !StringUtil::startsWith($k, $prefix)) {
                        continue;
                    }
                    if (in_array($v, $existsValues)) {
                        throw new SystemException(__CLASS__ . "有相同的常量值:" . $v);
                    } else {
                        $existsValues[] = $v;
                    }
                }
            } catch (\ReflectionException $e) {
                echo $e;
            }
        }
    }


    /**
     * 从值获取变量名,注意如果有相同的值会有问题
     */
    public static function getPropertyName($value)
    {
        try {
            if (empty(self::$varMap)) {
                $class = new ReflectionClass(__CLASS__);
                $constMap = $class->getConstants(); //返回所有常量名和值
                self::$varMap = array_flip($constMap);
            }
        } catch (\ReflectionException $e) {
            echo $e;
        }
        return isset(self::$varMap[$value]) ? self::$varMap[$value] : "";

    }
}
