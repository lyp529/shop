<?php


namespace common\base;


use common\constants\C;
use common\exceptions\SystemException;
use common\models\vo\ResponseVo;
use Yii;
use common\utils\YiiUtil;
use yii\base\Action;
use yii\db\Exception;
use yii\db\Transaction;
use yii\helpers\Json;
use yii\web\Controller;


/**
 * Class BaseWebController web控制器的基类
 * @package common\base
 */
class BaseWebController extends Controller {
    /**
     * @var null|Transaction
     */
    private $transaction = null;

    /**
     * 错误处理
     */
    public function actionError() {
        // 回滚事务
        if ($this->transaction !== null) {
            $this->transaction->rollBack();
        }

        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            Yii::$app->response->statusCode = 200;
            $msg = $exception->getMessage();
            $debug = $exception->getMessage() . ' ' . $exception->getTraceAsString();
            if ($exception instanceof SystemException) {
                $msg = "系统错误";
            }
            return $this->response(C::RESPONSE_CODE_EXCEPTION, $msg, [], $debug);
        }

        return "404";
    }

    /**
     * 通用响应接口
     * @param $code
     * @param $message
     * @param $data
     * @param string $debug
     * @return string
     */
    public function response($code, $message, $data = [], $debug = "") {
        $responseVo = new ResponseVo();
        $responseVo->c = $code;
        $responseVo->m = $message;
        $responseVo->d = $data;
        $responseVo->debug = $debug;
        return Json::encode($responseVo);
    }

    /**
     * 成功消息返回
     * @param mixed $data 返回给前端的数据
     * @return string
     */
    public function done($data = []) {
        $code = C::RESPONSE_CODE_DONE;
        return $this->response($code, C::getReturnCodeLabel($code), $data);
    }

    /**
     * 失败消息返回
     * @deprecated 不推荐使用。因为返回这个数据会导致系统无法捕捉异常，无法回滚事务
     * @param string $message 失败的消息
     * @return string
     */
    public function fail($message) {
        $code = C::RESPONSE_CODE_FAIL;
        return $this->response($code, $message);
    }

    /**
     * 首页页面
     */
    public function actionHome() {
        return $this->redirect("index.html");
    }

    /**
     * 获取控制层的参数
     * @param $name
     * @param string $default
     * @return mixed
     */
    public function param($name = "", $default = "") {
        $arrPost = Yii::$app->request->post();
        $arrGet = Yii::$app->request->get();
        if (empty($name)) {
            return array_merge($arrPost, $arrGet);
        } else if (isset($arrPost[$name])) {
            return $arrPost[$name];
        } else if (isset($arrGet[$name])) {
            return $arrGet[$name];
        } else {
            return $default;
        }
    }

    /**
     * @param Action $action
     * @param mixed $result
     * @return mixed
     * @throws Exception
     */
    public function afterAction($action, $result) {
        // 提交事务
        if ($this->transaction !== null) {
            $this->transaction->commit();
        }
        return parent::afterAction($action, $result);
    }

    /**
     * 启动事务
     */
    public function beginTransaction() {
        $this->transaction = Yii::$app->db->beginTransaction();
    }

    /**
     * 设置session
     * @param $key
     * @param $value
     */
    protected function setSession($key, $value) {
        YiiUtil::session($key, $value);
    }

    /**
     * 获取session值
     * @param $key
     * @return mixed
     */
    protected function getSession($key) {
        return YiiUtil::session($key);
    }

    /**
     * 销毁session
     */
    protected function destroySession() {
        Yii::$app->getSession()->destroy();
    }
}
