<?php


namespace common\base;


use common\models\BaseModel;
use Yii;
use yii\helpers\Json;

/**
 * Class BaseForm 基础表单
 * @package common\base
 */
class BaseForm extends BaseModel {
    /**
     * 从 请求 参数导入数据
     * @param string $paramName post\get 参数名称（值可以是 json对象 或 json字符串）
     * @return static
     */
    public static function initByParam($paramName = "") {
        if (!empty($paramName)) {
            $params = Yii::$app->request->get($paramName, null);
            if ($params === null) {
                $params = Yii::$app->request->post($paramName, null);
                if ($params === null) {
                    return new static();
                }
            }
        } else {
            $post = Yii::$app->request->post();
            $get = Yii::$app->request->get();
            $params = array_merge($post, $get);
        }

        if (is_object($params)) {
            $params = get_object_vars($params);
        }

        if (is_string($params)) {
            $params = Json::decode($params);
        }

        $baseFromModel = new static();
        $baseFromModel->setAttributes($params);

        return $baseFromModel;
    }

    /**
     * 去除所有字段的前后空格
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true)
    {
        parent::setAttributes($values, $safeOnly);
        $this->trimAll();
    }

    /**
     * 去除所有变量前后的空格
     */
    private function trimAll()
    {
        $array = $this->toArray();
        foreach ($array as $prop => $value) {
            if (is_string($value)) {
                $this->$prop = trim($value);
            } else if (is_array($value)) {
                $this->$prop = $this->trimArr($value);
            }
        }
    }

    /**
     * @param array $array 去除数组内每个元素的前后空格
     * @return array
     */
    private function trimArr(array $array) {
        foreach ($array as $key => $value) {
            if (is_string($value)) {
                $array[$key] = trim($value);
            } else if (is_array($value)) {
                $array[$key] = $this->trimArr($value);
            }
        }
        return $array;
    }
}