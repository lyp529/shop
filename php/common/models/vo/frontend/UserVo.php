<?php


namespace common\models\vo\frontend;


use common\constants\C;
use common\models\db\Vip;
use common\models\UserModel;
use common\models\vo\BaseVo;

/**
 * Class UserVo 会员端用户数据
 * @package common\models\vo\frontend
 */
class UserVo extends BaseVo
{
    /**
     * @var int 用户id
     */
    public $userId;
    /**
     * @var string 登录账号
     */
    public $username;
    /**
     * @var string 用户名
     */
    public $name;
    /**
     * @var int 信用额度（账户余额）
     */
    public $credit;

    /**
     * @var int VIP等级
     */
    public $vipLevel;
    /**
     * @var string VIP名称
     */
    public $vipName;
    /**
     * @var bool 是否签到
     */
    public $isSign;

    /**
     * @var string 积分
     */
    public $integral;

    /**
     * @var bool 是否已经设置资金密码
     */
    public $hasCashPassword;

    /**
     * @var int 提现剩余次数
     */
    public $withdrawRestTimes;

    /**
     * @var int 银行ID
     */
    public $bankId;

    /**
     * @var string 银行名称
     */
    public $bankName;

    /**
     * @var string 银行帐户
     */
    public $bankAccount;

    /**
     * @var string 银行用户名
     */
    public $bankUsername;

    /**
     * @var string 绑定邮箱
     */
    public $bankEmail;
    /**
     * @var int 前一天vip等级
     */
    public $beforeVipLevel;
    /**
     * @var string 前一天零点账户余额
     */
    public $beforeCredit;
    /**
     * @var int 每天下单金额返回百分比
     */
    public $backPercent;
    /**
     * @var int 昨天下单返利
     */
    public $placeOrderRebated;
    /**
     * @var int 今天预计下单返利
     */
    public $todayPredictRebate;

    /**
     * 使用用户模型初始化数据
     * @param UserModel $user
     */
    public function initByUserModel(UserModel $user) {
        $member = $user->getMember();
        $this->userId = $member->user_id;
        $this->username = $member->username;
        $this->name = $member->name;
        $this->credit = $member->credit;
        $this->vipLevel = $member->vip_level;
        $this->vipName = C::getVipLabels($this->vipLevel);
        $this->isSign = $member->last_sign_date;
        $this->integral = $member->integral;
        $this->hasCashPassword = !empty($user->getAccount()->password2);
        $this->withdrawRestTimes = $member->getWithdrawRestTimes();
        $this->bankId = $member->bank_id;
        $this->bankName = $member->bank_id ? C::getBankLabel($member->bank_id) : "";
        $this->bankAccount = $member->bank_account;
        $this->bankUsername = $member->bank_username;
        $this->bankEmail = $member->bank_email;
        $this->beforeVipLevel = C::getVipLabels((int)$member->before_vip_level);
        $this->beforeCredit = $member->before_credit;
        $this->backPercent = "";
        $this->placeOrderRebated = $member->place_order_rebated;
    }
}
