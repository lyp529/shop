<?php


namespace common\models\vo\frontend;


use common\models\db\CashRecord;
use common\models\vo\BaseVo;

/**
 * Class CashRecordRowVo 前端账变查询每行的数据封装
 * @package common\models\vo\frontend
 */
class CashRecordRowVo extends BaseVo {
    /**
     * @var int 单号
     */
    public $no;
    /**
     * @var string 日志类型标签
     */
    public $typeLabel;
    /**
     * @var string 记录事件
     */
    public $recordTime;
    /**
     * @var string 资金。单位：元
     */
    public $amount;
    /**
     * @var string 余额。单位：元
     */
    public $balance;
    /**
     * @var string 备注
     */
    public $remark;

    /**
     * @param CashRecord $cashRecord
     */
    public function initByCashRecord(CashRecord $cashRecord) {
        $this->no = $cashRecord->record_no;
        $this->typeLabel = $cashRecord->clog_type_label;
        $this->recordTime = $cashRecord->record_time;
        $this->amount = $cashRecord->amount;
        $this->balance = $cashRecord->balance;
        $this->remark = $cashRecord->remark;
    }
}