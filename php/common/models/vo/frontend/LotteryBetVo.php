<?php


namespace common\models\vo\frontend;


use common\constants\C;
use common\constants\P;
use common\models\db\Bet;
use common\models\vo\BaseVo;
use common\utils\TimeUtil;

/**
 * Class LotteryBetVo 彩票类型的注单数据（前端展示数据所需）
 * @package common\models\vo\frontend
 */
class LotteryBetVo extends BaseVo
{
    /**
     * @var int 注单id
     */
    public $betId;
    /**
     * @var string 注单编号
     */
    public $betNo;
    /**
     * @var string 游戏名字
     */
    public $kindName;
    /**
     * @var string 期号
     */
    public $period;
    /**
     * @var array 投注内容。如：[[1], [2,3]]
     */
    public $betContent;
    /**
     * @var string 投注类型（玩法）
     */
    public $betType;
    /**
     * @var int 倍数
     */
    public $multiple;
    /**
     * @var string 单位
     */
    public $unit;
    /**
     * @var int 总额（单位：元）
     */
    public $betAmount;
    /**
     * @var array 开奖号码
     */
    public $resultNums;
    /**
     * @var int 开奖结果
     */
    public $betResult;
    /**
     * @var string 状态。未开奖、未中奖、已中奖
     */
    public $status;
    /**
     * @var string 投注时间
     */
    public $betTime;

    /**
     * @param Bet $bet
     */
    public function initByBet(Bet $bet)
    {
        $schedule = $bet->getSchedule();

        $this->betId = $bet->id;
        $this->betNo = $bet->bet_no;
        $this->kindName = $schedule !== null ? $schedule->kind_name : "";
        $this->period = $schedule !== null ? $schedule->period : "";
        $this->betType = P::getScBetTypeLabel($bet->bet_type);
        $this->betContent = $bet->getBetContent();
        $this->status = C::BET_STATUS_LABEL_WAIT;
        if ($schedule !== null && $schedule->open_time < TimeUtil::now()) {
            $this->status = $bet->bet_result > 0 ? C::BET_STATUS_LABEL_WIN : C::BET_STATUS_LABEL_LOSE;
        }
        $this->betTime = $bet->record_time;
        $this->multiple = $bet->multiple;
        $this->unit = C::getBetUnitLabel($bet->unit);
        $this->betAmount = $bet->bet_amount;
        $this->resultNums = $schedule->getResultNums();
        $this->betResult = $bet->bet_result;
    }
}
