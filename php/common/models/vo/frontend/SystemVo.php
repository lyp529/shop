<?php


namespace common\models\vo\frontend;


use common\models\vo\BaseVo;

/**
 * Class HomeVo 首页所需的动态数据
 * @package common\models\vo\frontend
 */
class SystemVo extends BaseVo
{
    /**
     * @var int 在线认数
     */
    public $onlineNum = 0;
    /**
     * @var int 提现排队人数
     */
    public $withdrawWaitNum = 0;
    /**
     * @var double 今日盈亏
     */
    public $todayAmount = 0;
    /**
     * @var double 今日下注总额
     */
    public $todayBetAmount = 0;
    /**
     * @var double 昨日盈亏
     */
    public $yesterdayAmount = 0;
    /**
     * @var double 昨日下注金额
     */
    public $yesterdayBetAmount = 0;
    /**
     * 公告内容
     * @var string
     */
    public $notice = "";
    /**
     * 盈利排行榜内容
     * @var string
     */
    public $earnRan = "";

    /**
     * 玩家信用额
     * @var string
     */
    public $credit = "";
    /**
     * 玩家积分
     * @var int
     */
    public $integral = "";
    /**
     * 奖励中心
     * @var array
     */
    public $award = [];


    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->onlineNum = rand(9000, 10000);
    }
}
