<?php


namespace common\models\vo\frontend;


use common\models\vo\BaseVo;

/**
 * Class GameVo 前端单个游戏的数据封装
 * @package common\models\vo\frontend
 */
class GameVo extends BaseVo {
    /**
     * @var int 游戏类型
     */
    public $kind;
    /**
     * @var string 游戏名字
     */
    public $name;
    /**
     * @var string 游戏图片名字（相对于  "vue-cli/frontend/src/" 的路径）
     */
    public $src;

    /**
     * @param $kind
     * @param $name
     * @param $src
     * @return GameVo
     */
    public static function initByParams($kind, $name, $src) {
        $vo = new GameVo();
        $vo->kind = $kind;
        $vo->name = $name;
        $vo->src = $src;
        return $vo;
    }
}