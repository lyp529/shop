<?php


namespace common\models\vo;

/**
 * Class MessageVo websocket 通信传输的消息
 * @package common\models\vo
 */
class MessageVo extends BaseVo {
    /**
     * @var int 通信id （由客户端自定义，标识属于哪次请求返回的数据）
     */
    public $id;
    /**
     * @var string 请求的控制器
     */
    public $route;
    /**
     * @var ResponseVo|array 请求数据
     */
    public $vo;

    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->vo = new ResponseVo();
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->id = (int)$this->id;
        $this->vo = ResponseVo::initListByArr($this->vo);
    }
}