<?php


namespace common\models\vo\http;

/**
 * 单个历史记录结果VO
 */
class SscCellVo
{
    public $openTime; //开奖时间
    public $period; //期数
    public $result; //结果e.g. 94097
    public $sumA; //和特征A
    public $sumB;//和特征B
    public $rate; //奇偶比
    public $rateType; //奇偶形态
    public $spacing; //跨度
}
