<?php


namespace common\models\vo\http;


use common\models\vo\BaseVo;

/**
 * Class SscHttpVo 时时彩取网数据VO
 * @package common\models\vo\http
 */
class SscDataVo extends BaseVo
{

    /**
     * @var int 最近一期
     */
    public $lastPeriod;

    /**
     * @var string 最近一期结果
     */
    public $lastResult;

    /**
     * @var int 下一期
     */
    public $nextPeriod;

    /**
     * @var array 距离下一期开奖剩余时间 [日,时,分,秒]  -1为开奖中  (不一定有用)
     */
    public $nextPeriodCountDown;

    /**
     * @var string 下一期开奖时间  e.g. 2019-09-24 16:30:00
     */
    public $nextOpenTime;

    /**
     * @var SscCellVo[] 历史记录
     */
    public $historyCellVos;

    /**
     * 返回结果Map
     * @return SscCellVo[]
     */
    public function getHistoryCellVosMap() {
        $map = [];
        foreach ($this->historyCellVos as $sscCellVo) {
            $map[$this->fullPeriod($sscCellVo->period)] = $sscCellVo;
        }
        return $map;
    }

    /**
     * 完善时时彩期数
     * @param $period
     * @return string
     */
    private function fullPeriod($period) {
        return (strlen($period) >= 6) ? date('Ym') . substr($period, -5) : $period;
    }


}
