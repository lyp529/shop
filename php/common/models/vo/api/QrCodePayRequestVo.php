<?php


namespace common\models\vo\api;


use common\models\vo\BaseVo;

/**
 * Class QrCodePayRequestVo 二维码支付请求参数
 * @package common\models\vo\api
 */
class QrCodePayRequestVo extends BaseVo {
    /**
     * @var int 支付类型
     */
    public $type;
    /**
     * @var double 充值金额
     */
    public $amount;
    /**
     * @var string 订单
     */
    public $orderNo;
    /**
     * @var string 玩家唯一标识
     */
    public $memberCode;
    /**
     * @var string 玩家昵称  改为  付款帐户信息 by ewing 2019-10-17
     */
    public $nickname;
}
