<?php


namespace common\models\vo\api;


use common\models\vo\BaseVo;

/**
 * Class BankInfoResponseVo 获取银行信息返回数据
 * @package common\models\vo\api
 */
class BankInfoResponseVo extends BaseVo {
    /**
     * @var string 银行名字。如：中国工商银行
     */
    public $name;
    /**
     * @var string 银行账号。如：6200000000000000000
     */
    public $account;
    /**
     * @var string 银行户名。如：张三
     */
    public $username;
}