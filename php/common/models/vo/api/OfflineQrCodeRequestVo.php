<?php


namespace common\models\vo\api;


use common\models\vo\BaseVo;
use common\utils\ArrayUtil;

/**
 * Class OfflineQrCodeRequest 获取线下支付二维码 api 支付接口请求所需参数
 * @package common\models\vo\api
 */
class OfflineQrCodeRequestVo extends BaseVo {
    /**
     * @var int 用户id
     */
    public $userId;
    /**
     * @var string 用户账号
     */
    public $username;
    /**
     * @var double 充值金额
     */
    public $amount;
    /**
     * @var string 订单编号
     */
    public $orderNo;
    /**
     * @var int|string 支付类型
     */
    public $payType;

    /**
     * @return array
     */
    public function rules() {
        return ArrayUtil::merge(parent::rules(), [
            [["userId", "username", "amount", "orderNo", "payType"], "required"],
        ]);
    }
}