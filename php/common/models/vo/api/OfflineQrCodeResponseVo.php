<?php


namespace common\models\vo\api;


use common\models\vo\BaseVo;

/**
 * Class OfflineQrCodeResponse 取线下支付二维码 api 支付接口 返回参数
 * @package common\models\vo\api
 *
 * {"ReturnCode":1,"CodeUrl":"http://61.56.84.139/1.png"}
 */
class OfflineQrCodeResponseVo extends BaseVo {
    /**
     * @var int 请求结果
     */
    public $ReturnCode;
    /**
     * @var string 二维码地址
     */
    public $CodeUrl;

    /**
     * 是否处理成功
     * @return bool
     */
    public function isDone() {
        return $this->ReturnCode === 1;
    }
}