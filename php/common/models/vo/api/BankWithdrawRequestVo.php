<?php


namespace common\models\vo\api;


/**
 * Class BankWithdrawRequestVo 银行提款所需的参数
 * @package common\models\vo\api
 */
class BankWithdrawRequestVo {
    /**
     * @var int 充值金额。单位元
     */
    public $amount;
    /**
     * @var string 收款银行名字
     */
    public $recvName;
    /**
     * @var string 收款银行账号
     */
    public $recvAccount;
    /**
     * @var string 收款银行户名
     */
    public $recvUsername;
    /**
     * @var string 第三方支付订单
     */
    public $order3rdNo;
    /**
     * @var string 玩家唯一标识
     */
    public $memberCode;
    /**
     * @var string 玩家昵称
     */
    public $nickname;
}