<?php


namespace common\models\vo\order;


use common\models\db\OrderWallet;
use common\models\vo\BaseVo;

/**
 * Class WithdrawVo 提款信息
 * @package common\models\vo\order
 */
class WithdrawVo extends BaseVo {
    /**
     * @var int 提款单id
     */
    public $orderWalletId;
    /**
     * @var string 订单编号
     */
    public $orderNo;
    /**
     * @var string 银行卡编号
     */
    public $bankNo;
    /**
     * @var string 开户人姓名
     */
    public $bankName;

    /**
     * @param OrderWallet $orderWallet
     */
    public function initByOrderWallet(OrderWallet $orderWallet) {
        $this->orderWalletId = $orderWallet->id;
        $this->orderNo = $orderWallet->order_no;
        $this->bankNo = $orderWallet->bank_account;
        $this->bankName = $orderWallet->bank_name;
    }
}