<?php


namespace common\models\vo\order;


use common\constants\C;
use common\base\BaseForm;
use common\models\db\Order;
use common\models\db\OrderWallet;

/**
 * Class OrderWalletForm 钱包订单（充值提现）表单的模型
 * @package models\form
 */
class OrderWalletVo extends BaseForm {
    /**
     * @var int 订单ID
     */
    public $id;
    /**
     * @var int 用户ID
     */
    public $userId;
    /**
     * @var int 操作人员ID
     */
    public $adminId;
    /**
     * @var string 订单编号
     */
    public $orderNo;
    /**
     * @var int 订单类型
     */
    public $orderType;
    /**
     * @var int 存取款方式
     */
    public $orderSubType;
    /**
     * @var int 订单状态
     */
    public $orderState;
    public $isShow;
    /**
     * @var string 下单时间
     */
    public $orderTime;
    /**
     * @var string 完成支付时间
     */
    public $finishTime;
    /**
     * @var string 订单金额
     */
    public $amount;
    /**
     * @var int 支付通道
     */
    public $payType;
    /**
     * @var string 银行户名
     */
    public $bankName;
    public $goodsName;
    /**
     * @var string 银行账号
     */
    public $bankAccount;
    /**
     * @var string 开户姓名
     */
    public $bankUsername;
    /**
     * @var string 备注
     */
    public $remark;
    /**
     * @var string
     */
    public $updateTime;
    /**
     * @var string
     */
    public $recordTime;
    /**
     * @var bool 用于前端判断该订单是否显示【操作按钮】
     */
    public $isShowEnter = false;
    /**
     * @var string 会员账号
     */
    public $memberUsername;
    /**
     * @var string 操作人账号
     */
    public $adminUsername;
    /**
     * @var string 支付用户信息
     */
    public $payUsername;
    /**
     * @var bool 是否十提款
     */
    public $isWithdraw = false;

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->orderSubType = (int)$this->orderSubType;
        $this->amount = (double)$this->amount;
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'id' => '订单ID',
            'user_id' => '用户ID',
            'admin_id' => '操作人员ID',
            'order_no' => '订单编号',
            'order_type' => '1:存款,2:取款',
            'order_sub_type' => '存取款方式',
            'order_state' => '1:待支付,2:已支付,3:取消支付,4:支付失败',
            'order_time' => '下单时间',
            'finish_time' => '完成支付时间',
            'amount' => '订单金额',
            'pay_type' => '支付通道',
            'bank_no' => '银行卡编号',
            'bank_name' => '开户姓名',
            'remark' => '备注',
            'update_time' => 'Update Time',
            'record_time' => 'Record Time',
        ];
    }

    /**
     * @param Order $order
     * @param $memberUsername
     * @param $adminUsername
     */
    public function initByOrderWallet(Order $order, $memberUsername = "", $adminUsername = "") {
        $this->id = $order->id;
        $this->userId = $order->user_id;
        $this->adminId =$order->admin_id;
        $this->orderNo =$order->order_number;
        $this->orderTime =$order->order_time;
        $this->amount =$order->integral;
        $this->goodsName =$order->goods_name;
        $this->isShow = ($order->order_state === 1) ? TRUE : FALSE;
        $this->orderState = C::orderWalletStateLabel($order->order_state);
        $this->memberUsername = $memberUsername;
		$this->finishTime =$order->update_date;
    }
}
