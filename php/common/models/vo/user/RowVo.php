<?php


namespace common\models\vo\user;


use common\constants\C;
use common\exceptions\SystemException;
use common\models\UserModel;
use common\models\vo\BaseVo;
use common\utils\TimeUtil;

/**
 * Class RowVo 用户表格每行的数据封装
 * @package common\models\vo\user
 */
class RowVo extends BaseVo {
    // ------- 通用数据
    /**
     * @var int 用户id
     */
    public $userId;
    /**
     * @var int 用户类型
     */
    public $userType;
    /**
     * @var string 用户名
     */
    public $name;
    /**
     * @var string 登录账号
     */
    public $username;
    /**
     * @var string 注册日期。如：2000-01-01
     */
    public $registerDate;
    /**
     * @var string 最近登录时间。如：01-01 10:00
     */
    public $loginTime;
    /**
     * @var string 最近登录ip
     */
    public $loginIp;
    /**
     * @var bool 是否激活
     */
    public $active;
    /**
     * @var bool 是否登录
     */
    public $isLogin;

    // ------- 其他用户类型的特殊数据
    /**
     * @var int 信用额度
     */
    public $credit = 0;
    /**
     * @var int VIP 用户等级
     */
    public $vipLevel = 0;
    /**
     * @var int VIP 用户目标等级
     */
    public $targetVipLevel = 0;
    /**
     * @var int VIP 标签
     */
    public $vipLevelLabel;
    /**
     * @var int VIP 目标等级标签
     */
    public $targetVipLevelLabel;
    /**
     * @var int 上线id
     */
    public $upId;
    /**
     * @var int 上线类型
     */
    public $upType;
    /**
     * @var string 上线类型标签
     */
    public $upTypeLabel;
    /**
     * @var string 上级名字
     */
    public $upName;
    /**
     * @var double 本人占成 0-1 (如果是会员。则是上级占成)
     */
    public $fractionMax = 0;
    /**
     * @var double 上级占成 0-1 (如果是会员。则是上上级占成)
     */
    public $fractionUp = 0;

    /**
     * @param UserModel $user
     * @param bool $initAll 是否初始化所有数据（如果不初始化所有，可以减少性能消耗。占成数据设置比较消耗性能）
     * @throws SystemException
     */
    public function initByUserModel(UserModel $user, $initAll = true) {
        $account = $user->getAccount();
        $this->userId = $account->id;
        $this->userType = $user->userType;
        $this->name = $account->name;
        $this->username = $account->username;
        $this->credit = $user->getCredit();
        $this->registerDate = TimeUtil::timeToDate($account->record_time);
        $this->loginTime = TimeUtil::getMonthDateHourMinute($account->login_time);
        $this->loginIp = $account->login_ip;
        $this->active = $user->isActive();
        $this->isLogin = $account->session_dead_time > TimeUtil::now();
        $this->initCredit($user);
        $this->initUp($user);

        $user->userType === 1 && $this->getMemberMessage($user);

        if ($initAll) {
            $upUser = $user->getUpLineUserModel();
            if ($upUser === null && ($user->isHolder() || $user->isVendor() || $user->isAgent() || $user->isMember())) {
                throw new SystemException("上线不存在");
            }
            $this->upName = $upUser !== null ? $upUser->getAccount()->name : "";
            $this->initFraction($user, $upUser);
        }
    }

    /**
     * 会员独立信息
     * @param $user
     */
    private function getMemberMessage(UserModel $user){
        $this->vipLevel = $user->getVipLevel();
        $this->targetVipLevel = $user->checkVipLevelUp();
        $this->targetVipLevelLabel = C::getVipLabels($user->checkVipLevelUp());
        $this->vipLevelLabel = C::getVipLabels($user->getVipLevel());
    }
    /**
     * 设置信用额度
     * @param UserModel $user
     */
    private function initCredit(UserModel $user) {
        if ($user->isSuperHolder() || $user->isHolder() || $user->isVendor() || $user->isAgent() || $user->isMember()) {
            $this->credit;
        //    $this->credit = $user->getCredit(); //getCredit()方法不存在
        }
    }

    /**
     * 设置上级数据
     * @param UserModel $user
     */
    private function initUp(UserModel $user) {
        if ($user->isHolder() || $user->isVendor() || $user->isAgent() || $user->isMember()) {
            $this->upId = $user->getUpLineId();
            $this->upType = $user->getUpLineType();
            $this->upTypeLabel = C::getUserTypeLabel($this->upType);
        }
    }

    /**
     * 设置占成数据
     * @param UserModel $user 本用户
     * @param UserModel|null $upUser 上线用户
     */
    private function initFraction(UserModel $user, $upUser) {
        if ($user->isHolder()) {
            $holder = $user->getHolder();
            $this->fractionMax = $holder->fraction_max;
            $this->fractionUp = $holder->fraction_super_holder;
        } else if ($user->isVendor()) {
            $vendor = $user->getVendor();
            $this->fractionMax = $vendor->fraction_max;
            $this->fractionUp = $vendor->fraction_holder;
        } else if ($user->isAgent()) {
            $agent = $user->getAgent();
            $this->fractionMax = $agent->fration_max;
            $this->fractionUp = $agent->fraction_vendor;
        } else if ($user->isMember()) {
            if ($upUser->isAgent()) {
                $agent = $upUser->getAgent();
                $this->fractionMax = $agent->fration_max;
                $this->fractionUp = $agent->fraction_vendor;
            } else if ($upUser->isVendor()) {
                $vendor = $upUser->getVendor();
                $this->fractionMax = $vendor->fraction_max;
                $this->fractionUp = $vendor->fraction_holder;
            } else if ($upUser->isHolder()) {
                $holder = $upUser->getHolder();
                $this->fractionMax = $holder->fraction_max;
                $this->fractionUp = $holder->fraction_super_holder;
            }
        }
    }
}
