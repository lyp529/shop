<?php


namespace common\models\vo\user;


use common\constants\C;
use common\models\UserModel;
use common\models\vo\backend\HomeVo;
use common\models\vo\BaseVo;

/**
 * Class UserInfoVo 后台所需的用户数据信息
 * @package common\models\vo
 */
class InfoVo extends BaseVo
{
    /**
     * @var int 用户id
     */
    public $id;
    /**
     * @var string 用户名字
     */
    public $name;
    /**
     * @var string 头像图片地址
     */
    public $avatar;
    /**
     * @var string[] 角色权限
     */
    public $roles;
    /**
     * @var int 用户类型
     */
    public $userType;
    /**
     * @var HomeVo 首页数据
     */
    public $homeVo;

    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->homeVo = new HomeVo();
    }

    /**
     * @param UserModel $userModel
     */
    public function initByUserModel(UserModel $userModel) {
        $account = $userModel->getAccount();
        $this->id = $account->id;
        $this->name = $account->name;
        $this->avatar = C::USER_DEFAULT_AVATAR_URL;
        $this->roles = $userModel->isAdmin() ? ["admin"] : ["agent"];
        $this->userType = $userModel->userType;
        $this->homeVo->loginTime = $account->login_time;
        $this->homeVo->loginIp = $account->login_ip;
        $this->homeVo->lastLoginTime = $account->last_login_time;
        $this->homeVo->lastLoginIp = $account->last_login_ip;
    }
}
