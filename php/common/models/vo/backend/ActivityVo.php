<?php


namespace common\models\vo\backend;


use common\models\vo\BaseVo;
use common\utils\ValueUtil;

/**
 * Class ActivityVo
 * @package common\models\vo\backend
 */
class ActivityVo extends BaseVo {
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $key;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $value;
    /**
     * @var string
     */
    public $nowIntegral;
    /**
     * @var int
     */
    public $condition;

    public function initByActivity($arr,$nowIntegral) {
        $this->id = $arr->id;
        $this->key = $arr->key;
        $this->value = $arr->value;
        $this->title = $arr->title;
        $this->nowIntegral = $nowIntegral;
        $this->condition = $arr->condition;
    }
}
