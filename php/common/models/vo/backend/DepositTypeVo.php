<?php


namespace common\models\vo\backend;


use common\models\vo\BaseVo;
use common\utils\ValueUtil;

/**
 * Class PayTypeSwitchVo 订单存款类型（订单子类型）型开关
 * @package common\models\vo\backend
 */
class DepositTypeVo extends BaseVo {
    /**
     * @var int 支付方式
     */
    public $subType;
    /**
     * @var string 支付方式标签
     */
    public $label;
    /**
     * @var bool 是否激活
     */
    public $active = true;

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->subType = (int)$this->subType;
        $this->active = ValueUtil::toBool($this->active);
    }
}