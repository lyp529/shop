<?php


namespace common\models\vo\backend;


use common\models\vo\BaseVo;

/**
 * Class HomeVo 后台首页的数据封装
 * @package common\models\vo\backend
 */
class HomeVo extends BaseVo {
    /**
     * @var string 本次登录时间
     */
    public $loginTime;
    /**
     * @var string 本次登录IP
     */
    public $loginIp;
    /**
     * @var string 上次登录时间
     */
    public $lastLoginTime;
    /**
     * @var string 上次登录IP
     */
    public $lastLoginIp;
}