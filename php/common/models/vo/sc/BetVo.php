<?php


namespace common\models\vo\sc;


use common\constants\P;
use common\models\vo\BaseVo;
use common\utils\ArrayUtil;

/**
 * Class BetVo 客户前端投一个注单的数据封装
 * @package common\models\vo\sc
 */
class BetVo extends BaseVo {
    /**
     * @var string 下注类型（玩法类型）
     */
    public $betType;
    /**
     * @var array 用户下注结果
     */
    public $content;
    /**
     * @var int 单位
     */
    public $unit;
    /**
     * @var int 倍数
     */
    public $multiple;
    /**
     * @var int 一共多少注
     */
    public $groupNum = 0;

    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [["betType", "unit", "multiple"], "required"],
            [["content"], "required", "message" => "请选择号码"],
            [["unit", "multiple"], "number"],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            "betType" => "玩法",
            "content" => "投注号码",
            "unit" => "单位",
            "multiple" => "倍数",
            "groupNum" => "注数",
        ];
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        if (empty($this->content)) {
            $this->content = [];
        }
        $this->unit = (int)$this->unit;
        $this->multiple = (int)$this->multiple;
        $this->groupNum = (int)$this->groupNum;

        // 自动填充空数组（因为前端无法提交空数组，所以在这里自动补全）
        ArrayUtil::valuesToInt($this->content);
        $labels = P::getScBetTypeLabelMap($this->betType);
        $len = count($labels);
        for ($i = 0; $i < $len; $i++) {
            if (!isset($this->content[$i])) {
                $this->content[$i] = [];
            }
        }
        ksort($this->content);
        $this->content = array_values($this->content);
    }
}