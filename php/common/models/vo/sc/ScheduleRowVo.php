<?php


namespace common\models\vo\sc;


use common\constants\C;
use common\models\db\Schedule;
use common\models\vo\BaseVo;

/**
 * Class ScheduleRowVo 私彩期数vo
 * @package common\models\vo\sc
 */
class ScheduleRowVo extends BaseVo
{
    /**
     * @var string 彩种
     */
    public $kindName;

    /**
     * @var int 期数id
     */
    public $scheduleId;
    /**
     * @var string 期数
     */
    public $period;
    /**
     * @var string 状态标签
     */
    public $statusLabel;
    /**
     * @var string 开奖时间
     */
    public $openTime;
    /**
     * @var string 开奖结果
     */
    public $result;
    /**
     * @var string 预设开奖结果
     */
    public $predict;
    /**
     * @var bool 是否可修改
     */
    public $canMod = false;
    /**
     * @var string 派彩时间
     */
    public $payoutTime;


    /**
     * @param Schedule $schedule
     */
    public function initBySchedule(Schedule $schedule) {
        $this->kindName = $schedule->kind_name;
        $this->scheduleId = $schedule->id;
        $this->period = $schedule->period;
        $this->statusLabel = C::getScheduleStatusLabel($schedule->status);
        $this->openTime = $schedule->open_time;
        $this->result = $schedule->getResultNumsStr();
        $this->canMod = $schedule->status === C::SCHEDULE_STATUS_WAIT;
        $this->predict = $schedule->getPredictNumsStr();
        $this->payoutTime = $schedule->payout_time;
    }
}
