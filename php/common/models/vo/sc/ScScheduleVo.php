<?php


namespace common\models\vo\sc;


use common\models\db\Schedule;
use common\models\vo\BaseVo;

/**
 * Class ScScheduleVo 时彩期数数据
 * @package common\models\vo\sc
 */
class ScScheduleVo extends BaseVo
{
    /**
     * @var string 开奖时间
     */
    public $openTime;
    /**
     * @var int 期数id
     */
    public $scheduleId;
    /**
     * @var string 期数
     */
    public $period;
    /**
     * @var int 剩余投注时间。当时间位0是，不允许再投注
     */
    public $remainBetSecond = 0;
    /**
     * @var int[] 开奖结果
     */
    public $resultNums = [];

    /**
     * @var string 开奖结果
     */
    public $resultStr = '';

    /**
     * @param Schedule $schedule
     */
    public function initBySchedule(Schedule $schedule) {
        $this->scheduleId = $schedule->id;
        $this->period = $schedule->period;
        $this->remainBetSecond = $schedule->getRemainBetSeconds();
        $this->resultNums = $schedule->getResultNums();
        $this->resultStr = $schedule->result_nums;
        $this->openTime = $schedule->open_time;
    }
}
