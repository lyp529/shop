<?php


namespace common\models\vo\sc;


use common\constants\C;
use common\constants\P;
use common\models\db\Bet;
use common\models\vo\BaseVo;
use common\utils\TimeUtil;

/**
 * Class BetRowVo 后端投注列表每行的数据封装
 * @package common\models\vo\sc
 */
class BetRowVo extends BaseVo
{
    /**
     * @var int 注单id
     */
    public $betId;
    /**
     * @var string 注单编号
     */
    public $betNo;
    /**
     * @var string 游戏名字
     */
    public $kindName;
    /**
     * @var int 用户ID
     */
    public $userId;
    /**
     * @var string 用户名称
     */
    public $username;
    /**
     * @var string 期号
     */
    public $period;
    /**
     * @var string 投注内容
     */
    public $betContent;
    /**
     * @var string 投注类型（玩法）
     */
    public $betType;
    /**
     * @var string 倍数模式
     */
    public $multipleUnitLabel;
    /**
     * @var double 总额（单位：元）
     */
    public $betAmount;
    /**
     * @var array 开奖号码
     */
    public $resultNums;
    /**
     * @var double 奖金(元)
     */
    public $betResult;
    /**
     * @var string 状态。未开奖、未中奖、已中奖
     */
    public $status;
    /**
     * @var string 投注时间
     */
    public $betTime;
    /**
     * @var int 注数
     */
    public $groupNum;
    /**
     * 下注余额
     * @var double 余额
     */
    public $betBalance;

    /**
     * 当前信用额
     * @var string 信用额
     */
    public $credit;

    public $recordTime;

    /**
     * @param Bet $bet
     */
    public function initByBet(Bet $bet) {
        $schedule = $bet->getSchedule();

        $this->betId = $bet->id;
        $this->betNo = $bet->bet_no;
        $this->kindName = $schedule !== null ? $schedule->kind_name : "";
        $this->period = $schedule !== null ? $schedule->period : "";
        $this->betType = P::getScBetTypeLabel($bet->bet_type);
        $this->betContent = $this->transBetContent($bet->getBetContent());
        $this->status = C::BET_STATUS_LABEL_WAIT;
        if ($schedule !== null && $schedule->open_time < TimeUtil::now()) {
            $this->status = $bet->bet_result > 0 ? C::BET_STATUS_LABEL_WIN : C::BET_STATUS_LABEL_LOSE;
        }
        $this->betTime = $bet->record_time;
        $this->multipleUnitLabel = $bet->multiple . ' [' . C::getBetUnitLabel($bet->unit) . ']';
        $this->betAmount = $bet->bet_amount;
        $this->resultNums = $schedule !== null ? $schedule->getResultNumsStr() : "";
        $this->betResult = $bet->bet_result;
        $this->groupNum = $bet->getGroupNum();
        $this->betBalance = $bet->bet_balance;
        $this->userId = $bet->member_id;
        $this->username = $bet->username;
        $this->recordTime = TimeUtil::getMonthDateHourMinuteSecs($bet->record_time);
    }

    /**
     * 转换投注内容
     * @param array $betContent 如：[[1], [2], [1,2]]
     * @return string 如：1,2,12
     */
    private function transBetContent($betContent) {
        $groups = [];
        foreach ($betContent as $item) {
            if (empty($item)) {
                $groups[] = '-';
            } else {
                $groups[] = implode("", $item);
            }
        }
        return implode(",", $groups);
    }
}
