<?php


namespace common\models\vo;


use common\exceptions\SystemException;
use common\models\db\UploadFile;

/**
 * Class FileVo 文件vo
 * @package common\models\vo
 */
class FileVo extends BaseVo {
    /**
     * @var int 文件id
     */
    public $id;
    /**
     * @var string 文件名字（上传时的名字）
     */
    public $filename;
    /**
     * @var string 访问路径
     */
    public $url;

    /**
     * @param UploadFile $file
     * @throws SystemException
     */
    public function initByUploadFile(UploadFile $file) {
        $this->id = $file->id;
        $this->filename = $file->upload_name;
        $this->url = $file->getWebUrl();
    }
}