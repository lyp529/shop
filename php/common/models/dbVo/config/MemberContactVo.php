<?php


namespace common\models\dbVo\config;


use common\models\vo\BaseVo;

/**
 * Class MemberContactVo 会员联系方式
 * @package common\models\dbVo\config
 */
class MemberContactVo extends BaseVo {
    /**
     * @var string[] 联系QQ号
     */
    public $qqs = [];

    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [["qqs"], "required"],
        ]);
    }

    public function attributeLabels() {
        return [
            "qqs" => "联系QQ号码",
        ];
    }
}