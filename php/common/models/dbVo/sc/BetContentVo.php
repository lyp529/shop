<?php


namespace common\models\dbVo\sc;


use common\models\vo\BaseVo;

/**
 * Class BetResultVo 购买结构
 * @package common\models\dbVo\sc
 */
class BetContentVo extends BaseVo {
    /**
     * @var int[] 万位选择
     */
    public $wan = [];
    /**
     * @var int[] 千位选择
     */
    public $qian = [];
    /**
     * @var int[] 百位选择
     */
    public $bai = [];
    /**
     * @var int[] 十位选择
     */
    public $shi = [];
    /**
     * @var [] 个位选择
     */
    public $ge = [];


}
