<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_holder".
 *
 * @property int $user_id
 * @property string $username 用户名-重复数据用于查看
 * @property string $name 呢称-重复数据用于查看
 * @property int $active
 * @property int $is_delete
 * @property string $credit
 * @property int $super_holder_id
 * @property double $fraction_max 股东占成上限
 * @property double $fraction_super_holder 大股东占成
 */
class TableUserHolder extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_holder';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'name', 'credit', 'super_holder_id', 'fraction_max', 'fraction_super_holder'], 'required'],
            [['user_id', 'active', 'is_delete', 'super_holder_id'], 'integer'],
            [['credit', 'fraction_max', 'fraction_super_holder'], 'number'],
            [['username', 'name'], 'string', 'max' => 100],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => '用户名-重复数据用于查看',
            'name' => '呢称-重复数据用于查看',
            'active' => 'Active',
            'is_delete' => 'Is Delete',
            'credit' => 'Credit',
            'super_holder_id' => 'Super Holder ID',
            'fraction_max' => '股东占成上限',
            'fraction_super_holder' => '大股东占成',
        ];
    }
}
