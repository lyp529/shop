<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_cash_rewards".
 *
 * @property int $id
 * @property string $title 标题
 * @property string $key 类型
 * @property string $value
 * @property int $condition 条件
 */
class TableTabCashRewards extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_cash_rewards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'key', 'value'], 'required'],
            [['value'], 'number'],
            [['condition'], 'integer'],
            [['title', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'key' => '类型',
            'value' => 'Value',
            'condition' => '条件',
        ];
    }
}
