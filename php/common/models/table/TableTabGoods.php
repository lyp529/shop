<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_goods".
 *
 * @property int $id
 * @property string $credits 兑换积分
 * @property string $bonus_points 奖励分
 * @property string $name 商品名称
 * @property string $content 描述
 * @property string $images 图片
 * @property string $record_date 添加时间
 * @property string $update_date 更新时间
 */
class TableTabGoods extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['credits', 'bonus_points'], 'number'],
            [['name', 'content', 'record_date'], 'required'],
            [['content'], 'string'],
            [['record_date', 'update_date'], 'safe'],
            [['name', 'images'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'credits' => '兑换积分',
            'bonus_points' => '奖励分',
            'name' => '商品名称',
            'content' => '描述',
            'images' => '图片',
            'record_date' => '添加时间',
            'update_date' => '更新时间',
        ];
    }
}
