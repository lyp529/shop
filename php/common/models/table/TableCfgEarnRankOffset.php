<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_earn_rank_offset".
 *
 * @property int $er_offset 保存每次查询tab_earnings_ranking的offset
 * @property string $record_time
 */
class TableCfgEarnRankOffset extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_earn_rank_offset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['er_offset'], 'required'],
            [['er_offset'], 'integer'],
            [['record_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'er_offset' => '保存每次查询tab_earnings_ranking的offset',
            'record_time' => 'Record Time',
        ];
    }
}
