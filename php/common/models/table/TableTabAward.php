<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_award".
 *
 * @property int $id
 * @property string $value 值
 */
class TableTabAward extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_award';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => '值',
        ];
    }
}
