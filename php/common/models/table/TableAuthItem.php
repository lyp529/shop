<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "auth_item".
 *
 * @property int $id
 * @property int $role_id 角色id
 * @property string $item_name 权限项目名字
 * @property string $record_time 创建时间
 */
class TableAuthItem extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_id', 'item_name', 'record_time'], 'required'],
            [['role_id'], 'integer'],
            [['record_time'], 'safe'],
            [['item_name'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => '角色id',
            'item_name' => '权限项目名字',
            'record_time' => '创建时间',
        ];
    }
}
