<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "log_general".
 *
 * @property int $id
 * @property int $log_type 日志类型
 * @property string $log_label 日志类型名称
 * @property string $params 参数
 * @property string $remark 备注
 * @property string $record_time 记录时间
 */
class TableLogGeneral extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_general';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['log_type', 'log_label', 'params', 'remark', 'record_time'], 'required'],
            [['log_type'], 'integer'],
            [['record_time'], 'safe'],
            [['log_label'], 'string', 'max' => 200],
            [['params', 'remark'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_type' => '日志类型',
            'log_label' => '日志类型名称',
            'params' => '参数',
            'remark' => '备注',
            'record_time' => '记录时间',
        ];
    }
}
