<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_schedule".
 *
 * @property int $id
 * @property int $kind 彩票类型
 * @property string $kind_name 类型名称-重复数据方便查看
 * @property string $open_time 开赛时间
 * @property string $period 期数
 * @property int $status 0未开奖 1已开奖
 * @property int $active 是否开启
 * @property string $record_time 新增时间
 * @property string $update_time 更新时间
 * @property string $result_nums 中奖号码1@2@3
 * @property string $predict_nums 预设结果
 * @property string $payout_time 派彩时间
 */
class TableTabSchedule extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kind', 'open_time', 'active', 'record_time'], 'required'],
            [['kind', 'status', 'active'], 'integer'],
            [['open_time', 'record_time', 'update_time', 'payout_time'], 'safe'],
            [['kind_name', 'predict_nums'], 'string', 'max' => 100],
            [['period'], 'string', 'max' => 20],
            [['result_nums'], 'string', 'max' => 10],
            [['kind', 'period'], 'unique', 'targetAttribute' => ['kind', 'period']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kind' => '彩票类型',
            'kind_name' => '类型名称-重复数据方便查看',
            'open_time' => '开赛时间',
            'period' => '期数',
            'status' => '0未开奖 1已开奖',
            'active' => '是否开启',
            'record_time' => '新增时间',
            'update_time' => '更新时间',
            'result_nums' => '中奖号码1@2@3',
            'predict_nums' => '预设结果',
            'payout_time' => '派彩时间',
        ];
    }
}
