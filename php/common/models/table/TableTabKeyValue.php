<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_key_value".
 *
 * @property string $key 存储数据的key
 * @property string $value 存储内容
 */
class TableTabKeyValue extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_key_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'key' => '存储数据的key',
            'value' => '存储内容',
        ];
    }
}
