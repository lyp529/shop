<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_sign".
 *
 * @property int $id
 * @property string $user_id 用户id
 * @property string $sign_datetime 签到时间
 * @property string $record_time 创建时间
 */
class TableTabSign extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_sign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'sign_datetime', 'record_time'], 'required'],
            [['sign_datetime', 'record_time'], 'safe'],
            [['user_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => '用户id',
            'sign_datetime' => '签到时间',
            'record_time' => '创建时间',
        ];
    }
}
