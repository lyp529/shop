<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_bet".
 *
 * @property int $id 注单ID
 * @property string $bet_no 注单编号-uniqid
 * @property int $schedule_id 场次ID
 * @property string $username 用户名
 * @property int $member_id 会员ID
 * @property int $agent_id
 * @property int $vendor_id
 * @property int $holder_id
 * @property int $super_holder_id
 * @property string $bet_type 下注类型
 * @property string $bet_type_name 下注类型名称
 * @property string $bet_content 下注内容
 * @property int $multiple 倍数
 * @property int $unit 单位 1元 2角
 * @property int $group_num 分组的数量
 * @property string $bet_amount 用户下注金额
 * @property string $bet_result 派彩结果
 * @property string $bet_date 下注日期
 * @property string $bet_balance 投注后的余额
 * @property string $update_time 数据更新时间
 * @property string $record_time 数据创建时间
 */
class TableTabBet extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_bet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bet_no', 'schedule_id', 'username', 'member_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'bet_type', 'bet_type_name', 'bet_content', 'multiple', 'unit', 'group_num', 'bet_result', 'bet_balance'], 'required'],
            [['schedule_id', 'member_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'multiple', 'unit', 'group_num'], 'integer'],
            [['bet_amount', 'bet_result', 'bet_balance'], 'number'],
            [['bet_date', 'update_time', 'record_time'], 'safe'],
            [['bet_no', 'username', 'bet_type'], 'string', 'max' => 100],
            [['bet_type_name'], 'string', 'max' => 200],
            [['bet_content'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '注单ID',
            'bet_no' => '注单编号-uniqid',
            'schedule_id' => '场次ID',
            'username' => '用户名',
            'member_id' => '会员ID',
            'agent_id' => 'Agent ID',
            'vendor_id' => 'Vendor ID',
            'holder_id' => 'Holder ID',
            'super_holder_id' => 'Super Holder ID',
            'bet_type' => '下注类型',
            'bet_type_name' => '下注类型名称',
            'bet_content' => '下注内容',
            'multiple' => '倍数',
            'unit' => '单位 1元 2角',
            'group_num' => '分组的数量',
            'bet_amount' => '用户下注金额',
            'bet_result' => '派彩结果',
            'bet_date' => '下注日期',
            'bet_balance' => '投注后的余额',
            'update_time' => '数据更新时间',
            'record_time' => '数据创建时间',
        ];
    }
}
