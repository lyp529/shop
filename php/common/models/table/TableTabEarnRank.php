<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_earn_rank".
 *
 * @property int $id
 * @property string $username 中奖用户名
 * @property int $er_type 中奖玩法
 * @property string $amount 中奖金额
 */
class TableTabEarnRank extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_earn_rank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'er_type'], 'required'],
            [['er_type'], 'integer'],
            [['amount'], 'number'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => '中奖用户名',
            'er_type' => '中奖玩法',
            'amount' => '中奖金额',
        ];
    }
}
