<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_vip".
 *
 * @property int $level VIP等级
 * @property string $name VIP名称
 * @property int $amount 金额
 * @property string $amount_desc 金额描述
 * @property int $withdraw_times 每日可提现次数
 * @property int $single_limit 单笔限额
 * @property int $back_percent 每天下单金额返回百分比
 */
class TableCfgVip extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_vip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level', 'name', 'amount', 'amount_desc', 'withdraw_times', 'single_limit', 'back_percent'], 'required'],
            [['level', 'amount', 'withdraw_times', 'single_limit', 'back_percent'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['amount_desc'], 'string', 'max' => 100],
            [['level'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'level' => 'VIP等级',
            'name' => 'VIP名称',
            'amount' => '金额',
            'amount_desc' => '金额描述',
            'withdraw_times' => '每日可提现次数',
            'single_limit' => '单笔限额',
            'back_percent' => '每天下单金额返回百分比',
        ];
    }
}
