<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_order".
 *
 * @property int $id 订单ID
 * @property int $user_id 用户ID
 * @property int $admin_id 操作人员ID
 * @property int $goods_id 商品ID
 * @property string $order_number 订单编号
 * @property string $express_no
 * @property int $order_state 1:待发货,2:已发货
 * @property string $order_time 下单时间
 * @property string $integral 订单所需积分
 * @property string $record_date 添加时间
 * @property string $update_date 更新时间
 * @property string $goods_name 商品名
 */
class TableTabOrder extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'goods_id', 'order_number', 'order_time', 'record_date', 'goods_name'], 'required'],
            [['user_id', 'admin_id', 'goods_id', 'order_state'], 'integer'],
            [['order_time', 'record_date', 'update_date'], 'safe'],
            [['integral'], 'number'],
            [['order_number', 'express_no'], 'string', 'max' => 33],
            [['goods_name'], 'string', 'max' => 255],
            [['order_number'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '订单ID',
            'user_id' => '用户ID',
            'admin_id' => '操作人员ID',
            'goods_id' => '商品ID',
            'order_number' => '订单编号',
            'express_no' => 'Express No',
            'order_state' => '1:待发货,2:已发货',
            'order_time' => '下单时间',
            'integral' => '订单所需积分',
            'record_date' => '添加时间',
            'update_date' => '更新时间',
            'goods_name' => '商品名',
        ];
    }
}
