<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_cron".
 *
 * @property int $id
 * @property string $name 定时任务名称
 * @property string $route 定时任务路由
 * @property string $cron_time 运行时间。crontab格式
 * @property int $status 任务状态。1运行中 2运行结束
 * @property string $last_run_time 上次运行时间
 * @property string $next_run_time 下次运行时间
 * @property int $active 启用状态。1启用 0关闭
 * @property string $update_time 任务配置更新时间
 * @property string $record_time 任务配置创建时间
 */
class TableTabCron extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_cron';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'route', 'cron_time', 'status', 'last_run_time', 'next_run_time', 'update_time', 'record_time'], 'required'],
            [['status', 'active'], 'integer'],
            [['last_run_time', 'next_run_time', 'update_time', 'record_time'], 'safe'],
            [['name', 'route', 'cron_time'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '定时任务名称',
            'route' => '定时任务路由',
            'cron_time' => '运行时间。crontab格式',
            'status' => '任务状态。1运行中 2运行结束',
            'last_run_time' => '上次运行时间',
            'next_run_time' => '下次运行时间',
            'active' => '启用状态。1启用 0关闭',
            'update_time' => '任务配置更新时间',
            'record_time' => '任务配置创建时间',
        ];
    }
}
