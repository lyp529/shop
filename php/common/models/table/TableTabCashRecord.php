<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_cash_record".
 *
 * @property int $id
 * @property int $member_id
 * @property int $agent_id
 * @property int $vendor_id
 * @property int $holder_id
 * @property int $super_holder_id
 * @property string $username 用户名
 * @property string $record_no 单号-不同功能代表不同单呈
 * @property int $clog_type 帐变类型
 * @property string $clog_type_label 帐户日志类型名称
 * @property string $params 抽象类型
 * @property string $amount
 * @property string $balance
 * @property string $remark 备注(部分备注由类型,注单编号,内容自动生成)
 * @property string $update_time 更新时间
 * @property string $record_time 记录时间
 */
class TableTabCashRecord extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_cash_record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'username', 'clog_type', 'clog_type_label', 'params', 'amount', 'balance', 'record_time'], 'required'],
            [['member_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'clog_type'], 'integer'],
            [['amount', 'balance'], 'number'],
            [['update_time', 'record_time'], 'safe'],
            [['username', 'record_no'], 'string', 'max' => 100],
            [['clog_type_label', 'params'], 'string', 'max' => 200],
            [['remark'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'agent_id' => 'Agent ID',
            'vendor_id' => 'Vendor ID',
            'holder_id' => 'Holder ID',
            'super_holder_id' => 'Super Holder ID',
            'username' => '用户名',
            'record_no' => '单号-不同功能代表不同单呈',
            'clog_type' => '帐变类型',
            'clog_type_label' => '帐户日志类型名称',
            'params' => '抽象类型',
            'amount' => 'Amount',
            'balance' => 'Balance',
            'remark' => '备注(部分备注由类型,注单编号,内容自动生成)',
            'update_time' => '更新时间',
            'record_time' => '记录时间',
        ];
    }
}
