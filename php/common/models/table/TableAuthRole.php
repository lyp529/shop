<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "auth_role".
 *
 * @property int $id
 * @property int $type 角色类型。1用户 2分组
 * @property int $bind_id 绑定的数据表id。如用户id、分组id
 * @property string $record_time 创建时间
 */
class TableAuthRole extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'bind_id'], 'integer'],
            [['bind_id', 'record_time'], 'required'],
            [['record_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => '角色类型。1用户 2分组',
            'bind_id' => '绑定的数据表id。如用户id、分组id',
            'record_time' => '创建时间',
        ];
    }
}
