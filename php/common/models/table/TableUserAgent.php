<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_agent".
 *
 * @property int $user_id
 * @property int $vendor_id
 * @property int $holder_id
 * @property int $super_holder_id
 * @property string $username 用户名-重复数据用于查看
 * @property string $name 呢称-重复数据用于查看
 * @property int $active
 * @property int $is_delete
 * @property string $credit
 * @property string $fraction_vendor
 * @property string $fration_max
 * @property string $invite_code 邀请码
 */
class TableUserAgent extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'vendor_id', 'holder_id', 'super_holder_id', 'username', 'name', 'credit', 'fraction_vendor', 'fration_max'], 'required'],
            [['user_id', 'vendor_id', 'holder_id', 'super_holder_id', 'active', 'is_delete'], 'integer'],
            [['credit', 'fraction_vendor', 'fration_max'], 'number'],
            [['username', 'name'], 'string', 'max' => 100],
            [['invite_code'], 'string', 'max' => 50],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'vendor_id' => 'Vendor ID',
            'holder_id' => 'Holder ID',
            'super_holder_id' => 'Super Holder ID',
            'username' => '用户名-重复数据用于查看',
            'name' => '呢称-重复数据用于查看',
            'active' => 'Active',
            'is_delete' => 'Is Delete',
            'credit' => 'Credit',
            'fraction_vendor' => 'Fraction Vendor',
            'fration_max' => 'Fration Max',
            'invite_code' => '邀请码',
        ];
    }
}
