<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_vendor".
 *
 * @property int $user_id
 * @property int $holder_id
 * @property int $super_holder_id
 * @property string $username 用户名-重复数据用于查看
 * @property string $name 呢称-重复数据用于查看
 * @property int $active
 * @property int $is_delete
 * @property string $credit
 * @property string $fraction_holder 股东占成
 * @property double $fraction_max 总代理占成上限
 */
class TableUserVendor extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_vendor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'holder_id', 'super_holder_id', 'username', 'name', 'credit', 'fraction_holder', 'fraction_max'], 'required'],
            [['user_id', 'holder_id', 'super_holder_id', 'active', 'is_delete'], 'integer'],
            [['credit', 'fraction_holder', 'fraction_max'], 'number'],
            [['username', 'name'], 'string', 'max' => 100],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'holder_id' => 'Holder ID',
            'super_holder_id' => 'Super Holder ID',
            'username' => '用户名-重复数据用于查看',
            'name' => '呢称-重复数据用于查看',
            'active' => 'Active',
            'is_delete' => 'Is Delete',
            'credit' => 'Credit',
            'fraction_holder' => '股东占成',
            'fraction_max' => '总代理占成上限',
        ];
    }
}
