<?php


namespace common\models\table;


use common\utils\TimeUtil;
use yii\db\ActiveRecord;

/**
 * Class BaseDB 所属数据库模型的基础对象
 * @package common\models\db
 */
class BaseTable extends ActiveRecord
{

    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $propRecordTime = "record_time";
        if ($this->hasProperty($propRecordTime)) {
            $this->$propRecordTime = TimeUtil::now();
        }
    }

    /**
     * 验证前处理数据
     * @return bool
     */
    public function beforeValidate() {
        $propUpdateTime = "update_time";
        if ($this->hasProperty($propUpdateTime) && empty($this->$propUpdateTime)) {
            $this->$propUpdateTime = TimeUtil::now();
        }
        return parent::beforeValidate();
    }

    /**
     * 保存数据前
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert) {
        $propUpdateTime = "update_time";
        if ($this->hasProperty($propUpdateTime)) {
            $this->$propUpdateTime = TimeUtil::now();
        }
        return parent::beforeSave($insert);
    }

    /**
     * 获取第一个错误
     * @param string $attribute
     * @return string
     */
    public function getFirstError($attribute = "") {
        if (empty($attribute)) {
            $errors = $this->getErrors();
            if (count($errors) > 0) {
                $error = current($errors);
                if (is_array($error)) {
                    return current($error);
                } else {
                    return $error;
                }
            }
            return "";
        }
        return parent::getFirstError($attribute);
    }

    /**
     * 获取模型的map
     * @param null $ids 指定ID列表,空为所有
     * @param bool $isArray 是否模型转为数组
     * @return static[]
     */
    public static function getModelMap($ids = null, $isArray = false) {
        $map = [];
        $rows = $ids ? static::findAll($ids) : static::find()->all();
        foreach ($rows as $row) {
            $map[$row->primaryKey] = $isArray ? $row->toArray() : $row;
        }
        return $map;
    }

    /**
     * 获取模型的map（搜索条件和 findAll 相同）
     * @see ActiveRecord::findAll()
     * @param $condition
     * @return static[]
     */
    public static function findModelMap($condition) {
        $models = static::findAll($condition);
        $modelMap = [];
        foreach ($models as $model) {
            $modelMap[$model->primaryKey] = $model;
        }
        return $modelMap;
    }

}
