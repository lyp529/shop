<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_grouping".
 *
 * @property int $user_id
 * @property string $a_array A组
 * @property string $b_array B组
 * @property string $self_array 自己所在组
 */
class TableTabGrouping extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_grouping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'a_array', 'b_array', 'self_array'], 'required'],
            [['user_id'], 'integer'],
            [['a_array', 'b_array', 'self_array'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'a_array' => 'A组',
            'b_array' => 'B组',
            'self_array' => '自己所在组',
        ];
    }
}
