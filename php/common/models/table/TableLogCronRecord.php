<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "log_cron_record".
 *
 * @property int $id
 * @property int $cron_id 任务id
 * @property int $reason 运行结果。1成功 -1出错
 * @property string $use_second 用时秒数
 * @property int $record_time 记录时间
 */
class TableLogCronRecord extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_cron_record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cron_id', 'reason', 'record_time'], 'required'],
            [['cron_id', 'reason', 'record_time'], 'integer'],
            [['use_second'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cron_id' => '任务id',
            'reason' => '运行结果。1成功 -1出错',
            'use_second' => '用时秒数',
            'record_time' => '记录时间',
        ];
    }
}
