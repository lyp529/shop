<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_luck_run".
 *
 * @property int $id
 * @property int $reward_type
 * @property string $info
 * @property int $rate
 * @property int $reward_value
 */
class TableCfgLuckRun extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_luck_run';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reward_type', 'info', 'rate'], 'required'],
            [['reward_type', 'rate', 'reward_value'], 'integer'],
            [['info'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reward_type' => 'Reward Type',
            'info' => 'Info',
            'rate' => 'Rate',
            'reward_value' => 'Reward Value',
        ];
    }
}
