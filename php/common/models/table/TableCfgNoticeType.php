<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_notice_type".
 *
 * @property int $id
 * @property string $type_name 类名
 */
class TableCfgNoticeType extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_notice_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_name' => '类名',
        ];
    }
}
