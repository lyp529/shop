<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_admin".
 *
 * @property int $user_id
 * @property string $username 重复数据-便于查看
 * @property string $name 重复数据-便于查看
 * @property int $is_admin 是否超级管理员
 */
class TableUserAdmin extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'name', 'is_admin'], 'required'],
            [['user_id', 'is_admin'], 'integer'],
            [['username', 'name'], 'string', 'max' => 100],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => '重复数据-便于查看',
            'name' => '重复数据-便于查看',
            'is_admin' => '是否超级管理员',
        ];
    }
}
