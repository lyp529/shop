<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_member".
 *
 * @property int $user_id
 * @property int $is_direct 是否直属会员
 * @property int $direct_user_type 直属上线类型
 * @property int $direct_user_id 直属上线的ID
 * @property int $super_holder_id
 * @property int $holder_id
 * @property int $vendor_id
 * @property int $agent_id
 * @property string $username 用户名-重复数据用于查看
 * @property string $name 呢称-重复数据用于查看
 * @property int $vip_level VIP等级
 * @property int $active
 * @property int $is_delete
 * @property string $credit
 * @property string $last_back_date 最近返还下单金额日
 * @property string $integral
 * @property string $last_sign_date 签到时间
 * @property string $last_withdraw_date 最近提款时间
 * @property int $last_withdraw_times 最近提款次数
 * @property int $bank_id
 * @property string $bank_name
 * @property string $bank_account
 * @property string $bank_username
 * @property string $bank_email
 * @property int $before_vip_level 昨日vip等级
 * @property int $total_pay 累计充值
 * @property string $total_bet 累计下注(消费)
 * @property string $before_credit 昨日账户余额
 * @property string $before_date 上次记录之前信息的时间
 * @property string $place_order_rebated 昨天下单返利
 */
class TableUserMember extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_direct', 'direct_user_type', 'direct_user_id', 'super_holder_id', 'holder_id', 'vendor_id', 'agent_id', 'username', 'name', 'credit'], 'required'],
            [['user_id', 'is_direct', 'direct_user_type', 'direct_user_id', 'super_holder_id', 'holder_id', 'vendor_id', 'agent_id', 'vip_level', 'active', 'is_delete', 'last_withdraw_times', 'bank_id', 'before_vip_level', 'total_pay'], 'integer'],
            [['credit', 'integral', 'total_bet', 'before_credit', 'place_order_rebated'], 'number'],
            [['last_back_date', 'last_sign_date', 'last_withdraw_date', 'before_date'], 'safe'],
            [['username', 'name', 'bank_email'], 'string', 'max' => 100],
            [['bank_name', 'bank_account', 'bank_username'], 'string', 'max' => 50],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'is_direct' => '是否直属会员',
            'direct_user_type' => '直属上线类型',
            'direct_user_id' => '直属上线的ID',
            'super_holder_id' => 'Super Holder ID',
            'holder_id' => 'Holder ID',
            'vendor_id' => 'Vendor ID',
            'agent_id' => 'Agent ID',
            'username' => '用户名-重复数据用于查看',
            'name' => '呢称-重复数据用于查看',
            'vip_level' => 'VIP等级',
            'active' => 'Active',
            'is_delete' => 'Is Delete',
            'credit' => 'Credit',
            'last_back_date' => '最近返还下单金额日',
            'integral' => 'Integral',
            'last_sign_date' => '签到时间',
            'last_withdraw_date' => '最近提款时间',
            'last_withdraw_times' => '最近提款次数',
            'bank_id' => 'Bank ID',
            'bank_name' => 'Bank Name',
            'bank_account' => 'Bank Account',
            'bank_username' => 'Bank Username',
            'bank_email' => 'Bank Email',
            'before_vip_level' => '昨日vip等级',
            'total_pay' => '累计充值',
            'total_bet' => '累计下注(消费)',
            'before_credit' => '昨日账户余额',
            'before_date' => '上次记录之前信息的时间',
            'place_order_rebated' => '昨天下单返利',
        ];
    }
}
