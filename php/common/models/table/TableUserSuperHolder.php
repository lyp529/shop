<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_super_holder".
 *
 * @property int $user_id
 * @property string $username 用户名-重复数据用于查看
 * @property string $name 呢称-重复数据用于查看
 * @property int $active
 * @property int $is_delete
 * @property string $credit
 * @property int $commission_plan
 * @property int $company_report 是否可查看公司报表
 * @property int $include_below 是否包底
 */
class TableUserSuperHolder extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_super_holder';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'name', 'credit', 'commission_plan', 'company_report', 'include_below'], 'required'],
            [['user_id', 'active', 'is_delete', 'commission_plan', 'company_report', 'include_below'], 'integer'],
            [['credit'], 'number'],
            [['username', 'name'], 'string', 'max' => 100],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => '用户名-重复数据用于查看',
            'name' => '呢称-重复数据用于查看',
            'active' => 'Active',
            'is_delete' => 'Is Delete',
            'credit' => 'Credit',
            'commission_plan' => 'Commission Plan',
            'company_report' => '是否可查看公司报表',
            'include_below' => '是否包底',
        ];
    }
}
