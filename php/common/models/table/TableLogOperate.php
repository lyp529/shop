<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "log_operate".
 *
 * @property int $id
 * @property int $user_type 操作用户类型
 * @property int $user_id 操作用户
 * @property string $ip 用户IP
 * @property string $user_name 用户姓名(方便查看)
 * @property int $log_type 日志类型C.LOG*
 * @property string $log_label 日志的标注(方便查看,不用于程序)
 * @property string $params 自定义参数枚举|隔开
 * @property string $old_data model.toArray的数据
 * @property string $new_data model.load可以将数据载入
 * @property string $data_class 数据的类名(主要用于查看)
 * @property string $record_time 操作日期
 */
class TableLogOperate extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_operate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_type', 'user_id', 'ip', 'user_name', 'log_type', 'record_time'], 'required'],
            [['user_type', 'user_id', 'log_type'], 'integer'],
            [['params', 'old_data', 'new_data'], 'string'],
            [['record_time'], 'safe'],
            [['ip', 'user_name'], 'string', 'max' => 20],
            [['log_label', 'data_class'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_type' => '操作用户类型',
            'user_id' => '操作用户',
            'ip' => '用户IP',
            'user_name' => '用户姓名(方便查看)',
            'log_type' => '日志类型C.LOG*',
            'log_label' => '日志的标注(方便查看,不用于程序)',
            'params' => '自定义参数枚举|隔开',
            'old_data' => 'model.toArray的数据',
            'new_data' => 'model.load可以将数据载入',
            'data_class' => '数据的类名(主要用于查看)',
            'record_time' => '操作日期',
        ];
    }
}
