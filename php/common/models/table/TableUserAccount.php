<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_account".
 *
 * @property int $id
 * @property int $user_type 用户类型
 * @property string $user_type_name 用户类型名称
 * @property string $username
 * @property string $password
 * @property string $password2 资金密码
 * @property string $name
 * @property string $remark 备注
 * @property string $record_time
 * @property string $update_time
 * @property int $modify_user_type
 * @property int $modify_user_id
 * @property string $modify_username
 * @property string $modify_ip
 * @property string $session_id
 * @property string $session_dead_time session有效截止时间。用于判断会员是否登录
 * @property int $login_fail_times 登录失败次数
 * @property int $lock 是否锁定
 * @property string $login_time 最近登录时间
 * @property string $login_ip 最近登录ip
 * @property string $last_login_time 上次登录时间
 * @property string $last_login_ip 上次登录IP
 */
class TableUserAccount extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_type', 'user_type_name', 'username', 'password', 'name', 'record_time', 'modify_user_id', 'login_fail_times', 'lock'], 'required'],
            [['user_type', 'modify_user_type', 'modify_user_id', 'login_fail_times', 'lock'], 'integer'],
            [['record_time', 'update_time', 'session_dead_time', 'login_time', 'last_login_time'], 'safe'],
            [['user_type_name'], 'string', 'max' => 20],
            [['username', 'name', 'modify_username', 'session_id', 'login_ip', 'last_login_ip'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 200],
            [['password2'], 'string', 'max' => 500],
            [['remark'], 'string', 'max' => 1000],
            [['modify_ip'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_type' => '用户类型',
            'user_type_name' => '用户类型名称',
            'username' => 'Username',
            'password' => 'Password',
            'password2' => '资金密码',
            'name' => 'Name',
            'remark' => '备注',
            'record_time' => 'Record Time',
            'update_time' => 'Update Time',
            'modify_user_type' => 'Modify User Type',
            'modify_user_id' => 'Modify User ID',
            'modify_username' => 'Modify Username',
            'modify_ip' => 'Modify Ip',
            'session_id' => 'Session ID',
            'session_dead_time' => 'session有效截止时间。用于判断会员是否登录',
            'login_fail_times' => '登录失败次数',
            'lock' => '是否锁定',
            'login_time' => '最近登录时间',
            'login_ip' => '最近登录ip',
            'last_login_time' => '上次登录时间',
            'last_login_ip' => '上次登录IP',
        ];
    }
}
