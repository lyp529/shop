<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_notice".
 *
 * @property int $id
 * @property int $admin_id 操作人员
 * @property int $notice_type 类型
 * @property string $record_date 添加时间
 * @property string $update_date 更新时间
 * @property string $title 标题
 * @property string $content 内容
 */
class TableTabNotice extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_notice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_id', 'notice_type', 'title', 'content'], 'required'],
            [['admin_id', 'notice_type'], 'integer'],
            [['record_date', 'update_date'], 'safe'],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => '操作人员',
            'notice_type' => '类型',
            'record_date' => '添加时间',
            'update_date' => '更新时间',
            'title' => '标题',
            'content' => '内容',
        ];
    }
}
