<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "log_login".
 *
 * @property int $id
 * @property int $user_type 用户类型
 * @property int $user_id 登录人id
 * @property string $ip 登陆ip
 * @property int $login_type 1登入 2登出
 * @property string $record_time 登陆或登出时间
 * @property string $username 用户名
 */
class TableLogLogin extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_type', 'user_id', 'ip', 'login_type', 'record_time', 'username'], 'required'],
            [['user_type', 'user_id', 'login_type'], 'integer'],
            [['record_time'], 'safe'],
            [['ip'], 'string', 'max' => 20],
            [['username'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_type' => '用户类型',
            'user_id' => '登录人id',
            'ip' => '登陆ip',
            'login_type' => '1登入 2登出',
            'record_time' => '登陆或登出时间',
            'username' => '用户名',
        ];
    }
}
