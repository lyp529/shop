<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_order_wallet".
 *
 * @property int $id 订单ID
 * @property int $user_id 用户ID
 * @property int $agent_id
 * @property int $vendor_id
 * @property int $holder_id
 * @property int $super_holder_id
 * @property int $admin_id 操作人员ID
 * @property string $order_no 订单编号
 * @property int $order_type 1:存款,2:取款
 * @property int $order_sub_type 存取款方式
 * @property int $order_state 1:待处理,2:已完成,3:已取消,4:已失败
 * @property string $order_time 下单时间
 * @property string $finish_time 完成支付时间
 * @property string $amount 订单金额
 * @property int $pay_type 支付通道
 * @property string $pay_username 支付用户信息
 * @property string $bank_name 银行名字。如：中国工商银行
 * @property string $bank_account 银行卡编号
 * @property string $bank_username 开户姓名
 * @property string $remark 备注
 * @property string $update_time
 * @property string $record_time
 */
class TableTabOrderWallet extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_order_wallet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'order_no', 'order_sub_type', 'order_time', 'pay_type'], 'required'],
            [['user_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'admin_id', 'order_type', 'order_sub_type', 'order_state', 'pay_type'], 'integer'],
            [['order_time', 'finish_time', 'update_time', 'record_time'], 'safe'],
            [['amount'], 'number'],
            [['order_no'], 'string', 'max' => 33],
            [['pay_username'], 'string', 'max' => 200],
            [['bank_name'], 'string', 'max' => 50],
            [['bank_account', 'bank_username'], 'string', 'max' => 255],
            [['remark'], 'string', 'max' => 500],
            [['order_no'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '订单ID',
            'user_id' => '用户ID',
            'agent_id' => 'Agent ID',
            'vendor_id' => 'Vendor ID',
            'holder_id' => 'Holder ID',
            'super_holder_id' => 'Super Holder ID',
            'admin_id' => '操作人员ID',
            'order_no' => '订单编号',
            'order_type' => '1:存款,2:取款',
            'order_sub_type' => '存取款方式',
            'order_state' => '1:待处理,2:已完成,3:已取消,4:已失败',
            'order_time' => '下单时间',
            'finish_time' => '完成支付时间',
            'amount' => '订单金额',
            'pay_type' => '支付通道',
            'pay_username' => '支付用户信息',
            'bank_name' => '银行名字。如：中国工商银行',
            'bank_account' => '银行卡编号',
            'bank_username' => '开户姓名',
            'remark' => '备注',
            'update_time' => 'Update Time',
            'record_time' => 'Record Time',
        ];
    }
}
