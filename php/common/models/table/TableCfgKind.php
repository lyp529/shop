<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_kind".
 *
 * @property int $kind
 * @property string $kind_name
 * @property string $start_time 每天开始时间
 * @property string $end_time 每天结束时间
 * @property int $minute 每期多少分钟
 * @property int $periods 每天总期数
 * @property int $active 有效无效
 */
class TableCfgKind extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_kind';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kind', 'kind_name', 'start_time', 'end_time', 'minute', 'periods'], 'required'],
            [['kind', 'minute', 'periods', 'active'], 'integer'],
            [['start_time', 'end_time'], 'safe'],
            [['kind_name'], 'string', 'max' => 100],
            [['kind'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kind' => 'Kind',
            'kind_name' => 'Kind Name',
            'start_time' => '每天开始时间',
            'end_time' => '每天结束时间',
            'minute' => '每期多少分钟',
            'periods' => '每天总期数',
            'active' => '有效无效',
        ];
    }
}
