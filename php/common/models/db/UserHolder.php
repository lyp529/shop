<?php
namespace common\models\db;

use common\constants\C;
use common\models\table\TableUserHolder;

/**
 * 数据表 TableUserHolder 的方法扩展 
 */
class UserHolder extends TableUserHolder 
{
    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->active = C::TRUE;
        $this->is_delete = C::FALSE;
        $this->credit = 0;
        $this->fraction_max = 0;
        $this->fraction_super_holder = 0;
    }

    /**
     * @return UserVendor[]
     */
    public function getVendorList() {
        return UserVendor::findAll(["holder_id" => $this->user_id]);
    }

    /**
     * @return UserAgent[]
     */
    public function getAgentList() {
        return UserAgent::findAll(["holder_id" => $this->user_id]);
    }

    /**
     * @return UserMember[]
     */
    public function getMemberList() {
        return UserMember::findAll(["holder_id" => $this->user_id]);
    }
}