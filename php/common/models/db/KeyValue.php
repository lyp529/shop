<?php
namespace common\models\db;

use common\constants\C;
use common\exceptions\ForeseeableException;
use common\models\dbVo\config\MemberContactVo;
use common\models\table\TableTabKeyValue;
use common\utils\DLogger;
use Yii;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * 系统一个 key value 存储系统
 */
class KeyValue extends TableTabKeyValue 
{
    /**
     * 设置值
     * @param string $key
     * @param string $value
     * @return bool
     */
    public static function set($key, $value) {
        try {
            Yii::$app->db->createCommand("REPLACE INTO `tab_key_value` (`key`, `value`) VALUES (:key, :value)", [":key" => $key, ":value" => $value])->execute();
            return true;
        } catch (Exception $exception) {
            DLogger::error($exception->getMessage());
            return false;
        }
    }

    /**
     * 获取值
     * @param $key
     * @return string
     */
    public static function get($key) {
        $keyValue = KeyValue::find()->asArray()->where(["key" => $key])->one();
        return $keyValue !== null ? $keyValue["value"] : "";
    }

    /**
     * 获取 会员端联系信息
     * @return MemberContactVo
     */
    public static function getMemberContactVo() {
        $jsonStr = KeyValue::get(C::DB_KEY_MEMBER_CONTACT_VO);
        $vo = new MemberContactVo();
        if (!empty($vo)) {
            $vo->setAttributes(Json::decode($jsonStr));
        }
        return $vo;
    }

    /**
     * 保存 会员端联系信息
     * @param MemberContactVo $memberContactVo
     * @throws ForeseeableException
     */
    public static function saveMemberContactVo(MemberContactVo $memberContactVo) {
        if (!$memberContactVo->validate()) {
            throw new ForeseeableException($memberContactVo->getFirstError());
        }
        KeyValue::set(C::DB_KEY_MEMBER_CONTACT_VO, Json::encode($memberContactVo));
    }
}