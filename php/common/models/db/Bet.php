<?php

namespace common\models\db;

use common\models\table\TableTabBet;
use common\utils\game\SscUtil;
use yii\helpers\Json;

/**
 * 数据表 TableTabBet 的方法扩展
 */
class Bet extends TableTabBet
{
    /**
     * 获取注单vo
     * @return string
     */
    public function generateBetNo() {
        return strtoupper(uniqid());//$this->bet_type
    }

    /**
     * 设置客户下注内容
     * @param array $content
     */
    public function setBetContent(array $content) {
        $this->bet_content = Json::encode($content);
    }

    /**
     * 获取用户下注内容
     * @return array
     */
    public function getBetContent() {
        return !empty($this->bet_content) ? Json::decode($this->bet_content) : [];
    }

    /**
     * 获取用户下注内容字符串
     * @return string
     */
    public function getBetContentStr() {
        $arr = $this->getBetContent();
        $str = "";
        foreach ($arr as $value) {
            if (empty($value)) {
                $str .= "-";
            }
        }
    }


    /**
     * @return Schedule|null
     */
    public function getSchedule() {
        return Schedule::findOne($this->schedule_id);
    }

    /**
     * 获取注数
     * @return int
     */
    public function getGroupNum() {
        return SscUtil::getGroupNum($this->bet_type, $this->getBetContent());
    }
}
