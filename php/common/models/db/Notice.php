<?php

namespace common\models\db;

use common\models\table\TableTabNotice;

/**
 * 数据表 TableTabNotice 的方法扩展
 */
class Notice extends TableTabNotice
{
    /**
     * 获取公告列
     * @param $offset
     * @param $limit
     * @return array
     */
    public static function getNoticeLists($offset, $limit){
        $query = Notice::find();
        $count = $query->count();
        $list = $query->offset($offset)->limit($limit)->all();
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 公告更新
     * @param Notice $notice
     * @param $title
     * @param $content
     * @param $noticeType
     * @param $updateDate
     * @param null $recordDate
     * @param null $adminId
     * @return Notice
     */
    public static function updateNotice(Notice $notice,$title,$content,$noticeType,$updateDate,$recordDate = null,$adminId = null){
        $notice->title = (string)$title;
        $notice->content = (string)$content;
        $notice->notice_type = (int)$noticeType;
        $notice->update_date = (string)$updateDate;
        $adminId && $notice->admin_id = (int)$adminId;
        $recordDate && $notice->record_date = (string)$recordDate;
        return $notice;
    }
}