<?php
namespace common\models\db;

use common\constants\C;
use common\models\table\TableUserSuperHolder;

/**
 * 数据表 TableUserSuperHolder 的方法扩展 
 */
class UserSuperHolder extends TableUserSuperHolder 
{
    /**
     * 初始化数据
     */
    public function init() {
        $this->active = C::TRUE;
        $this->is_delete = C::FALSE;
        $this->credit = 0;
        $this->commission_plan = 0;
        $this->company_report = 0;
        $this->include_below = 0;
    }
}