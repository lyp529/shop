<?php

namespace common\models\db;

use common\models\table\BaseTable;
use Yii;

/**
 * This is the model class for table "_debug_log".
 *
 * @property int $id
 * @property string $log_type
 * @property string $title
 * @property string $content
 * @property string $record_date
 */
class DebugLog extends BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '_debug_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['log_type', 'title', 'content', 'record_date'], 'required'],
            [['content'], 'string'],
            [['record_date'], 'safe'],
            [['log_type'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_type' => 'Log Type',
            'title' => 'Title',
            'content' => 'Content',
            'record_date' => 'Record Date',
        ];
    }
}
