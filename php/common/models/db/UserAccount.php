<?php
namespace common\models\db;

use common\constants\C;
use common\models\table\TableUserAccount;

/**
 * 数据表 TableUserAccount 的方法扩展
 */
class UserAccount extends TableUserAccount
{
    /**
     * 初始化参数
     */
    public function init() {
        parent::init();
        $this->login_fail_times = 0;
        $this->lock = C::FALSE;
    }

    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            ["username", "string", "min" => 3],
        ]);
    }

    /**
     * @return bool
     */
    public function beforeValidate() {
        if (empty($this->user_type_name) && !empty($this->user_type)) {
            $this->user_type_name = C::getUserTypeLabel($this->user_type);
        }
        return parent::beforeValidate();
    }

    /**
     * 是否是会员
     * @return bool
     */
    public function isMember() {
        return $this->user_type === C::USER_MEMBER;
    }

    /**
     * 是否是代理
     * @return bool
     */
    public function isAgent() {
        return $this->user_type === C::USER_AGENT;
    }

    /**
     * 是否是总代理
     * @return bool
     */
    public function isVendor() {
        return $this->user_type === C::USER_VENDOR;
    }

    /**
     * 是否是股东
     * @return bool
     */
    public function isHolder() {
        return $this->user_type === C::USER_HOLDER;
    }

    /**
     * 是否是大股东
     * @return bool
     */
    public function isSuperHolder() {
        return $this->user_type === C::USER_SOLDER;
    }

    /**
     * 是否是管理员
     * @return bool
     */
    public function isAdmin() {
        return $this->user_type === C::USER_ADMIN;
    }

    /**
     * 生成sessionId
     */
    public function generalSessionId() {
        return uniqid("SID");
    }
}
