<?php
namespace common\models\db;

use common\models\table\TableTabCron;
use common\utils\CronParserUtil;

/**
 * 数据表 TableTabCron 的方法扩展
 */
class Cron extends TableTabCron 
{
    /**
     * 获取下一次运行时间
     * @return string 时间
     * @throws \Exception
     */
    public function getNextRunTime()
    {
        $array = CronParserUtil::formatToDate($this->cron_time);
        return $array[0];
    }

    /**
     * 获取详情名字
     * @return string
     */
    public function getDetailName()
    {
        return $this->cron_time . " " . $this->route . " " . $this->name;
    }

    /**
     * @param string $name 任务名称
     * @param string $route 任务运行路由
     * @param string $cronTime 运行周期时间 和 crontab 中的格式相同。目前仅兼容 * 和 数字 的计算。请勿使用如： "* / 15" 之类暂时不支持。如：[ * * * * * ]、[ 0 1 * * * ]
     * @return Cron
     */
    public static function initBase($name, $route, $cronTime)
    {
        $cron = new Cron();
        $cron->name = $name;
        $cron->route = $route;
        $cron->cron_time = $cronTime;
        return $cron;
    }
}