<?php

namespace common\models\db;

use common\constants\C;
use common\models\table\TableUserMember;
use common\utils\TimeUtil;

/**
 * 数据表 TableUserMember 的方法扩展
 */
class UserMember extends TableUserMember
{
    /**
     * 初始化数据
     */
    public function init()
    {
        parent::init();
        $this->is_direct = C::FALSE;
        $this->active = C::TRUE;
        $this->is_delete = C::FALSE;
        $this->credit = 0;
        $this->super_holder_id = 0;
        $this->holder_id = 0;
        $this->vendor_id = 0;
        $this->agent_id = 0;
    }

    /**
     * 获取上线id
     * @return int
     */
    public function getUpLineId()
    {
        $upLineId = $this->agent_id;
        if ($this->is_direct === C::TRUE) {
            if ($this->direct_user_type === C::USER_AGENT) {
                $upLineId = $this->agent_id;
            } else if ($this->direct_user_type === C::USER_VENDOR) {
                $upLineId = $this->vendor_id;
            } else if ($this->direct_user_type === C::USER_HOLDER) {
                $upLineId = $this->holder_id;
            } else if ($this->direct_user_type === C::USER_SOLDER) {
                $upLineId = $this->super_holder_id;
            }
        }
        return $upLineId;
    }

    /**
     * 每日返回下注金额接口
     * @param int $yesterdayBetAmount 昨天下注金额
     */
    public function addDailyBackAmount($yesterdayBetAmount)
    {


    }

    /**
     * 增加取款次数
     */
    public function addWithdrawTimes()
    {
        $today = TimeUtil::today();
        if ($today == $this->last_withdraw_date) {
            $this->last_withdraw_times++;
        } else {
            $this->last_withdraw_date = $today;
            $this->last_withdraw_times = 1;
        }
    }

    /**
     * 获取今天提现剩余次数
     */
    public function getWithdrawRestTimes()
    {
        $vip = $this->getVip();
        $todayTimes = $this->getTodayWithdrawTimes();
        return $vip->withdraw_times > $todayTimes ? $vip->withdraw_times - $todayTimes : 0;
    }

    /**
     * 获取今天取款次数
     */
    public function getTodayWithdrawTimes()
    {
        $today = TimeUtil::today();
        if ($today == $this->last_withdraw_date) {
            return $this->last_withdraw_times;
        } else {
            return 0;
        }
    }

    /**
     * 获取我的VIP信息
     * @return Vip
     */
    public function getVip()
    {
        return Vip::findOne($this->vip_level);
    }


    /**
     * 增加会员累计充值
     * @param int $amount
     */
    public function addTotalPay($amount)
    {
        $this->total_pay += $amount;
    }

    /**
     * 增加会员累计下注
     * @param double $amount
     */
    public function addTotalBet($amount)
    {
        $this->total_bet += $amount;
        if ($this->total_bet >= $this->total_pay) { //消费比例达100%后,清0
            $this->total_bet = $this->total_pay = 0;
        }
    }

    /**
     * IN user_id 查询信用额
     * @param array $ids
     * @return array
     */
    public static function byIdsGetCredit(array $ids) {
        return UserMember::find()->select('user_id,credit')->where(['user_id' => $ids])->asArray()->all();
    }
}
