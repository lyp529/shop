<?php

namespace common\models\db;

use common\models\table\TableCfgVip;

/**
 * 数据表 TableCfgVip 的方法扩展
 */
class Vip extends TableCfgVip
{
    /**
     * 找到当前合适的VIP等级和余额
     * @param int $vipLevel 当前VIP等级
     * @param int $balance 余额
     * @return Vip
     */
    public static function getFitVip($vipLevel, $balance) {
        $vipMap = self::getModelMap();
        for ($level = $vipLevel; $level > 0; $level--) {
            $vip = $vipMap[$level];
            if ($balance >= $vip->amount) {
                return $vip;
            }
        }
        return $vipMap[0];

    }

}
