<?php
namespace common\models\db;

use common\constants\C;
use common\models\table\TableUserVendor;

/**
 * 数据表 TableUserVendor 的方法扩展 
 */
class UserVendor extends TableUserVendor 
{
    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->active = C::TRUE;
        $this->is_delete = C::FALSE;
        $this->credit = 0;
        $this->fraction_max = 0;
        $this->fraction_holder = 0;
    }

    /**
     * @return UserAgent[]
     */
    public function getAgentList() {
        return UserAgent::findAll(["vendor_id" => $this->user_id]);
    }

    /**
     * @return UserMember[]
     */
    public function getMemberList() {
        return UserMember::findAll(["vendor_id" => $this->user_id]);
    }
}