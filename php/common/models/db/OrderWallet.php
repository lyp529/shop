<?php

namespace common\models\db;

use common\constants\C;
use common\models\table\TableTabOrderWallet;
use common\utils\Debug;
use common\utils\game\UserUtil;

/**
 * 数据表 TableTabOrderWallet 的方法扩展
 */
class OrderWallet extends TableTabOrderWallet
{
    /**
     * 获取订单前缀
     * @param $orderType
     * @return string
     */
    public static function getOrderNoPrefix($orderType) {
        $prefix = [
            C::ORDER_WALLET_TYPE_DEPOSIT => "DE",
            C::ORDER_WALLET_TYPE_WITHDRAW => "WI",
        ];
        return isset($prefix[$orderType]) ? $prefix[$orderType] : "";
    }

    /**
     * 获取订单编号
     * @param $orderType
     * @return string
     */
    public static function getOrderNo($orderType) {
        return strtoupper(uniqid(OrderWallet::getOrderNoPrefix($orderType)));
    }

    public static function getOrderList($startTime, $endTime, $orderType, $orderState, $orderNumber, $offset, $limit) {
        $query = OrderWallet::find();
        $startTime && $query->andWhere([">=", "order_time", $startTime]);
        $endTime && $query->andWhere(["<=", "order_time", $endTime]);
        $orderType && $query->andWhere(["order_type" => $orderType]);
        $orderState && $query->andWhere(["order_state" => $orderState]);
        $orderNumber && $query->andWhere(["order_no" => $orderNumber]);
        $query->andWhere(UserUtil::getUserQuery());
        // Debug::showLastSql($query);
        $count = $query->count();
        $list = $query->offset($offset)->limit($limit)->orderBy("id DESC")->all();
        return ['list' => $list, 'count' => $count];
    }
}
