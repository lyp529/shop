<?php
namespace common\models\db;

use common\constants\C;
use common\models\table\TableUserAgent;

/**
 * 数据表 TableUserAgent 的方法扩展 
 */
class UserAgent extends TableUserAgent 
{
    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->active = C::TRUE;
        $this->is_delete = C::FALSE;
        $this->credit = 0;
        $this->fraction_vendor = 0;
        $this->fration_max = 0;
    }

    /**
     * @return UserMember[]
     */
    public function getMemberList() {
        return UserMember::findAll(["agent_id" => $this->user_id]);
    }
}