<?php
namespace common\models\db;

use common\models\table\TableCfgNoticeType;

/**
 * 数据表 TableCfgNoticeType 的方法扩展 
 */
class NoticeType extends TableCfgNoticeType 
{
    /**
     * 获取公告类
     * @param int $id
     * @return array
     */
    public static function getList($id)
    {
        $query = NoticeType::find()->asArray()->select(["id", "type_name"]);
        $id > 1 && $query->where(["id" => (int)$id]);
        return $query->all();
    }
}