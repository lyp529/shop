<?php
namespace common\models\db;

use common\models\table\TableTabGoods;

/**
 * 数据表 TableTabGoods 的方法扩展
 */
class Goods extends TableTabGoods
{
    /**
     *  商品更新
     * @param Goods $goods
     * @param $name
     * @param $credits
     * @param $bonusPoints
     * @param $content
     * @param null $images
     * @param $nowTime
     * @param null $recordDate
     * @return Goods
     */
    public static function updateGoods(Goods $goods,$name,$credits,$bonusPoints,$content,$nowTime,$images = null,$recordDate = null){
        $goods->name = (string)$name;
        $goods->credits = (string)$credits;
        $goods->bonus_points = (string)$bonusPoints;
        $goods->content = (string)$content;
        $images && $goods->images = (string)$images;
        $goods->update_date = $nowTime;
        $recordDate && $goods->record_date = $recordDate;
        return $goods;
    }
}
