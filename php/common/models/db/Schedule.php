<?php

namespace common\models\db;

use common\constants\C;
use common\constants\P;
use common\models\table\TableTabSchedule;
use common\utils\StringUtil;
use common\utils\TimeUtil;
use common\utils\ValueUtil;

/**
 * 数据表 TableTabSchedule 的方法扩展
 */
class Schedule extends TableTabSchedule
{
    /**
     * 获取随机结果
     * @param int $digits 数字的位数
     * @param int $min 数字最大值
     * @param int $max 数字最小值
     * @return int[]
     */
    public static function getRandomResultNums($digits = 5, $min = 0, $max = 9) {
        $resultNums = [];
        for ($i = 0; $i < $digits; $i++) {
            $resultNums[] = rand($min, $max);
        }
        return $resultNums;
    }

    /**
     * 获取私彩期数号码【注：必须先设置 open_time 属性】
     * @param string $openTime 开奖时间
     * @return string
     */
    public static function getPersonalPeriod($openTime) {
        $secs = TimeUtil::getTwoTimeSecs(TimeUtil::getDateStartTime($openTime), $openTime);
        $periodNum = floor($secs / P::SC_PERSONAL_PERIOD_SEC) + 1;//add +1 by ewing 10-16
        return date("Ymd", strtotime($openTime)) . ValueUtil::fillZero($periodNum, 3);
    }

    /**
     * 获取公彩期数号码【注：必须先设置 open_time 属性】
     * @param string $openTime 开奖时间
     * @param string $startTime 配置的开始时间 比如 00:30:00
     * @return string
     */
    public static function getOfficialPeriod($openTime, $startTime) {
        $secs = TimeUtil::getTwoTimeSecs(date('Y-m-d', strtotime($openTime)) . ' ' . $startTime, $openTime);
        $periodNum = floor($secs / P::SC_OFFICIAL_PERIOD_SEC) + 1;//add +1 by ewing 10-16
        if (date('H', strtotime($openTime)) >= 7) {
            $periodNum = $periodNum - 12;
        }
        $periodNum = date("Ymd", strtotime($openTime)) . ValueUtil::fillZero($periodNum, 3);
        return $periodNum;
    }


    /**
     * 获取剩余下注时间。如果时间为0，则不允许下注
     * @return int 剩余秒数。最小值为 0
     */
    public function getRemainBetSeconds() {
        // 彩种剩余下注时间配置
        $configMap = [
            C::KIND_SSC_BANGKOK => 30,
            C::KIND_SSC_BERLIN => 0,
        ];
        // 禁止下注的截至时间
        $defaultSecs = isset($configMap[$this->kind]) ? $configMap[$this->kind] : 120;

        $seconds = strtotime($this->open_time) - time() - $defaultSecs; // 默认是提前一分钟不允许继续下注
        return $seconds > 0 ? $seconds : 0;
    }

    /**
     * 获取结果的字符串模式 e.g. 1,2,3,4,5
     * @return string
     */
    public function getResultNumsStr() {
        return str_replace("@", ",", $this->result_nums);
    }

    /**
     * 获取结果的字符串模式 e.g. 1,2,3,4,5
     * @return string
     */
    public function getPredictNumsStr() {
        return str_replace("@", ",", $this->predict_nums);
    }

    /**
     * @return int[]
     */
    public function getResultNums() {
        if (!empty($this->result_nums)) {
            if (StringUtil::contains($this->result_nums, "@")) {
                return explode("@", $this->result_nums);
            } else {
                return explode(",", $this->result_nums);
            }
        } else {
            return [];
        }
    }

    /**
     * @param int[] $nums 如：[1, 2, 3, 4, 5]
     */
    public function setResultNums($nums) {
        $this->result_nums = implode(",", $nums);
    }

    /**
     * @return int[]
     */
    public function getPredictNums() {
        if (StringUtil::contains($this->predict_nums, "@")) {
            return explode("@", $this->predict_nums);
        } else {
            return explode(",", $this->predict_nums);
        }
    }

    /**
     * @param int[] $nums 如：[1, 2, 3, 4, 5]
     */
    public function setPredictNums($nums) {
        $this->predict_nums = implode(",", $nums);
    }

    /**
     * 获取当前期数下所有的注单列表
     * @return Bet[]
     */
    public function getBetList() {
        return Bet::findAll(["schedule_id" => $this->id]);
    }

    /**
     * 是否有结果
     * @return bool
     */
    public function hasResult() {
        return !empty($this->result_nums);
    }

    /**
     * 获取上一期
     * @return Schedule|null
     */
    public function findPastSchedule() {
        return Schedule::find()->where(["kind" => $this->kind])->andWhere(["<", "open_time", $this->open_time])->orderBy(["open_time" => SORT_DESC])->one();
    }

    /**
     * 获取下一期
     * @return Schedule|null
     */
    public function findNextSchedule() {
        return Schedule::find()->where(["kind" => $this->kind])->andWhere([">", "open_time", $this->open_time])->orderBy(["open_time" => SORT_ASC])->one();
    }

    /**
     * 获取短期号
     */
    public function getShortPeriod() {
        return substr($this->period, 4);
    }
}
