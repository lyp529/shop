<?php


namespace common\models;


use common\constants\C;
use common\constants\L;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\UserAccount;
use common\models\db\UserAdmin;
use common\models\db\UserAgent;
use common\models\db\UserHolder;
use common\models\db\UserMember;
use common\models\db\UserSuperHolder;
use common\models\db\UserVendor;
use common\models\db\Vip;
use common\utils\DLogger;
use common\utils\game\LogUtil;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use common\utils\YiiUtil;

/**
 * Class UserModel 用户模型
 * @package common\models
 */
class UserModel extends BaseModel
{
    /**
     * @var int
     */
    public $userType; //见define.php
    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $idField;
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    private $credit = 0;

    /**
     * @var int 上线的大股东ID
     */
    public $superHolderId;
    /**
     * @var int 上线大股东ID
     */
    public $holderId;
    /**
     * @var int 上线的总代理ID
     */
    public $vendorId;
    /**
     * @var int 上线的代理ID
     */
    public $agentId;
    /**
     * @var int 上线的会员ID
     */
    public $memberId;

    /**
     * @var UserAccount;
     * @remark 延迟属性,使用 getAccount()方法获取
     */
    private $account;
    /**
     * @var UserAdmin;
     */
    private $admin;
    /**
     * @var UserSuperHolder;
     */
    private $superHolder;
    /**
     * @var UserHolder;
     */
    private $holder;
    /**
     * @var UserVendor;
     */
    private $vendor;
    /**
     * @var UserAgent;
     */
    private $agent;
    /**
     * @var UserMember;
     */
    private $member;
    /**
     * @var string
     */
    private $bankName;
    /**
     * @var int
     */
    private $vipLevel;
    /**
     * @var string
     */
    private $bankAccount;
    /**
     * @var string
     */
    private $bankUsername;
    /**
     * @var string
     */
    private $bankEmail;

    //==================== 在已知用户类型的时候,可以指定初始化 =====================
    public static function initAdmin($userId) {
        return self::getUser(C::USER_ADMIN, $userId);
    }

    public static function initSuperHolder($userId) {
        return self::getUser(C::USER_SOLDER, $userId);
    }

    public static function initHolder($userId) {
        return self::getUser(C::USER_HOLDER, $userId);
    }

    public static function initVendor($userId) {
        return self::getUser(C::USER_VENDOR, $userId);
    }

    public static function initAgent($userId) {
        return self::getUser(C::USER_AGENT, $userId);
    }

    public static function initMember($userId) {
        return self::getUser(C::USER_MEMBER, $userId);
    }

    /**
     * 初始化
     * @param $userId
     * @return UserModel|null
     * @remark  如果知道用户类型的时候,尽量调用 getUser 以免多查一下user_account表.  获取当前在线用户使用UserUtil.getUser();
     */
    public static function initUser($userId) {
        $userAccount = UserAccount::findOne($userId);
        if ($userAccount === null) {
            return null;
        }
        return self::getUser($userAccount->user_type, $userId);
    }

    /**
     * 获取简单的用户模型,延迟加载Account的用户模型
     * @param $userType
     * @param $userId
     * @return UserModel
     */
    public static function getUser($userType, $userId) {
        $user = new UserModel();
        $user->userType = $userType;
        $user->userId = $userId;
        $user->account = null;

        $user->idField = UserUtil::getIdField($user->userType);
        if ($user->isAdmin()) {
            $user->admin = UserAdmin::findOne($userId);
            $user->username = $user->admin->username;
            $user->name = $user->admin->name;
        } elseif ($user->isSuperHolder()) {
            $user->superHolder = UserSuperHolder::findOne($userId);
            $user->superHolderId = $userId;
            $user->credit = $user->superHolder->credit;
            $user->username = $user->superHolder->username;
            $user->name = $user->superHolder->name;
        } elseif ($user->isHolder()) {
            $user->holder = UserHolder::findOne($userId);
            $user->credit = $user->holder->credit;
            $user->holderId = $userId;
            $user->superHolderId = $user->holder->super_holder_id;
            $user->username = $user->holder->username;
            $user->name = $user->holder->name;
        } elseif ($user->isVendor()) {
            $user->vendor = UserVendor::findOne($userId);
            $user->credit = $user->vendor->credit;
            $user->vendorId = $userId;
            $user->holderId = $user->vendor->holder_id;
            $user->superHolderId = $user->vendor->super_holder_id;
            $user->username = $user->vendor->username;
            $user->name = $user->vendor->name;
        } elseif ($user->isAgent()) {
            $user->agent = UserAgent::findOne($userId);
            $user->credit = $user->agent->credit;
            $user->vendorId = $user->agent->vendor_id;
            $user->holderId = $user->agent->holder_id;
            $user->superHolderId = $user->agent->super_holder_id;
            $user->username = $user->agent->username;
            $user->name = $user->agent->name;
        } elseif ($user->isMember()) {
            $user->member = UserMember::findOne($userId);
            $user->credit = $user->member->credit;
            $user->memberId = $user->member->user_id;
            $user->agentId = $user->member->agent_id;
            $user->vendorId = $user->member->vendor_id;
            $user->holderId = $user->member->holder_id;
            $user->superHolderId = $user->member->super_holder_id;
            $user->username = $user->member->username;
            $user->name = $user->member->name;
            $user->bankName = $user->member->bank_name;
            $user->vipLevel = $user->member->vip_level;
            $user->bankAccount = $user->member->bank_account;
            $user->bankUsername = $user->member->bank_username;
            $user->bankEmail = $user->member->bank_email;

        }
        return $user;
    }


    /**
     * 获取用户余额
     * @return int
     */
    public function getCredit() {
        return $this->credit;
    }

    /**
     * 获取用户等级 VIP
     * @return int
     */
    public function getVipLevel() {
        return $this->vipLevel;
    }


    /**
     * 设置信用额度
     * @param $credit
     */
    private function setCredit($credit) {
        $this->credit = $credit;
        $this->isSuperHolder() && ($this->getSuperHolder()->credit = $credit);
        $this->isHolder() && ($this->getHolder()->credit = $credit);
        $this->isVendor() && ($this->getVendor()->credit = $credit);
        $this->isAgent() && ($this->getAgent()->credit = $credit);
        $this->isMember() && ($this->getMember()->credit = $credit);
    }

    /**
     * 减少信用额
     * @param double $amount
     * @param bool $isBet 是否是下注减少
     * @return int
     * @throws ForeseeableException
     */
    public function cutCredit($amount, $isBet = false) {
        if ($amount <= 0) {
            throw new ForeseeableException("减少的额度异常");
        }
        if ($this->credit < $amount) {
            throw new ForeseeableException("信用额度不足");
        }
        $this->setCredit($this->credit - $amount);
        $this->member->addTotalBet($amount);
        if ($isBet) {
            $this->addIntegral($amount);
        }
        return $this->credit;
    }

    /**
     * 增加信用额
     * @param double $amount 正数负数都可
     * @param bool $isPay 是否充值增加
     * @return int
     */
    public function addCredit($amount, $isPay = false) {
        $this->setCredit($this->credit + $amount);
        if ($isPay) {
            $this->member->addTotalPay($amount);
        }
        return $this->credit;
    }

    public function isAdmin() {
        return $this->userType == C::USER_ADMIN;
    }

    public function isSuperHolder() {
        return $this->userType == C::USER_SOLDER;
    }

    public function isHolder() {
        return $this->userType == C::USER_HOLDER;
    }

    public function isVendor() {
        return $this->userType == C::USER_VENDOR;
    }

    public function isAgent() {
        return $this->userType == C::USER_AGENT;
    }

    public function isMember() {
        return $this->userType == C::USER_MEMBER;
    }

    /**
     * @return UserAccount
     */
    public function getAccount() {
        if ($this->account == null) {
            $this->account = UserAccount::findOne($this->userId);
        }
        return $this->account;
    }

    /**
     * @return UserAdmin
     */
    public function getAdmin() {
        if ($this->admin == null) {
            $this->account = UserAdmin::findOne($this->userId);
        }
        return $this->admin;
    }

    /**
     * @return UserSuperHolder
     */
    public function getSuperHolder() {
        if ($this->superHolder === null) {
            $this->superHolder = UserSuperHolder::findOne($this->userId);
        }
        return $this->superHolder;
    }

    /**
     * @return UserHolder
     */
    public function getHolder() {
        if ($this->holder === null) {
            $this->holder = UserHolder::findOne($this->userId);
        }
        return $this->holder;
    }

    /**
     * @return UserVendor
     */
    public function getVendor() {
        if ($this->vendor === null) {
            $this->vendor = UserVendor::findOne($this->userId);
        }
        return $this->vendor;
    }

    /**
     * @return UserAgent
     */
    public function getAgent() {
        if ($this->agent === null) {
            $this->agent = UserAgent::findOne($this->userId);
        }
        return $this->agent;
    }

    /**
     * @return UserMember
     */
    public function getMember() {
        if ($this->member === null) {
            $this->member = UserMember::findOne($this->userId);
        }
        return $this->member;
    }

    /**
     * 获取直属上级用户
     * @remark 只有直属会员才有
     */
    public function getDirectUserModel() {

    }

    /**
     * 获取直属下级
     * @param int $userType 需要获取直属下级的用户类型
     * @param bool $onlyNotDelete
     * @param null $username
     * @param bool $onlyActive
     * @return UserModel[]
     * @throws SystemException
     */
    public function getChildUserModelList($userType, $onlyNotDelete = true, $username = null, $onlyActive = false) {
        $queryArr = [];
        if ($this->isAdmin()) {
            // 不需要任何查询语句
            $username != null && $queryArr = ["username" => $username];
        } else if ($this->isSuperHolder()) {
            if ($userType < C::USER_SOLDER) {
                $queryArr = ["super_holder_id" => $this->userId];
            } else {
                $queryArr = ["user_id" => $this->userId];
            }
        } else if ($this->isHolder()) {
            if ($userType < C::USER_HOLDER) {
                $queryArr = ["holder_id" => $this->userId];
            } else {
                $queryArr = ["user_id" => $this->userId];
            }
        } else if ($this->isVendor()) {
            if ($userType < C::USER_VENDOR) {
                $queryArr = ["vendor_id" => $this->userId];
            } else {
                $queryArr = ["user_id" => $this->userId];
            }
        } else if ($this->isAgent()) {
            if ($userType < C::USER_AGENT) {
                $queryArr = ["agent_id" => $this->userId];
            } else {
                $queryArr = ["user_id" => $this->userId];
            }
        } else if ($this->isMember()) {
            return []; // 会员没有直属下级，直接返回空数组
        } else {
            throw new SystemException("当前用户类型是非法类型");
        }

        // 只查询没有删除的用户
        if ($onlyNotDelete) {
            $queryArr["is_delete"] = C::FALSE;
        }
        // 只查询激活的用户
        if ($onlyActive) {
            $queryArr["active"] = C::TRUE;
        }

        if ($userType === C::USER_SOLDER && $this->isAdmin()) {
            $rows = UserSuperHolder::find()->asArray()->select(["user_id"])->orderBy(["user_id" => SORT_DESC])->all();
        } else if ($userType === C::USER_HOLDER) {
            $rows = UserHolder::find()->asArray()->select(["user_id"])->where($queryArr)->orderBy(["user_id" => SORT_DESC])->all();
        } else if ($userType === C::USER_VENDOR) {
            $rows = UserVendor::find()->asArray()->select(["user_id"])->where($queryArr)->orderBy(["user_id" => SORT_DESC])->all();
        } else if ($userType === C::USER_AGENT) {
            $rows = UserAgent::find()->asArray()->select(["user_id"])->where($queryArr)->orderBy(["user_id" => SORT_DESC])->all();
        } else if ($userType === C::USER_MEMBER) {
            $rows = UserMember::find()->asArray()->select(["user_id"])->where($queryArr)->orderBy(["user_id" => SORT_DESC])->all();
        } else if ($userType === C::USER_ADMIN) {
            $rows = UserAdmin::find()->all();
        } else {
            throw new SystemException("未知的用户类型：" . $userType);
        }
        $userIds = [];
        foreach ($rows as $row) {
            $userIds[] = $row["user_id"];
        }
        $userModelList = [];
        foreach ($userIds as $userId) {
            $userModelList[] = UserModel::initUser($userId);
        }
        return $userModelList;
    }

    /**
     * 获取直属会员
     * @param bool $activeOnly
     * @return UserModel[]
     */
    public function getDirectMemberModelList($activeOnly = true) {
        $query = UserMember::find()->asArray()->select(["user_id"])->where(["direct_user_type" => $this->userType, "direct_user_id" => $this->userId]);
        if ($activeOnly) {
            $query->andWhere(["active" => C::TRUE, "is_delete" => C::FALSE]);
        }
        $rows = $query->all();
        $userIds = [];
        foreach ($rows as $row) {
            $userIds[] = $row["user_id"];
        }
        $userModelList = [];
        foreach ($userIds as $userId) {
            $userModelList[] = UserModel::initUser($userId);
        }
        return $userModelList;
    }

    /**
     * 获取上线的用户模型
     */
//    public function getUpLineUserModel() {
//        return UserModel::initUser($this->getUpLineId());
//    }

    /**
     * 获取上线的用户模型
     * @return UserModel|null
     */
    public function getUpLineUserModel() {
        $upLineType = $this->getUpLineType();
        $upLineId = $this->getUpLineId();
        if (empty($upLineType) || empty($upLineId)) {
            return null;
        }
        return self::getUser($upLineType, $upLineId);
    }

    /**
     * 获取上线用户类型
     */
    public function getUpLineType() {
        $upLineType = C::USER_ADMIN;
        if ($this->isHolder()) {
            $upLineType = C::USER_SOLDER;
        } else if ($this->isVendor()) {
            $upLineType = C::USER_HOLDER;
        } else if ($this->isAgent()) {
            $upLineType = C::USER_VENDOR;
        } else if ($this->isMember()) {
            $upLineType = $this->getMember()->direct_user_type;
        }
        return $upLineType;
    }

    /**
     * 获取上线用户ID
     */
    public function getUpLineId() {
        $upLineId = 0;
        if ($this->isHolder()) {
            $upLineId = $this->getHolder()->super_holder_id;
        }
        if ($this->isVendor()) {
            $upLineId = $this->getVendor()->holder_id;
        }
        if ($this->isAgent()) {
            $upLineId = $this->getAgent()->vendor_id;
        }
        if ($this->isMember()) {
            $upLineId = $this->getMember()->getUpLineId();
        }
        return $upLineId;
    }

    /**
     * 更新用户密码
     * @param $oldPwd
     * @param $newPwd
     * @param bool $isCash 是否是修改资金密码
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function updatePwd($oldPwd, $newPwd, $isCash = false) {
        $field = $isCash ? 'password2' : 'password';
//        echo "oldPwd=".$oldPwd."<br>";
//        echo "newPwd=".$newPwd."<br>";
//        echo "field=".$field."<br>";
//        echo "dbpwd=".$this->getAccount()->$field."<br>";

        if ($oldPwd && $oldPwd !== $this->getAccount()->$field) {
            throw new ForeseeableException("旧密码不一致 > " . $oldPwd . ' ' . $this->getAccount()->$field);
        }
        $this->getAccount()->$field = $newPwd;
        if (!$this->save()) {
            throw new SystemException($this->getFirstError());
        }
    }

    /**
     * 更新积分
     * @param $integral
     */
    private function updateIntegral($integral) {
        $this->member->integral = $integral;
    }

    /**
     * 增加积分
     * @param $integral
     */
    public function addIntegral($integral) {
        // 舍弃小数部分
        $this->updateIntegral(floor($this->member->integral + $integral));
    }

    /**
     * 减少积分
     * @param $integral
     * @throws ForeseeableException
     */
    public function cutIntegral($integral) {
        if ($integral <= 0) {
            throw new ForeseeableException("减少的积分异常");
        }
        if ($this->member->integral < $integral) {
            throw new ForeseeableException("积分不足");
        }
        $this->updateIntegral($this->member->integral - $integral);
    }

    /**
     * 检查这个用户是不是我的下线
     * @param $userType
     * @param $userId
     */
    public function checkMyDownLine($userType, $userId) {


    }

    /**
     * 检测是否直属会员
     */
    public function isDirectMember() {
        return $this->userType == C::USER_MEMBER && $this->getMember()->is_direct;
    }

    /**
     * 是否激活（如果删除了则不是激活的）
     * @return bool
     */
    public function isActive() {
        $isActive = false;
        if ($this->isAdmin()) {
            $isActive = true;
        } else if ($this->isSuperHolder()) {
            $isActive = $this->getSuperHolder()->active === C::TRUE && $this->getSuperHolder()->is_delete === C::FALSE;
        } else if ($this->isHolder()) {
            $isActive = $this->getHolder()->active === C::TRUE && $this->getHolder()->is_delete === C::FALSE;
        } else if ($this->isVendor()) {
            $isActive = $this->getVendor()->active === C::TRUE && $this->getVendor()->is_delete === C::FALSE;
        } else if ($this->isAgent()) {
            $isActive = $this->getAgent()->active === C::TRUE && $this->getAgent()->is_delete === C::FALSE;
        } else if ($this->isMember()) {
            $isActive = $this->getMember()->active === C::TRUE && $this->getMember()->is_delete === C::FALSE;
        }
        return $isActive;
    }

    /**
     * 色湖之是否激活
     * @param $active
     * @throws ForeseeableException
     */
    public function setActive($active) {
        $activeInt = $active ? C::TRUE : C::FALSE;
        if ($this->isMember()) {
            $this->getMember()->active = $activeInt;
        } else if ($this->isAgent()) {
            $this->getAgent()->active = $activeInt;
        } else if ($this->isVendor()) {
            $this->getVendor()->active = $activeInt;
        } else if ($this->isHolder()) {
            $this->getHolder()->active = $activeInt;
        } else if ($this->isSuperHolder()) {
            $this->getSuperHolder()->active = $activeInt;
        } else {
            $userTypeLabel = C::getUserTypeLabel($this->userType);
            throw new ForeseeableException("用户类型【" . $userTypeLabel . "】不支持修改激活状态");
        }
    }

    /**
     * 重设 session 截至时间（用于判断是否登录中）
     * @param int $second
     */
    public function resetSessionDeadTime($second = 30) {
        $this->getAccount()->session_dead_time = TimeUtil::getTimeAfterSecond(TimeUtil::now(), $second);
        $this->save();
    }


    /**
     * 登入
     * @throws \Throwable
     */
    public function login() {
        YiiUtil::session(C::SESSION_KEY_USER_ID, $this->userId);
        YiiUtil::session(C::SESSION_KEY_USER_TYPE, $this->userType);
        YiiUtil::session(C::SESSION_KEY_USERNAME, $this->username);
        YiiUtil::session(C::SESSION_KEY_SESSION_ID, $this->getAccount()->session_id);

        LogUtil::loginLog(L::LOG_LOGIN, $this->userType, $this->userId, $this->username);
    }

    /**
     * 登出
     * @throws \Throwable
     */
    public function logout() {
        YiiUtil::destroySession();
        LogUtil::loginLog(L::LOG_LOGOUT, $this->userType, $this->userId, $this->username);
    }

    /**
     * 判断当前是否登录。判断 SESSION 中是否有值
     * @return bool
     */
    public function isLogin() {
        $sessionId = YiiUtil::session(C::SESSION_KEY_SESSION_ID);
        return !empty($sessionId);
    }

    /**
     * 登录是否有效。判断 SESSION 中的 SESSION_ID 是否一致。
     *  SESSION_ID 每次登录时会变化，用于不能多地方同时登录
     */
    public function isLoginValid() {
        $sessionId = YiiUtil::session(C::SESSION_KEY_SESSION_ID);
        $dbSessionId = $sessionId ? $this->getAccount()->session_id : null;
        return !empty($dbSessionId) && $sessionId === $dbSessionId && $this->isActive();
    }

    /**
     * 踢出将当前用户踢出登录，即清除 SESSION_ID 。调用这个方法会触发 save() 方法
     * @throws SystemException
     */
    public function kick() {
        $this->getAccount()->session_id = "";
        if (!$this->save()) {
            throw new SystemException($this->getFirstError());
        }
    }


    /**
     * 保存数据
     * @return bool
     */
    public function save() {
        if ($this->account !== null && !$this->account->save()) {
            $this->addError("account", $this->account->getFirstError());
        }

        if ($this->isAdmin() && $this->admin !== null) {
            if (!$this->admin->save()) {
                $this->addError("admin", $this->admin->getFirstError());
            }
        } else if ($this->isSuperHolder() && $this->superHolder !== null) {
            if (!$this->superHolder->save()) {
                $this->addError("superHolder", $this->superHolder->getFirstError());
            }
        } else if ($this->isHolder() && $this->holder !== null) {
            if (!$this->holder->save()) {
                $this->addError("holder", $this->holder->getFirstError());
            }
        } else if ($this->isVendor() && $this->vendor !== null) {
            if (!$this->vendor->save()) {
                $this->addError("vendor", $this->vendor->getFirstError());
            }
        } else if ($this->isAgent() && $this->agent !== null) {
            if (!$this->agent->save()) {
                $this->addError("agent", $this->agent->getFirstError());
            }
        } else if ($this->isMember() && $this->member !== null) {
            if (!$this->member->save()) {
                $this->addError("member", $this->member->getFirstError());
            }
        } else {
            $this->addError("userType", "未知的用户类型");
        }
        return !$this->hasErrors();
    }


    /**
     * 检测玩家的VIP等级是否可提升
     * @param bool $isSave 是否保存
     * @return int
     */
    public function checkVipLevelUp($isSave = false) {
        if ($this->isMember()) {
            $vipLevel = $this->member->vip_level;
            $targetVipLevel = $vipLevel; //目标等级
            $vipMap = Vip::getModelMap();
            foreach ($vipMap as $level => $vip) {
                if ($this->credit >= $vip->amount && $vip->level > $targetVipLevel) {
                    $targetVipLevel = $vip->level;
                }
            }
            if (!$isSave) {
                return $targetVipLevel;
            }
            if ($isSave && $targetVipLevel > $vipLevel) {
                $this->member->vip_level = $targetVipLevel;
                $this->member->save();
                LogUtil::pLog(L::PLOG_VIP_UP, [$this->member->username . ', ' . C::getVipLabels($vipLevel) . ' - ' . C::getVipLabels($targetVipLevel)]);
                //DLogger::debug($this->username . " update to vip_level:" . $targetVipLevel);
            }
        } else {
            return 0;
        }
    }

    /**
     * 获取用户当前能激活的VIP信息
     * 因为余额少于当前VIP所需金额时,会降到符合的VIP
     * @return Vip
     */
    public function getActiveVip() {
        $targetVip = null;
        $vipLevel = $this->member->vip_level;
        $vip = Vip::findOne($vipLevel + 1);
        if ($this->credit >= $vip->amount) {
            $targetVip = $vip;
        } else {
            $vipMap = Vip::getModelMap();
            krsort($vipMap);
            foreach ($vipMap as $vip) {
                if ($this->credit >= $vip->amount) {
                    $targetVip = $vip;
                    break;
                }
            }
        }
        if ($targetVip === null) {
            DLogger::error("用户:" . $this->userId . ' ' . $this->username . " 找不到合适的VIP等级配置");
        }
        return $targetVip;

    }
}
