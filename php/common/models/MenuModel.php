<?php


namespace common\models;


use common\constants\A;
use common\constants\M;

/**
 * Class MenuModel 菜单模型
 * @package common\models
 */
class MenuModel extends BaseModel {
    /**
     * @var string 组件名
     */
    public $component;
    /**
     * @var string 菜单名
     */
    public $name;
    /**
     * @var MenuModel[] 子菜单
     */
    public $children = [];
    /**
     * @var string icon名字
     */
    public $icon;
    /**
     * @var int 权限模块
     */
    public $module;
    /**
     * @var int[] 动作权限
     */
    public $actions = [];

    /**
     * 获取所有菜单
     * @return MenuModel
     */
    public static function getMenu() {
        $root = new MenuModel("菜单");

        if (YII_ENV_DEV) {
            // 开发环境
            $root->addChild((new MenuModel("开发测试"))
                ->addChild((new MenuModel("开发工具", "DevelopTool")))
                ->addChild((new MenuModel("小微上传文件", "XiaoWeiUploadFile")))
            );
        }

        $root->addChild((new MenuModel("用户管理"))
                ->addChild((new MenuModel("用户列表", "UserList"))->setAuth(M::USER_LIST, [A::VIEW, A::ADD, A::MOD, A::DEL]))
                ->addChild((new MenuModel("小商户列表", "MemberList"))->setAuth(M::MEMBER_LIST, [A::VIEW, A::ADD, A::MOD]))
            )
            ->addChild((new MenuModel("交易管理"))
                ->addChild((new MenuModel("交易记录", "OrderList"))->setAuth(M::ORDER_LIST, [A::VIEW]))
            )
            ->addChild((new MenuModel("个人中心"))
                ->addChild((new MenuModel("个人信息", "TestList"))->setAuth(M::ORDER_LIST, [A::VIEW]))
            );

        return $root;
    }

    /**
     * MenuModel constructor.
     * @param $name
     * @param string $component
     */
    public function __construct($name, $component = "") {
        parent::__construct([]);
        $this->name = $name;
        $this->component = $component;
    }

    /**
     * 添加子项
     * @param MenuModel $menuModel
     * @return $this
     */
    public function addChild(MenuModel $menuModel) {
        $this->children[] = $menuModel;
        return $this;
    }

    /**
     * 设置图表
     * @param $icon
     * @return $this
     */
    public function setIcon($icon) {
        $this->icon = $icon;
        return $this;
    }

    /**
     * 设置权限
     * @param $module
     * @param int[] $actions 动作列表
     * @return MenuModel
     */
    public function setAuth($module, $actions = []) {
        $this->module = $module;
        $this->actions = $actions;
        return $this;
    }
}