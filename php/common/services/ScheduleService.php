<?php


namespace common\services;


use common\constants\C;
use common\constants\P;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\Kind;
use common\models\db\Schedule;
use common\models\vo\http\SscDataVo;
use common\models\vo\sc\ScheduleRowVo;
use common\models\vo\sc\ScScheduleVo;
use common\traits\InstanceTrait;
use common\utils\DLogger;
use common\utils\StringUtil;
use common\utils\TimeUtil;

/**
 * Class ScheduleService 期数管理器
 * @package common\services
 */
class ScheduleService extends BaseService
{
    use InstanceTrait;

    /**
     * 获取所有未派彩的期数记录
     * @return Schedule[]
     */
    public function getNotPayoutScheduleList($kind) {
        return Schedule::find()->where(["status" => C::SCHEDULE_STATUS_WAIT, "kind" => $kind])->andWhere("result_nums IS NOT NULL")
            //->andWhere(["<=", "open_time", TimeUtil::now()])
            ->all();
    }

    /**
     * 获取所有过去未有结果的期数
     * @return Schedule[]
     */
    public function getNotResultScheduleMap($kind) {
        $scheduleList = Schedule::find()->andWhere(["kind" => $kind])->andWhere("result_nums is NULL")->andWhere(["<=", "open_time", TimeUtil::now()])->all();
        $scheduleMap = [];
        foreach ($scheduleList as $schedule) {
            $scheduleMap[$schedule->period] = $schedule;
        }
        return $scheduleMap;
    }

    /**
     * 更新时时彩记录
     * @param SscDataVo $sccDataVo
     * @throws \Throwable
     */
    public function updateSSCResult(SscDataVo $sccDataVo) {
        $historyMap = $sccDataVo->getHistoryCellVosMap();
        $scheduleMap = $this->getNotResultScheduleMap(C::KIND_SSC_CHONGQING);
        foreach ($scheduleMap as $schedule) {
            $period = $schedule->period;
            if (isset($historyMap[$period])) {
                $sscCellVo = $historyMap[$period];
                $schedule->setResultNums(str_split($sscCellVo->result));
                $schedule->save();
                DLogger::info("更新了期数:" . $schedule->id . ' ' . $schedule->period . "的结果为" . $sscCellVo->result, "重庆时时彩取结果");
            }
        }


//
//        $latestEntry = Schedule::find()->orderBy('id DESC')->offset(0)->limit(1)->all();
//        $latestEntry = isset($latestEntry[0]) ? $latestEntry[0] : NULL;
//        /** 更新已开奖记录*/
//        foreach (array_reverse($sccDataVo->historyCellVos) as $record) {
//            //完善期数
//            $period = $this->perfectPeriod($record->period);
//            if ($latestEntry && ($latestEntry->period > $period)) {
//                continue;
//            }
//            if ($latestEntry && ($latestEntry->period == $period)) {
//                $latestEntry->status = C::SCHEDULE_STATUS_OPEN;
//                $latestEntry->result_nums = $this->setResultNums(str_split((string)$record->result));
//                if (!$latestEntry->update()) {
//                    throw new SystemException($latestEntry->getFirstError());
//                }
//                continue;
//            }
//            $kind = C::KIND_SSC_CHONGQING;
//            $kindName = C::getKindLabel(C::KIND_SSC_CHONGQING);
//            $status = C::SCHEDULE_STATUS_OPEN;
//            $this->keepingRecords($kind, $kindName, $record->openTime, $period, $status, $record->result);
//        }
//        /** 更新下一期开奖记录 */
//        if ($sccDataVo->nextPeriod && $sccDataVo->nextOpenTime) {
//            $kind = C::KIND_SSC_CHONGQING;
//            $kindName = C::getKindLabel(C::KIND_SSC_CHONGQING);
//            $nextPeriod = $this->perfectPeriod($sccDataVo->nextPeriod);
//            $nextStatus = C::SCHEDULE_STATUS_WAIT;
//            ($nextPeriod != $latestEntry->period) && $this->keepingRecords($kind, $kindName, $sccDataVo->nextOpenTime, $nextPeriod, $nextStatus);
//        }
    }

    /**
     * 生成私彩数据
     * @param int $kind 彩票类型
     * @throws SystemException
     */
    public function generatePersonSchedule($kind) {
        // 曼谷彩周期
        $secs = P::SC_PERSONAL_PERIOD_SEC;

        $lastSchedule = $this->getLastSchedule($kind);
        // 开始生成
        $startTime = $lastSchedule !== null ? TimeUtil::getTimeAfterSecond($lastSchedule->open_time, $secs) : TimeUtil::getTodayStart();
        // 结束生成时间（当前时间再生成两次）
        // $now = TimeUtil::now();
        // $endTime = TimeUtil::getTimeAfterSecond($now, $secs * 2);
        $endTime = TimeUtil::getTimeAfterSecond(TimeUtil::getTodayEnd(), 86400);
        for ($openTime = $startTime; $openTime <= $endTime; $openTime = TimeUtil::getTimeAfterSecond($openTime, $secs)) {
            // 是否以开奖
            //$isOpen = $openTime < $now - C::SCHEDULE_OPEN_DELAY;

            $schedule = new Schedule();
            $schedule->kind = $kind;
            $schedule->kind_name = C::getKindLabel($kind);
            $schedule->open_time = $openTime;
            $schedule->period = Schedule::getPersonalPeriod($openTime);
            $schedule->status = C::SCHEDULE_STATUS_WAIT; //$isOpen ? C::SCHEDULE_STATUS_OPEN : C::SCHEDULE_STATUS_WAIT
            $schedule->active = C::ACTIVE_YES;
            $schedule->setPredictNums(Schedule::getRandomResultNums());

            if (!$schedule->save()) {
                throw new SystemException($schedule->getFirstError());
            }
        }
    }


    /**
     * 生成公彩数据
     * @param int $kind 彩票类型
     * @throws SystemException
     */
    public function generateOfficialSchedule($kind) {
        $cfgKind = Kind::findOne($kind);
        $secs = $cfgKind->minute * 60;

        $lastSchedule = $this->getLastSchedule($kind);
        // 开始生成
        $startTime = $lastSchedule !== null ? TimeUtil::getTimeAfterSecond($lastSchedule->open_time, $secs) : TimeUtil::today() . ' ' . $cfgKind->start_time;

        // 结束生成时间（当前时间再生成两次）
        // $now = TimeUtil::now();
        // $endTime = TimeUtil::getTimeAfterSecond($now, $secs * 2);
        $endTime = TimeUtil::getTimeAfterSecond(TimeUtil::today() . ' ' . $cfgKind->end_time, 86400);
        $count = 0;
        for ($openTime = $startTime; $openTime <= $endTime; $openTime = TimeUtil::getTimeAfterSecond($openTime, $secs)) {
            //暂时处理吧
            if (StringUtil::endsWith($openTime, '00:10:00')) {
                continue;
            }
            $His = date('H:i:s', strtotime($openTime));
            // 重庆时时彩这段时间不开
            if ($His > '03:10:00' && $His < '07:30:00') {
                continue;
            }
            $schedule = new Schedule();
            $schedule->kind = $kind;
            $schedule->kind_name = C::getKindLabel($kind);
            $schedule->open_time = $openTime;
            $schedule->period = Schedule::getOfficialPeriod($openTime, $cfgKind->start_time);
            $schedule->status = C::SCHEDULE_STATUS_WAIT; //$isOpen ? C::SCHEDULE_STATUS_OPEN : C::SCHEDULE_STATUS_WAIT
            $schedule->active = C::ACTIVE_YES;
            $schedule->predict_nums = null;
            if (!$schedule->save()) {
                throw new SystemException($schedule->getFirstError());
            }
            $count++;
        }

        $msg = " 生成公彩数据>> 开始时间:$startTime  结束时间:$endTime 影响行数:" . $count;
        echo $msg;
        if($count>0){
            DLogger::info($msg, "生成公彩数据");
        }
    }

    /**
     * 获取最近的期数
     * @param $kind
     * @return Schedule|null
     */
    private function getLastSchedule($kind) {
        return Schedule::find()->where(["kind" => $kind])->orderBy(["open_time" => SORT_DESC])->one();
    }

    /**
     * 私彩生成结果
     * @param int $kind 私彩类型
     * @throws SystemException
     */
    public function openPersonalSchedule($kind) {
        /** @var Schedule[] $scheduleList */
        $query = Schedule::find()
            ->where(["kind" => $kind, "status" => C::SCHEDULE_STATUS_WAIT])
            ->andWhere(["<=", "open_time", TimeUtil::getNowBefore(C::SCHEDULE_OPEN_DELAY)]);
        //Debug::showLastSql($query);
        $scheduleList = $query->all();
        foreach ($scheduleList as $schedule) {
            if ($schedule->hasResult()) {
                continue;
            }
            if ($schedule->predict_nums) {
                $resultNums = $schedule->getPredictNums();
            } else {
                $resultNums = $schedule::getRandomResultNums();
            }
            $schedule->setResultNums($resultNums);
            //$schedule->payout_time = TimeUtil::now();
            if (!$schedule->save()) {
                throw new SystemException($schedule->getFirstError());
            }
        }
    }

    /**
     * 保存记录
     * @param $kind
     * @param $kindName
     * @param $openTime
     * @param $period
     * @param $status
     * @param null $resultNums
     * @throws SystemException
     */
    public function keepingRecords($kind, $kindName, $openTime, $period, $status, $resultNums = NULL) {
        $schedule = new Schedule();
        $schedule->kind = $kind;
        $schedule->kind_name = $kindName;
        $schedule->open_time = $openTime;
        $schedule->period = $period;
        $schedule->status = $status;
        $schedule->active = C::TRUE;
        $schedule->result_nums = $this->setResultNums(str_split($resultNums));
        if (!$schedule->save()) {
            throw new SystemException($schedule->getFirstError());
        }
    }

    /**
     * 获取最新一期未开奖的记录
     * @param $kind
     * @return Schedule|null
     */
    public function getNowSchedule($kind) {
        $now = TimeUtil::now();

        $schedule = Schedule::find()->where(["kind" => $kind])
            ->andWhere([">=", "open_time", $now])
            ->orderBy(["open_time" => SORT_ASC])->one();
        if ($schedule && empty($schedule->getRemainBetSeconds())) {
            // 当前已经停止下注。获取下一期
            $schedule = $schedule->findNextSchedule();
        }
        return $schedule;
    }

    /**
     * 获取上期开奖记录
     * @param $kind
     * @return Schedule|null
     */
    public function getPastSchedule($kind) {
        $now = TimeUtil::now();

        $schedule = Schedule::find()->where(["kind" => $kind])
            ->andWhere(["<", "open_time", $now])
            ->orderBy(["open_time" => SORT_DESC])->one();
        return $schedule;
    }

    /**
     * 获取往期开奖结果列表
     * @param $kind
     * @param $size
     * @param $offset
     * @return ScScheduleVo[]
     */
    public function getOldScheduleVoList($kind, $size, $offset) {
        $query = Schedule::find()->where(["kind" => $kind]);
        $query->andWhere((["<", "open_time", TimeUtil::now()]));
        $query->orderBy(["open_time" => SORT_DESC]);

        $count = (int)$query->count();
        /** @var CashRecord[] $cashRecordList */
        $cashRecordList = $query->offset($offset)->limit($size)->all();
        $voList = [];
        foreach ($cashRecordList as $schedule) {
            $vo = new ScScheduleVo();
            $vo->initBySchedule($schedule);
            $voList[] = $vo;
        }
        return [
            "cashRecordList" => $voList,
            "count" => $count
        ];
    }


    /**
     * @param $nums
     * @return string
     */
    public function setResultNums($nums) {
        return implode("@", $nums);
    }

    /**
     * 获取私彩数据
     * @param int $kind C::KIND_SSC_*
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getScheduleTable($kind, $offset, $limit, $kind, $period, $date, $recently) {
        $query = Schedule::find();
        if ($kind) {
            $query->andWhere(['kind' => $kind]);
        }
        if ($period) {
            $query->andWhere(['period' => $period]);
        }
        if ($date) {
            $startTime = $date . ' 00:00:00';
            $endTime = $date . ' 23:59:59';
            $query->andWhere("open_time>='$startTime' AND open_time<='$endTime'");
        }

        if ($recently == "true") {
            $startTime = TimeUtil::getNowAfter(P::SC_PERSONAL_PERIOD_SEC * 2);
            $query->andWhere("open_time<='$startTime'");
        }

        $count = (int)$query->count();
        /** @var Schedule[] $scheduleList */
        $query->orderBy(["open_time" => SORT_DESC])->offset($offset)->limit($limit);
        // Debug::showLastSql($query);
        $scheduleList = $query->all();
//        if (count($scheduleList) >= 2) {
//            if ($scheduleList[0]->status == C::SCHEDULE_STATUS_WAIT && $scheduleList[1]->status == C::SCHEDULE_STATUS_WAIT) {
//                array_shift($scheduleList); //移掉一个未开奖的 by lizr
//            }
//        }

        $scheduleVoList = [];
        foreach ($scheduleList as $schedule) {
            $vo = new ScheduleRowVo();
            $vo->initBySchedule($schedule);
            $scheduleVoList[] = $vo;
        }

        return [
            "count" => $count,
            "rows" => $scheduleVoList,
        ];
    }

    /**
     * 预设结果
     * @param $scheduleId
     * @param $result
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function predictScheduleResult($scheduleId, $result) {
        $schedule = Schedule::findOne($scheduleId);
        if ($schedule === null) {
            throw new SystemException("记录不存在");
        }
        if ($schedule->kind !== C::KIND_SSC_BANGKOK && $schedule->kind !== C::KIND_SSC_BERLIN) {
            throw new ForeseeableException("只有曼谷时时彩和柏林五分彩才能修改结果");
        }
        if (strlen($result) !== 5 || !is_numeric($result)) {
            throw new ForeseeableException("结果必须是0-9组成的5位数字");
        }
        if ($schedule->status !== C::SCHEDULE_STATUS_WAIT) { //$schedule->open_time < TimeUtil::now() ||
            throw new ForeseeableException("已开奖，无法修改");
        }

        $resultNums = str_split($result);
        $schedule->setPredictNums($resultNums);
        if (!$schedule->save()) {
            throw new SystemException($schedule->getFirstError());
        }
    }

}
