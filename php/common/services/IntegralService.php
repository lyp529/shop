<?php


namespace common\services;

use common\constants\C;
use common\constants\L;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\LuckRun;
use common\traits\InstanceTrait;
use common\utils\game\LogUtil;
use common\utils\game\UserUtil;
use common\utils\YiiUtil;
use Throwable;

/**
 * Class IntegralService 积分功能
 * @package common\services
 */
class IntegralService extends BaseService
{
    use InstanceTrait;

    /**
     * 计算获取积分 向下取整 1:1
     * @param $integral
     * @return mixed
     */
    public function calculationIntegral($integral) {
        return floor($integral);
    }

    /**
     * 积分兑换RMB
     * @param $integral
     * @return int
     */
    public function exchangeRmb($integral) {
        return (int)(floor($integral / C::INTEGRAL_RATE));
    }

    /**
     * 获取用户积分、兑换rmb
     * @return array
     */
    public function getIntegral() {
        $integral = UserUtil::getUser()->getMember()->integral;
        return ['integral' => $integral, 'rmb' => $this->exchangeRmb($integral)];
    }

    /**
     * 积分兑换信用额
     * @param $integral
     * @return int
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     */
    public function exchangeCredit($integral) {
        if ($integral < C::INTEGRAL_RATE) {
            throw new ForeseeableException("积分不能低于" . C::INTEGRAL_RATE);
        }
        $exchangeCredit = $this->exchangeRmb($integral);
        $exchangeIntegral = $exchangeCredit * C::INTEGRAL_RATE;

        $user = UserUtil::getUser();
        $integral = $user->getMember()->integral;
        if ($integral < $exchangeIntegral) {
            throw new ForeseeableException("用户积分不足");
        }
        $user->cutIntegral($exchangeIntegral);
        $user->addCredit($exchangeCredit);
        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }
        LogUtil::cLog(L::CLOG_INTEGRAL, $user, $exchangeCredit);
        return $user->getMember()->integral;
    }

    /**
     * 获取幸运大转盘数据列
     * @return array
     */
    public function getLuckRunList() {
        $luckRunList = LuckRun::find()->all();
        foreach ($luckRunList as $value) {
            $value->reward_type = C::getLuckRunTypeLabel($value->reward_type);
        }
        return $luckRunList;
    }

    /**
     * 修改幸运大转盘信息
     * @param $id
     * @param $rate
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function editLuckRun($id, $rate) {
        $run = LuckRun::findOne($id);
        if (!$run) {
            throw new ForeseeableException("该轮盘奖励类型不存在");
        }
        $run->rate = (int)$rate;
        if (!$run->update()) {
            throw new SystemException($run->getFirstError());
        }
    }

    /**
     * 获取幸运大转盘概率结果
     * @return int|mixed
     * @throws ForeseeableException
     */
    public function getLuckRunResult() {
        $luckRunList = LuckRun::find()->all();
        if (!$luckRunList) {
            throw new ForeseeableException("轮盘奖励异常");
        }
        $luckRunId = 1;
        $sumRate = 0;
        foreach ($luckRunList as $value) {
            $sumRate += $value->rate;
        }
        $luckyNum = mt_rand(1, $sumRate);
        $endCursor = 0;
        foreach ($luckRunList as $value) {
            $beginCursor = $endCursor;
            $endCursor += $value->rate;
            if ($luckyNum > $beginCursor && $luckyNum <= $endCursor) {
                $luckRunId = $value->id;
                break;
            }
        }
        return $luckRunId;
    }

    /**
     * 参加幸运大转盘 -- 开启
     * @return int|mixed
     * @throws ForeseeableException
     * @throws Throwable
     */
    public function luckyRunStart() {
        $user = UserUtil::getUser();
        $user->cutIntegral(C::INTEGRAL_LUCKY_RUN_NEED);
        if (!$user->save()){
            throw new SystemException($user->getFirstError());
        }
        $luckRunId = $this->getLuckRunResult();
        YiiUtil::session(C::SESSION_KEY_LUCKY_RUN, $luckRunId);
        return $luckRunId - 1;
    }

    /**
     * 参加幸运大转盘 -- 结束
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     */
    public function luckyRunEnd() {
        $user = UserUtil::getUser();
        $luckRunId = YiiUtil::session(C::SESSION_KEY_LUCKY_RUN);
        if ($luckRunId <= 0) {
            throw new ForeseeableException("操作失败");
        }
        $luckRun = LuckRun::findOne($luckRunId);
        switch ($luckRun->reward_type) {
            case C::REWARD_TYPE_INTEGRAL:
                $user->addIntegral($luckRun->reward_value);
                break;
            case C::REWARD_TYPE_CREDIT:
                $user->addCredit($luckRun->reward_value);
                LogUtil::cLog(L::CLOG_LUCK_RUN, $user, $luckRun->reward_value);
                break;
        }
        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }
        YiiUtil::session(C::SESSION_KEY_LUCKY_RUN, 0);
        return $luckRun->info;
    }
}
