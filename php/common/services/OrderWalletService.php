<?php


namespace common\services;


use common\constants\C;
use common\constants\L;
use common\exceptions\ForeseeableException;
use common\models\db\CashRecord;
use common\models\UserModel;
use common\models\vo\api\BankPayRequestVo;
use common\models\vo\api\BankWithdrawRequestVo;
use common\models\vo\api\OfflineQrCodeRequestVo;
use common\models\vo\api\QrCodePayRequestVo;
use common\models\vo\frontend\CashRecordRowVo;
use common\models\vo\order\OrderWalletVo;
use common\traits\InstanceTrait;
use common\models\db\OrderWallet;
use common\models\db\Goods;
use common\models\db\Order;
use common\models\db\Award;
use common\models\db\CashRewards;
use common\models\db\UserMember;
use common\exceptions\SystemException;
use common\utils\DLogger;
use common\utils\game\LogUtil;
use common\utils\TimeUtil;
use frontend\models\form\OrderForm;
use common\utils\game\UserUtil;
use yii\base\UserException;

/**
 * Class OrderWalletService 钱包订单（充值、提现）
 * @package common\services
 */
class OrderWalletService extends BaseService
{
    use InstanceTrait;

    public function getOrderList() {
        $userId = UserUtil::getUserId();
        $query = Order::find();
        $query->select(["id", "order_number", "order_state", "order_time", "goods_name"]);
        $query->andWhere(["user_id" => $userId]);
        $query->orderBy("id DESC");
        $count = $query->count();
        $orderList = $query->all();

        foreach ($orderList as $k => $v) {
            $orderList[$k]->order_time = date("m-d H:i",strtotime($v->order_time));
        }
        return ['list' => $orderList, 'count' => $count];
    }
    public function getOrderList1() {
        $userId = UserUtil::getUserId();
        $query = Order::find();
        $query->select(["id", "order_number", "order_state", "order_time", "goods_name"]);
        $query->andWhere(["user_id" => $userId, "order_state" => 1]);
        $query->orderBy("id DESC");
        $count = $query->count();
        $orderList = $query->all();

        foreach ($orderList as $k => $v) {
            $orderList[$k]->order_time = date("m-d H:i",strtotime($v->order_time));
        }
        return ['list' => $orderList, 'count' => $count];
    }
    public function getOrderList2() {
        $userId = UserUtil::getUserId();
        $query = Order::find();
        $query->select(["id", "order_number", "order_state", "order_time", "goods_name"]);
        $query->andWhere(["user_id" => $userId, "order_state" => 2]);
        $query->orderBy("id DESC");
        $count = $query->count();
        $orderList = $query->all();

        foreach ($orderList as $k => $v) {
            $orderList[$k]->order_time = date("m-d H:i",strtotime($v->order_time));
        }
        return ['list' => $orderList, 'count' => $count];
    }
    /**
     * 兑换商品
     * @param $id
     * @throws ForeseeableException
     * @throws SystemException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function forGoods($id){
        $goods = Goods::findOne($id);
        if (!$goods) {
            throw new ForeseeableException("商品不存在");
        }
        $userId = UserUtil::getUserId();
        $user = UserMember::findOne($userId);
        if (!$user) {
            throw new ForeseeableException("用户错误");
        }
        if ($user->integral < $goods->credits){
            throw new ForeseeableException("用户积分不足");
        }
        if ($user->vip_level < 2){
            throw new ForeseeableException("只有VIP3才能拥有购买权力");
        }
        $this->addOrder($userId,$id,$goods->credits,$goods->name);

        //扣积分
        $user->integral = (string)($user->integral - $goods->credits);
        if (!$user->update()) {
            throw new SystemException($user->getFirstError());
        }
        //用户奖励
        $this->award($user,$goods->bonus_points);

        //分配奖励中心
        $this->allAward($goods->bonus_points);
    }

    /**
     * 分配奖励中心
     * @param $bonusPoints
     * @throws SystemException
     * @throws \Throwable
     */
    public function allAward($bonusPoints){
        $award = Award::findOne(1);
        $value = json_decode($award->value);
        $team= 0;
        $logistics= 0;
        $director= 0;
        $manager= 0;
        $majordomo= 0;
        $chairman= 0;
        $garage= 0;
        $cashRewards = CashRewards::find()->all();
        foreach ($cashRewards as $v) {
            $v->key === 'TEAM' && $team = $bonusPoints * $v->value;
            $v->key === 'LOGISTICS' && $logistics = $bonusPoints * $v->value;
            $v->key === 'DIRECTOR' && $director = $bonusPoints * $v->value;
            $v->key === 'MANAGER' && $manager = $bonusPoints * $v->value;
            $v->key === 'MAJORDOMO' && $majordomo = $bonusPoints * $v->value;
            $v->key === 'CHAIRMAN' && $chairman = $bonusPoints * $v->value;
            $v->key === 'GARAGE' && $garage = $bonusPoints * $v->value;
        }
        $value->team = $value->team + $team; //团队
        $value->logistics = $value->logistics + $logistics; //物流
        $value->director = $value->director + $director; //主管
        $value->manager = $value->manager + $manager; //经理
        $value->majordomo = $value->majordomo + $majordomo; //总监
        $value->chairman = $value->chairman + $chairman; //董事
        $value->garage = $value->garage + $garage; //车房

        $award->value = json_encode($value);
        if (!$award->update()) {
            throw new SystemException($award->getFirstError());
        }
    }
    /**
     * 用户奖励
     * @param UserMember $user
     * @param $bonusPoints
     * @throws SystemException
     */
    public function award(UserMember $user,$bonusPoints){
        $bonus = 0;
        $credit = (int)$user->credit;
        if ($credit < 300000){
            $bonus = $bonusPoints * 0.09;
        } elseif ($credit >= 300000 && $credit < 1000000) {
            $bonus = $bonusPoints * 0.11;
        } elseif ($credit >= 1000000 && $credit < 3000000) {
            $bonus = $bonusPoints * 0.13;
        } elseif ($credit >= 3000000 && $credit < 6000000) {
            $bonus = $bonusPoints * 0.15;
        } elseif ($credit >= 6000000 && $credit < 10000000) {
            $bonus = $bonusPoints * 0.17;
        } elseif ($credit === 10000000) {
            $bonus = $bonusPoints * 0.19;
        } elseif ($credit > 10000000) {
            $bonus = $bonusPoints * 0.21;
        }

        $user->credit = $user->credit + $bonus;
        $user->integral = $user->integral + $bonusPoints * 0.25;
        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }
    }

    /**
     * 添加订单
     * @param $userId
     * @param $goodsId
     * @param $integral
     * @param $goodsName
     * @throws SystemException
     */
    public function addOrder($userId,$goodsId,$integral,$goodsName){
        $order = new Order();
        $order->user_id = $userId;
        $order->goods_id = $goodsId;
        $order->order_number = time().mt_rand(1000, 9999);
        $order->order_state = 1;
        $order->goods_name = $goodsName;
        $order->integral = $integral;
        $order->order_time = TimeUtil::now();
        $order->record_date = TimeUtil::now();

        if (!$order->save()) {
            throw new SystemException($order->getFirstError());
        }
    }


    /**
     * 获取订单列表
     * @param $startTime
     * @param $endTime
     * @param $orderState
     * @param $orderNumber
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getWalletOrderList($startTime, $endTime, $orderState, $orderNumber, $offset, $limit) {
		$query = Order::find();
		$startTime && $query->andWhere([">=", "order_time", $startTime]);
		$endTime && $query->andWhere(["<=", "order_time", $endTime]);
		$orderState && $query->andWhere(["order_state" => $orderState]);
		$orderNumber && $query->andWhere(["order_number" => $orderNumber]);
		$query->orderBy("id DESC");
		$count = $query->count();
		$orderList = $query->offset($offset)->limit($limit)->all();

		$orderFormList = [];
		foreach ($orderList as $val) {
			$orderForm = new OrderWalletVo();
			$user = UserModel::getUser(1, $val->user_id);
			$orderForm->initByOrderWallet($val,$user->username);
			$orderFormList[] = $orderForm;
		}
		return ['rows' => $orderFormList, 'count' => $count];
    }

    /**
     * @param OrderForm $form
     * @param UserModel $user
     * @return string 二维码访问地址
     * @throws ForeseeableException
     * @throws SystemException
     */
//    public function addDepositOrderByOfflineQr(OrderForm $form, UserModel $user) {
//        $account = $user->getAccount();
//        $orderWallet = new OrderWallet();
//        $orderWallet->user_id = $account->id;
//        $orderWallet->order_type = C::ORDER_WALLET_TYPE_DEPOSIT;
//        $orderWallet->order_no = OrderWallet::getOrderNo($orderWallet->order_type);
//        $orderWallet->order_sub_type = $form->orderSubType;
//        $orderWallet->order_state = C::ORDER_WALLET_STATE_WAIT;
//        $orderWallet->order_time = TimeUtil::now();
//        $orderWallet->amount = $form->amount;
//        $orderWallet->pay_type = SM::getApiService()->getPayType();
//        $orderWallet->remark = $form->remark;
//        if (!$orderWallet->save()) {
//            throw new SystemException($orderWallet->getFirstError());
//        }
//        $requestVo = new OfflineQrCodeRequestVo();
//        $requestVo->userId = $orderWallet->user_id;
//        $requestVo->username = $account->username;
//        $requestVo->amount = $orderWallet->amount;
//        $requestVo->orderNo = $orderWallet->order_no;
//        $requestVo->payType = $orderWallet->pay_type;
//
//        $responseVo = SM::getApiService()->getOfflineQrCode($requestVo);
//        return $responseVo->CodeUrl;
//    }

    /**
     * 添加线下银行存款的订单
     * @param OrderForm $form
     * @param UserModel $user
     * @throws SystemException
     */
    public function addDepositOrderByOfflineBank(OrderForm $form, UserModel $user) {
        // 生成本地订单
        $orderWallet = new OrderWallet();
        $orderWallet->user_id = $user->userId;
        $orderWallet->order_type = C::ORDER_WALLET_TYPE_DEPOSIT;
        $orderWallet->order_no = OrderWallet::getOrderNo($orderWallet->order_type);
        $orderWallet->order_sub_type = $form->orderSubType;
        $orderWallet->order_state = C::ORDER_WALLET_STATE_WAIT;
        $orderWallet->order_time = TimeUtil::now();
        $orderWallet->amount = $form->amount;
        $orderWallet->pay_type = 0;
        $orderWallet->pay_username = $form->payUsername;
        $orderWallet->remark = $form->remark;
        $orderWallet->agent_id=$user->agentId;
        $orderWallet->vendor_id=$user->vendorId;
        $orderWallet->holder_id=$user->holderId;
        $orderWallet->super_holder_id=$user->superHolderId;

        if (!$orderWallet->save()) {
            throw new SystemException($orderWallet->getFirstError());
        }

        $account = $user->getAccount();

        // 请求支付平台
        $requestVo = new BankPayRequestVo();
        $requestVo->amount = $form->amount;
        $requestVo->recvName = $form->recvName;
        $requestVo->recvAccount = $form->recvAccount;
        $requestVo->recvUsername = $form->recvUsername;
        $requestVo->payUsername = $form->payUsername;
        $requestVo->payTime = TimeUtil::now();
        $requestVo->order3rdNo = $orderWallet->order_no;
        $requestVo->memberCode = $account->username;
        $requestVo->nickname = $account->name;
        SM::getApiService()->bankPay($requestVo);
    }

    /**
     * 上传的二围码收款
     * @param OrderForm $form
     * @param UserModel $user
     * @return string 二维码地址
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function addDepositOrderByQrCode(OrderForm $form, UserModel $user) {
        $qrCodePayType = $this->getQrCodePayType($form->orderSubType);

        // 生成本地订单
        $orderWallet = new OrderWallet();
        $orderWallet->user_id = $user->userId;
        $orderWallet->order_type = C::ORDER_WALLET_TYPE_DEPOSIT;
        $orderWallet->order_no = OrderWallet::getOrderNo($orderWallet->order_type);
        $orderWallet->order_sub_type = $form->orderSubType;
        $orderWallet->order_state = C::ORDER_WALLET_STATE_WAIT;
        $orderWallet->order_time = TimeUtil::now();
        $orderWallet->amount = $form->amount;
        $orderWallet->pay_type = $qrCodePayType;
        $orderWallet->pay_username = $form->payUsername;
        $orderWallet->remark = $form->remark;
        $orderWallet->agent_id=$user->agentId;
        $orderWallet->vendor_id=$user->vendorId;
        $orderWallet->holder_id=$user->holderId;
        $orderWallet->super_holder_id=$user->superHolderId;
        if (!$orderWallet->save()) {
            throw new SystemException($orderWallet->getFirstError());
        }

        $account = $user->getAccount();
        $responseVo = new QrCodePayRequestVo();
        $responseVo->type = $qrCodePayType;
        $responseVo->amount = $orderWallet->amount;
        $responseVo->orderNo = $orderWallet->order_no;
        $responseVo->memberCode = $account->username;
        $responseVo->nickname = $form->payUsername; // by ewing 10-17
        $data = SM::getApiService()->qrCodePay($responseVo);

        return $data["qrCodeUrl"];
    }

    /**
     * 添加提款订单
     * @param OrderForm $form
     * @param UserModel $user 当前登录的会员
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function addWithdrawOrder(OrderForm $form, UserModel $user) {
        if ($form->amount > $user->getCredit()) {
            throw new ForeseeableException("余额不足：{$form->amount}，无法提款");
        }
        $member = $user->getMember();

        $orderWallet = new OrderWallet();
        $orderWallet->user_id = $user->userId;
        $orderWallet->order_type = C::ORDER_WALLET_TYPE_WITHDRAW;
        $orderWallet->order_no = OrderWallet::getOrderNo($orderWallet->order_type);
        $orderWallet->order_sub_type = C::ORDER_SUB_TYPE_WITHDRAW_OFFLINE_BANK;
        $orderWallet->order_state = C::ORDER_WALLET_STATE_WAIT;
        $orderWallet->order_time = TimeUtil::now();
        $orderWallet->amount = -$form->amount;
        $orderWallet->pay_type = SM::getApiService()->getPayType();
        $orderWallet->bank_name = $member->bank_name;
        $orderWallet->bank_account = $member->bank_account;
        $orderWallet->bank_username = $member->bank_username;
        $orderWallet->remark = $form->remark;
        $orderWallet->agent_id=$user->agentId;
        $orderWallet->vendor_id=$user->vendorId;
        $orderWallet->holder_id=$user->holderId;
        $orderWallet->super_holder_id=$user->superHolderId;
        if (!$orderWallet->save()) {
            throw new SystemException($orderWallet->getFirstError());
        }

        $amount = -abs($form->amount);
        $account = $user->getAccount();

        // 请求支付平台
        $requestVo = new BankWithdrawRequestVo();
        $requestVo->amount = $amount;
        $requestVo->recvName = $member->bank_name;
        $requestVo->recvAccount = $member->bank_account;
        $requestVo->recvUsername = $member->bank_username;
        $requestVo->order3rdNo = $orderWallet->order_no;
        $requestVo->memberCode = $account->username;
        $requestVo->nickname = $account->name;
        SM::getApiService()->bankWithdraw($requestVo);

        // 扣除用户余额
        $user->cutCredit($form->amount);
        // 增加本日取款次数
        $user->getMember()->addWithdrawTimes();
        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }
        LogUtil::cLog(L::CLOG_DRAW_COOL, $user, $amount, $orderWallet->order_no, [], "银行提现");
    }

    /**
     * 支付平台回调确认收款
     * @param $orderNo
     * @param $amount
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function ensurePay($orderNo, $amount) {
        $orderWallet = OrderWallet::findOne(["order_no" => $orderNo]);
        if ($orderWallet === null) {
            throw new SystemException("订单不存在");
        }
        if ($orderWallet->order_state !== C::ORDER_STATUS_WAIT) {
            throw new SystemException("订单已处理");
        }
        if (doubleval($orderWallet->amount) !== doubleval($amount)) {
            throw new SystemException("充值金额不一致");
        }


        if ($orderWallet->order_type === C::ORDER_WALLET_TYPE_WITHDRAW) {
            // 提款
            $orderWallet->order_state = C::ORDER_STATUS_PAY;
            $orderWallet->finish_time = TimeUtil::now();
            if (!$orderWallet->save()) {
                DLogger::error($orderWallet->getFirstError(), "OrderWalletService::ensurePay()");
                throw new SystemException("确认收款失败");
            }
        } else {
            // 存款
            $this->updateWalletState($orderWallet->id, C::ORDER_STATUS_PAY);
        }


    }

    /**
     * 支付平台回调取消.收款
     * @param $orderNo
     * @param string $remark 扣款原因
     * @throws SystemException
     */
    public function cancelPay($orderNo, $remark) {
        $orderWallet = OrderWallet::findOne(["order_no" => $orderNo]);
        if ($orderWallet === null) {
            throw new SystemException("订单不存在");
        }
        if ($orderWallet->order_state !== C::ORDER_STATUS_WAIT) {
            throw new SystemException("订单已处理");
        }

        $orderWallet->order_state = C::ORDER_WALLET_STATE_CANCEL;
        $orderWallet->remark = $remark;
        if (!$orderWallet->save()) {
            throw new SystemException($orderWallet->getFirstError());
        }

        if ($orderWallet->order_type === C::ORDER_WALLET_TYPE_WITHDRAW) {
            // 如果是提款。则返还余额给会员
            $user = UserModel::getUser(C::USER_MEMBER, $orderWallet->user_id);
            $user->addCredit(abs($orderWallet->amount));
            LogUtil::cLog(L::CLOG_DRAW_FAIL_BACK, $user, $orderWallet->amount, $orderNo);
            if (!$user->save()) {
                throw new SystemException($user->getFirstError());
            }
        }
    }

    public function updateWalletState($orderId, $adminId = 0) {
        $order = Order::findOne($orderId);
        if ($order->order_state !== C::ORDER_STATUS_WAIT) {
            throw new SystemException('该订单已被处理');
        }

        $order->order_state = 2;
        $adminId > 0 && $order->admin_id = (int)$adminId;
        $order->update_date = TimeUtil::now();
        if (!$order->save()) {
            throw new SystemException($order->getFirstError());
        }
    }

    /**
     * get交易列表
     * @param $userId
     * @param $startTime
     * @param $endTime
     * @param $orderType
     * @param $orderState
     * @return array
     */
    public function getOrderLists($userId, $startTime, $endTime, $orderType, $orderState) {

        $query = OrderWallet::find();
        $query->select(["user_id", "order_no", "order_type", "order_state", "order_time", "amount","remark"]);
        $startTime && $query->andWhere([">=", "order_time", $startTime . ' 00:00:00']);
        $endTime && $query->andWhere(["<=", "order_time", $endTime . ' 23:59:59']);
        $orderType && $query->andWhere(["order_type" => $orderType]);
        $orderState && $query->andWhere(["order_state" => $orderState]);
        $userId && $query->andWhere(["user_id" => $userId]);
        $count = $query->count();
        $query->orderBy("id DESC");
//        $orderList = $query->offset($offset)->limit($limit)->all();
        $orderList = $query->all();
        $orderFormList = [];

        foreach ($orderList as $val) {
            $orderForm = new OrderWalletVo();
            $orderForm->initByOrderWallet($val);
            $orderFormList[] = $orderForm;
        }

        return ["rows" => $orderFormList, "count" => $count];
    }

    /**
     * 【客户端】获取账变列表数据
     * @param $offset
     * @param $limit
     * @param UserModel $user
     * @param $logType
     * @param $startTime
     * @param $endTime
     * @return array
     */
    public function getCashRecordRowVoTable($offset, $limit, UserModel $user, $logType, $startTime, $endTime) {
        $query = CashRecord::find()->where(["member_id" => $user->userId]);
        if (!empty($logType)) {
            $query->andWhere(["clog_type" => $logType]);
        }
        if (!empty($startTime)) {
            $query->andWhere([">=", "record_time", $startTime]);
        }
        if (!empty($endTime)) {
            $query->andWhere(["<=", "record_time", $endTime]);
        }
        $query->orderBy(["record_time" => SORT_DESC]);

        $count = (int)$query->count();
        /** @var CashRecord[] $cashRecordList */
        $cashRecordList = $query->offset($offset)->limit($limit)->all();

        $rowVoList = [];
        foreach ($cashRecordList as $cashRecord) {
            $vo = new CashRecordRowVo();
            $vo->initByCashRecord($cashRecord);
            $rowVoList[] = $vo;
        }

        return [
            "count" => $count,
            "rows" => $rowVoList,
        ];
    }

    /**
     * 使用订单子类型获取对应的 api 二维码类型
     * @param $orderSubType
     * @return int
     */
    public function getQrCodePayType($orderSubType) {
        $map = [
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_WX => C::PAY_QR_CODE_WX,
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_ZFB => C::PAY_QR_CODE_ZFB,
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_YSF => C::PAY_QR_CODE_YSF,
        ];

        return isset($map[$orderSubType]) ? $map[$orderSubType] : 0;
    }
}
