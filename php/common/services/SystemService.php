<?php


namespace common\services;


use common\constants\C;
use common\constants\L;
use common\models\db\KeyValue;
use common\models\db\OrderWallet;
use common\models\db\EarnRank;
use common\models\db\EarnRankReal;
use common\models\db\EarnRankOffset;
use common\models\vo\backend\DepositTypeVo;
use common\traits\InstanceTrait;
use common\utils\TimeUtil;
use common\utils\ValueUtil;
use common\utils\YiiUtil;
use common\utils\game\LogUtil;
use Yii;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * Class SystemService 系统服务。一些系统性、没有具体模块的功能
 * @package common\services
 */
class SystemService extends BaseService
{
    use InstanceTrait;

    /**
     * 获取系统在线人数
     */
    public function getOnlineNum()
    {
        $num = YiiUtil::getCache(C::CACHE_KEY_ONLINE_NUM);
        if (empty($num)) {
            $num = (int)KeyValue::get(C::DB_KEY_ONLINE_NUM);
            YiiUtil::setCache(C::CACHE_KEY_ONLINE_NUM, $num, 60);
        }
        return $num;
    }

    /**
     * 获取提现排队人数
     */
    public function getWithdrawWaitNum()
    {
        $num = YiiUtil::getCache(C::CACHE_KEY_WITHDRAW_WAIT_NUM);
        if (empty($num)) {
            $num = (int)OrderWallet::find()->select(["user_id"])
                ->where(["order_type" => C::ORDER_WALLET_TYPE_WITHDRAW, "order_state" => C::ORDER_STATUS_WAIT])
                ->groupBy(["user_id"])->count();
            YiiUtil::setCache(C::CACHE_KEY_WITHDRAW_WAIT_NUM, $num, 30);
        }
        return $num;
    }

    /**
     * 刷新在线人数
     */
    public function refreshOnlineNum()
    {
        // 时间段在线人数配置
        $config = [
            "19:00-00:00" => [10000, 12000],
        ];
        // 默认最小值
        $min = 6000;
        // 默认最大值
        $max = 8000;
        // 当前时间戳
        $timestamp = time();
        // 根据配置重设最小值和最大值
        foreach ($config as $timeArr => $nums) {
            $tmpArr = explode("-", $timeArr);
            $startHourMin = $timeArr[0];
            $endHourMin = $timeArr[1];

            $startTimestamp = strtotime(TimeUtil::today() . " " . $startHourMin . ":00");
            $endTimestamp = strtotime(TimeUtil::today() . " " . $endHourMin . ":00");
            if ($startTimestamp > $endTimestamp) {
                $endTimestamp += 86400;
            }

            if ($timestamp > $startTimestamp && $timestamp < $endTimestamp) {
                $min = $nums[0];
                $max = $nums[1];
            }
        }


        $onlineNum = KeyValue::get(C::DB_KEY_ONLINE_NUM);
        $onlineNum = !empty($onlineNum) ? (int)$onlineNum : $min;

        $waveNum = rand(1, 20); // 波动数量
        if ($onlineNum < $min) {
            $isPlus = true;
        } else if ($onlineNum > $max) {
            $isPlus = false;
        } else {
            $isPlus = rand($min, $max) > $onlineNum;
        }

        if ($isPlus) {
            $onlineNum += $waveNum;
        } else {
            $onlineNum -= $waveNum;
        }

        KeyValue::set(C::DB_KEY_ONLINE_NUM, $onlineNum);
        YiiUtil::delCache(C::CACHE_KEY_ONLINE_NUM);
    }

    /**
     * 获取盈利排行榜/ offset
     * @return array
     */
    private function getEarnRank()
    {
        $earnRankOffset = new EarnRankOffset();
        $erOffset = $earnRankOffset::find()->orderBy(["er_offset" => SORT_DESC])->one();
        $offset = 0;
        //首次添加
        if ($erOffset == null) {
            $earnRankOffset->er_offset = (int)$offset;
            $earnRankOffset->record_time = TimeUtil::now();
        } elseif ($this->earnRankFrequency($erOffset->record_time)) {
            //1000条数据全部搜索完后，清空 er_offset
            if ($erOffset->er_offset > 1000) {
                $earnRankOffset::deleteAll();
                $erOffset = null;
            }
            $erOffset != null && $offset = ($erOffset->er_offset == 0) ? $erOffset->er_offset + 4 : $erOffset->er_offset + 5;

            $earnRankOffset->er_offset = (int)$offset;
            $earnRankOffset->record_time = TimeUtil::now();
        } else {
            $erOffset != null && $offset = (int)$erOffset->er_offset;
        }

        $earnRankOffset->save();

        return ['earnRankRealList' => $this->getEarnRankReal($erOffset), 'earnRankOffset' => $offset];
    }

    /**
     * 频率控制
     * @param $recordTime
     * @return bool
     */
    private function earnRankFrequency($recordTime)
    {
        return $recordTime ? (int)TimeUtil::getTwoTimeMin(TimeUtil::now(), $recordTime) >= C::EARN_RANK_FREQUENCY : false;
    }

    /**
     * 获取真实的盈利排行榜
     * @param EarnRankOffset $erOffset
     * @return array
     */
    public function getEarnRankReal(EarnRankOffset $erOffset)
    {
        if ($this->earnRankFrequency($erOffset->record_time)) {
            $earnRankReal = EarnRankReal::find()->select(['id'])->orderBy(["id" => SORT_ASC])->offset(0)->limit(5)->all();
            $id = $earnRankReal ? max($earnRankReal)->id : 0;
            EarnRankReal::deleteAll(["<=", "id", $id]);
        }
        return EarnRankReal::find()->orderBy(["id" => SORT_ASC])->offset(0)->limit(5)->all();
    }

    /**
     * 获取盈利排行榜信息
     * @return string
     */
    public function getEarnRankStr()
    {
        $earnRank = $this->getEarnRank();
        $earnRankList = EarnRank::find()->offset($earnRank['earnRankOffset'])->limit(5)->all();
        $earnRankAll = array_merge($earnRank['earnRankRealList'], $earnRankList);
        //初始化盈利排行榜
        if ($earnRankList == null) {
            SM::getActivityService()->initializeEarnRank();
        }
        $earnRankStr = '';
        foreach ($earnRankAll as $earnRankValue) {
            $username = $earnRankValue->username;
            strlen($username) === 6 && $username = mb_substr($username, 0, 1, 'utf-8') . '**';
            strlen($username) === 9 && $username = mb_substr($username, 0, 1, 'utf-8') . '*' . mb_substr($username, -1, 1, 'utf-8');
            strlen($username) > 9 && $username = mb_substr($username, 0, 3, 'utf-8') . '***' . mb_substr($username, -3, 3, 'utf-8');

            $erType = rand(0, 1);
            $earnRankStr .= '恭喜【' . $username . '】 在' . C::getKindLabel($erType ? $earnRankValue->er_type : C::KIND_SSC_CHONGQING) . '中盈利' . $earnRankValue->amount . '元　　　　';
        }
        return $earnRankStr;
    }

    /**
     * 获取存款类型开关数据
     * @return DepositTypeVo[]
     */
    public function getDepositTypeVoList()
    {
        $jsonStr = KeyValue::get(C::DB_KEY_DEPOSIT_TYPE_MAP);
        // 记录 [subType => bool(是否激活)]
        $depositTypeMap = [];
        if (empty($jsonStr)) {
            $this->initDepositTypeMap();
        } else {
            $depositTypeMap = Json::decode($jsonStr);
        }
        $subTypeLabels = C::getOrderSubTypeDepositLabel();

        $depositTypeVoList = [];
        foreach ($subTypeLabels as $subType => $label) {
            $active = isset($depositTypeMap[$subType]) ? ValueUtil::toBool($depositTypeMap[$subType]) : true;

            $depositTypeVo = new DepositTypeVo();
            $depositTypeVo->subType = (int)$subType;
            $depositTypeVo->label = $label;
            $depositTypeVo->active = $active;
            $depositTypeVoList[] = $depositTypeVo;
        }

        return $depositTypeVoList;
    }

    /**
     * 初始化村快类型 vo map
     */
    private function initDepositTypeMap()
    {
        $subTypeLabels = C::getOrderSubTypeDepositLabel();
        $depositTypeMap = [];
        foreach ($subTypeLabels as $subType => $label) {
            $depositTypeMap[intval($subType)] = $label;
        }
        KeyValue::set(C::DB_KEY_DEPOSIT_TYPE_MAP, Json::encode($depositTypeMap));
    }

    /**
     * 保存存款类型开关 vo map
     * @param DepositTypeVo[] $depositTypeVoList
     */
    public function saveDepositTypeVoList(array $depositTypeVoList)
    {
        //获取更新前的list，判断具体是哪个更新
        $getDepositTypeList = json_decode(KeyValue::get(C::DB_KEY_DEPOSIT_TYPE_MAP),true);
        $params = [];
        $depositTypeMap = [];
        foreach ($depositTypeVoList as $depositTypeVo) {
            $depositTypeVo->active != $getDepositTypeList[$depositTypeVo->subType] && $params = [$depositTypeVo->label.'：'.($depositTypeVo->active ? '开启' : '关闭')];
            $depositTypeMap[$depositTypeVo->subType] = $depositTypeVo->active;
        }
        KeyValue::set(C::DB_KEY_DEPOSIT_TYPE_MAP, Json::encode($depositTypeMap));

        LogUtil::plog(L::PLOG_ORDER_MOD_DEPOSIT_TYPE, $params);
    }

    /**
     * 过滤没有开启的支付方式
     */
    public function filterOrderSubTypeDepositLabels()
    {
        $depositTypeVoList = $this->getDepositTypeVoList();
        $filterSubTypeLabels = [];
        foreach ($depositTypeVoList as $depositTypeVo) {
            if ($depositTypeVo->active) {
                $filterSubTypeLabels[$depositTypeVo->subType] = $depositTypeVo->label;
            }
        }
        return $filterSubTypeLabels;
    }

    /**
     * 清理垃圾数据
     * @throws Exception
     */
    public function clearTrash()
    {
        $this->clearLogCronRecord();
    }

    /**
     * 清除超过7天，且运行没有问题的运行记录
     * @throws Exception
     */
    private function clearLogCronRecord()
    {
        $date = TimeUtil::getDateBeforeDays(7);
        $timestamp = strtotime($date);

        Yii::$app->db->createCommand("DELETE FROM log_cron_record WHERE reason = :reason AND record_time <= :time", [":reason" => C::CRON_REASON_SUCCESS, ":time" => $timestamp])->execute();
    }

    /**
     * 获取联系QQ号
     * @return string[]
     */
    public function getQQs() {
        $memberContactVo = KeyValue::getMemberContactVo();
        return $memberContactVo->qqs;
    }
}

