<?php


namespace common\services;


use backend\tools\AuthTool;
use common\constants\A;
use common\constants\C;
use common\exceptions\ForeseeableException;
use common\models\db\AuthItem;
use common\models\db\AuthRole;
use common\models\vo\auth\ActionVo;
use common\models\vo\auth\ModuleVo;
use common\traits\InstanceTrait;
use Throwable;

/**
 * Class AuthService 权限服务
 * @package common\services
 */
class AuthService extends BaseService {
    use InstanceTrait;

    /**
     * 获取用户可以分配的权限菜单列表（包含指定用户当前的权限）
     * @param $userId
     * @return ModuleVo[]
     * @throws ForeseeableException
     */
    public function getModuleVoList($userId) {
        $menuTree = SM::getUserService()->getMenuTree();

        $authItemMap = $this->getUserAuthItemMap($userId);

        $moduleVoList = [];
        foreach ($menuTree->children as $menu) {
            foreach ($menu->children as $subMenu) {
                // 只获取有配置权限的菜单数据
                if (!empty($subMenu->module)) {
                    $moduleVo = new ModuleVo();
                    $moduleVo->parentLabel = $menu->name;
                    $moduleVo->module = $subMenu->module;
                    $moduleVo->label = $subMenu->name;
                    foreach ($subMenu->actions as $action) {
                        $activeVo = new ActionVo();
                        $activeVo->action = $action;
                        $activeVo->label = A::getLabel($action);
                        $itemName = AuthTool::makeItemName($subMenu->module, $activeVo->action);
                        $activeVo->has = isset($authItemMap[$itemName]);
                        $moduleVo->actionVoList[] = $activeVo;
                    }
                    $moduleVoList[] = $moduleVo;
                }
            }
        }

        return $moduleVoList;
    }

    /**
     * @param $userId
     * @return AuthItem[] 以 itemName 为 key
     */
    public function getUserAuthItemMap($userId) {
        $authRole = AuthRole::findByUserId($userId);
        if ($authRole === null) {
            return [];
        }
        /** @var AuthItem[] $authItemList */
        $authItemList = AuthItem::findAll(["role_id" => $authRole->id]);
        $authItemMap = [];
        foreach ($authItemList as $authItem) {
            $authItemMap[$authItem->item_name] = $authItem;
        }
        return $authItemMap;
    }

    /**
     * 修改用户权限
     * @param $userId
     * @param ModuleVo[] $moduleVoList
     * @throws ForeseeableException
     * @throws Throwable
     */
    public function modUserAuth($userId, array $moduleVoList) {
        // 删除旧的权限数据重新写入
        $authRole = AuthRole::findByUserId($userId);

        if ($authRole === null) {
            $authRole = new AuthRole();
            $authRole->type = C::AUTH_ROLE_TYPE_USER;
            $authRole->bind_id = $userId;
            if ($authRole->insert() === false) {
                throw new ForeseeableException("权限角色创建失败。" . $authRole->getFirstError());
            }
        }
        AuthItem::deleteAll(["role_id" => $authRole->id]);

        // 写入新的权限数据
        foreach ($moduleVoList as $moduleVo) {
            foreach ($moduleVo->actionVoList as $actionVo) {
                if (!$actionVo->has) {
                    continue;
                }
                $itemName = AuthTool::makeItemName($moduleVo->module, $actionVo->action);
                $authItem = new AuthItem();
                $authItem->role_id = $authRole->id;
                $authItem->item_name = $itemName;
                if ($authItem->insert() === false) {
                    throw new ForeseeableException("权限数据写入失败。" . $authItem->getFirstError());
                }
            }
        }
    }
}