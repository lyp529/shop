<?php


namespace common\services;


use common\constants\L;
use common\constants\C;
use common\models\db\CashRecord;
use common\models\db\LogOperate;
use common\models\db\LogLogin;
use common\models\db\UserAccount;
use common\traits\InstanceTrait;
use common\utils\Debug;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;

/**
 * Class LogService 日志服务
 * @package common\services
 */
class LogService extends BaseService
{
    use InstanceTrait;


    /**
     * 帐变日志
     * @param $queryPlus
     * @param $startTime
     * @param $endTime
     * @param $offset
     * @param $limit
     * @param $amount
     * @return array
     */
    public function getCashRecordList($queryPlus, $startTime, $endTime, $offset, $limit, $amount) {
        if (empty($startTime) && empty($endTime)) {
            //TimeUtil::setToday($startTime, $endTime);
        }
        $query = CashRecord::find();
        if ($queryPlus) {
            $query->andWhere($queryPlus);
        }
        $startTime && $query->andWhere([">=", "record_time", $startTime]);
        $endTime && $query->andWhere(["<=", "record_time", $endTime]);
        $amount && $query->andWhere([">=", "amount", $amount]);
        $query->andWhere(UserUtil::getUserQuery());
        $count = $query->count();
        /** @var CashRecord[] $cashRecords */
        $cashRecords = $query->offset($offset)->limit($limit)->orderBy("id DESC")->all();
        $cashRecordList = [];
        foreach ($cashRecords as $cashRecord) {
            $logType = $cashRecord->clog_type;
            $data = $cashRecord->toArray();
            $data['recordNoLabel'] = L::getRecordNoLabel($logType) . $cashRecord->record_no;
            $data['typeLabel'] = L::getLabel($logType);
            $data['remark'] = $cashRecord->remark;
            $cashRecordList[] = $data;
        }
        return ["rows" => $cashRecordList, "count" => $count];
    }


    /**
     * 获取会员盈亏报表-总统计数据
     */
    public function getCashReportSum($startTime, $endTime) {
        $startTime .= ' 00:00:00';
        $endTime .= ' 23:59:59';
        $userId = UserUtil::getUserId();
        $query = CashRecord::find();
        $query->select(['sum(IF(amount>0,amount,0)) as income, sum(IF(amount<0,amount,0)) as outcome'])->asArray();
        $query->andWhere(['member_id' => $userId])->andWhere("record_time>='$startTime' AND record_time<='$endTime'"); //->groupBy('clog_type')
        $row = $query->one();
        if (empty($row)) {
            $row = [
                'income' => "0.00",
                'outcome' => "0.00",
            ];
        }
        return $row;
    }

    /**
     * 获取会员盈亏报表-分组统计数据
     */
    public function getCashReportGroup($startTime, $endTime) {
        $startTime .= ' 00:00:00';
        $endTime .= ' 23:59:59';
        $userId = UserUtil::getUserId();
        $query = CashRecord::find();
        $query->select(['sum(amount) as amount', 'clog_type'])->asArray();
        $query->andWhere(['member_id' => $userId])->andWhere("record_time>='$startTime' AND record_time<='$endTime'")->groupBy('clog_type');
        $rows = $query->all();
        return $rows;
    }

    /**
     * 盈亏统计
     * @param $queryPlus
     * @param $startTime
     * @param $endTime
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getReportSummary($queryPlus, $startTime, $endTime, $offset, $limit) {
        if (empty($startTime) && empty($endTime)) {
            TimeUtil::setToday($startTime, $endTime);
        }
        $query = CashRecord::find()->asArray();
        if ($queryPlus) {
            $query->andWhere($queryPlus);
        }
        $query->select(['sum(amount) AS total', 'clog_type', 'member_id', 'username']);
        $query->andWhere(['clog_type' => [L::CLOG_USER_PAY, L::CLOG_DRAW_OK]]);
        $startTime && $query->andWhere([">=", "record_time", $startTime]);
        $endTime && $query->andWhere(["<=", "record_time", $endTime]);
        $query->andWhere(UserUtil::getUserQuery());
        $query->groupBy('member_id');
        $count = $query->count();
        // Debug::showLastSql($query);
        $rows = $query->offset($offset)->limit($limit)->all();
        $dataMap = [];
        $allTotalIncome = $allTotalWithDraw = 0;
        foreach ($rows as $row) {
            $clogType = $row['clog_type'];
            if (!isset($dataMap[$row['member_id']])) {
                $dataMap[$row['member_id']] = ['INCOME' => 0, 'WITHDRAW' => 0, 'username' => $row['username']];
            }
            if ($clogType == L::CLOG_USER_PAY) {
                $dataMap[$row['member_id']]['INCOME'] += $row['total'];
                $allTotalIncome += $row['total'];
            } elseif ($clogType == L::CLOG_DRAW_OK) {
                $dataMap[$row['member_id']]['WITHDRAW'] += $row['total'];
                $allTotalWithDraw += $row['total'];
            }
        }
        $dataList = [];
        foreach ($dataMap as $key => $data) {
            $data['REST'] = $data['INCOME'] - $data['WITHDRAW'];
            $data['INCOME'] = credit_format($data['INCOME']);
            $data['WITHDRAW'] = credit_format($data['WITHDRAW']);
            $data['REST'] = credit_format($data['REST']);

            $dataList[] = $data;
        }
        $dataList[] = ['username' => '本页总计:', 'INCOME' => credit_format($allTotalIncome), 'WITHDRAW' => credit_format($allTotalWithDraw), 'REST' => credit_format($allTotalIncome - $allTotalWithDraw)];
        return ["rows" => $dataList, "count" => $count];
    }

    /**
     * 盈亏统计
     * @param $userType
     * @param $queryPlus
     * @param $startTime
     * @param $endTime
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getReportList($userType, $queryPlus, $startTime, $endTime, $offset, $limit) {
        $idField = UserUtil::getIdField($userType);
        $query = CashRecord::find()->asArray();
        if ($queryPlus) {
            $query->andWhere($queryPlus);
        }
        $query->select(['sum(amount) AS total', 'clog_type', 'member_id', 'agent_id', 'vendor_id', 'holder_id', 'super_holder_id', 'username']);
        $query->andWhere(['clog_type' => [L::CLOG_USER_PAY, L::CLOG_DRAW_OK]]);
        $startTime && $query->andWhere([">=", "record_time", $startTime]);
        $endTime && $query->andWhere(["<=", "record_time", $endTime]);
        $query->andWhere(UserUtil::getUserQuery());
        $query->groupBy($idField);
        $count = $query->count();
        //调试临时输出SQL
        //Debug::showLastSql($query);
        $rows = $query->offset($offset)->limit($limit)->all();
        $dataMap = [];
        $allTotalIncome = $allTotalWithDraw = 0;
        foreach ($rows as $row) {
            $userId = $row[$idField];
            $clogType = $row['clog_type'];
            if (!isset($dataMap[$userId])) {
                $username = $userType == C::USER_MEMBER ? $row['username'] : UserAccount::findOne(['id' => $userId])->username;
                $dataMap[$userId] = ['INCOME' => 0, 'WITHDRAW' => 0, 'username' => $username];
            }
            if ($clogType == L::CLOG_USER_PAY) {
                $dataMap[$userId]['INCOME'] += $row['total'];
                $allTotalIncome += $row['total'];
            } elseif ($clogType == L::CLOG_DRAW_OK) {
                $dataMap[$userId]['WITHDRAW'] += $row['total'];
                $allTotalWithDraw += $row['total'];
            }
        }
        $dataList = [];
        foreach ($dataMap as $key => $data) {
            $data['REST'] = $data['INCOME'] - $data['WITHDRAW'];
            $data['INCOME'] = credit_format($data['INCOME']);
            $data['WITHDRAW'] = credit_format($data['WITHDRAW']);
            $data['REST'] = credit_format($data['REST']);
            $data['lastColumn'] = false;

            $dataList[] = $data;
        }
        $dataList[] = ['username' => '本页总计:', 'INCOME' => credit_format($allTotalIncome), 'WITHDRAW' => credit_format($allTotalWithDraw), 'REST' => credit_format($allTotalIncome - $allTotalWithDraw),'lastColumn' => true];
        return ["rows" => $dataList, "count" => $count];
    }

    /**
     * 获取操作日志
     * @param $startTime
     * @param $endTime
     * @param $offset
     * @param $limit
     * @param $username
     * @return array
     */
    public function getOperateLog($startTime, $endTime, $offset, $limit, $username) {
        $query = LogOperate::find();
        $startTime && $query->andWhere([">=", "record_time", $startTime]);
        $endTime && $query->andWhere(["<=", "record_time", $endTime]);
        $username && $query->andWhere(["user_name" => $username]);
        $count = $query->count();
        $rows = $query->orderBy(["record_time" => SORT_DESC])->offset($offset)->limit($limit)->all();
        return ['rows' => $rows, 'count' => $count];
    }

    /**
     * 获取登录日志
     * @param $startTime
     * @param $endTime
     * @param $offset
     * @param $limit
     * @param $username
     * @return array
     */
    public function getLoginLog($startTime, $endTime, $offset, $limit, $username) {
        $query = LogLogin::find()->asArray();
        $startTime && $query->andWhere([">=", "record_time", $startTime]);
        $endTime && $query->andWhere(["<=", "record_time", $endTime]);
        $username && $query->andWhere(["username" => $username]);
        $count = $query->count();
        $rows = $query->orderBy(["record_time" => SORT_DESC])->offset($offset)->limit($limit)->all();
        foreach ($rows as $key => $value) {
            $rows[$key]['login_type_label'] = C::getLoginTypeLabel($value['login_type']);
        }
        return ['rows' => $rows, 'count' => $count];
    }

}
