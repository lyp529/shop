<?php


namespace common\services;

use common\constants\C;
use common\utils\game\UserUtil;
use common\models\db\Notice;
use common\utils\TimeUtil;
use common\exceptions\SystemException;
use common\exceptions\ForeseeableException;
use common\traits\InstanceTrait;
use Throwable;

/**
 * Class NoticeService 公告
 * @package common\services
 */
class NoticeService extends BaseService {
    use InstanceTrait;

    /**
     * 添加公告
     * @param $title
     * @param $content
     * @param $noticeType
     * @throws SystemException
     */
    public function addNotice($title,$content,$noticeType) {
        $notice = new Notice();
        $notice = Notice::updateNotice($notice,$title,$content,$noticeType,TimeUtil::now(),TimeUtil::now(),UserUtil::getUserId());
        if (!$notice->save()) {
            throw new SystemException($notice->getFirstError());
        }
    }

    /**
     * 编辑公告
     * @param $id
     * @param $title
     * @param $content
     * @param $noticeType
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function editNotice($id,$title,$content,$noticeType) {
        $notice = Notice::findOne($id);
        if (!$notice) {
            throw new ForeseeableException("公告不存在！");
        }
        $notice = Notice::updateNotice($notice,$title,$content,$noticeType,TimeUtil::now(),null,UserUtil::getUserId());
        if (!$notice->update()) {
            throw new SystemException($notice->getFirstError());
        }
    }

    /**
     * 获取公告列表
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getNoticeList($offset, $limit){
        $query = Notice::find();
        $count = $query->count();
        $noticeList = $query->offset($offset)->limit($limit)->all();
        foreach ($noticeList as $key => $value) {
            $value->notice_type = C::getNoticeTypeLabel($value->notice_type);
        }
        return ["rows" => $noticeList,"count" => $count];
    }

    /**
     * 删除公告
     * @param $noticeId
     * @throws ForeseeableException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delNotice($noticeId){
        $notice = Notice::findOne($noticeId);
        if (!$notice) {
            throw new ForeseeableException("公告不存在！");
        }
        if (!$notice->delete()) {
            throw new ForeseeableException("公告删除失败。" . $notice->getFirstError());
        }
    }

    /**
     * 获取会员网公告列表
     * @return Notice[]
     */
    public function getMemberNoticeList() {
        $noticeList = Notice::find()->where([
            "notice_type" => [C::NOTICE_ALL_SITES, C::NOTICE_MEMBER_NETWORK],
        ])->orderBy(["record_date" => SORT_DESC])->all();

        return $noticeList;
    }
}
