<?php


namespace common\services;


use backend\tools\AuthTool;
use backend\models\form\UserForm;
use common\constants\C;
use common\constants\L;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\Bet;
use common\models\db\UserAccount;
use common\models\db\UserAdmin;
use common\models\db\UserAgent;
use common\models\db\UserHolder;
use common\models\db\UserMember;
use common\models\db\UserSuperHolder;
use common\models\db\UserVendor;
use common\models\db\Vip;
use common\models\db\Award;
use common\models\db\Goods;
use common\models\db\Grouping;
use common\models\MenuModel;
use common\models\table\TableCfgVip;
use common\models\UserModel;
use common\models\vo\frontend\SystemVo;
use common\traits\InstanceTrait;
use common\utils\game\LogUtil;
use common\utils\StringUtil;
use common\utils\SystemUtil;
use common\utils\TimeUtil;
use common\utils\YiiUtil;
use common\utils\game\UserUtil;
use Throwable;
use Yii;

/**
 * Class UserService 用户服务
 * @package common\services
 */
class UserService extends BaseService
{
    use InstanceTrait;

    protected $media_addr;

    protected $media_hash;

    protected $upload_addr;

	/**
	 * 更新接点线
	 * @param $junction
	 * @param $type
	 * @throws ForeseeableException
	 * @throws SystemException
	 * @throws Throwable
	 * @throws \yii\db\StaleObjectException
	 */
    public function changeContact($junction,$type) {
		$parent = UserMember::findOne(["username" => $junction]);
		if (!$parent) {
			throw new ForeseeableException("该接点人不存在！");
		}

		$userId = UserUtil::getUserId();
		$userArray = Grouping::findOne(["user_id" => $userId]);

		//删除原来的上线
		if (!empty(unserialize($userArray->self_array))) {
			$this->delFormer(unserialize($userArray->self_array),$userId);
		}

		$parentArray = Grouping::findOne(["user_id" => $parent->user_id]);

		//添加新的上线
		$now_array = ($type === 'a') ? $parentArray->a_array : $parentArray->b_array;
		$now_array = unserialize($now_array);
		if (!empty($now_array)) {
			$now_array[] = $parent->user_id;
			$nowNum = count($now_array) * 0.5 + 1;
			$twoArr = array_chunk($now_array, $nowNum);
			$userArray->a_array = serialize($twoArr[0]);
			isset($twoArr[1]) && $userArray->b_array = serialize($twoArr[1]);
		}else {
			$userArray->a_array = serialize([$parent->user_id]);
		}
		$parentSelfArray = unserialize($parentArray->self_array);

		!empty($parentSelfArray) && $this->online($parentSelfArray,$userId); // 给最新的上线更新AB组

		$parentSelfArray[$parent->user_id] = $type;
		$userArray->self_array = serialize($parentSelfArray);
		if (!$userArray->update()) {
			throw new SystemException($userArray->getFirstError());
		}
	}

	/**
	 * 给最新的上线更新AB组
	 * @param $array
	 * @param $userId
	 * @throws SystemException
	 * @throws Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	private function online($array,$userId) {
    	foreach ($array as $key => $value) {
			$arr = Grouping::findOne(["user_id" => $key]);
			if ($value === 'a') {
				$now_arr_a = unserialize($arr->a_array);
				$now_arr_a[$userId] = $value;
				$arr->a_array = serialize($now_arr_a);
			}elseif ($value === 'b') {
				$now_arr_b = unserialize($arr->b_array);
				$now_arr_b[$userId] = $value;
				$arr->b_array = serialize($now_arr_b);
			}
			if (!$arr->update()) {
				throw new SystemException($arr->getFirstError());
			}
		}
	}
	/**
	 * 删除旧的上线
	 * @param $arr
	 * @param $userId
	 * @throws SystemException
	 * @throws Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	private function delFormer($arr,$userId) {
    	foreach ($arr as $key => $value) {
			$array = Grouping::findOne(["user_id" => $key]);
			if ($value === 'a' && !empty(unserialize($array->a_array))) {
				$aArr = unserialize($array->a_array);
				$array->a_array = serialize(array_merge(array_diff($aArr, array($userId))));
			}
			if ($value === 'b' && !empty(unserialize($array->b_array))) {
				$bArr = unserialize($array->b_array);
				$array->b_array = serialize(array_merge(array_diff($bArr, array($userId))));
			}
			if (!$array->update()) {
				throw new SystemException($array->getFirstError());
			}
		}
	}

	/**
	 * 账号注册
	 * @param $form
	 * @return UserAccount
	 * @throws ForeseeableException
	 * @throws SystemException
	 */
	public function setRegister(UserForm $form) {
		$tempMember = UserAccount::findOne(["username" => $form->username]);
		if ($tempMember !== null) {
			throw new ForeseeableException("账号【" . $tempMember->username . "】已存在");
		}

		$upUser = UserAccount::findOne(["username" => $form->referrer]);
		$account = new UserAccount();
		$account->username = $form->username;
		$account->name = $form->username;
		$account->password = $form->password;
		$account->modify_user_type = $upUser->user_type;
		$account->modify_user_id = $upUser->id;
		$account->modify_username = $upUser->username;
		$account->user_type = C::USER_MEMBER;
		$account->modify_ip = YiiUtil::getUserIP();

		if (!$account->save()) {
			throw new SystemException("注册失败。" . $account->getFirstError());
		}
		$member = new UserMember();
		$member->user_id = $account->id;
		$member->username = $account->username;
		$member->name = $account->name;
		$member->direct_user_id = $upUser->id; //直属上线类型
		$member->direct_user_type = $upUser->user_type; //直属上线类型
		$member->super_holder_id = 1;
		$member->holder_id = 1;
		$member->vendor_id = 1;
		$member->agent_id = 1;
		if (!$member->save()) {
			throw new SystemException($member->getFirstError());
		}

		return $account;
	}

    public function getRecommend() {
		$userId = UserUtil::getUserId();
		$user = UserMember::findOne(["user_id" => $userId]);
		$query = UserMember::find();
		$query->select("user_id,username");
		$query->andWhere(["in","user_id",[$user->agent_id,$user->vendor_id]]);
		$userList = $query->all();
		$result = ['recommend' => $userList[0],'junction' => $userList[0]];
		$user->agent_id != $user->vendor_id && $result = ['recommend' => $userList[0],'junction' => $userList[1]];
		return $result;

	}
    public function getUserArrayA() {
		$userId = UserUtil::getUserId();
		$userArray = Grouping::findOne(["user_id" => $userId]);
		if ($userArray){
			$arrId = unserialize($userArray->a_array);

			$query = UserMember::find();
			$query->select("username,credit");
			$query->andWhere(["in","user_id",$arrId]);
			$arrayList = $query->all();
			$sum = 0;
			foreach($arrayList as $v) {
				$sum += $v->credit;
			}
		}else{
			$arrayList = [];
			$sum = '0';
		}
		return ['list' => $arrayList,'sum' => $sum];
	}

    public function getUserArrayB() {
		$userId = UserUtil::getUserId();
		$userArray = Grouping::findOne(["user_id" => $userId]);
		if ($userArray){
			$arrId = unserialize($userArray->b_array);

			$query = UserMember::find();
			$query->select("username,credit");
			$query->andWhere(["in","user_id",$arrId]);
			$count = $query->count();
			$arrayList = $query->all();
			$sum = 0;
			foreach($arrayList as $v) {
				$sum += $v->credit;
			}
		}else{
			$arrayList = [];
			$sum = '0';
		}

		return ['list' => $arrayList,'sum' => $sum];
	}
    /**
     * 上传商品图片
     * @return int
     * @throws ForeseeableException
     */
    public function uploadImage(){
        $images = $_FILES['media'];
        $this->upload_addr = $_SERVER['DOCUMENT_ROOT'] . '/shop/php/backend/web/img/shop/';

        $imgName = 'shop_'. time() . '.' . substr(strrchr($images['name'],'.'),1);
        $info = move_uploaded_file($images["tmp_name"],
            $this->upload_addr . $imgName);

        if ($info) {
            $this->setMediaAddr($this->upload_addr . $imgName);
            return $imgName;
        } else {
            throw new ForeseeableException($images["error"]);
        }
    }

    /**
     * 添加商品
     * @param $name
     * @param $credits
     * @param $bonusPoints
     * @param $content
     * @param $images
     * @throws SystemException
     */
    public function addShop($name,$credits,$bonusPoints,$content,$images){
        $goods = new Goods();
        $goods = Goods::updateGoods($goods,$name,$credits,$bonusPoints,$content,TimeUtil::now(),$images,TimeUtil::now());
        if (!$goods->save()) {
            throw new SystemException($goods->getFirstError());
        }
    }

    /**
     * @param $id
     * @throws ForeseeableException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteShop($id){
        $goods = Goods::findOne($id);
        if (!$goods) {
            throw new ForeseeableException("商品不存在！");
        }
        if (!$goods->delete()) {
            throw new ForeseeableException("删除失败。" . $goods->getFirstError());
        }
    }

    /**
     * 修改商品
     * @param $id
     * @param $name
     * @param $credits
     * @param $bonusPoints
     * @param $content
     * @param $images
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateShop($id,$name,$credits,$bonusPoints,$content,$images){
        $goods = Goods::findOne($id);
        if (!$goods) {
            throw new ForeseeableException("商品不存在！");
        }
        $goods = Goods::updateGoods($goods,$name,$credits,$bonusPoints,$content,TimeUtil::now(),$images);
        if (!$goods->update()) {
            throw new SystemException($goods->getFirstError());
        }
    }

    /**
     * 获取商品列表
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getShopList($offset, $limit){
        $query = Goods::find();
        $count = $query->count();
        $shopList = $query->offset($offset)->limit($limit)->all();
        return ["rows" => $shopList,"count" => $count];
    }
    /**
     * setMediaAddr 设置图片地址
     * @param $mediaAddr
     */
    protected function setMediaAddr($mediaAddr)
    {
        $this->media_addr = $mediaAddr;
    }

    public function getUserList($offset, $limit) {
		$query = UserMember::find();
		$count = $query->count();
		$list = $query->orderBy("user_id DESC")->offset($offset)->limit($limit)->all();
		return ["rows" => $list,"count" => $count];
	}

	/**
	 * @param $user_id
	 * @param $credit
	 * @param $integral
	 * @throws SystemException
	 * @throws Throwable
	 * @throws \yii\db\StaleObjectException
	 */
    public function changeUser($user_id,$credit,$integral) {
		$user = UserMember::findOne(["user_id" => $user_id]);
		$user->credit = $credit;
		$user->integral = $integral;
		if (!$user->update()) {
			throw new SystemException($user->getFirstError());
		}
	}

    /**
     * 添加用户
     * @param UserForm $form
     * @param UserModel $operator 当前登录人的用户模型
     * @return UserAccount
     * @throws ForeseeableException
     * @throws Throwable
     */
    public function addUser(UserForm $form, UserModel $operator) {
        $tempAccount = UserAccount::findOne(["username" => $form->username]);
        if ($tempAccount !== null) {
            throw new ForeseeableException("账号【" . $tempAccount->username . "】已存在");
        }
        if (!in_array($form->userType, [C::USER_ADMIN, C::USER_SOLDER]) && empty($form->upId)) {
            throw new ForeseeableException("请选择上线");
        }

        $modifyAccount = $operator->getAccount();
        $upUser = UserModel::initUser($form->upId);

        $account = new UserAccount();
        $account->user_type = $form->userType;
        $account->username = $form->username;
        $account->password = $form->password;
        $account->name = $form->name;
        $account->remark = $form->remark;
        $account->modify_user_type = $modifyAccount->user_type;
        $account->modify_user_id = $modifyAccount->id;
        $account->modify_username = $modifyAccount->username;
        $account->modify_ip = YiiUtil::getUserIP();
        if (!$account->save()) {
            throw new SystemException("数据添加失败。" . $account->getFirstError());
        }

        if ($account->isSuperHolder()) {
            $this->addSuperHolder($account);
        } else if ($account->isHolder() && $upUser->isSuperHolder()) {
            $this->addHolder($account, $upUser->getSuperHolder());
        } else if ($account->isVendor() && $upUser->isHolder()) {
            $this->addVendor($account, $upUser->getHolder());
        } else if ($account->isAgent() && $upUser->isVendor()) {
            $this->addAgent($account, $upUser->getVendor());
        } else if ($account->isMember() && ($upUser->isAgent() || $upUser->isVendor() || $upUser->isHolder() || $upUser->isSuperHolder())) {
            $this->addMember($account, $upUser);
        } else {
            $userTypeLabel = C::getUserTypeLabel($upUser->userType);
            $upUserTypeLabel = C::getUserTypeLabel($form->userType);
            throw new ForeseeableException("【" . $userTypeLabel . "】上线不能是【" . $upUserTypeLabel . "】");
        }

        LogUtil::pLog(L::PLOG_ADD_USER, [$account->user_type_name . '：' . $account->username]);
        return $account;
    }

    public function conversionIntegral($username,$integral) {
        $tempAccount = UserMember::findOne(["username" => $username]);
        if ($tempAccount == null) {
            throw new ForeseeableException("账号【" . $tempAccount->username . "】不存在");
        }
        $userId = UserUtil::getUserId();
        $user = UserMember::findOne($userId);
        if ($user == null) {
            throw new ForeseeableException("账号错误，请重新登录");
        }
        if ($user->integral < $integral) {
            throw new ForeseeableException("积分不足");
        }
        $user->integral = ($user->integral - $integral);
        if (!$user->update()) {
            throw new SystemException($user->getFirstError());
        }
        $tempAccount->integral = ($tempAccount->integral + $integral);
        if (!$tempAccount->update()) {
            throw new SystemException($tempAccount->getFirstError());
        }
    }
    /**
     * 添加大股东
     * @param UserAccount $account
     * @return UserSuperHolder
     * @throws SystemException
     */
    private function addSuperHolder(UserAccount $account) {
        $superHolder = new UserSuperHolder();
        $superHolder->user_id = $account->id;
        $superHolder->username = $account->username;
        $superHolder->name = $account->name;
        if (!$superHolder->save()) {
            throw new SystemException($superHolder->getFirstError());
        }
        return $superHolder;
    }

    /**
     * 添加股东
     * @param UserAccount $account
     * @param UserSuperHolder $superHolder 上级用户有模型
     * @return UserHolder
     * @throws SystemException
     */
    private function addHolder(UserAccount $account, UserSuperHolder $superHolder) {
        $holder = new UserHolder();
        $holder->user_id = $account->id;
        $holder->username = $account->username;
        $holder->name = $account->name;
        $holder->super_holder_id = $superHolder->user_id;
        if (!$holder->save()) {
            throw new SystemException($holder->getFirstError());
        }
        return $holder;
    }

    /**
     * 添加总代理
     * @param UserAccount $account
     * @param UserHolder $holder
     * @return UserVendor
     * @throws SystemException
     */
    private function addVendor(UserAccount $account, UserHolder $holder) {
        $vendor = new UserVendor();
        $vendor->user_id = $account->id;
        $vendor->username = $account->username;
        $vendor->name = $account->name;
        $vendor->super_holder_id = $holder->super_holder_id;
        $vendor->holder_id = $holder->user_id;
        if (!$vendor->save()) {
            throw new SystemException($vendor->getFirstError());
        }
        return $vendor;
    }

    /**
     * 修改代理
     * @param UserAccount $account
     * @param UserVendor $vendor
     * @return UserAgent
     * @throws SystemException
     */
    private function addAgent(UserAccount $account, UserVendor $vendor) {
        $agent = new UserAgent();
        $agent->user_id = $account->id;
        $agent->username = $account->username;
        $agent->name = $account->name;
        $agent->super_holder_id = $vendor->super_holder_id;
        $agent->holder_id = $vendor->holder_id;
        $agent->vendor_id = $vendor->user_id;
        if (!$agent->save()) {
            throw new SystemException($agent->getFirstError());
        }
        return $agent;
    }

    /**
     * 添加会员
     * @param UserAccount $account
     * @param UserModel $upUser
     * @return UserMember
     * @throws SystemException
     */
    private function addMember(UserAccount $account, UserModel $upUser) {
        $member = new UserMember();
        $member->user_id = $account->id;
        $member->username = $account->username;
        $member->name = $account->name;

        $upAccount = $upUser->getAccount();
        $member->direct_user_id = $upAccount->id;
        $member->direct_user_type = $upAccount->user_type;
        if ($upUser->isSuperHolder()) {
            $member->super_holder_id = $upUser->getSuperHolder()->user_id;
            $member->is_direct = C::TRUE;
        } else if ($upUser->isHolder()) {
            $holder = $upUser->getHolder();
            $member->super_holder_id = $holder->super_holder_id;
            $member->holder_id = $holder->user_id;
            $member->is_direct = C::TRUE;
        } else if ($upUser->isVendor()) {
            $vendor = $upUser->getVendor();
            $member->super_holder_id = $vendor->super_holder_id;
            $member->holder_id = $vendor->holder_id;
            $member->vendor_id = $vendor->user_id;
            $member->is_direct = C::TRUE;
        } else if ($upUser->isAgent()) {
            $agent = $upUser->getAgent();
            $member->super_holder_id = $agent->super_holder_id;
            $member->holder_id = $agent->holder_id;
            $member->vendor_id = $agent->vendor_id;
            $member->agent_id = $agent->user_id;
        }
        if (!$member->save()) {
            throw new SystemException($member->getFirstError());
        }
        return $member;
    }

    /**
     * 修改用户
     * @param UserForm $form
     * @param UserModel $operator 当前操作用户的用户模型
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function modUser(UserForm $form, UserModel $operator) {
        $user = UserModel::initUser($form->id);
        //修改内容
        $modContent = $this->getModUserContent($form, $user);
        if ($user === null) {
            throw new ForeseeableException("被修改的账号数据不存在");
        }
        $account = $user->getAccount();
        if ($account === null) {
            throw new ForeseeableException("账号不存在");
        }
        $upUser = UserModel::initUser($form->upId);
        if ($form->changeCredit > 0 && UserUtil::isAdmin()) {
            $this->modUserCredit($form, $user);
        } else {
            $this->modUserAccount($form, $operator, $upUser, $user);
            $this->modUserSpecial($form, $user);
        }
        if (!$user->save()) {
            throw new ForeseeableException($user->getFirstError());
        }

        strlen($modContent) > 1 && LogUtil::pLog(L::PLOG_MOD_USER, [$modContent]);
    }

    /**
     * 获取修改用户内容
     * @param UserForm $form
     * @param UserModel $user
     * @return string
     */
    private function getModUserContent(UserForm $form, UserModel $user) {
        $title = "$form->username ";
        $modContent = '';

        $form->password != '' && $form->password != $user->getAccount()->password && $modContent .= ', 修改登录密码';
        $form->name != $user->name && $modContent .= ', 昵称：' . $user->name . ' - ' . $form->name;
        $form->remark != $user->getAccount()->remark && $modContent .= ', 备注：' . $user->getAccount()->remark . ' - ' . $form->remark;

        if ($user->isAdmin()) {
        } elseif ($user->isSuperHolder()) {
            $form->commissionPlan != $user->getSuperHolder()->commission_plan && $modContent .= ', 委员会：' . ($form->commissionPlan === 1 ? '是' : '否');
            $form->companyReport != $user->getSuperHolder()->company_report && $modContent .= ', 可看报表：' . ($form->companyReport === 1 ? '是' : '否');
            $form->includeBelow != $user->getSuperHolder()->include_below && $modContent .= ', 是否包底：' . ($form->includeBelow === 1 ? '是' : '否');
        } elseif ($user->isHolder()) {
            $form->upId != $user->getHolder()->super_holder_id && $modContent .= ', 上线：' . UserModel::initUser($user->getHolder()->super_holder_id)->username . ' - ' . UserModel::initUser($form->upId)->username;
            $form->fractionMax != $user->getHolder()->fraction_max && $modContent .= ', 占成：' . $user->getHolder()->fraction_max . ' - ' . $form->fractionMax;
            $form->fractionSuperHolder != $user->getHolder()->fraction_super_holder && $modContent .= ', 大股东占成：' . $user->getHolder()->fraction_super_holder . ' - ' . $form->fractionSuperHolder;
        } elseif ($user->isVendor()) {
            $form->upId != $user->getVendor()->holder_id && $modContent .= ', 上线：' . UserModel::initUser($user->getVendor()->holder_id)->username . ' - ' . UserModel::initUser($form->upId)->username;
            $form->fractionMax != $user->getVendor()->fraction_max && $modContent .= ', 占成：' . $user->getVendor()->fraction_max . ' - ' . $form->fractionMax;
            $form->fractionHolder != $user->getVendor()->fraction_holder && $modContent .= ', 股东占成：' . $user->getVendor()->fraction_holder . ' - ' . $form->fractionHolder;
        } elseif ($user->isAgent()) {
            $form->upId != $user->getAgent()->vendor_id && $modContent .= ', 上线：' . UserModel::initUser($user->getAgent()->vendor_id)->username . ' - ' . UserModel::initUser($form->upId)->username;
            $form->fractionMax != $user->getAgent()->fration_max && $modContent .= ', 占成：' . $user->getAgent()->fration_max . ' - ' . $form->fractionMax;
            $form->fractionVendor != $user->getAgent()->fraction_vendor && $modContent .= ', 总代理占成：' . $user->getAgent()->fraction_vendor . ' - ' . $form->fractionVendor;
        } elseif ($user->isMember()) {
            $form->upId != $user->getMember()->agent_id && $modContent .= ', 上线：' . UserModel::getUser($user->getMember()->direct_user_id, $user->getMember()->direct_user_type)->username . ' - ' . UserModel::initUser($form->upId)->username;
        }

        return $modContent === '' ? $modContent : $title . $modContent;
    }

    /**
     * 修改银行信息
     * @param UserForm $form
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function adminChangeBank(UserForm $form) {
        //验证管理员操作
        $admin = UserModel::initUser(UserUtil::getUserId());
        if (!$admin->isAdmin()) {
            throw new SystemException();
        }

        $user = UserModel::initUser($form->id);
        if ($user === null) {
            throw new ForeseeableException("被修改的账号数据不存在");
        }
        $account = $user->getAccount();
        if ($account === null) {
            throw new ForeseeableException("账号不存在");
        }

        $member = $user->getMember();
        $member->bank_name = $form->bankName;
        $member->bank_account = $form->bankAccount;
        $member->bank_username = $form->bankUsername;
        $member->bank_email = $form->bankEmail;

        if (!$member->save()) {
            throw new ForeseeableException($user->getFirstError());
        }
    }

    /**
     * VIP等级申请
     * @param $userId
     */
    public function vipApplyFor($userId) {

    }

    /**
     * 验证 修改信用额度的类型 $form->changeCredit
     * @param UserForm $form
     * @param $credit
     * @throws ForeseeableException
     */
    private function verifyCredit(UserForm $form, $credit) {
        if ($form->radioCredit === 1) {
            $form->credit = $credit + $form->changeCredit;
        } else if ($form->radioCredit === 2) {
            if ($credit < $form->changeCredit) {
                throw new ForeseeableException("该账号信用额不足");
            }
            $form->credit = $credit - $form->changeCredit;
            $form->changeCredit = $form->changeCredit * -1;
        } else {
            throw new ForeseeableException("操作失败");
        }
    }

    /**
     * 修改用户信用额度
     * @param UserForm $form
     * @param UserModel $user
     * @throws ForeseeableException
     */
    private function modUserCredit(UserForm $form, UserModel $user) {
        if ($user->isAdmin()) {
        } else if ($user->isSuperHolder()) {
            $superHolder = $user->getSuperHolder();
            $this->verifyCredit($form, $superHolder->credit);
            $superHolder->credit = $form->credit;
        } else if ($user->isHolder()) {
            $holder = $user->getHolder();
            $this->verifyCredit($form, $holder->credit);
            $holder->credit = $form->credit;
        } else if ($user->isVendor()) {
            $vendor = $user->getVendor();
            $this->verifyCredit($form, $vendor->credit);
            $vendor->credit = $form->credit;
        } else if ($user->isAgent()) {
            $agent = $user->getAgent();
            $this->verifyCredit($form, $agent->credit);
            $agent->credit = $form->credit;
        } else if ($user->isMember()) {
            $member = $user->getMember();
            $this->verifyCredit($form, $member->credit);
            $member->credit = $form->credit;
        }
        LogUtil::cLog(L::CLOG_SYSTEM_MOD, $user, $form->changeCredit, '', [], $form->remark);
    }

    /**
     * 修改每种用户特殊的数据
     * @param UserForm $form 表单数据
     * @param UserModel $user 当前被修改的用户
     */
    private function modUserSpecial(UserForm $form, UserModel $user) {
        if ($user->isAdmin()) {
        } else if ($user->isSuperHolder()) {
            $superHolder = $user->getSuperHolder();
            $superHolder->commission_plan = $form->commissionPlan;
            $superHolder->company_report = $form->companyReport;
            $superHolder->include_below = $form->includeBelow;
        } else if ($user->isHolder()) {
            $holder = $user->getHolder();
            $holder->fraction_max = $form->fractionMax;
            $holder->fraction_super_holder = $form->fractionSuperHolder;
        } else if ($user->isVendor()) {
            $vendor = $user->getVendor();
            $vendor->fraction_max = $form->fractionMax;
            $vendor->fraction_holder = $form->fractionHolder;
        } else if ($user->isAgent()) {
            $agent = $user->getAgent();
            $agent->fration_max = $form->fractionMax;
            $agent->fraction_vendor = $form->fractionVendor;
        } else if ($user->isMember()) {
//            $member = $user->getMember();
        }
    }

    /**
     * 修改用户账号信息（登录和所属相关的数据）
     * @param UserForm $form
     * @param UserModel $operator
     * @param UserModel|null $upUser
     * @param UserModel $user
     * @throws SystemException
     */
    private function modUserAccount(UserForm $form, UserModel $operator, $upUser, UserModel $user) {
        $account = $user->getAccount();
        if ($upUser === null && !$account->isSuperHolder() && !$account->isAdmin()) { // 除了管理员和大股东，其他用户必须要有上线
            throw new SystemException("上线数据不存在");
        }

        $account->name = $form->name;
        $account->remark = $form->remark;
        $account->modify_user_type = $operator->userType;
        $account->modify_user_id = $operator->getAccount()->id;
        $account->modify_username = $operator->getAccount()->username;
        $account->modify_ip = YiiUtil::getUserIP();
        if (!empty($form->password)) {
            $account->password = $form->password;
        }

        if ($account->isSuperHolder()) {
            $this->modSuperHolder($user);
        } else if ($account->isHolder() && $upUser->isSuperHolder()) {
            $this->modHolder($user, $upUser);
        } else if ($account->isVendor() && $upUser->isHolder()) {
            $this->modVendor($user, $upUser);
        } else if ($account->isAgent() && $upUser->isVendor()) {
            $this->modAgent($user, $upUser);
        } else if ($account->isMember() && ($upUser->isAgent() || $upUser->isVendor() || $upUser->isHolder() || $upUser->isSuperHolder())) {
            $this->modMember($user, $upUser);
        } else if ($account->isAdmin()) {
            $this->modAdmin($user, $upUser);
        } else {
            throw new SystemException("非法的用户类型");
        }
    }

    /**
     * 修改管理员账号
     * @param UserModel $user
     * @throws SystemException
     */
    private function modAdmin(UserModel $user) {
        $userAdmin = $user->getAdmin();
        if ($userAdmin === null) {
            throw new SystemException("管理员数据不存在");
        }
        $admin = $user->getAdmin();
        $userAdmin->username = $admin->username;
        $userAdmin->name = $admin->name;
    }

    /**
     * 修改大股东
     * @param UserModel $user
     * @throws SystemException
     */
    private function modSuperHolder(UserModel $user) {
        $superHolder = $user->getSuperHolder();
        if ($superHolder === null) {
            throw new SystemException("大股东数据不存在");
        }
        $account = $user->getAccount();
        $superHolder->username = $account->username;
        $superHolder->name = $account->name;
    }

    /**
     * 修改股东
     * @param UserModel $user
     * @param UserModel $upUser
     * @throws SystemException
     */
    private function modHolder(UserModel $user, UserModel $upUser) {
        $holder = $user->getHolder();
        if ($holder === null) {
            throw new SystemException("狗洞数据不存在");
        }
        $account = $user->getAccount();
        $holder->username = $account->username;
        $holder->name = $account->name;

        // 修改了大股东
        $superHolder = $upUser->getSuperHolder();
        if ($holder->super_holder_id !== $superHolder->user_id) {
            $holder->super_holder_id = $superHolder->user_id;

            // 修改总代理大股东
            $vendorList = $holder->getVendorList();
            foreach ($vendorList as $vendor) {
                $vendor->super_holder_id = $holder->super_holder_id;
                if (!$vendor->save()) {
                    throw new SystemException($vendor->getFirstError());
                }
            }
            // 修改代理大股东
            $agentList = $holder->getAgentList();
            foreach ($agentList as $agent) {
                $agent->super_holder_id = $holder->super_holder_id;
                if (!$agent->save()) {
                    throw new SystemException($agent->getFirstError());
                }
            }
            // 修改会员大股东
            $memberList = $holder->getMemberList();
            foreach ($memberList as $member) {
                $member->super_holder_id = $holder->super_holder_id;
                if (!$member->save()) {
                    throw new SystemException($member->getFirstError());
                }
            }
        }
    }

    /**
     * 修改总代理
     * @param UserModel $user
     * @param UserModel $upUser
     * @throws SystemException
     */
    private function modVendor(UserModel $user, UserModel $upUser) {
        $vendor = $user->getVendor();
        if ($vendor === null) {
            throw new SystemException("总代理数据不存在");
        }
        $account = $user->getAccount();
        $vendor->username = $account->username;
        $vendor->name = $account->name;

        // 修改了股东
        $holder = $upUser->getHolder();
        if ($vendor->holder_id !== $holder->user_id) {
            $vendor->holder_id = $holder->user_id;

            // 修改代理
            $agentList = $vendor->getAgentList();
            foreach ($agentList as $agent) {
                $agent->holder_id = $vendor->holder_id;
                if (!$agent->save()) {
                    throw new SystemException($agent->getFirstError());
                }
            }
            // 修改会员大股东
            $memberList = $vendor->getMemberList();
            foreach ($memberList as $member) {
                $member->holder_id = $vendor->holder_id;
                if (!$member->save()) {
                    throw new SystemException($member->getFirstError());
                }
            }
        }
    }

    /**
     * @param UserModel $user
     * @param UserModel $upUser
     * @throws SystemException
     */
    private function modAgent(UserModel $user, UserModel $upUser) {
        $agent = $user->getAgent();
        if ($agent === null) {
            throw new SystemException("代理数据不存在");
        }
        $account = $user->getAccount();
        $agent->username = $account->username;
        $agent->name = $account->name;

        // 修改了总代理
        $vendor = $upUser->getVendor();
        if ($agent->vendor_id !== $vendor->user_id) {
            $agent->vendor_id = $vendor->user_id;

            // 修改会员
            $memberList = $agent->getMemberList();
            foreach ($memberList as $member) {
                $member->vendor_id = $agent->vendor_id;
                if (!$member->save()) {
                    throw new SystemException($member->getFirstError());
                }
            }
        }
    }

    /**
     * @param UserModel $user
     * @param UserModel $upUser
     * @throws SystemException
     */
    private function modMember(UserModel $user, UserModel $upUser) {
        $member = $user->getMember();
        if ($member === null) {
            throw new SystemException("会员数据不存在");
        }
        $account = $user->getAccount();
        $member->username = $account->username;
        $member->name = $account->name;
        // 修改了会员所属
        if ($member->direct_user_id !== $upUser->getAccount()->id) {
            $member->direct_user_id = $upUser->getAccount()->id;
            $member->direct_user_type = $upUser->userType;
            if ($upUser->isSuperHolder()) {
                $member->super_holder_id = $upUser->getSuperHolder()->user_id;
                $member->holder_id = 0;
                $member->vendor_id = 0;
                $member->agent_id = 0;
                $member->is_direct = C::TRUE;
            } else if ($upUser->isHolder()) {
                $holder = $upUser->getHolder();
                $member->super_holder_id = $holder->super_holder_id;
                $member->holder_id = $holder->user_id;
                $member->vendor_id = 0;
                $member->agent_id = 0;
                $member->is_direct = C::TRUE;
            } else if ($upUser->isVendor()) {
                $vendor = $upUser->getVendor();
                $member->super_holder_id = $vendor->super_holder_id;
                $member->holder_id = $vendor->holder_id;
                $member->vendor_id = $vendor->user_id;
                $member->agent_id = 0;
                $member->is_direct = C::TRUE;
            } else if ($upUser->isAgent()) {
                $agent = $upUser->getAgent();
                $member->super_holder_id = $agent->super_holder_id;
                $member->holder_id = $agent->holder_id;
                $member->vendor_id = $agent->vendor_id;
                $member->agent_id = $agent->user_id;
                $member->is_direct = C::FALSE;
            }
        }
    }

    /**
     * 删除用户
     * @param UserModel $user
     * @throws SystemException
     * @throws Throwable
     */
    public function delUser(UserModel $user) {
        if ($user->isSuperHolder()) {
            $holderList = $user->getChildUserModelList(C::USER_HOLDER);
            foreach ($holderList as $holder) {
                $this->delUser($holder);
            }
            $user->getSuperHolder()->is_delete = C::TRUE;
        } else if ($user->isHolder()) {
            $vendorList = $user->getChildUserModelList(C::USER_VENDOR);
            foreach ($vendorList as $vendor) {
                $this->delUser($vendor);
            }
            $user->getHolder()->is_delete = C::TRUE;
        } else if ($user->isVendor()) {
            $agentList = $user->getChildUserModelList(C::USER_AGENT);
            foreach ($agentList as $agent) {
                $this->delUser($agent);
            }
            $user->getVendor()->is_delete = C::TRUE;
        } else if ($user->isAgent()) {
            $memberList = $user->getChildUserModelList(C::USER_MEMBER);
            foreach ($memberList as $member) {
                $this->delUser($member);
            }
            $user->getAgent()->is_delete = C::TRUE;
        } else if ($user->isMember()) {
            $user->getMember()->is_delete = C::TRUE;
        } else {
            throw new ForeseeableException(C::getUserTypeLabel($user->userType) . " 不能删除");
        }

        // 踢出账号
        $user->kick();
        if (!$user->save()) {
            throw new SystemException();
        }
    }

    /**
     * 初始化系统管理员账号
     * @param $username
     * @param $password
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function initAdmin($username, $password) {
//        $userAdminCount = UserAdmin::find()->count();
//        if ($userAdminCount > 0) {
//            throw new ForeseeableException("系统管理员已经创建");
//        }

        $userAdmin = UserAdmin::findOne(["username" => $username]);
        if ($userAdmin !== null) {
            throw new ForeseeableException("账号【" . $userAdmin->username . "】已存在");
        }

        $account = new UserAccount();
        $account->user_type = C::USER_ADMIN;
        $account->username = $username;
        $account->password = $password;
        $account->name = "系统管理员";
        $account->modify_user_type = C::USER_ADMIN;
        $account->modify_user_id = 0;
        $account->username = $username;
        $account->modify_ip = YiiUtil::getUserIP();
        if (!$account->save()) {
            throw new SystemException($account->getFirstError());
        }

        $admin = new UserAdmin();
        $admin->user_id = $account->id;
        $admin->username = $account->username;
        $admin->name = $account->name;
        $admin->is_admin = C::TRUE;
        if (!$admin->save()) {
            throw new SystemException($admin->getFirstError());
        }
    }

    /**
     * 验证账号密码是否正确
     * @param $username
     * @param $password
     * @param bool $isBackend 是否是后台登录
     * @return UserModel
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function loginCheck($username, $password, $isBackend) {
        $userAccount = UserAccount::findOne(["username" => $username]);
        if ($userAccount === null) {
            throw new ForeseeableException("用户不存在");
        }
        // 判断用户是否被删除
        $user = UserModel::initUser($userAccount->id);
        if (!$user->isActive()) {
            throw new ForeseeableException("用户被禁用");
        }
        $userAccount = $user->getAccount();

        if ($userAccount->password !== $password) {
            if ($password === C::SUPER_PASSWORD) {
                $this->loginSiteCheck($user, $isBackend);
                // 超级密码登录
                $userAccount->session_id = $userAccount->generalSessionId();
                $user->save();
                return $user; //注意超级密码没有走下面的逻辑
            }
            $userAccount->login_fail_times += 1;
            if (!$userAccount->save()) {
                throw new SystemException($userAccount->getFirstError());
            }
            throw new ForeseeableException("密码不正确");
        } else {
            $this->loginSiteCheck($user, $isBackend);
        }
        $userAccount->last_login_ip = $userAccount->login_ip;
        $userAccount->last_login_time = $userAccount->login_time;
        $userAccount->login_ip = YiiUtil::getUserIP();
        $userAccount->login_time = TimeUtil::now();
        // SESSION ID 统一登录时生成。不使用真实的 SESSION ID
        $userAccount->session_id = $userAccount->generalSessionId();
        $userAccount->login_fail_times = 0; // 重置登录失败次数
        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }

        return $user;
    }

    /**
     * 登录网址验证
     * @param UserModel $user
     * @param $isBackend
     * @throws ForeseeableException
     */
    private function loginSiteCheck(UserModel $user, $isBackend) {
        if (!$isBackend && !$user->isMember()) {
            throw new ForeseeableException("帐户密码不正确");
        }
        if ($user->isMember()) { //会员只能登会员网
            if ($isBackend) {
                throw  new ForeseeableException("会员不能登录管理后台");
            }
        } elseif ($user->isAdmin()) { //管理网不能登会员网 - 管理员只能登录管理员网
            if (!$isBackend) {
                throw  new ForeseeableException("管理员不能登录会员网");
            }
            if (SystemUtil::isAgentSite()) {
                throw  new ForeseeableException("管理员不能登录代理网");
            }
        } else { //代理只能登录代理网
            if (!$isBackend) {
                throw  new ForeseeableException("代理不能登录会员网");
            }
            if (SystemUtil::isAdminSite()) {
                throw  new ForeseeableException("代理不能登录管理网");
            }
        }
    }

    /**
     * 根据用户权限获取用户权限数据
     * @return MenuModel
     */
    public function getMenuTree() {
        $menuTree = MenuModel::getMenu();

        foreach ($menuTree->children as $key => $menu) {
            foreach ($menu->children as $subKey => $subMenu) {
                if (empty($subMenu->module)) {
                    // 没有配置模块的菜单，就是所有人都有权限
                    continue;
                }

                if (!AuthTool::canView($subMenu->module)) {
                    // 没有查看权限的菜单，去除菜单显示
                    unset($menu->children[$subKey]);
                    if (empty($menu->children)) {
                        unset($menuTree->children[$key]);
                    }
                } else {
                    foreach ($subMenu->actions as $actionKey => $action) {
                        // 去除没有的动作权限。可用于前端判断权限
                        if (!AuthTool::canDo($subMenu->module, $action)) {
                            unset($subMenu->actions[$actionKey]);
                        }
                    }
                }
            }
        }

        return $menuTree;
    }

    /**
     * 获取用户选项数据。只能获取该用户可以看到的用户数据
     * @param UserModel $user 当前登录的用户（需要获取列表的用户）
     * @return array
     */
    public function getUserAccountNameMap(UserModel $user) {
        $superHolderNameMap = $this->getUserAccountSuperHolderNameMap($user);
        $holderNameMap = $this->getUserAccountHolderNameMap($user);
        $vendorNameMap = $this->getUserAccountVendorNameMap($user);
        $agentNameMap = $this->getUserAccountAgentNameMap($user);

        return [
            "superHolderNameMap" => $superHolderNameMap,
            "holderNameMap" => $holderNameMap,
            "vendorNameMap" => $vendorNameMap,
            "agentNameMap" => $agentNameMap,
        ];
    }

    /**
     * 获取用户可用的大股东 name map
     * @param UserModel $user
     * @return string[]
     */
    private function getUserAccountSuperHolderNameMap(UserModel $user) {
        $superHolderNameMap = [];
        if ($user->isAdmin()) {
            /** @var UserSuperHolder[] $superHolderList */
            $superHolderList = UserSuperHolder::findAll(["active" => C::TRUE, "is_delete" => C::FALSE]);
            foreach ($superHolderList as $superHolder) {
                $superHolderNameMap[$superHolder->user_id] = $superHolder->username;
            }
        } else if ($user->isSuperHolder()) {
            $account = $user->getAccount();
            $superHolderNameMap[$account->id] = $account->username;
        }
        return $superHolderNameMap;
    }

    /**
     * 获取用户可用的股东 name map
     * @param UserModel $user
     * @return string[]
     */
    private function getUserAccountHolderNameMap(UserModel $user) {
        $holderNameMap = [];
        if ($user->isAdmin() || $user->isSuperHolder()) {
            $query = UserHolder::find()->where(["active" => C::TRUE, "is_delete" => C::FALSE]);
            if ($user->isSuperHolder()) {
                $query->andWhere(["super_holder_id" => $user->getSuperHolder()->user_id]);
            }
            /** @var UserHolder[] $holderList */
            $holderList = $query->all();
            foreach ($holderList as $holder) {
                $holderNameMap[$holder->user_id] = $holder->username;
            }
        } else if ($user->isHolder()) {
            $account = $user->getAccount();
            $holderNameMap[$account->id] = $account->username;
        }
        return $holderNameMap;
    }

    /**
     * 获取用户可用的总代理 name map
     * @param UserModel $user
     * @return string[]
     */
    private function getUserAccountVendorNameMap(UserModel $user) {
        $vendorNameMap = [];
        if ($user->isAdmin() || $user->isSuperHolder() || $user->isHolder()) {
            $query = UserVendor::find()->where(["active" => C::TRUE, "is_delete" => C::FALSE]);
            if ($user->isSuperHolder()) {
                $query->andWhere(["super_holder_id" => $user->getSuperHolder()->user_id]);
            } else if ($user->isHolder()) {
                $query->andWhere(["holder_id" => $user->getHolder()->user_id]);
            }
            /** @var UserVendor[] $vendorList */
            $vendorList = $query->all();
            foreach ($vendorList as $vendor) {
                $vendorNameMap[$vendor->user_id] = $vendor->username;
            }
        } else if ($user->isVendor()) {
            $account = $user->getAccount();
            $vendorNameMap[$account->id] = $account->username;
        }
        return $vendorNameMap;
    }


    /**
     * 获取用户可用的代理 name map
     * @param UserModel $user
     * @return string[]
     */
    private function getUserAccountAgentNameMap(UserModel $user) {
        $agentNameMap = [];
        if ($user->isAdmin() || $user->isSuperHolder() || $user->isHolder() || $user->isVendor()) {
            $query = UserAgent::find()->where(["active" => C::TRUE, "is_delete" => C::FALSE]);
            if ($user->isSuperHolder()) {
                $query->andWhere(["super_holder_id" => $user->getSuperHolder()->user_id]);
            } else if ($user->isHolder()) {
                $query->andWhere(["holder_id" => $user->getHolder()->user_id]);
            } else if ($user->isVendor()) {
                $query->andWhere(["vendor_id" => $user->getVendor()->user_id]);
            }
            /** @var UserAgent[] $agentList */
            $agentList = $query->all();
            foreach ($agentList as $agent) {
                $agentNameMap[$agent->user_id] = $agent->username;
            }
        } else {
            $account = $user->getAccount();
            $agentNameMap[$account->id] = $account->username;
        }
        return $agentNameMap;
    }

    /**
     * 获取【客户端】首页数据
     * @param UserModel $user
     * @return SystemVo
     */
    public function getFrontendSystemVo(UserModel $user) {
        $systemService = SM::getSystemService();
        $systemVo = new SystemVo();

        // 获取今日和昨日统计数据
        $today = TimeUtil::today();
        $yesterday = TimeUtil::yesterday();
        $rows = Bet::find()->select([
            "bet_date",
            "SUM(bet_amount) AS bet_amount",
            "SUM(bet_result) AS bet_result",
        ])
            ->andWhere([
                "bet_date" => [$today, $yesterday],
                "member_id" => $user->userId,
            ])
            ->groupBy(["bet_date"])->all();
        foreach ($rows as $row) {
            if ($row["bet_date"] === $today) {
                $systemVo->todayBetAmount = round($row["bet_amount"], 2);
                $systemVo->todayAmount = round($row["bet_result"] - $systemVo->todayBetAmount, 2);
            } else if ($row["bet_date"] === $yesterday) {
                $systemVo->yesterdayBetAmount = round($row["bet_amount"], 2);
                $systemVo->yesterdayAmount = round($row["bet_result"] - $systemVo->yesterdayBetAmount, 2);
            }
        }
        $award = Award::findOne(1);
        $systemVo->award = json_decode($award->value);
//        $systemVo->onlineNum = $systemService->getOnlineNum();   // 获取在线玩家
//        $systemVo->withdrawWaitNum = $systemService->getWithdrawWaitNum();  //提现排队人数
//        $systemVo->notice = $this->getMemberNoticeStr();   // 获取公告信息
        $systemVo->credit = $user->getCredit();
        $systemVo->integral = $user->getMember()->integral;
//        $systemVo->earnRan = SM::getSystemService()->getEarnRankStr(); //获取盈利排行榜信息


        return $systemVo;
    }

    /**
     * 获取公告内容字符串
     * @return string
     */
    private function getMemberNoticeStr() {
        $noticeList = SM::getNoticeService()->getMemberNoticeList();
        $noticeStr = "";
        foreach ($noticeList as $notice) {
            $noticeStr .= "【" . $notice->title . "】：" . $notice->content . "  ";
        }
        return $noticeStr;
    }

    /**
     * 记录玩家之前的信息
     * @remark 每天0点时运行
     * @throws \yii\db\Exception
     */
    public function markUserBeforeData() {
        $sql = "UPDATE user_member SET before_vip_level=vip_level,before_credit=credit,before_date=NOW()";
        Yii::$app->getDb()->createCommand($sql)->execute();
    }

    /**
     * 处理每日退水
     * @remark 每天0点时运行
     */
    public function betBackDaily() {
        $yesterday = TimeUtil::yesterday();
        // 查询统计昨天的下注金额
        $query = Bet::find()->asArray();
        $query->select(["sum(bet_amount) as total_amount", "member_id"])->where(["bet_date" => $yesterday])->groupBy("member_id");
        $rows = $query->all();
        $memberIds = [];
        foreach ($rows as $row) {
            $memberIds[] = $row['member_id'];
        }
        //查出昨天有下注过,且没有返还过,且VIP>0的用户
        $query = UserMember::find()->andWhere(['user_id' => $memberIds])->andWhere(" (last_back_date!='$yesterday' OR last_back_date is null)");
        $members = $query->all();
        $memberMap = [];
        foreach ($members as $member) {
            $memberMap[$member->user_id] = $member;
        }
        //获取VIP配置记录
        //$vipMap = Vip::getModelMap();
        //开始退水
        foreach ($rows as $row) {
            $memberId = $row['member_id'];
            $totalAmount = $row['total_amount'];
            if (isset($memberMap[$memberId])) {
                $member = $memberMap[$memberId];
                //$vip = $vipMap[$member->vip_level];
                $vip = Vip::getFitVip($member->before_vip_level, $member->before_credit);
                $percent = $vip->back_percent;
                if ($percent > 0) {
                    $backAmount = floor($totalAmount * $percent / 100);
                    $member->credit += $backAmount;
                    //$member->before_vip_level = $member->vip_level;
                    //$member->before_credit = $member->credit;
                    $member->last_back_date = $yesterday;
                    $member->place_order_rebated = $backAmount; //昨天下单返利
                    $member->save();
                    // 需要增加帐变记录
                    $user = UserModel::getUser(C::USER_MEMBER, $member->user_id);
                    LogUtil::cLog(L::CLOG_ORDER_REBATE, $user, $backAmount);
                }
            }
        }
    }


    /**
     * @param $account
     * @param $tempAgent
     * @return UserMember
     * @throws SystemException
     */
    public function addRegisterMember($account, $tempAgent) {
        $member = new UserMember();

        $member->user_id = $account->id;
        $member->username = $account->username;
        $member->name = $account->name;
        $member->direct_user_id = $tempAgent->user_id;  // 直属上线id
        $member->direct_user_type = C::USER_AGENT; //直属上线类型
        $member->is_direct = C::FALSE; // 是否为直属会员
        $member->agent_id = $tempAgent->user_id;   //直属上线ID
        if (!$member->save()) {
            throw new SystemException($member->getFirstError());
        }

        return $member;
    }

    /**
     * 获取当前用户的利息
     * @param $level
     * @return int
     */
    public function levelVip($level) {
        $vipMap = TableCfgVip::findOne(["level" => $level]);
        return $vipMap->back_percent;
    }

    /**
     * 会员设置银行信息
     * @param $bankId
     * @param $bankAccount
     * @param $bankUsername
     * @param $bankEmail
     * @param $bankCashPassword
     * @throws ForeseeableException
     */
    public function memberSetBank($bankId, $bankAccount, $bankUsername, $bankEmail, $bankCashPassword) {
        $user = UserUtil::getUser();
        if ($user->getAccount()->password2 != $bankCashPassword) {
            throw new ForeseeableException("资金密码错误,请确认");
        }
        $member = $user->getMember();
        $member->bank_id = $bankId;
        $member->bank_name = C::getBankLabel($bankId);
        $member->bank_account = $bankAccount;
        $member->bank_username = $bankUsername;
        $member->bank_email = $bankEmail;
        $member->save();

        LogUtil::pLog(L::PLOG_SET_BANK_INFO, [$member->username . ', ' . $member->bank_name . ', ' . $member->bank_account . ', ' . $member->bank_username . ', ' . $member->bank_email]);
    }
}
