<?php


namespace common\services;


use common\constants\C;
use common\constants\L;
use common\constants\P;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\Bet;
use common\models\db\Schedule;
use common\models\db\UserMember;
use common\models\UserModel;
use common\models\vo\frontend\LotteryBetVo;
use common\models\vo\sc\BetRowVo;
use common\traits\InstanceTrait;
use common\utils\game\LogUtil;
use common\utils\game\SscUtil;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use frontend\models\form\BetForm;

/**
 * Class BetService 投注服务
 * @package common\services
 */
class BetService extends BaseService
{
    use InstanceTrait;

    /**
     * 检查注单数据以及修改注单的注数数据
     * @param BetForm $form
     * @throws ForeseeableException
     */
    public function checkAndModBetForm(BetForm $form) {
        $this->checkBetForm($form);
        foreach ($form->betVoList as $betVo) {
            $betVo->groupNum = SscUtil::getGroupNum($betVo->betType, $betVo->content);
        }
    }

    /**
     * 检测注单是否合法
     * @param BetForm $form
     * @throws ForeseeableException
     */
    private function checkBetForm(BetForm $form) {
        $schedule = Schedule::findOne($form->scheduleId);
        if ($schedule === null) {
            throw new ForeseeableException("投注期数不存在");
        }
        if ($schedule->status === C::SCHEDULE_STATUS_OPEN) {
            throw new ForeseeableException("所投期数已开奖。无法继续投注");
        }
        if ($schedule->getRemainBetSeconds() <= 0) {
            throw new ForeseeableException("本期已截至下注。");
        }
        foreach ($form->betVoList as $betVo) {
            if (!SscUtil::checkData($betVo->betType, $betVo->content)) {
                throw new ForeseeableException("投注号码不合法。请重选");
            }
        }
    }

    /**
     * 确认投注
     * @param BetForm $form
     * @param UserModel $user 当前客户
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function submitCheckBetForm(BetForm $form, UserModel $user) {
        $this->checkBetForm($form);
        $member = $user->getMember();

        // 为用户下注
        foreach ($form->betVoList as $betVo) {
            $bet = new Bet();
            $bet->schedule_id = $form->scheduleId;
            $bet->username = $member->username;
            $bet->member_id = $member->user_id;
            $bet->agent_id = $member->agent_id;
            $bet->vendor_id = $member->vendor_id;
            $bet->holder_id = $member->holder_id;
            $bet->super_holder_id = $member->super_holder_id;
            $bet->bet_type = $betVo->betType;
            $bet->bet_type_name = P::getScBetTypeLabel($bet->bet_type);
            $bet->setBetContent($betVo->content);
            $bet->multiple = $betVo->multiple;
            $bet->unit = $betVo->unit;
            $unitNum = $betVo->unit === C::BET_UNIT_YUAN ? P::SC_BET_UNIT_NUM : P::SC_BET_UNIT_NUM / 10;
            $groupNum = SscUtil::getGroupNum($betVo->betType, $betVo->content);
            $bet->group_num = $groupNum;
            $bet->bet_amount = $unitNum * $betVo->multiple * $groupNum;
            $bet->bet_result = 0;
            $bet->bet_date = TimeUtil::today();
            $bet->bet_no = $bet->generateBetNo();
            $remainCredit = $user->cutCredit($bet->bet_amount, true);
            $bet->bet_balance = $remainCredit;
            if (!$bet->save()) {
                throw new SystemException($bet->getFirstError());
            }
            // 添加注单记录
            LogUtil::cLog(L::CLOG_BET, $user, -1 * $bet->bet_amount, $bet->bet_no);
        }

        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }

    }

    /**
     * 【前端】获取投注记录列表数据
     * @param int $offset
     * @param int $size
     * @param int $kind
     * @param string $date
     * @param int $result 结果>=
     * @return array
     */
    public function getBetVoList($offset, $size, $kind, $date, $result) {
        $user = UserUtil::getUser();
        $query = Bet::find()->select(["tab_bet.*"])
            ->leftJoin("tab_schedule", "tab_schedule.id = tab_bet.schedule_id")
            ->where(["member_id" => $user->userId])
            ->orderBy(["tab_bet.id" => SORT_DESC]);
        if (!empty($kind)) {
            $query->andWhere(["tab_schedule.kind" => $kind]);
        }
        if (!empty($date)) {
            $query->andWhere(["tab_bet.bet_date" => $date]);
        }
        $query->andWhere("bet_result >=" . (int)$result);
        // Debug::showLastSql($query);
        $count = (int)$query->count();

        /** @var Bet[] $betList */
        $betList = $query->offset($offset)->limit($size)->all();

        $lotteryBetVoList = [];
        foreach ($betList as $bet) {
            $vo = new LotteryBetVo();
            $vo->initByBet($bet);
            $lotteryBetVoList[] = $vo;
        }

        return [
            "rows" => $lotteryBetVoList,
            "count" => $count,
        ];
    }

    /**
     * 【后台】获取注单列表数据（如果不是管理员，则会排除不属于自己的会员的数据）
     * @param $offset
     * @param $limit
     * @param UserModel $user
     * @param $kind
     * @param $period
     * @param $startTime
     * @param $endTime
     * @param $username
     * @param $minBetResult
     * @param $maxBetResult
     * @return array
     */
    public function getBetRowVoTable($offset, $limit, UserModel $user, $kind, $period, $startTime, $endTime, $username, $minBetResult, $maxBetResult) {
        $query = Bet::find()->select(["tab_bet.*"])
            ->leftJoin("tab_schedule", "tab_schedule.id = tab_bet.schedule_id");

        if (!empty($kind)) {
            $query->andWhere(["tab_schedule.kind" => $kind]);
        }
        if (!empty($period)) {
            $query->andWhere(["tab_schedule.period" => $period]);
        }
        if (!empty($username)) {
            $query->andWhere(["tab_bet.username" => $username]);
        }
        if (!empty($startTime)) {
            $query->andWhere([">=", "tab_bet.record_time", $startTime]);
        }
        if (!empty($endTime)) {
            $query->andWhere(["<=", "tab_bet.record_time", $endTime]);
        }
        if (!empty($minBetResult)) {
            $query->andWhere([">=", "tab_bet.bet_result", $minBetResult]);
        }
        if (!empty($maxBetResult)) {
            $query->andWhere(["<=", "tab_bet.bet_result", $maxBetResult]);
        }

        // 过滤不属于自己的会员数据
        if (!$user->isAdmin()) {
//            $query->leftJoin("user_member", "user_member.user_id = tab_bet.member_id");
//            if ($user->isSuperHolder()) {
//                $query->andWhere(["user_member.super_holder_id" => $user->userId]);
//            } else if ($user->isHolder()) {
//                $query->andWhere(["user_member.holder_id" => $user->userId]);
//            } else if ($user->isVendor()) {
//                $query->andWhere(["user_member.vendor_id" => $user->userId]);
//            } else if ($user->isAgent()) {
//                $query->andWhere(["user_member.agent_id" => $user->agentId]);
//            }
            $query->andWhere(UserUtil::getUserQuery());
        }
        $query->orderBy(["tab_bet.id" => SORT_DESC]);

        $count = (int)$query->count();
        /** @var Bet[] $betList */
        $betList = $query->offset($offset)->limit($limit)->all();
        $betRowVoList = [];
        $memberIdSet = [];
        foreach ($betList as $bet) {
            $memberIdSet[] = $bet['member_id'];
            $vo = new BetRowVo();
            $vo->initByBet($bet);
            $betRowVoList[] = $vo;
        }
        //获取用户当前信用额
        $creditSet = UserMember::byIdsGetCredit(array_unique($memberIdSet));
        $creditSetArr = array_column($creditSet,'credit','user_id');

        foreach ($betRowVoList as $betRowVo) {
            $betRowVo->credit = $creditSetArr[$betRowVo->userId];
        }
        return [
            "count" => $count,
            "rows" => $betRowVoList
        ];
    }

    /**
     * 获取会员当天下注总额
     * @return string
     */
    public function getBetAmountToday(){
        $betAmount = Bet::find()->where(["member_id" => UserUtil::getUserId(), "bet_date" => TimeUtil::today()])->select("SUM(bet_amount) as bet_amount")->one();
        return $betAmount->bet_amount;
    }
}
