<?php


namespace common\services;


use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\vo\api\BankInfoResponseVo;
use common\models\vo\api\BankPayRequestVo;
use common\models\vo\api\BankWithdrawRequestVo;
use common\models\vo\api\OfflineQrCodeRequestVo;
use common\models\vo\api\OfflineQrCodeResponseVo;
use common\models\vo\api\QrCodePayRequestVo;
use common\traits\InstanceTrait;
use common\utils\ApiUtil;
use common\utils\CurlUtil;
use common\utils\DLogger;
use Yii;
use yii\helpers\Json;

/**
 * Class AuthService api服务
 * @package common\services
 */
class ApiService extends BaseService
{
    use InstanceTrait;


    /**
     * 查询订单是否完成充值
     * @param $orderNo
     * @return bool
     * @throws ForeseeableException
     */
    public function payQuery($orderNo) {
        $partnerCode = ApiUtil::getPartnerCode();
        $param = [time(), $partnerCode, $orderNo];
        $hash = ApiUtil::makeHash($partnerCode, $param);
        $response = CurlUtil::httpsRequest(ApiUtil::getPayApiUrl() . '/pay-query?Hash=' . (string)$hash . '&PartnerCode=' . (string)$partnerCode);
        if (intval($response[1]) !== 200) {
            throw new ForeseeableException("请求支付平台失败。请重试");
        }

        $tmpArr = Json::decode($response[0]);
        return isset($tmpArr["TradeState"]) ? intval($tmpArr["TradeState"]) === 2 : false;
    }

    /**
     * 获取充值订单号（第三方给的订单号）
     * @param OfflineQrCodeRequestVo $vo
     * @return OfflineQrCodeResponseVo
     * @throws SystemException
     */
    public function getOfflineQrCode(OfflineQrCodeRequestVo $vo) {
        if (!$vo->validate()) {
            throw new SystemException($vo->getFirstError());
        }

        $partnerCode = ApiUtil::getPartnerCode();
        $param = [time(), $partnerCode, (string)$vo->userId, $vo->username, $vo->orderNo, $vo->amount, $vo->payType];
        $hash = ApiUtil::makeHash($partnerCode, $param);
        $response = CurlUtil::httpsRequest(ApiUtil::getPayApiUrl() . '/pay?Hash=' . (string)$hash . '&PartnerCode=' . (string)$partnerCode);

        $responseVo = new OfflineQrCodeResponseVo();
        $tmpArr = Json::decode($response[0]);
        $responseVo->setAttributes($tmpArr);
        if (!$responseVo->isDone()) {
            throw new SystemException("API 请求失败。返回结果：" . $response[0]);
        }
        return $responseVo;
    }

    /**
     * 获取支付类型
     * @return mixed
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function getPayType() {
        $url = ApiUtil::getPayApiUrl() . '/prepare-pay';
        $response = CurlUtil::httpsRequest($url);
        if (intval($response[1]) !== 200) {
            throw new SystemException("api 请求失败。" . Json::encode($response));
        }
        $data = Json::decode($response[0]);
        if ($data['ReturnCode'] !== C::RESPONSE_CODE_DONE) {
            throw new ForeseeableException("操作失败！");
        }
        return $data['PayType'];
    }

    /**
     * 获取银行信息
     * @return BankInfoResponseVo
     * @throws SystemException
     */
    public function getBankInfo() {
        $partnerCode = ApiUtil::getPartnerCode();
        $param = [time(), $partnerCode];
        $hash = ApiUtil::makeHash($partnerCode, $param);
        $url = ApiUtil::getPayApiUrl() . "/bank-info" . "?Hash=" . (string)$hash . "&PartnerCode=" . (string)$partnerCode;
        $response = CurlUtil::httpsRequest($url);
        $httpCode = intval($response[1]);
        if ($httpCode !== 200) {
            throw new SystemException("api请求失败。状态码:" . $httpCode);
        }
        $tmpArr = Json::decode($response[0]);
        if ($tmpArr == false || $tmpArr["code"] < 0) {
            throw new SystemException("请求失败。" . $tmpArr["message"]);
        }
        $responseVo = new BankInfoResponseVo();
        $responseVo->setAttributes($tmpArr["data"]);
        return $responseVo;
    }

    /**
     * 银行支付
     * @param BankPayRequestVo $requestVo
     * @return string 支付订单id
     * @throws SystemException
     */
    public function bankPay(BankPayRequestVo $requestVo) {
        $partnerCode = ApiUtil::getPartnerCode();
        $param = [
            time(), $partnerCode, $requestVo->amount,
            $requestVo->recvName, $requestVo->recvAccount, $requestVo->recvUsername,
            $requestVo->payUsername, $requestVo->payTime,
            $requestVo->order3rdNo, $requestVo->memberCode, $requestVo->nickname
        ];
        $hash = ApiUtil::makeHash($partnerCode, $param);
        $url = ApiUtil::getPayApiUrl() . "/bank-pay";
        $response = CurlUtil::httpsRequest($url . "?Hash=" . (string)$hash . "&PartnerCode=" . (string)$partnerCode);
        $httpCode = intval($response[1]);
        if ($httpCode !== 200) {
            throw new SystemException("api请求失败。状态码:" . $httpCode);
        }

        $tmpArr = Json::decode($response[0]);
        if ($tmpArr["code"] < 0) {
            throw new SystemException("请求失败。" . $tmpArr["message"]);
        }
        return $tmpArr["data"]["orderNo"];
    }

    /**
     * @param $requestVo
     * @return mixed
     * @throws SystemException
     */
    public function bankWithdraw(BankWithdrawRequestVo $requestVo) {
        $partnerCode = ApiUtil::getPartnerCode();
        $param = [
            time(), $partnerCode, $requestVo->amount,
            $requestVo->recvName, $requestVo->recvAccount, $requestVo->recvUsername,
            $requestVo->order3rdNo, $requestVo->memberCode, $requestVo->nickname
        ];
        $hash = ApiUtil::makeHash($partnerCode, $param);
        $url = ApiUtil::getPayApiUrl() . "/bank-withdraw?Hash=" . (string)$hash . "&PartnerCode=" . (string)$partnerCode;
        $response = CurlUtil::httpsRequest($url);
        $httpCode = intval($response[1]);
        if ($httpCode !== 200) {
            throw new SystemException("api请求失败。状态码:" . $httpCode);
        }

        $tmpArr = Json::decode($response[0]);
        if ($tmpArr["code"] < 0) {
            throw new SystemException("请求失败。" . $tmpArr["message"]);
        }
        return $tmpArr["data"]["orderNo"];
    }

    /**
     * 二维码付款
     * @param QrCodePayRequestVo $responseVo
     * @return array
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function qrCodePay(QrCodePayRequestVo $responseVo) {
        $partnerCode = ApiUtil::getPartnerCode();
        $param = [time(), $partnerCode, $responseVo->type, $responseVo->amount, $responseVo->orderNo, $responseVo->memberCode, $responseVo->nickname];
        $hash = ApiUtil::makeHash($partnerCode, $param);
        $url = ApiUtil::getPayApiUrl() . "/qr-code-pay?Hash=" . (string)$hash . "&PartnerCode=" . (string)$partnerCode;
        $response = CurlUtil::httpsRequest($url);
        $httpCode = intval($response[1]);
        if ($httpCode !== 200) {
            Yii::error($url, "api请求失败。状态码:" . $httpCode);
            throw new SystemException("api请求失败。状态码:" . $httpCode);
        }

        $tmpArr = Json::decode($response[0]);
        $code = intval($tmpArr["code"]);
        if ($code === -1) {
            throw new ForeseeableException($tmpArr["message"]);
        } else if ($code < 0) {
            throw new SystemException("请求失败。" . $tmpArr["message"]);
        }

        return [
            "orderNo" => $tmpArr["data"]["orderNo"], // 支付平台的订单号（可用于查询订单）
            "qrCodeUrl" => $tmpArr["data"]["qrCodeUrl"],  // 支付二维码
        ];
    }
}
