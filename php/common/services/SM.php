<?php


namespace common\services;


/**
 * Class SM Services Manager 服务管理器
 * @package common\services
 */
class SM
{


    /**
     * @return FileService
     */
//    public static function getFileService() {
//        return FileService::getIns();
//    }

    /**
     * @return UserService
     */
    public static function getUserService() {
        return UserService::getIns();
    }

    /**
     * @return AuthService
     */
    public static function getAuthService() {
        return AuthService::getIns();
    }

    /**
     * @return NoticeService
     */
    public static function getNoticeService() {
        return NoticeService::getIns();
    }

    /**
     * @return OrderWalletService
     */
    public static function getOrderWalletService() {
        return OrderWalletService::getIns();
    }

    /**
     * @return ApiService
     */
    public static function getApiService() {
        return ApiService::getIns();
    }

    /**
     * @return ActivityService
     */
    public static function getActivityService() {
        return ActivityService::getIns();
    }

    /**
     * @return ScheduleService
     */
    public static function getScheduleService() {
        return ScheduleService::getIns();
    }

    /**
     * @return BetService
     */
    public static function getBetService() {
        return BetService::getIns();
    }

    /**
     * @return IntegralService
     */
    public static function getIntegralService() {
        return IntegralService::getIns();
    }

    /**
     * @return PayoutService
     */
    public static function getPayoutService() {
        return PayoutService::getIns();
    }

    /**
     * @return LogService
     */
    public static function getLogService() {
        return LogService::getIns();
    }

    /**
     * @return SystemService
     */
    public static function getSystemService() {
        return SystemService::getIns();
    }
}
