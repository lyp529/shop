<?php


namespace common\services;


use common\constants\C;
use common\constants\L;
use common\exceptions\SystemException;
use common\models\db\Schedule;
use common\models\UserModel;
use common\traits\InstanceTrait;
use common\utils\DBUtil;
use common\utils\DLogger;
use common\utils\game\LogUtil;
use common\utils\game\SscUtil;
use common\utils\game\SscWinUtil;
use common\utils\TimeUtil;
use Exception;
use Throwable;
use Yii;

/**
 * Class PayoutService 派彩服务
 * @package common\services
 */
class PayoutService extends BaseService
{
    use InstanceTrait;

    /**
     * 自动派彩
     * @param $kind
     * @throws SystemException
     */
    public function autoPayout($kind) {
        $lockCacheKey = "PayoutService::autoPayout()";
//        $value = Yii::$app->cache->get("PayoutService::autoPayout()");
//        if (!empty($value)) {
//            // 有一个进程正在跑，停止进行派彩。防止重复派彩
//            echo "有一个进程正在跑，停止进行派彩。防止重复派彩<br>";
//            return;
//        }
//        Yii::$app->cache->set($lockCacheKey, time(), 60);

        // 暂时先使用这种方式 by lizr 2019-10-15
        if (1) {
            $scheduleList = SM::getScheduleService()->getNotPayoutScheduleList($kind);
            foreach ($scheduleList as $schedule) {
                $this->payout($schedule);
            }
            Yii::$app->cache->delete($lockCacheKey);
        } else {
//            $exception = null;
//            try {
//                $scheduleList = $this->getNotPayoutScheduleList();
//                foreach ($scheduleList as $schedule) {
//                    $this->payout($schedule);
//                }
//            } catch (Exception $e) {
//                $exception = $e;
//            } catch (Throwable $e) {
//                $exception = $e;
//            } finally {
//                Yii::$app->cache->delete($lockCacheKey);
//                if ($exception !== null) {
//                    DLogger::error($exception->getMessage() . $exception->getTraceAsString(), "cron/ssc-data SM::getPayoutService()->autoPayout();");
//                    throw new $exception;
//                }
//            }
        }
    }



    /**
     * 派彩本期所有注单
     * @param Schedule $schedule
     * @throws SystemException
     */
    public function payout(Schedule $schedule) {
        //DBUtil::runOnTransaction(function () use ($schedule) {
        if (empty($schedule->getResultNums())) {
            return;
        }

        $betList = $schedule->getBetList();
        foreach ($betList as $bet) {
            $winMultiple = SscWinUtil::getGroupWin($bet->bet_type, $bet->getBetContent(), $schedule->getResultNums());
            $bet->bet_result = C::getBetUnitValue($bet->unit) / 2 * $winMultiple * $bet->multiple;
            if (!$bet->save()) {
                throw new SystemException($bet->getFirstError());
            }
            if ($bet->bet_result > 0) {
                $user = UserModel::initMember($bet->member_id);
                if ($user === null) {
                    throw new SystemException("会员信息不存在");
                }
                $user->addCredit($bet->bet_result);
                if (!$user->save()) {
                    throw new SystemException($user->getFirstError());
                }
                //会员中奖超1w
                $bet->bet_result > 10000 && SM::getActivityService()->addEarnRankReal($bet->username, $schedule->kind, $bet->bet_result);

                LogUtil::cLog(L::CLOG_WIN, $user, $bet->bet_result, $bet->bet_no);
            }
        }
        $schedule->status = C::SCHEDULE_STATUS_OPEN;
        $schedule->payout_time = TimeUtil::now();
        if (!$schedule->save()) {
            throw new SystemException($schedule->getFirstError());
        }
        //});
    }
}
