<?php


namespace common\constants;


use common\models\vo\frontend\GameVo;
use common\utils\ArrayUtil;

/**
 * Class 彩票大厅模块常量
 * @note 一经定义上线后不得修改,不然会影响旧数据
 * @package common\constants
 */
class P
{
    /**
     * 游戏类型
     */
    const GAME_TYPE_SC = 1; //时彩
    const GAME_TYPE_X5 = 2; //11选5
    const GAME_TYPE_K3 = 3; //快3


    //================================= 时彩 ======================================
    /**
     * 五星玩法
     */
    //const SC_TYPE_FIVE = '';
    const SC_FIVE = 'SC_FIVE'; //五星
    const SC_FIVE_120 = 'SC_FIVE_120'; //五星组选120
    const SC_FIVE_60 = 'SC_FIVE_60'; //五星组选60
    const SC_FIVE_30 = 'SC_FIVE_30'; //五星组选30
    const SC_FIVE_20 = 'SC_FIVE_20'; //五星组选20
    const SC_FIVE_10 = 'SC_FIVE_10'; //五星组选10
    const SC_FIVE_5 = 'SC_FIVE_5'; //五星组选5

    //---------------------------------
    /**
     * 四星玩法
     */
    //const SC_TYPE_FOUR = '';
    const SC_FOUR_Q4 = 'SC_FOUR_Q4';//前四
    const SC_FOUR_H4 = 'SC_FOUR_H4';//后四
    const SC_FOUR_24 = 'SC_FOUR_24';
    const SC_FOUR_12 = 'SC_FOUR_12';
    const SC_FOUR_6 = 'SC_FOUR_6';
    const SC_FOUR_4 = 'SC_FOUR_4';
    //------------------------------------

    /**
     * 三星玩法
     */
    //const SC_TYPE_THREE = '';
    const SC_THREE_Q3 = 'SC_THREE_Q3';
    const SC_THREE_Z3 = 'SC_THREE_Z3';
    const SC_THREE_H3 = 'SC_THREE_H3';
    //------------------------------------
    /**
     * 三星组选
     */
    //const SC_TYPE_THREE_ZX = '';
    //3组3
    const SC_THREE_Q3Z3 = 'SC_THREE_Q3Z3';
    const SC_THREE_Z3Z3 = 'SC_THREE_Z3Z3';
    const SC_THREE_H3Z3 = 'SC_THREE_H3Z3';
    //3组6
    const SC_THREE_Q3Z6 = 'SC_THREE_Q3Z6';
    const SC_THREE_Z3Z6 = 'SC_THREE_Z3Z6';
    const SC_THREE_H3Z6 = 'SC_THREE_H3Z6';

    //------------------------------------
    /**
     * 二星直选
     */
    //const SC_TYPE_TWO = '';
    const SC_TWO_Q2 = 'SC_TWO_Q2';
    const SC_TWO_H2 = 'SC_TWO_H2';
    //------------------------------------
    /**
     * 二星组选
     */
    //const SC_TYPE_TWO_ZX = '';
    const SC_TWO_Q2ZX = 'SC_TWO_Q2ZX';
    const SC_TWO_H2ZX = 'SC_TWO_H2ZX';
    //------------------------------------
    /**
     * 定位胆
     */
    //const SC_TYPE_DWD = '';
    const SC_FIVE_DWD = 'SC_FIVE_DWD'; //五星定位胆
    //------------------------------------
    /**
     * 不定胆
     */
    //const SC_TYPE_BDD = '';
    const SC_BDD_Q31M = 'SC_BDD_Q31M';
    const SC_BDD_Z31M = 'SC_BDD_Z31M';
    const SC_BDD_H31M = 'SC_BDD_H31M';
    const SC_BDD_Q32M = 'SC_BDD_Q32M';
    const SC_BDD_H32M = 'SC_BDD_H32M';
    const SC_BDD_4X1M = 'SC_BDD_4X1M';
    const SC_BDD_4X2M = 'SC_BDD_4X2M';
    const SC_BDD_5X2M = 'SC_BDD_5X2M';
    const SC_BDD_5X3M = 'SC_BDD_5X3M';

    //------------------------------------
    /**
     * 任意玩法
     */
    //const SC_TYPE_RY = '';
    const SC_RY_R2FS = 'SC_RY_R2FS';
    const SC_RY_R2DS = 'SC_RY_R2DS';
    const SC_RY_R2ZFS = 'SC_RY_R2ZFS';
    const SC_RY_R2ZDS = 'SC_RY_R2ZDS';
    const SC_RY_R3FS = 'SC_RY_R3FS';
    const SC_RY_R3DS = 'SC_RY_R3DS';
    const SC_RY_R3ZS = 'SC_RY_R3ZS';
    const SC_RY_R3FL = 'SC_RY_R3FL';
    const SC_RY_R3HH = 'SC_RY_R3HH';
    const SC_RY_R4FS = 'SC_RY_R4FS';
    const SC_RY_R4DS = 'SC_RY_R4DS';


    //------------------------------------
    /**
     * 时彩位数：个
     */
    const SC_DIGIT_GE = 1;
    /**
     * 时彩位数：十
     */
    const SC_DIGIT_SHI = 2;
    /**
     * 时彩位数：百
     */
    const SC_DIGIT_BAI = 3;
    /**
     * 时彩位数：千
     */
    const SC_DIGIT_QIAN = 4;
    /**
     * 时彩位数：万
     */
    const SC_DIGIT_WAN = 5;


    /**
     * 趋势图类型：万
     */
    const SC_TREND_TYPE_WAN = 1;
    /**
     * 趋势图类型：千
     */
    const SC_TREND_TYPE_QIAN = 2;
    /**
     * 趋势图类型：百
     */
    const SC_TREND_TYPE_BAI = 3;
    /**
     * 趋势图类型：十
     */
    const SC_TREND_TYPE_SHI = 4;
    /**
     * 趋势图类型：个
     */
    const SC_TREND_TYPE_GE = 5;


    /**
     * 数值类型：万
     */
    const SC_LABEL_WAN = "万位";
    /**
     * 数值类型：千
     */
    const SC_LABEL_QIAN = "千位";
    /**
     * 数值类型：百
     */
    const SC_LABEL_BAI = "百位";
    /**
     * 数值类型：十
     */
    const SC_LABEL_SHI = "十位";
    /**
     * 数值类型：个
     */
    const SC_LABEL_GE = "个位";
    /**
     * 数值类型：选择
     */
    const SC_LABEL_XZ = "选择";
    /**
     * 数值类型：位数
     */
    const SC_LABEL_WS = "位数";
    /**
     * 数值类型：单号
     */
    const SC_LABEL_DH = "单号";
    /**
     * 数值类型：二重号
     */
    const SC_LABEL_2CH = "二重号";
    /**
     * 数值类型：单号
     */
    const SC_LABEL_3CH = "三重号";
    /**
     * 数值类型：单号
     */
    const SC_LABEL_4CH = "四重号";
    /**
     * 数值类型：不定位
     */
    const SC_LABEL_BDW = "不定位";
    /**
     * 数值类型：位数
     */
    const SC_LABEL_DIGIT = "位数";

    /**
     * 玩法选项：五星-万千百十个
     */
    const SC_OPT_5 = "5";
    /**
     * 玩法选项：前四-万千百十
     */
    const SC_OPT_Q4 = "Q4";
    /**
     * 玩法选项：后四-千百十个
     */
    const SC_OPT_H4 = "H4";
    /**
     * 玩法选项：前三-万千百
     */
    const SC_OPT_Q3 = "Q3";
    /**
     * 玩法选项：中三-千百十
     */
    const SC_OPT_Z3 = "Z3";
    /**
     * 玩法选项：后三-百十个
     */
    const SC_OPT_H3 = "H3";
    /**
     * 玩法选项：前二-万千
     */
    const SC_OPT_Q2 = "Q2";
    /**
     * 玩法选项：后二-十个
     */
    const SC_OPT_H2 = "H2";
    /**
     * 玩法线性：二重号
     */
    const SC_OPT_2CH = "2CH";
    /**
     * 玩法选项：二重号、单号
     */
    const SC_OPT_2CH_DH = "2CH_DH";
    /**
     * 玩法选项：三重号、单号
     */
    const SC_OPT_3CH_DH = "3CH_DH";
    /**
     * 玩法选项：三重号、二重号
     */
    const SC_OPT_3CH_2CH = "3CH_2CH";
    /**
     * 玩法选项：四重号、单号
     */
    const SC_OPT_4CH_DH = "4CH_DH";
    /**
     * 玩法选项：不定位
     */
    const SC_OPT_BDW = "BDW";
    /**
     * 玩法选项：选择
     */
    const SC_OPT_XZ = "XZ";

    /**
     * 时彩单注金额（单位由其他字段决定）
     */
    const SC_BET_UNIT_NUM = 2;

    /**
     * 私彩周期 /秒
     */
    const SC_PERSONAL_PERIOD_SEC = 300;

    /**
     * 公彩周期 /秒
     */
    const SC_OFFICIAL_PERIOD_SEC = 1200;


    //================================= 11选5  ======================================
//    const EF_TYPE_Q1 = '';
//    const EF_Q1_ZX = '';//前一直选
//
//    const EF_TYPE_Q2 = '';
//    const EF_Q2_DX = '';//前二直选
//    const EF_Q2_ZX = '';//前二组选
//
//
//    const EF_TYPE_Q3 = '';
//    const EF_Q3_DX = '';//前三直选
//    const EF_Q3_ZX = '';//前三组选
//
//    const EF_TYPE_DWD = '';
//    const EF_DWD = '';//定位胆
//
//    const EF_TYPE_BZW = '';
//    const EF_BZW = ''; //不定位
//
//    /**
//     * 任意复选
//     */
//    const EF_TYPE_RYFS = '';
//    const EF_RYFS_1Z1 = '';
//    const EF_RYFS_2Z2 = '';
//    const EF_RYFS_3Z3 = '';
//    const EF_RYFS_4Z4 = '';
//    const EF_RYFS_5Z5 = '';
//    const EF_RYFS_6Z5 = '';
//    const EF_RYFS_7Z5 = '';
//    const EF_RYFS_8Z5 = '';
//
//    /**
//     * 任意组选
//     */
//    const EF_TYPE_RYZX = '';
//    const EF_RYZX_1Z1 = '';
//    const EF_RYZX_2Z2 = '';
//    const EF_RYZX_3Z3 = '';
//    const EF_RYZX_4Z4 = '';
//    const EF_RYZX_5Z5 = '';
//    const EF_RYZX_6Z5 = '';
//    const EF_RYZX_7Z5 = '';
//    const EF_RYZX_8Z5 = '';


    /**
     * 是否是复式玩法
     * @param $subType
     */
    public function isFS($subType) {

    }

    /**
     * 是否是单式玩法
     * @param $subType
     */
    public function isDS($subType) {

    }

    /**
     * 是否是组选玩法
     * @param $subType
     */
    public function isZS($subType) {

    }

    /**
     * 获取彩票游戏类型分组列表
     */
    public static function getPKindMap() {
        $config = [
            // 时彩
            P::GAME_TYPE_SC => [
                C::KIND_SSC_CHONGQING => "lottery/icon/chongqing.png", // 游戏类型 => 图片src （相对于  `vue-cli/frontend/src/assets/img` 的路径）
//                C::KIND_SSC_BANGKOK => "lottery/icon/bangkok.jpg", // 游戏类型 => 图片src （相对于  `vue-cli/frontend/src/assets/img` 的路径）
                C::KIND_SSC_BERLIN => "lottery/icon/berlin.png"
            ],
            // 11选5
            //...
            // 快3
            // ...
        ];

        // 将配置转为前端所读的格式
        $pKindList = [];
        foreach ($config as $gameType => $kindMap) {
            $pKindList[$gameType] = [];
            foreach ($kindMap as $kind => $src) {
                $kindName = C::getKindLabel($kind);
                $pKindList[$gameType][] = GameVo::initByParams($kind, $kindName, $src);
            }
        }
        return $pKindList;
    }

    /**
     * 获取时彩
     * @param int|null $digit
     * @return string|string[]
     */
    public static function getScDigitLabel($digit = null) {
        $labels = [
            P::SC_DIGIT_GE => "个",
            P::SC_DIGIT_SHI => "十",
            P::SC_DIGIT_BAI => "百",
            P::SC_DIGIT_QIAN => "千",
            P::SC_DIGIT_WAN => "万",
        ];
        return C::commonReturn($labels, $digit);
    }

    /**
     * 获取时彩变量的标签
     * @param string|null $type
     * @return string|string[]
     */
    public static function getScBetTypeLabel($type = null) {
        $labels = [
            P::SC_FIVE => "五星",
            P::SC_FIVE_120 => "五星组选120",
            P::SC_FIVE_60 => "五星组选60",
            P::SC_FIVE_30 => "五星组选30",
            P::SC_FIVE_20 => "五星组选20",
            P::SC_FIVE_10 => "五星组选10",
            P::SC_FIVE_5 => "五星组选5",
            P::SC_FOUR_Q4 => "前四",
            P::SC_FOUR_H4 => "后四",
            P::SC_FOUR_24 => "四星组选24",
            P::SC_FOUR_12 => "四星组选12",
            P::SC_FOUR_6 => "四星组选6",
            P::SC_FOUR_4 => "四星组选4",
            P::SC_THREE_Q3 => "前三",
            P::SC_THREE_Z3 => "中三",
            P::SC_THREE_H3 => "后三",
            P::SC_THREE_Q3Z3 => "前三组选3",
            P::SC_THREE_Q3Z6 => "前三组选6",
            P::SC_THREE_Z3Z3 => "中三组选3",
            P::SC_THREE_Z3Z6 => "中三组选6",
            P::SC_THREE_H3Z3 => "后三组选3",
            P::SC_THREE_H3Z6 => "后三组选6",
            P::SC_TWO_Q2 => "前二",
            P::SC_TWO_H2 => "后二",
            P::SC_TWO_Q2ZX => "前二组选",
            P::SC_TWO_H2ZX => "后二组选",
            P::SC_FIVE_DWD => "五星定位胆",
            P::SC_BDD_Q31M => "前三一码",
            P::SC_BDD_Z31M => "中三一码",
            P::SC_BDD_H31M => "后三一码",
            P::SC_BDD_Q32M => "前三二码",
            P::SC_BDD_H32M => "后三二码",
            P::SC_BDD_4X1M => "四星一码",
            P::SC_BDD_4X2M => "四星二码",
            P::SC_BDD_5X2M => "五星二码",
            P::SC_BDD_5X3M => "五星三码",
        ];

        return C::commonReturn($labels, $type);
    }

    /**
     * 获取时彩玩法分组列表
     */
    public static function getScGroupList() {
        return [
            "五星" => [
                P::SC_FIVE, P::SC_FIVE_120, //, P::SC_FIVE_60, P::SC_FIVE_30, P::SC_FIVE_20, P::SC_FIVE_10, P::SC_FIVE_5
            ],
            "四星" => [
                P::SC_FOUR_Q4, P::SC_FOUR_H4, P::SC_FOUR_24,//, P::SC_FOUR_12, P::SC_FOUR_6, P::SC_FOUR_4
            ],
            "三星" => [
                P::SC_THREE_Q3, P::SC_THREE_Z3, P::SC_THREE_H3,
                //P::SC_THREE_Q3Z3, P::SC_THREE_Q3Z6, P::SC_THREE_Z3Z3, P::SC_THREE_Z3Z6, P::SC_THREE_H3Z3, P::SC_THREE_H3Z6,
            ],
            "二星" => [
                P::SC_TWO_Q2, P::SC_TWO_H2,// P::SC_TWO_Q2ZX, P::SC_TWO_H2ZX,
            ],
            "定位胆" => [
                P::SC_FIVE_DWD,
            ],
//            "不定胆" => [
//                P::SC_BDD_Q31M, P::SC_BDD_Z31M, P::SC_BDD_H31M, P::SC_BDD_Q32M, P::SC_BDD_H32M, P::SC_BDD_4X1M, P::SC_BDD_4X2M, P::SC_BDD_5X2M, P::SC_BDD_5X3M,
//            ],
        ];
    }

    /**
     * 获取玩法选项标签
     * @param null $betType
     * @return array
     */
    public static function getScBetTypeLabelMap($betType = null) {
        /** @var array $map [玩法 => 选项配置key] */
        $map = [
            P::SC_FIVE => P::SC_OPT_5,
            P::SC_FIVE_120 => P::SC_OPT_XZ,
            P::SC_FIVE_60 => P::SC_OPT_2CH_DH,
            P::SC_FIVE_30 => P::SC_OPT_2CH_DH,
            P::SC_FIVE_20 => P::SC_OPT_3CH_DH,
            P::SC_FIVE_10 => P::SC_OPT_3CH_2CH,
            P::SC_FIVE_5 => P::SC_OPT_4CH_DH,
            P::SC_FOUR_Q4 => P::SC_OPT_Q4,
            P::SC_FOUR_H4 => P::SC_OPT_H4,
            P::SC_FOUR_24 => P::SC_OPT_XZ,
            P::SC_FOUR_12 => P::SC_OPT_2CH_DH,
            P::SC_FOUR_6 => P::SC_OPT_2CH,
            P::SC_FOUR_4 => P::SC_OPT_3CH_DH,
            P::SC_THREE_Q3 => P::SC_OPT_Q3,
            P::SC_THREE_Z3 => P::SC_OPT_Z3,
            P::SC_THREE_H3 => P::SC_OPT_H3,
            P::SC_THREE_Q3Z3 => P::SC_OPT_XZ,
            P::SC_THREE_Q3Z6 => P::SC_OPT_XZ,
            P::SC_THREE_Z3Z3 => P::SC_OPT_XZ,
            P::SC_THREE_Z3Z6 => P::SC_OPT_XZ,
            P::SC_THREE_H3Z3 => P::SC_OPT_XZ,
            P::SC_THREE_H3Z6 => P::SC_OPT_XZ,
            P::SC_TWO_Q2 => P::SC_OPT_Q2,
            P::SC_TWO_H2 => P::SC_OPT_H2,
            P::SC_TWO_Q2ZX => P::SC_OPT_XZ,
            P::SC_TWO_H2ZX => P::SC_OPT_XZ,
            P::SC_FIVE_DWD => P::SC_OPT_5,
            P::SC_BDD_Q31M => P::SC_OPT_XZ,
            P::SC_BDD_Z31M => P::SC_OPT_XZ,
            P::SC_BDD_H31M => P::SC_OPT_XZ,
            P::SC_BDD_Q32M => P::SC_OPT_XZ,
            P::SC_BDD_H32M => P::SC_OPT_XZ,
            P::SC_BDD_4X1M => P::SC_OPT_BDW,
            P::SC_BDD_4X2M => P::SC_OPT_BDW,
            P::SC_BDD_5X2M => P::SC_OPT_BDW,
            P::SC_BDD_5X3M => P::SC_OPT_BDW,
        ];
        // 转化数组
        /** @var array $labelMap [玩法 => 选项配置] */
        $labelMap = [];
        $optMap = P::getScBetTypeLabelOptMap();
        foreach ($map as $playType => $optKey) {
            $labelMap[$playType] = ArrayUtil::issetDefault($optMap, $optKey, []);
        }
        return C::commonReturn($labelMap, $betType, []);
    }

    /**
     * 玩法选项配置
     */
    private static function getScBetTypeLabelOptMap() {
        return [
            // 五星 => 万、千、百、十、个
            P::SC_OPT_5 => [
                P::SC_LABEL_WAN, P::SC_LABEL_QIAN, P::SC_LABEL_BAI, P::SC_LABEL_SHI, P::SC_LABEL_GE
            ],
            // 前四 => 万、千、百、十
            P::SC_OPT_Q4 => [
                P::SC_LABEL_WAN, P::SC_LABEL_QIAN, P::SC_LABEL_BAI, P::SC_LABEL_SHI
            ],
            P::SC_OPT_H4 => [
                P::SC_LABEL_QIAN, P::SC_LABEL_BAI, P::SC_LABEL_SHI, P::SC_LABEL_GE
            ],
            P::SC_OPT_Q3 => [
                P::SC_LABEL_WAN, P::SC_LABEL_QIAN, P::SC_LABEL_BAI
            ],
            P::SC_OPT_Z3 => [
                P::SC_LABEL_QIAN, P::SC_LABEL_BAI, P::SC_LABEL_SHI
            ],
            P::SC_OPT_H3 => [
                P::SC_LABEL_BAI, P::SC_LABEL_SHI, P::SC_LABEL_GE
            ],
            P::SC_OPT_Q2 => [
                P::SC_LABEL_WAN, P::SC_LABEL_QIAN
            ],
            P::SC_OPT_H2 => [
                P::SC_LABEL_SHI, P::SC_LABEL_GE
            ],
            P::SC_OPT_2CH => [
                P::SC_LABEL_2CH
            ],
            P::SC_OPT_XZ => [
                P::SC_LABEL_XZ
            ],
            P::SC_OPT_2CH_DH => [
                P::SC_LABEL_2CH, P::SC_LABEL_DH
            ],
            P::SC_OPT_3CH_DH => [
                P::SC_LABEL_3CH, P::SC_LABEL_DH
            ],
            P::SC_OPT_3CH_2CH => [
                P::SC_LABEL_3CH, P::SC_LABEL_2CH
            ],
            P::SC_OPT_BDW => [
                P::SC_LABEL_BDW
            ],
            P::SC_OPT_4CH_DH => [
                P::SC_LABEL_4CH, P::SC_LABEL_DH
            ]
        ];
    }

    /**
     * 获取时彩玩法说明
     */
    public static function getScBetTypeNodeMap() {
        return [
            P::SC_FIVE => [
                "label" => "从万位、千位、百位、十位、个位任意1个位置或多个位置上选择1个号码，所选号码与相同位置上的开奖号码一致，即为中奖。",
                "example" => "投注方案： 12345 ； 开奖号码： 12345 ，即中奖。",
            ],
            P::SC_FIVE_120 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_FIVE_60 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_FIVE_30 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_FIVE_20 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_FIVE_10 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_FIVE_5 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_FOUR_Q4 => [
                "label" => "从万位、千位、百位、十位中选择一个 4 位数号码组成一注，所选号码与开奖号码相同，且顺序一致，即为中奖。",
                "example" => "投注方案： 1234 ； 开奖号码： 12345 ，即中奖。",
            ],
            P::SC_FOUR_H4 => [
                "label" => "从千位、百位、十位、个位中选择一个 4 位数号码组成一注，所选号码与开奖号码相同，且顺序一致，即为中奖。",
                "example" => "投注方案： 2345 ； 开奖号码： 12345 ，即中奖。",
            ],
            P::SC_FOUR_24 => [
                "label" => "从 0-9 中任意选择 4 个号码组成一注，所选号码与开奖号码的千位、百位、十位、个位相同，且顺序不限，即为中奖。",
                "example" => "投注方案： 0568 ，开奖号码的四个数字只要包含 0 、 5 、 6 、 8 ，即可中四星组选 24 。",
            ],
            P::SC_FOUR_12 => [
                "label" => "选择 1 个二重号码和 2 个单号号码组成一注，所选号码与开奖号码的千位、百位、十位、个位相同，且所选二重号码在开奖号码千位、百位、十位、个位中出现了 2 次，即为中奖。",
                "example" => "投注方案：二重号： 8 ，单号： 0 、 6 ，只要开奖的四个数字从小到大排列为 0 、 6 、 8 、 8 ，即可中四星组选 12 。",
            ],
            P::SC_FOUR_6 => [
                "label" => "选择 2 个二重号码组成一注，所选的 2 个二重号码在开奖号码千位、百位、十位、个位分别出现了 2 次，即为中奖。",
                "example" => "投注方案：二重号：2 、 8 ，只要开奖的四个数字从小到大排列为 0 、 2 、 2 、 8 、 8 ，即可中四星组选 6 。",
            ],
            P::SC_FOUR_4 => [
                "label" => "选择 1 个三重号码和 1 个单号号码组成一注，所选号码与开奖号码的千位、百位、十位、个位相同，且所选三重号码在开奖号码千位、百位、十位、个位中出现了 3 次，即为中奖。",
                "example" => "投注方案：三重号： 8 ，单号： 0 、 2 ，只要开奖的四个数字从小到大排列为 0 、 2 、 8 、 8 、 8 ，即可中四星组选 4 。",
            ],
            P::SC_THREE_Q3 => [
                "label" => "从万位、千位、百位中选择一个 3 位数号码组成一注，所选号码与开奖号码前 3 位相同，且顺序一致，即为中奖。",
                "example" => "投注方案： 123 ； 开奖号码： 12345 ，即中前三直选。"
            ],
            P::SC_THREE_Z3 => [
                "label" => "从千位、百位、十位中选择一个 3 位数号码组成一注，所选号码与开奖号码中 3 位相同，且顺序一致，即为中奖。",
                "example" => "投注方案： 234 ； 开奖号码： 12345 ，即中中三直选。"
            ],
            P::SC_THREE_H3 => [
                "label" => "从百位、十位、个位中选择一个 3 位数号码组成一注，所选号码与开奖号码后 3 位相同，且顺序一致，即为中奖。",
                "example" => "投注方案： 345 ； 开奖号码： 12345 ，即中后三直选。"
            ],
            P::SC_THREE_Q3Z3 => [
                "label" => "从 0-9 中选择 2 个数字组成两注，所选号码与开奖号码的万位、千位、百位相同，且顺序不限，即为中奖。",
                "example" => "投注方案： 5,8,8 ；开奖号码前三位： 1 个 5 ， 2 个 8 ( 顺序不限 ) ，即中前三组三。"
            ],
            P::SC_THREE_Q3Z6 => [
                "label" => "从 0-9 中任意选择 3 个号码组成一注，所选号码与开奖号码的万位、千位、百位相同，顺序不限，即为中奖。",
                "example" => "投注方案： 2,5,8 ；开奖号码前三位： 1 个 2 、 1 个 5 、 1 个 8 ( 顺序不限 ) ，即中前三组六。"
            ],
            P::SC_THREE_Z3Z3 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_THREE_Z3Z6 => [
                "label" => "",
                "example" => ""
            ],
            P::SC_THREE_H3Z3 => [
                "label" => "从 0-9 中选择 2 个数字组成两注，所选号码与开奖号码的百位、十位、个位相同，且顺序不限，即为中奖。",
                "example" => "投注方案： 5,8,8 ；开奖号码后三位： 1 个 5 ， 2 个 8 ( 顺序不限 ) ，即中后三组三。"
            ],
            P::SC_THREE_H3Z6 => [
                "label" => "从 0-9 中任意选择 3 个号码组成一注，所选号码与开奖号码的百位、十位、个位相同，顺序不限，即为中奖。",
                "example" => "投注方案： 2,5,8 ；开奖号码后三位： 1 个 2 、 1 个 5 、 1 个 8 ( 顺序不限 ) ，即中后三组六。"
            ],
            P::SC_TWO_Q2 => [
                "label" => "从万位、千位中选择一个 2 位数号码组成一注，所选号码与开奖号码的前 2 位相同，且顺序一致，即为中奖。",
                "example" => "投注方案： 58 ；开奖号码前二位： 58 ，即中前二直选。"
            ],
            P::SC_TWO_H2 => [
                "label" => "从 0-9 中选 2 个号码组成一注，所选号码与开奖号码的十位、个位相同，顺序不限（不含对子号），即为中奖。",
                "example" => "投注方案： 5,8 ；开奖号码后二位： 85 或 58 ( 顺序不限，不含对子号 ) ，即中后二组选。"
            ],
            P::SC_TWO_Q2ZX => [
                "label" => "从 0-9 中选 2 个号码组成一注，所选号码与开奖号码的万位、千位相同，顺序不限（不含对子号），即中奖。",
                "example" => "投注方案： 5,8 ；开奖号码前二位： 85 或 58( 顺序不限，不含对子号 ) ，即中前二组选。"
            ],
            P::SC_TWO_H2ZX => [
                "label" => "从 0-9 中选 2 个号码组成一注，所选号码与开奖号码的十位、个位相同，顺序不限（不含对子号），即为中奖。",
                "example" => "投注方案： 5,8 ；开奖号码后二位： 85 或 58 ( 顺序不限，不含对子号 ) ，即中后二组选。"
            ],
            P::SC_FIVE_DWD => [
                "label" => "从万位、千位、百位、十位、个位任意位置上至少选择 1 个以上号码，所选号码与相同位置上的开奖号码一致，即为中奖。",
                "example" => "投注方案：万位 1 ；开奖号码万位： 1 ，即中定位胆万位。"
            ],
            P::SC_BDD_Q31M => [
                "label" => "从 0-9 中选择 1 个号码，每注由 1 个号码组成，只要开奖号码的万位、千位、百位中包含所选号码，即为中奖。",
                "example" => "投注方案： 1 ；开奖号码前三位：至少出现 1 个 1 ，即中前三一码不定位。"
            ],
            P::SC_BDD_Z31M => [
                "label" => "",
                "example" => ""
            ],
            P::SC_BDD_H31M => [
                "label" => "从 0-9 中选择 1 个号码，每注由 1 个号码组成，只要开奖号码的百位、十位、个位中包含所选号码，即为中奖。",
                "example" => "投注方案： 1 ；开奖号码后三位：至少出现 1 个 1 ，即中后三一码不定位。"
            ],
            P::SC_BDD_Q32M => [
                "label" => "从 0-9 中选择 2 个号码，每注由 2 个不同的号码组成，开奖号码的万位、千位、百位中同时包含所选的 2 个号码，即为中奖。",
                "example" => "投注方案： 1,2 ；开奖号码前三位：至少出现 1 和 2 各 1 个，即中前三二码不定位。"
            ],
            P::SC_BDD_H32M => [
                "label" => "从 0-9 中选择 2 个号码，每注由 2 个不同的号码组成，开奖号码的百位、十位、个位中同时包含所选的 2 个号码，即为中奖。",
                "example" => "投注方案： 1,2 ；开奖号码后三位：至少出现 1 和 2 各 1 个，即中后三二码不定位。"
            ],
            P::SC_BDD_4X1M => [
                "label" => "从 0-9 中选择 1 个号码，每注由 1 个号码组成，只要开奖号码的千位、百位、十位、个位中包含所选号码，即为中奖。",
                "example" => "投注方案： 1 ；开奖号码后四位：至少出现 1 个 1 ，即中四星一码不定位。"
            ],
            P::SC_BDD_4X2M => [
                "label" => "从 0-9 中选择 2 个号码，每注由 2 个不同的号码组成，开奖号码的千位、百位、十位、个位中同时包含所选的 2 个号码，即为中奖。",
                "example" => "投注方案： 1,2 ；开奖号码后四位：至少出现 1 和 2 各 1 个，即中四星二码不定位。"
            ],
            P::SC_BDD_5X2M => [
                "label" => "从 0-9 中选择 2 个号码，每注由 2 个不同的号码组成，开奖号码的万位、千位、百位、十位、个位中同时包含所选的 2 个号码，即为中奖。",
                "example" => "投注方案： 1,2 ；开奖号码：至少出现 1 和 2 各 1 个，即中五星二码不定位。"
            ],
            P::SC_BDD_5X3M => [
                "label" => "从 0-9 中选择 3 个号码，每注由 3 个不同的号码组成，开奖号码的万位、千位、百位、十位、个位中同时包含所选的 3 个号码，即为中奖。",
                "example" => "投注方案： 1,2,3 ；开奖号码：至少出现 1 、 2 、 3 各 1 个，即中五星三码不定位。"
            ],
        ];
    }

    /**
     * 获取趋势图显示类型
     * @param null $type
     * @return string|string[]
     */
    public static function getScTreadTypeLabels($type = null) {
        $labels = [
            P::SC_TREND_TYPE_WAN => "万",
            P::SC_TREND_TYPE_QIAN => "千",
            P::SC_TREND_TYPE_BAI => "百",
            P::SC_TREND_TYPE_SHI => "十",
            P::SC_TREND_TYPE_GE => "个",
        ];

        return C::commonReturn($labels, $type);
    }

}
