<?php


namespace common\constants;


use common\utils\ArrayUtil;

/**
 * Class A 权限动作
 * @package common\constants
 */
class A
{
    /**
     * 动作：查看（基本操作权限）
     */
    const VIEW = 1;
    /**
     * 动作：添加
     */
    const ADD = 2;
    /**
     * 动作：修改
     */
    const MOD = 3;
    /**
     * 动作：删除
     */
    const DEL = 4;
    /**
     * 动作：查看所有数据
     */
    const VIEW_ALL = 5;

    /**
     * @param $action
     * @return array
     */
    public static function getLabel($action = null) {
        $labels = [
            A::VIEW => "查看",
            A::ADD => "添加",
            A::MOD => "修改",
            A::DEL => "删除",
        ];

        if ($action !== null) {
            return ArrayUtil::issetDefault($labels, $action, "");
        }

        return $labels;
    }
}
