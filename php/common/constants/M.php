<?php


namespace common\constants;


use common\models\MenuModel;
use common\utils\ArrayUtil;

/**
 * Class M 菜单模块
 * @package common\constants
 */
class M {
    /**
     * 模块：用户列表
     */
    const USER_LIST = 1;
    /**
     * 模块：订单列表
     */
    const ORDER_LIST = 2;
    /**
     * 模块：小商户列表
     */
    const MEMBER_LIST = 3;
    /**
     * 模块：实时订单
     */
    const ORDER_REAL_TIME = 4;

    private static $labels = [];

    /**
     * 获取模块标签（引用菜单中的数据）
     * @param $module
     * @return mixed
     */
    public static function getLabel($module) {
        $labels = M::baseLabels();
        return ArrayUtil::issetDefault($labels, $module, "");
    }

    /**
     * 从菜单中获取label值（带缓存）
     * @return string[]
     */
    private static function baseLabels() {
        if (!empty(M::$labels)) {
            return M::$labels;
        }

        $root = MenuModel::getMenu();
        foreach ($root->children as $menu) {
            foreach ($menu->children as $subMenu) {
                if (empty($subMenu)) {
                    continue;
                }
                M::$labels[$subMenu->module] = $subMenu->name;
            }
        }
        return M::$labels;
    }
}