<?php


namespace common\constants;


/**
 * 日志常量
 * @remark 一经定义,不得修改,不然会引起数据错乱
 */
class L
{
    //主类型
    const MAIN_TYPE_PLOG = 1; //操作日志主类型
    const MAIN_TYPE_CLOG = 2; //帐变日志主类型
    const MAIN_TYPE_GLOG = 9; //通用日志主类型

    // ========================= 操作日志类型定义 log_operation =========================
    //帐户类
    const PLOG_VIP_UP = 10101; //VIP申请升级操作日志
    const PLOG_SET_BANK_INFO = 10102; //更新银行信息
    const PLOG_ADD_USER = 10103; //添加用户
    const PLOG_MOD_USER = 10104; //修改用户
    //开奖类
    const PLOG_PREDICT_RESULT = 10201; //修改预设结果  将期数-修改前结果-修改后结果记录
    // 存款金额
    const PLOG_ORDER_MOD_DEPOSIT_TYPE = 10301; // 修改支付方式

    // ========================= 帐变日志类型定义log_cash_record =========================
    //帐户类
    //const CLOG_REGISTER = 20101;//注册奖励
    const CLOG_USER_PAY = 20102;//用户充值
    const CLOG_SYSTEM_MOD = 20103;//管理员加扣款
    const CLOG_PAY_REWARD = 20104;//充值奖励
    const CLOG_DRAW_COOL = 20105;//提现冻结
    const CLOG_DRAW_FAIL_BACK = 20106;//提现失败返还
    const CLOG_DRAW_OK = 20107;//提现成功扣除
    //const CLOG_BIND_BANK_REWARD = 20108;//绑定银行奖励

    //游戏类
    const CLOG_BET = 20201;//投注扣款
    //const CLOG_OPEN = 20202;//开奖扣除
    const CLOG_WIN = 20203;//中奖奖金
    const CLOG_CANCEL_BET = 20204;//撤单返款
    const CLOG_FOLLOW_BET = 20205;//追号投注
    const CLOG_FOLLOW_CANCEL = 20206;//追号撤单
    const CLOG_NOT_OPEN_BACK = 20207;//未开奖返还

    //活动类
    const CLOG_SIGN = 20301;//签到赠送
    const CLOG_LUCK_RUN = 20302;//幸运大转盘
    const CLOG_INTEGRAL = 20303; //积分兑换
    const CLOG_ORDER_REBATE = 20304; //下单返利
    //代理类
    // ========================= 登录日志类型定义 log_login ========================
    const LOG_LOGIN = 30101; //登入
    const LOG_LOGOUT = 30102; //登出
    // ========================= 通用日志类型定义 log_general ======================
    const GLOG_TEST = 90101; //通用测试日志
    const GLOG_CRON = 90102; //自动运行日志


    /**
     * 获取主类型
     * @param $logType
     * @return int
     */
    public static function getMainType($logType) {
        return (int)floor($logType / 10000);
    }


    /**
     * 判断日志类型是否是操作类型
     * @param $logType
     * @return bool
     */
    public static function isOperateLogType($logType) {
        return L::getMainType($logType) === L::MAIN_TYPE_PLOG;
    }


    /**
     * 判断日志类型是否是账变类型
     * @param $logType
     * @return bool
     */
    public static function isCashLogType($logType) {
        return L::getMainType($logType) === L::MAIN_TYPE_CLOG;
    }

    /**
     * 判断日志类型是否是账变类型
     * @param $logType
     * @return bool
     */
    public static function isGeneralLogType($logType) {
        return L::getMainType($logType) === L::MAIN_TYPE_GLOG;
    }

    /**
     * 获取报表必须要显示的帐变日志类型配置
     * @return array
     */
    public static function getMustShowClogTypeConfig() {
        return [L::CLOG_SIGN, L::CLOG_ORDER_REBATE];
    }

    /**
     * 每个类型的标注 (上面加完类型 下面要对应上注释)
     * @return array
     */
    public static function labels() {
        $labels = [
            //操作日志-帐户类
            L::PLOG_VIP_UP =>"VIP申请升级",
            L::PLOG_SET_BANK_INFO =>"更新银行信息",
            L::PLOG_ADD_USER =>"添加用户",
            L::PLOG_MOD_USER =>"修改用户",

            // 操作日志-订单类
            L::PLOG_ORDER_MOD_DEPOSIT_TYPE => "修改存款类型",

            //帐变日志-帐户类
            //L::CLOG_REGISTER => "注册奖励",
            L::CLOG_USER_PAY => "用户充值",
            L::CLOG_SYSTEM_MOD => "管理员加扣款",
            L::CLOG_PAY_REWARD => "充值奖励",
            L::CLOG_DRAW_COOL => "提现冻结",
            L::CLOG_DRAW_FAIL_BACK => "提现失败返还",
            L::CLOG_DRAW_OK => "提现成功扣除",
            //L::CLOG_BIND_BANK_REWARD => "绑定银行奖励",

            //帐变日志-游戏类
            L::CLOG_BET => "投注扣款",
            //L::CLOG_OPEN => "开奖扣除",
            L::CLOG_WIN => "中奖奖金",
            L::CLOG_CANCEL_BET => "撤单返款",
            L::CLOG_FOLLOW_BET => "追号投注",
            L::CLOG_FOLLOW_CANCEL => "追号撤单",
            L::CLOG_NOT_OPEN_BACK => "未开奖返还",

            //帐变日志-活动类
            L::CLOG_SIGN => "签到赠送",
            L::CLOG_LUCK_RUN => "幸运大转盘",
            L::CLOG_INTEGRAL => "积分兑换",
            L::CLOG_ORDER_REBATE => "下单返利",

            //通用日志
            L::GLOG_CRON => "自动运行日志",
        ];
        return $labels;
    }

    /**
     * 获取类型的说明
     * @param $logType
     * @return string
     */
    public static function getLabel($logType) {
        $arr = self::labels();
        return $arr[$logType];
    }


    /**
     * 帐户变日志类型分类
     * @return array
     */
    public static function getCashLabels() {
        $arr = self::labels();
        $groups = [
            //帐户类
            "帐户类" => [L::CLOG_USER_PAY,],
            //游戏类
            "游戏类" => [L::CLOG_BET, L::CLOG_WIN],
            //活动类
            "活动类" => [L::CLOG_SIGN, L::CLOG_LUCK_RUN, L::CLOG_INTEGRAL,L::CLOG_ORDER_REBATE],
        ];
        $cashLabels = [];
        foreach ($groups as $index => $group) {
            $cashLabels[$index] = [];
            foreach ($group as $value) {
                $cashLabels[$index][$value] = $arr[$value];
            }
        }
        return $cashLabels;

    }


    /**
     * 获取单号说明
     */
    public static function getRecordNoLabel($logType) {
        $label = '';
        if ($logType == L::CLOG_USER_PAY) {
            $label = '充值订单:';
        } else if ($logType == L::CLOG_BET) {
            $label = '投注:';
        } else if ($logType == L::CLOG_WIN) {
            $label = '中奖:';
        } else if ($logType == L::CLOG_DRAW_COOL) {
            $label = '提现冻结:';
        }
        return $label;

    }
}

