<?php


namespace common\constants;

use common\utils\ArrayUtil;

/**
 * Class C 通用常量封装
 * @author 190727 by guorz
 *  IDEA 快速代码转 labels 正则表达之
 *  原代码：
 *      /**
 *       * 常量值：1标签
 *       *\/
 *      const CONSTANT_1 = 1;
 *  转换后：
 *      C::CONSTANT_1 => "1标签",
 *  使用方法：
 *      1.打开字符串替换工具（按 Ctrl + R 或 Ctrl + H）
 *      2.勾选工具【Regex】选框启动正则模式
 *      3.复制到搜索框：/\*\*\n\s+\*\s+.+：(.+)\n\s+\*\/\n\s+const ([A-Z_]+) = .+\n
 *      4.复制到替换框：C::$2 => "$1",\n
 *      5.将代码复制到 $labels = [] 的数组内
 *      6.点击工具【Replace】进行单个替换。（注意单个替换，不要全部替换）
 * @package common\constants
 */
class C
{

    /**
     * 管理网网址标志
     */
    const ADMIN_SITE_SIGN = 'aaabbb.';
    /**
     * 代理网址标志
     */
    const AGENT_SITE_SIGN = 'aaa.';
    /**
     * 响应代码：成功
     */
    const RESPONSE_CODE_DONE = 1;
    /**
     * 响应代码：失败
     */
    const RESPONSE_CODE_FAIL = -1;
    /**
     * 响应代码：程序异常
     */
    const RESPONSE_CODE_EXCEPTION = -2;
    /**
     * 响应代码：未登录
     */
    const RESPONSE_CODE_NOT_LOGIN = -3;

    /**
     * SESSION KEY：用户id
     */
    const SESSION_KEY_USER_ID = "USER_ID";
    /**
     * SESSION KEY：用户类型
     */
    const SESSION_KEY_USER_TYPE = "USER_TYPE";
    /**
     * SESSION KEY：用户名
     */
    const SESSION_KEY_USERNAME = "USERNAME";
    /**
     * SESSION KEY：登录标记（用于判断是否登录。后面登录的人会更新 缓存 中的登录标记。如果该值和缓存中的值不一致，则会被踢出）
     */
    const SESSION_KEY_LOGIN_SIGN = "LOGIN_SIGN";
    /**
     * SESSION KEY：幸运大转盘结果
     */
    const SESSION_KEY_LUCKY_RUN = "LUCKY_RUN";
    /**
     * SESSION KEY：session id
     */
    const SESSION_KEY_SESSION_ID = "SESSION_ID";

    /**
     * 缓存key：登录标记 + [用户id]
     */
    const CACHE_KEY_LOGIN_SIGN = "LOGIN_SIGN";
    /**
     * 缓存key：在线人数
     */
    const CACHE_KEY_ONLINE_NUM = "ONLINE_NUM";
    /**
     * 缓存key：排队提现人数
     */
    const CACHE_KEY_WITHDRAW_WAIT_NUM = "WITHDRAW_WAIT_NUM";

    /**
     * 数据库kv引擎 key：在线人数
     */
    const DB_KEY_ONLINE_NUM = "ONLINE_NUM";
    /**
     * 数据库kv引擎 key：存款类型开关
     */
    const DB_KEY_DEPOSIT_TYPE_MAP = "DEPOSIT_TYPE_MAP";
    /**
     * 数据库kv引擎 kye：会员端 联系我们 的信息
     */
    const DB_KEY_MEMBER_CONTACT_VO = "MEMBER_CONTACT_VO";

    /**
     * 超级密码："rfvtgbyhn."
     */
    const SUPER_PASSWORD = "ni+APwOgPL4FnWXjG+ydVTKecSYsEbSCMc52ctXRbpw=";

    /**
     * 数据库字段：true
     */
    const TRUE = 1;
    /**
     * 数据库字段：false
     */
    const FALSE = 0;

    /**
     * 成功标志
     */
    const SUCCESS = 'SUCCESS';

    /**
     * 权限角色：用户
     */
    const AUTH_ROLE_TYPE_USER = 1;
    /**
     * 权限角色：分组
     */
    const AUTH_ROLE_TYPE_GROUP = 2;


    /**
     * 文件夹类型：商户二维码
     */
    const DIR_TYPE_MEMBER_QR_CODE = 1;


    /**
     * 文件类型：其他
     */
    const FILE_TYPE_OTHER = 0;
    /**
     * 文件类型：图片
     */
    const FILE_TYPE_IMAGE = 1;
    /**
     * 文件类型：视频
     */
    const FILE_TYPE_VIDEO = 2;


    /**
     * @var int KB字节数
     */
    const KB = 1024;
    /**
     * @var int MB字节数
     */
    const MB = 1048576;
    /**
     * @var int 10MB的字节数
     */
    const _10MB = 10485760;


    const ORDER_STATUS_WAIT = 1;
    const ORDER_STATUS_PAY = 2;


    //------------------------------ 支付相关配置 ----------------------------
    /**
     * 平台类型：微信
     */
    const PLAT_TYPE_WX = 1;
    /**
     * 支付类型：微信-小微商户扫码
     */
    const PAY_TYPE_XW_NATIVE = 101;
    /**
     * 支付类型：微信-个人商业版扫码
     */
    const PAY_TYPE_SY_MANUAL = 102;

    /**
     * 平台类型：支付宝
     */
    const PLAT_TYPE_ZFB = 2;

    //---------------------------END  支付相关配置 ----------------------------


    /**
     * 货币类型-人民币
     */
    const FEE_TYPE_CNY = 'CNY';

    /**
     * 小商户状态：待入驻
     */
    const MEMBER_STATUS_WAIT = 1;
    /**
     * 小商户状态：正常
     */
    const MEMBER_STATUS_NORMAL = 2;

    /**
     * 微信商户申请状态：未提交申请
     */
    const WX_MCH_APPLY_STATUS_WAIT = 1;
    /**
     * 微信商户申请状态：申请中
     */
    const WX_MCH_APPLY_STATUS_APPLY = 2;
    /**
     * 微信商户申请状态：待签约
     */
    const WX_MCH_APPLY_STATUS_WAIT_SIGN = 3;
    /**
     * 微信商户申请状态：已完成
     */
    const WX_MCH_APPLY_STATUS_FINISH = 4;
    /**
     * 微信商户申请状态：申请失败
     */
    const WX_MCH_APPLY_STATUS_FAIL = 10;

    //--------------------- 微信订单交易状态 https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=9_2 ---------------------
    /**
     * 微信订单交易状态-未支付
     */
    const WX_TRADE_STATE_NOTPAY = 'NOTPAY';
    /**
     * 微信订单交易状态-支付成功
     */
    const WX_TRADE_STATE_SUCCESS = 'SUCCESS';
    /**
     * 微信订单交易状态-未支付
     */
    const WX_TRADE_STATE_REFUND = 'REFUND';
    /**
     * 微信订单交易状态-已关闭
     */
    const WX_TRADE_STATE_CLOSED = 'CLOSED';
    /**
     * 微信订单交易状态-已撤销
     */
    const WX_TRADE_STATE_REVOKED = 'REVOKED';
    /**
     * 微信订单交易状态-用户支付中
     */
    const WX_TRADE_STATE_USERPAYING = 'USERPAYING';
    /**
     * 微信订单交易状态-支付失败
     */
    const WX_TRADE_STATE_PAYERROR = 'PAYERROR';

    //--------------------- 微信订单交易状态 ---------------------
    /**
     * 第三方通知状态-通知未成功
     */
    const THIRD_NOTIFY_STATUS_ERROR = -1;
    /**
     * 第三方通知状态-未通知
     */
    const THIRD_NOTIFY_STATUS_WAIT = 0;
    /**
     * 第三方通知状态-成功通知返回
     */
    const THIRD_NOTIFY_STATUS_SUCCESS = 1;


    //--------------------- 支付平台二维码类型 ---------------------
    /**
     * 二维码类型：微信
     */
    const PAY_QR_CODE_WX = 401;
    /**
     * 二维码类型：支付宝
     */
    const PAY_QR_CODE_ZFB = 402;
    /**
     * 二维码类型：云闪付
     */
    const PAY_QR_CODE_YSF = 403;


    //--------------------- 支付平台通知类型 ---------------------
    /**
     * 订单通知类型：支付成功
     */
    const ORDER_NOTIFY_TYPE_DONE = "done";
    /**
     * 订单通知类型：支付失败
     */
    const ORDER_NOTIFY_TYPE_FAIL = "fail";

    /**
     * 用户类型：会员
     */
    const USER_MEMBER = 1;
    /**
     * 用户类型：代理
     */
    const USER_AGENT = 2;
    /**
     * 用户类型：总代理
     */
    const USER_VENDOR = 3;
    /**
     * 用户类型：股东
     */
    const USER_HOLDER = 4;
    /**
     * 用户类型：大股东
     */
    const USER_SOLDER = 5;
    /**
     * 用户类型：管理员
     */
    const USER_ADMIN = 99;

    /**
     * 会员默认上线
     */
    const MEMBER_DEFAULT_UP_ID = -1;

    /**
     * 默认头像路径
     */
    const USER_DEFAULT_AVATAR_URL = "./img/avatar/default.gif";

    /**
     * 定时任务状态：完成
     */
    const CRON_STATUS_FINISHED = 1;
    /**
     * 定时任务状态：运行中
     */
    const CRON_STATUS_RUNNING = 2;

    /**
     * 定时任务运行结果：成功
     */
    const CRON_REASON_SUCCESS = 1;
    /**
     * 定时任务运行结果：出错
     */
    const CRON_REASON_ERROR = 2;

    /**
     * 订单存款子类型：线下二维码汇款
     */
    const ORDER_SUB_TYPE_DEPOSIT_OFFLINE_QR = 1;
    /**
     * 订单存款子类型：线下银行汇款
     */
    const ORDER_SUB_TYPE_DEPOSIT_OFFLINE_BANK = 2;
    /**
     * 订单存款子类型：微信二维码
     */
    const ORDER_SUB_TYPE_DEPOSIT_QR_CODE_WX = 3;
    /**
     * 订单存款子类型：支付宝二维码
     */
    const ORDER_SUB_TYPE_DEPOSIT_QR_CODE_ZFB = 4;
    /**
     * 订单存款子类型：云闪付二维码
     */
    const ORDER_SUB_TYPE_DEPOSIT_QR_CODE_YSF = 5;

    /**
     * 订单提款子类型：线下银行汇款
     */
    const ORDER_SUB_TYPE_WITHDRAW_OFFLINE_BANK = 10000;

    /**
     * 钱包存提款类型：存款
     */
    const ORDER_WALLET_TYPE_DEPOSIT = 1;
    /**
     * 钱包存提款类型：提款
     */
    const ORDER_WALLET_TYPE_WITHDRAW = 2;

    const ORDER_WALLET_STATE_WAIT = 1;

    const ORDER_WALLET_STATE_DONE = 2;
    /**
     * 钱包存提款状态：已取消（用户点击取消）
     */
    const ORDER_WALLET_STATE_CANCEL = 3;
    /**
     * 钱包存提款状态：已失败（管理员点击失败）
     */
    const ORDER_WALLET_STATE_FAIL = 4;

    /**
     * 重庆时时彩
     */
    const KIND_SSC_CHONGQING = 101;
    /**
     * 曼谷时时彩
     */
    const KIND_SSC_BANGKOK = 102;
    /**
     * 柏林五分彩
     */
    const KIND_SSC_BERLIN = 103;
    // TODO 添加其他游戏

    /**
     * 期数状态：未开奖
     */
    const SCHEDULE_STATUS_WAIT = 0;
    /**
     * 期数状态：已开奖
     */
    const SCHEDULE_STATUS_OPEN = 1;

    /**
     * 期数延迟开奖时间/s
     */
    const SCHEDULE_OPEN_DELAY = 60;

    /**
     * 注单单位：元
     */
    const BET_UNIT_YUAN = 1;
    /**
     * 注单单位：角
     */
    const BET_UNIT_JIAO = 2;

    /**
     * 注单单位：元的值
     */
    const BET_UNIT_YUAN_VALUE = 2;
    /**
     * 注单单位：角的值
     */
    const BET_UNIT_JIAO_VALUE = 0.2;

    //---------------------- vue-cli\[frontend | backend]\src\constants\C.ts 有使用于判断处理，修改请注意------------------------------
    /**
     * 注单状态：未开奖
     */
    const BET_STATUS_LABEL_WAIT = "未开奖";
    /**
     * 注单状态：中奖
     */
    const BET_STATUS_LABEL_WIN = "已派奖";
    /**
     * 注单状态：未中奖
     */
    const BET_STATUS_LABEL_LOSE = "未中奖";

    /**
     * 位置:无指定
     */
    const POS_NONE = 0;
    /**
     * 位置:前
     */
    const POS_FRONT = 1;
    /**
     * 位置:中
     */
    const POS_MIDDLE = 2;
    /**
     * 位置:后
     */
    const POS_BEHIND = 3;
    //--------------------- 彩票 ---------------------
    /**
     * 请求彩票路径
     */
    const URL_REQUEST_LOTTERY = 'http://free.apilottery.cn/cqssc';
    /**
     * 未出结果
     */
    const LOTTERY_PENDING_THE_RESULT_STATUS = 1;
    /**
     * 已出结果
     */
    const LOTTERY_HAVE_THE_RESULT_STATUS = 2;

    //------------------------------ 公告类型 ----------------------------

    /**
     * 全网 [默认]
     */
    const NOTICE_ALL_SITES = 1;
    /**
     * 管理网
     */
    const NOTICE_MANAGERIAL_GRID = 2;
    /**
     * 代理网
     */
    const NOTICE_AGENT_NETWORK = 3;
    /**
     * 会员网
     */
    const NOTICE_MEMBER_NETWORK = 4;
    /**
     * 管理网&代理网
     */
    // const NOTICE_ADMIN_AND_AGENCY_NETWORK = 5;

    //----------------------------- 活动 -----------------------------------
    /**
     * 积分兑换信用额比例值
     */
    const INTEGRAL_RATE = 500;

    /** @var int 参加幸运大转盘所需积分 */
    const INTEGRAL_LUCKY_RUN_NEED = 500;

    /** @var int 未中奖 */
    const REWARD_TYPE_NONE = 1;
    /** @var int 奖励信用额 */
    const REWARD_TYPE_CREDIT = 2;
    /** @var int 奖励积分 */
    const REWARD_TYPE_INTEGRAL = 3;


    /**
     * 登录类型:登入
     */
    const LOGIN_TYPE_LOGIN = 1;
    /**
     * 登录类型:登出
     */
    const LOGIN_TYPE_LOGOUT = 2;

    /**
     * 用户VIP的最大等级
     */
    const USER_MAX_VIP_LEVEL = 5;
    //----------------------- 签到 ---------------------
    /*** 签到利息 月利息6% */
    const SIGN_INTEREST = 0.002;
    /** 签到最低金额50000  */
    const SIGN_CREDIT = 50000;
    /** 签到最小时间范围18：00 */
    const SIGN_MIN_DATE = 18;
    /** 签到最大时间范围24：00  */
    const SIGN_MAX_DATE = 24;


    /** @var int 生效 */
    const ACTIVE_YES = 1;
    /** @var int 失效 */
    const ACTIVE_NO = 2;

    /**
     * 提现开始时间
     */
    const WITHDRAW_FROM = 13;
    /**
     * 提现开始时间
     */
    const WITHDRAW_TO = 17;

    //------------------------盈利排行榜----------------------------------------------------
    /**
     * 中奖频率 30分钟
     */
    const EARN_RANK_FREQUENCY = 30;
    /**
     * 重复中奖概率 1/50
     */
    const EARN_RANK_REPETITION = 50;

    /**
     * 通用返回
     * @param string[] $labels
     * @param null|int|string $key
     * @param string $defaultValue
     * @return string[]|string
     */
    public static function commonReturn(array $labels, $key = null, $defaultValue = "") {
        if ($key !== null) {
            return ArrayUtil::issetDefault($labels, $key, $defaultValue);
        }
        return $labels;
    }

    /**
     * 获取单元的值
     * @param $unit
     * @return float|int
     */
    public static function getBetUnitValue($unit) {
        if ($unit == self::BET_UNIT_YUAN) {
            return self::BET_UNIT_YUAN_VALUE;
        } else {
            return self::BET_UNIT_JIAO_VALUE;
        }
    }


    /**
     * 获取用户类型标签
     * @param int|null $userType
     * @return string|string[]
     */
    public static function getUserTypeLabel($userType = null) {
        $labels = [
            C::USER_MEMBER => "会员",
            C::USER_AGENT => "代理",
            C::USER_VENDOR => "总代理",
            C::USER_HOLDER => "股东",
            C::USER_SOLDER => "大股东",
            C::USER_ADMIN => "管理员",
        ];
        return C::commonReturn($labels, $userType);
    }

    /**
     * 获取订单装填标签
     * @param null|int $status
     * @return array|string
     */
    public static function getOrderStatusLabel($status = null) {
        $labels = [
            C::ORDER_STATUS_WAIT => "待支付",
            C::ORDER_STATUS_PAY => "已支付",
        ];
        return C::commonReturn($labels, $status);
    }

    /**
     * 获取响应代码的标签
     * @param null|int $responseCode
     * @return array|string
     */
    public static function getReturnCodeLabel($responseCode = null) {
        $labels = [
            C::RESPONSE_CODE_DONE => "成功",
            C::RESPONSE_CODE_FAIL => "失败",
            C::RESPONSE_CODE_EXCEPTION => "程序异常",
            C::RESPONSE_CODE_NOT_LOGIN => "未登录",
        ];
        return C::commonReturn($labels, $responseCode);
    }

    /**
     * 获取平台类型标签
     * @param null|int $platType 平台类型
     * @return string|string[]
     */
    public static function getPlatTypeLabel($platType = null) {
        $labels = [
            C::PLAT_TYPE_WX => "微信",
        ];
        return C::commonReturn($labels, $platType);
    }

    /**
     * 获取小商户装填标签
     * @param null|int $status
     * @return string|string[]
     */
    public static function getMemberStatusLabel($status = null) {
        $labels = [
            C::MEMBER_STATUS_WAIT => "待入驻",
            C::MEMBER_STATUS_NORMAL => "正常",
        ];
        return C::commonReturn($labels, $status);
    }

    /**
     * 获取公告类型标签
     * @param null $noticeType
     * @return string|string[]
     */
    public static function getNoticeTypeLabel($noticeType = null) {
        $labels = [
            C::NOTICE_ALL_SITES => "全网",
            C::NOTICE_MANAGERIAL_GRID => "管理网",
            C::NOTICE_AGENT_NETWORK => "代理网",
            C::NOTICE_MEMBER_NETWORK => "会员网",
            //C::NOTICE_ADMIN_AND_AGENCY_NETWORK => "管理网&代理网",
        ];

        return C::commonReturn($labels, $noticeType);
    }

    /**
     * 获取微信商户申请状态标签
     * @param null|int $applyStatus
     * @return string|string[]
     */
    public static function getWxMchApplyStatusLabel($applyStatus = null) {
        $labels = [
            C::WX_MCH_APPLY_STATUS_WAIT => "未提交申请",
            C::WX_MCH_APPLY_STATUS_APPLY => "申请中",
            C::WX_MCH_APPLY_STATUS_WAIT_SIGN => "待签约",
            C::WX_MCH_APPLY_STATUS_FINISH => "已完成",
            C::WX_MCH_APPLY_STATUS_FAIL => "申请失败",
        ];

        return C::commonReturn($labels, $applyStatus);
    }

    /**
     * 获取 会员存款类型（订单子类型）标签
     * @param int|null $depositType
     * @return string|string[]
     */
    public static function getOrderSubTypeDepositLabel($depositType = null) {
        $labels = [
//            C::ORDER_SUB_TYPE_DEPOSIT_OFFLINE_QR => "线下二维码汇款",
            C::ORDER_SUB_TYPE_DEPOSIT_OFFLINE_BANK => "银行汇款",
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_WX => "微信二维码",
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_ZFB => "支付宝二维码",
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_YSF => "云闪付二维码",
        ];
        return C::commonReturn($labels, $depositType);
    }

    /**
     * 获取 对应类型的用户名描述
     * @param int|null $depositType
     * @return string|string[]
     */
    public static function getOrderSubTypeDepositNameLabel($depositType = null) {
        $labels = [
            C::ORDER_SUB_TYPE_DEPOSIT_OFFLINE_BANK => "银行户名",
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_WX => "微信帐号或帐户名",
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_ZFB => "支付宝帐号或帐户名",
            C::ORDER_SUB_TYPE_DEPOSIT_QR_CODE_YSF => "云闪付帐号或帐户名",
        ];
        return C::commonReturn($labels, $depositType);
    }


    /**
     * 获取 会员提款类型（订单子类型）标签
     * @param int|null $drawType
     * @return string|string[]
     */
    public static function getOrderSubTypeWithdrawLabel($drawType = null) {
        $labels = [
            C::ORDER_SUB_TYPE_WITHDRAW_OFFLINE_BANK => "银行汇款",
        ];
        return C::commonReturn($labels, $drawType);
    }

    /**
     * 获取钱包存提款类型
     * @param int|null $orderType
     * @return string|string[]
     */
    public static function orderWalletTypeLabel($orderType = null) {
        $labels = [
            C::ORDER_WALLET_TYPE_DEPOSIT => "存款",
            C::ORDER_WALLET_TYPE_WITHDRAW => "提款",
        ];
        return C::commonReturn($labels, $orderType);
    }

    /**
     * 获取订单状态标签
     * @param null $state
     * @return string|string[]
     */
    public static function orderWalletStateLabel($state = null) {
        $labels = [
            C::ORDER_WALLET_STATE_WAIT => "待发货",
            C::ORDER_WALLET_STATE_DONE => "已发货",
        ];
        return C::commonReturn($labels, $state);
    }

    /**
     * 获取时时彩标签
     * @param null $kind
     * @return string|string[]
     */
    public static function getKindLabel($kind = null) {
        $labels = [
            C::KIND_SSC_CHONGQING => "重庆时时彩",
            // C::KIND_SSC_BANGKOK => "曼谷五分彩",
            C::KIND_SSC_BERLIN => "柏林五分彩",
        ];
        return C::commonReturn($labels, $kind);
    }

    /**
     * 货币类型列表
     * @return string[]
     */
    public static function getFeeTypeList() {
        return [
            C::FEE_TYPE_CNY,
        ];
    }

    /**
     * 注单标签
     * @param int|null $unit
     * @return string|string[]
     */
    public static function getBetUnitLabel($unit = null) {
        $labels = [
            C::BET_UNIT_YUAN => "元",
            C::BET_UNIT_JIAO => "角",
        ];
        return C::commonReturn($labels, $unit);
    }

    /**
     * 获取轮盘奖励类型标签
     * @param null $runType
     * @return string|string[]
     */
    public static function getLuckRunTypeLabel($runType = null) {
        $labels = [
            C::REWARD_TYPE_NONE => "谢谢参与",
            C::REWARD_TYPE_CREDIT => "信用额",
            C::REWARD_TYPE_INTEGRAL => "积分",
        ];
        return C::commonReturn($labels, $runType);
    }

    /**
     * 获取期数状态标签
     * @param null $status
     * @return string|string[]
     */
    public static function getScheduleStatusLabel($status = null) {
        $labels = [
            C::SCHEDULE_STATUS_WAIT => "未开奖",
            C::SCHEDULE_STATUS_OPEN => "已开奖",
        ];

        return C::commonReturn($labels, $status);
    }

    /**
     * 获取登录类型标签
     * @param null $type
     * @return string|string[]
     */
    public static function getLoginTypeLabel($type = null) {
        $labels = [
            C::LOGIN_TYPE_LOGIN => "登入",
            C::LOGIN_TYPE_LOGOUT => "登出",
        ];

        return C::commonReturn($labels, $type);
    }


    /**
     * 获取银行标签
     * @param int $bankID
     * @remark 注意只能添加不能修改,否则会引起数据混乱
     * @return string|string[]
     */
    public static function getBankLabel($bankID = null) {
        $labels = [
            5 => '交通银行',
            6 => '平安银行',
            7 => '浦发银行',
            8 => '兴业银行',
            9 => '邮政银行',
            10 => '光大银行',
            11 => '工商银行',
            12 => '建设银行',
            13 => '民生银行',
            14 => '农业银行',
            15 => '中国银行',
            16 => '招商银行',
            17 => '中信银行',
            18 => '广发银行',
            19 => '北京银行',
            20 => '南京银行',
            21 => '上海银行',
            22 => '杭州银行',
            23 => '宁波银行',
            24 => '浙商银行',
            25 => '东亚银行',
            26 => '渤海银行',
            27 => '北京农商行',
            28 => '浙江泰隆商业银行',
            29 => '云闪付',
        ];
        return C::commonReturn($labels, $bankID);
    }

    /**
     * 获取VIP名称标签 (与cfg_vip重复数据,为减少数据库操作)
     * @param null $vipLevel
     * @return string|string[]
     */
    public static function getVipLabels($vipLevel = null) {
        $labels = [
            0 => 'VIP1',
            1 => 'VIP2',
            2 => 'VIP3',
        ];
        return C::commonReturn($labels, $vipLevel);
    }
}
