<?php


namespace common\constants;


use common\models\db\Member;
use common\utils\ArrayUtil;

/**
 * Class B 绑定模块相关的常量
 * @package common\constants
 */
class B {
    /**
     * 表：商户
     */
    const TABLE_MEMBER = 1;
    /**
     * 类型：二维码
     */
    const MEMBER_TYPE_QR_CODE = 101;


    /**
     * 获取表明
     * @param $table
     * @return string
     */
    public static function getTableName($table) {
        $names = [
            B::TABLE_MEMBER => Member::tableName(),
        ];
        return ArrayUtil::issetDefault($names, $table, "");
    }
}