<?php


namespace frontend\controllers;


use common\base\BaseWebController;
use common\constants\C;
use common\exceptions\ForeseeableException;
use common\models\UserModel;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;

/**
 * Class BaseFrontendController 前台控制器基类
 * @package payment\controllers
 */
class BaseFrontendController extends BaseWebController {
    /**
     * @var null|UserModel
     */
    private $user = null;

    /**
     * @param Action $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action) {
        $uniqueId = $action->getUniqueId();
        $user = UserUtil::getUser();

        // 判断是否是不用登录也可访问的路由
        if (!$this->isSkipLoginCheckRoute($uniqueId) && !$user->isLoginValid()) {
            $responseCode = C::RESPONSE_CODE_NOT_LOGIN;
            $message = C::getReturnCodeLabel($responseCode);

            // 当前有登录，但是登录无效。原因：被踢出
            if ($user->isLogin()) {
                $message = "您的账号被踢出。可能在其他地方被登录";
            }
            Yii::$app->response->data = $this->response($responseCode, $message); // 设置返回数据
            return false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $route
     * @return bool
     */
    private function isSkipLoginCheckRoute($route) {
        $skipLoginCheckRoute = [
            "base-frontend/error" => true,
            "user/login" => true,
            "user/logout" => true,
            "user/login-by-session-id" => true,
            "user/on-register" => true,
        ];
        return isset($skipLoginCheckRoute[$route]);
    }

    /**
     * 获取当前登录用户的用户id
     * @deprecated 191007
     * @see UserUtil::getUserId() 替代方法
     * @return int
     */
    protected function getUserId() {
        return UserUtil::getUserId();
    }

    /**
     * 获取当前登录用户模型
     * @deprecated 191007
     * @return UserModel|null
     * @see UserUtil::getUser() 替代方法
     */
    protected function getUserModel() {
        return UserUtil::getUser();
    }
}