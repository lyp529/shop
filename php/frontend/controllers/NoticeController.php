<?php

namespace frontend\controllers;

use common\services\SM;



/**
 * Class NoticeController 公告控制器
 * @package backend\BaseBackendController
 */
class NoticeController extends BaseFrontendController {
    /**
     * 获取公告列表
     * @return string
     */
    public function actionGetNoticeList(){
        $page = $this->param("page", 1);
        $rows = $this->param("size", 10);
        return $this->done(SM::getNoticeService()->getNoticeList(($page - 1) * $rows, $rows));
    }
}
