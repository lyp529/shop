<?php


namespace frontend\controllers;


use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\Bet;
use common\models\db\Schedule;
use common\models\vo\sc\BetVo;
use common\services\SM;
use common\utils\game\SscUtil;
use common\utils\game\UserUtil;
use frontend\models\form\BetForm;
use yii\helpers\Json;

/**
 * Class BetController 下注控制器
 * @package frontend\controllers
 */
class BetController extends BaseFrontendController {

    /**
     * 检测注单数据是否合法
     * @throws ForeseeableException
     */
    public function actionCheckBetVo() {
        $betVoArr = $this->param("betVo");
        $betVo = new BetVo();
        $betVo->setAttributes($betVoArr);
        if (!$betVo->validate()) {
            throw new ForeseeableException($betVo->getFirstError());
        }
        if (!SscUtil::checkData($betVo->betType, $betVo->content)) {
            throw new ForeseeableException("号码不符合规则。请重选");
        }
        $betVo->groupNum = SscUtil::getGroupNum($betVo->betType, $betVo->content);

        return $this->response(C::RESPONSE_CODE_DONE, "添加成功。别忘了确认下注~~~", ["betVo" => $betVo]);
    }

    /**
     * 检测并返回正确的表单数据（可提交）
     * @return string
     * @throws ForeseeableException
     */
    public function actionCheckBetForm() {
        $form = BetForm::initByParam();
        if (!$form->validate()) {
            throw new ForeseeableException($form->getFirstError());
        }

        SM::getBetService()->checkAndModBetForm($form);

        return $this->done(["form" => $form]);
    }

    /**
     * 提交订单数据
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionSubmitBetForm() {
        $form = BetForm::initByParam();
        if (!$form->validate()) {
            throw new ForeseeableException($form->getFirstError());
        }

        $this->beginTransaction();
        SM::getBetService()->submitCheckBetForm($form, UserUtil::getUser());

        return $this->done();
    }

    /**
     * 获取注单列表
     * @return string
     * @throws ForeseeableException
     */
    public function actionGetBetVoList() {
        $offset = (int)$this->param("offset");
        $size = (int)$this->param("size");
        $kind = $this->param("kind");
        $date = $this->param("date");
        $result = $this->param("result");
        $tableData = SM::getBetService()->getBetVoList($offset, $size, $kind, $date, $result);
        return $this->done($tableData);
    }

    /**
     * 检测是否中奖
     * @return string
     * @throws ForeseeableException
     */
    public function actionCheckIsWin() {
        $scheduleId = $this->param("scheduleId");

        $schedule = Schedule::findOne($scheduleId);
        if ($scheduleId === null) {
            throw new ForeseeableException("开奖期数不存在");
        }

        // 默认没有数据（没有投注的话，不会有消息）
        $message = "";
        $betVoList = Bet::findAll(["schedule_id" => $scheduleId, "member_id" => UserUtil::getUserId()]);
        if (!empty($betVoList)) {
            $message = $schedule->period . "期 没有中奖";
        }

        $winAmount = 0;
        foreach ($betVoList as $bet) {
            $winAmount += doubleval($bet->bet_result);
        }
        $winAmount = !empty($winAmount) ? round($winAmount, 2) : 0;
        $message = !empty($winAmount) ? $schedule->period . "期 中奖 " . $winAmount . " 元" : $message;

        return $this->done(["message" => $message]);
    }
}
