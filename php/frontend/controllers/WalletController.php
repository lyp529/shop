<?php

namespace frontend\controllers;

use common\constants\L;
use common\exceptions\SystemException;
use common\utils\DLogger;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use frontend\models\form\OrderForm;
use common\constants\C;
use common\services\SM;
use common\exceptions\ForeseeableException;


/**
 * Class WalletController
 * @package frontend\controllers
 */
class WalletController extends BaseFrontendController
{
    /**
     * 兑换商品
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionForGoods() {
        $id = $this->param('goodsId');
        $this->beginTransaction();
        SM::getOrderWalletService()->forGoods($id);
        return $this->done();
    }
    public function actionGetOrderList() {
        return $this->done(SM::getOrderWalletService()->getOrderList());
    }
    public function actionGetOrderList1() {
        return $this->done(SM::getOrderWalletService()->getOrderList1());
    }
    public function actionGetOrderList2() {
        return $this->done(SM::getOrderWalletService()->getOrderList2());
    }
}
