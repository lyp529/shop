<?php

namespace frontend\controllers;

use common\constants\C;
use common\exceptions\ForeseeableException;
use common\models\db\Schedule;
use common\models\vo\sc\ScScheduleVo;
use common\services\SM;

/**
 * Class ScheduleController 期数控制器（获取游戏的期数）
 * @package frontend\controllers
 */
class ScheduleController extends BaseFrontendController
{

    /**
     * 获取最新的期数数据。最新一起以及即将开奖的一期
     * @throws ForeseeableException
     */
    public function actionGetScheduleVo() {
        $kind = $this->param("kind");
        if ($kind === null) {
            throw new ForeseeableException("请选择游戏");
        }
        $scheduleService = SM::getScheduleService();

        $nowSchedule = $scheduleService->getNowSchedule($kind);
        $nosScheduleVo = new ScScheduleVo();
        if ($nowSchedule !== null) {
            $nosScheduleVo->initBySchedule($nowSchedule);
        } else {
            $nosScheduleVo = null;
        }

        $pastSchedule = $nowSchedule !== null ? $nowSchedule->findPastSchedule() : $scheduleService->getPastSchedule($kind);
        $pastScheduleVo = new ScScheduleVo();
        if ($pastSchedule !== null) {
            $pastScheduleVo->initBySchedule($pastSchedule);
        }

        return $this->done(["nowScheduleVo" => $nosScheduleVo, "pastScheduleVo" => $pastScheduleVo]);
    }

    /**
     * 获取往期开奖结果（默认获取当前-支持分页）
     * @return string
     * @throws ForeseeableException
     */
    public function actionGetOldScheduleVoList() {
        $kind = $this->param("kind"); // 什么游戏
        $size = $this->param("size", 30); // 获取多少条数据
        $offset = $this->param("offset");

        if (empty($kind)) {
            $kind = C::KIND_SSC_BERLIN;
        }

        $scheduleVoList = SM::getScheduleService()->getOldScheduleVoList($kind, $size,$offset);

        return $this->done([
            "scheduleVoList" => $scheduleVoList["cashRecordList"],
            "count" => $scheduleVoList["count"]
        ]);
    }

    /**
     * 获取获取最近开奖的二级路
     * @throws ForeseeableException
     */
    public function actionGetPastScheduleVoList() {
        // 游戏
        $kind = $this->param("kind");
        // 当前已开奖或准备开奖（即往期）的 期数id
        $scheduleId = $this->param("scheduleId");
        // 获取数据的条数
        $size = $this->param("size", 5);

        if ($kind === null) {
            throw new ForeseeableException("请选择游戏");
        }

        $schedule = Schedule::findOne($scheduleId);
        if ($schedule === null) {
            $scheduleList = [];
        } else {
            /** @var Schedule[] $scheduleList */
            $scheduleList = Schedule::find()->where(["kind" => $kind])
                ->andWhere((["<", "open_time", $schedule->open_time]))
                ->orderBy(["open_time" => SORT_DESC])->limit($size)->all();
        }

        $scheduleVoList = [];
        foreach ($scheduleList as $schedule) {
            $vo = new ScScheduleVo();
            $vo->initBySchedule($schedule);
            $scheduleVoList[] = $vo;
        }

        return $this->done(["scheduleVoList" => $scheduleVoList]);
    }
}
