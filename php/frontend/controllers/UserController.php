<?php

namespace frontend\controllers;


use backend\models\form\UserForm;
use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\UserAccount;
use common\models\UserModel;
use common\models\vo\frontend\UserVo;
use common\services\SM;
use common\services\UserService;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use common\utils\YiiUtil;


/**
 * Class UserController 用户控制器
 * @package backend\BaseBackendController
 */
class UserController extends BaseFrontendController
{
	/**
	 * @return string
	 * @throws ForeseeableException
	 * @throws SystemException
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionChangeContact() {
		$junction = $this->param('junction');
		$type = $this->param('type');
		$this->beginTransaction();
		return $this->done(SM::getUserService()->changeContact($junction,$type));
	}

	public function actionGetRecommend() {
		return $this->done(SM::getUserService()->getRecommend());
	}

	public function actionGetUserArrayA() {
		return $this->done(SM::getUserService()->getUserArrayA());
	}

	public function actionGetUserArrayB() {
		return $this->done(SM::getUserService()->getUserArrayB());
	}

	public function actionConversionIntegral() {
        $username = $this->param('username');
        $integral = $this->param('integral');
        $this->beginTransaction();
        SM::getUserService()->conversionIntegral($username, $integral);
        return $this->done();
    }
    /**
     * 登录系统
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws \Throwable
     */
    public function actionLogin() {
        $username = $this->param('username');
        $password = $this->param('password');

        $user = SM::getUserService()->loginCheck($username, $password, false);
        if (!$user->isMember()) {
            throw new ForeseeableException("不是会员账户");
        }
        // 设置登录信息
        $user->login();

        return $this->response(C::RESPONSE_CODE_DONE, "登录成功", ["sessionId" => $user->getAccount()->session_id]);
    }

    /**
     * 退出登录
     * @return string
     * @throws \Throwable
     */
    public function actionLogout() {
        if (UserUtil::hasLoginUser()) {
            $user = UserUtil::getUser();
            $user->logout();
        }

        $this->destroySession();
        return $this->done();
    }

    /**
     * 获取会员信息
     */
    public function actionGetUserVo() {
        $user = UserUtil::getUser();
        $userVo = new UserVo();
        $userVo->initByUserModel($user);

        if ($userVo->isSign == TimeUtil::getCurrDate()) {
            $userVo->isSign = true;
        } else {
            $userVo->isSign = false;
        }
        $userLevel = SM::getUserService()->levelVip($userVo->vipLevel);
        $userVo->backPercent = $userLevel . "%";

        $todayBetAmount = SM::getBetService()->getBetAmountToday();
        $userVo->todayPredictRebate = $todayBetAmount * $userLevel * 0.01;

        return $this->done(["userVo" => $userVo]);
    }

    /**
     * 使用 session id 进行登录
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws \Throwable
     */
    public function actionLoginBySessionId() {
        $sessionId = $this->param("sessionId");
        if (empty($sessionId)) {
            throw new ForeseeableException("登录信息已过期，请重新登录");
        }
        $account = UserAccount::findOne(["session_id" => $sessionId]);
        if ($account === null) {
            throw new ForeseeableException("登录信息已过期，请重新登录");
        }
        $user = UserModel::initUser($account->id);
        if (!$user->isActive()) {
            throw new ForeseeableException("账号已禁用");
        }
        if (!$account->isMember()) {
            throw new SystemException("不是会员账号");
        }
        if ($account->login_ip !== YiiUtil::getUserIP()) {
            // ip 和上次登录时的不一致，不允许登录
            throw new ForeseeableException("登录信息已过期，请重新登录");
        }

        // 设置登录信息
        $user->login();

        return $this->response(C::RESPONSE_CODE_DONE, "登录成功");
    }


	/**
	 * 用户注册
	 * @return string
	 * @throws ForeseeableException
	 * @throws SystemException
	 * @throws \Throwable
	 */
	public function actionOnRegister() {
		$form = UserForm::initByParam();
		$addUser = SM::getUserService()->setRegister($form);

		$user = SM::getUserService()->loginCheck($addUser->username, $addUser->password, false);
		if (!$user->isMember()) {
			throw new ForeseeableException("不是会员账户");
		}
		// 设置登录信息
		$user->login();

		return $this->response(C::RESPONSE_CODE_DONE, "登录成功", ["sessionId" => $user->getAccount()->session_id]);
	}

    /**
     * 修改密码
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionChangePassword() {
        $oldPassword = $this->param("oldPassword");
        $newPassword = $this->param("newPassword");

        $user = UserUtil::getUser();
        $user->updatePwd($oldPassword, $newPassword);

        return $this->done();
    }

    /**
     * 修改资金密码
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionChangeCashPassword() {
        $oldPassword = $this->param("oldPassword");
        $newPassword = $this->param("newPassword");
        $user = UserUtil::getUser();
        $user->updatePwd($oldPassword, $newPassword, true);

        return $this->done();
    }

    /**
     * 设置银行信息
     * @throws ForeseeableException
     */
    public function actionSetBankInfo() {
        $bankId = $this->param("bankId");
        $bankAccount = $this->param("bankAccount");
        $bankUsername = $this->param("bankUsername");
        $bankEmail = $this->param("bankEmail");
        $bankCashPassword = $this->param("bankCashPasswordEncrypt");

        SM::getUserService()->memberSetBank($bankId, $bankAccount, $bankUsername, $bankEmail, $bankCashPassword);
        return $this->done();
    }


    /**
     * 首页所需的数据
     */
    public function actionGetSystemVo() {
        $user = UserUtil::getUser();
        $systemVo = SM::getUserService()->getFrontendSystemVo($user);
        $user->resetSessionDeadTime();
        return $this->done(["systemVo" => $systemVo]);
    }
}
