<?php

namespace frontend\controllers;

use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\services\SM;
use frontend\models\form\ActivitysForm;

/**\
 * Class ActivityController 活动控制器
 * @package frontend\controllers
 */
class ActivityController extends BaseFrontendController
{
    /**
     * 签到
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionSignIn() {
        $this->beginTransaction();
        $signAmount = SM::getActivityService()->signIn();
        return $this->done(["signAmount" => credit_format($signAmount)]);
    }

    /**
     * @return string
     * @throws ForeseeableException
     */
    public function actionBetting() {
        $this->beginTransaction();

        $form = ActivitysForm::initByParam();
        if (!$form->validate()) {
            throw new ForeseeableException($form->getFirstError());
        }

        SM::getActivityService()->betting($form);

        return $this->done();
    }

    /**
     * 积分兑换
     * @return string
     * @throws SystemException
     * @throws \Throwable
     * @throws \common\exceptions\ForeseeableException
     */
    public function actionExchangeCredit() {
        $this->beginTransaction();
        $integral = $this->param('integral');
        $integral = SM::getIntegralService()->exchangeCredit($integral);
        return $this->done(['integral' => $integral]);
    }

    public function actionGoodsList() {
        return $this->done(SM::getActivityService()->goodsList());
    }










    /**
     * 获取幸运大转盘列表
     * @return string
     */
    public function actionGetLuckRunList() {
        return $this->done(SM::getIntegralService()->getLuckRunList());
    }

    /**
     * 参加幸运大转盘 -- 开启
     * @return string
     * @throws \Throwable
     * @throws \common\exceptions\ForeseeableException
     */
    public function actionLuckyRunStart() {
        $this->beginTransaction();
        return $this->done(SM::getIntegralService()->luckyRunStart());
    }

    /**
     * 参加幸运大转盘 -- 结束
     * @return string
     * @throws SystemException
     * @throws \Throwable
     * @throws \common\exceptions\ForeseeableException
     */
    public function actionLuckyRunEnd() {
        return $this->done(SM::getIntegralService()->luckyRunEnd());
    }
}
