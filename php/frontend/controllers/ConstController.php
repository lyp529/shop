<?php


namespace frontend\controllers;

use common\constants\C;
use common\constants\L;
use common\constants\P;
use common\services\SM;

/**
 * Class ConstController
 * @package frontend\controllers
 */
class ConstController extends BaseFrontendController
{

    /**
     * 获取系统常量
     * @return string
     */
    public function actionGet() {
        $systemService = SM::getSystemService();

        return $this->done([
            "orderSubTypeDepositLabels" => $systemService->filterOrderSubTypeDepositLabels(), // 订单子类型（存款方式）标签
            "orderSubTypeDepositNameLabels" => C::getOrderSubTypeDepositNameLabel(), // 名字标签
            "orderSubTypeWithdrawLabels" => C::getOrderSubTypeWithdrawLabel(), // 订单子类型（提款方式）标签
            "orderWalletTypeLabels" => C::orderWalletTypeLabel(),  // 获取订单类型
            "orderWalletStateLabel" => C::orderWalletStateLabel(), // 获取订单状态
            "betUnitLabels" => C::getBetUnitLabel(), // 注单单位标签
            "cashGroupLabels" => L::getCashLabels(), // 账变类型（分组）
            "bankLabels" => C::getBankLabel(), //银行标签
            "qqs" => $systemService->getQQs(), // 获取联系我们qq
        ]);
    }

    /**
     * 彩票大厅常量数据
     */
    public function actionGetLottery() {
        return $this->done([
            "pKindMap" => P::getPKindMap(), // 游戏大厅分组游戏列表数据

            // 时彩所需的常量
            "scDigitLabels" => P::getScDigitLabel(), // 时彩 位数标签
            "scBetTypeLabels" => P::getScBetTypeLabel(), // 时彩 玩法标签
            "scGroupList" => P::getScGroupList(), // 时彩玩法分组
            "scBetTypeLabelMap" => P::getScBetTypeLabelMap(), // 时彩玩法数值
            "scBetTypeNodeMap" => P::getScBetTypeNodeMap(),
            "scTreadTypeLabels" => P::getScTreadTypeLabels(), // 趋势图类型标签
        ]);
    }

}
