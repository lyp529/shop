<?php


namespace frontend\models\form;


use common\base\BaseForm;
use common\models\vo\sc\BetVo;

/**
 * Class BetForm 下注表单
 * @package frontend\models\form
 */
class BetForm extends BaseForm
{
    /**
     * @var int 期数id
     */
    public $scheduleId;
    /**
     * @var BetVo[] 注单列表
     */
    public $betVoList = [];

    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [["scheduleId"], "required"],
        ]);
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true) {
        parent::validate($attributeNames, $clearErrors);
        foreach ($this->betVoList as $betVo) {
            if (!$betVo->validate()) {
                $this->addError("betVoList", $betVo->getFirstError());
            }
        }
        return !$this->hasErrors();
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            "scheduleId" => "下注期数",
            "betVoList" => "注单列表列表"
        ];
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->scheduleId = (int)$this->scheduleId;
        $this->betVoList = BetVo::initListByArr($this->betVoList);
    }
}
