<?php


namespace frontend\models\form;


use common\base\BaseForm;
use common\constants\C;
use common\models\db\OrderWallet;

/**
 * Class DepositForm 存提款表单数据
 * @package frontend\models\form
 */
class OrderForm extends BaseForm
{
    /**
     * @var int 支付|收款方式
     */
    public $orderSubType;
    /**
     * @var double|int 存款金额
     */
    public $amount;
    /**
     * @var string 备注
     */
    public $remark = "";

    /**
     * @var string 收款银行名字
     */
    public $recvName;
    /**
     * @var string 收款银行账号
     */
    public $recvAccount;
    /**
     * @var string 收款银行户名
     */
    public $recvUsername;
    /**
     * @var string 付款银行户名 或者 帐号或帐户名(微信支付宝)
     */
    public $payUsername;
    /**
     * @var string 汇款时间
     */
    public $payTime;

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->orderSubType = (int)$this->orderSubType;
        $this->amount = (double)$this->amount;
    }

    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [["orderSubType", "amount"], "required"],
            [["orderSubType", "amount"], "number"],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            "depositType" => "存取款方式",
            "amount" => "金额",
            "remark" => "备注",
            "bankNo" => "银行卡号",
            "bankName" => "开户名字",
        ];
    }
}
