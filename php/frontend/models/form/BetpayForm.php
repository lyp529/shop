<?php


namespace frontend\models\form;


use common\base\BaseForm;

/**
 * Class NoticeForm 新增、修改公告表单的模型
 * @package models\form
 */
class BetpayForm extends BaseForm {
    /**
     * @var int  注单ID
     */
    public $id;
    /**
     * @var int 会员ID
     */
    public $member_id;
    /**
     * @var string 注单编号-uniqid
     */
    public $bet_no;
    /**
     * @var int 场次ID
     */
    public $schedule_id;
    /**
     * @var int 倍数
     */
    public $multiple;
    /**
     * @var string 下注类型
     */
    public $bet_type;
    /**
     * @var  string 下注类型名称
     */
    public $bet_type_name;
    /**
     * @var string 下注内容
     */
    public $bet_content;
    /**
     * @var int 单位 1元 2角
     */
    public $unit;
    /**
     * @var int  下注金额
     */
    public $bet_amount;
    /**
     * @var string  派彩结果
     */
    public $bet_result;



    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [['member_id','bet_amount' , 'bet_no'], 'required'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'id' => '注单ID',
            'bet_no' => '注单编号-uniqid',
            'schedule_id' => '场次ID',
            'member_id' => '会员ID',
            'bet_type' => '下注类型',
            'bet_type_name' => '下注类型名称',
            'bet_content' => '下注内容',
            'multiple' => '倍数',
            'unit' => '单位 1元 2角',
            'bet_amount' => '下注金额',
            'bet_result' => '派彩结果'
        ];
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes
        ($values, $safeOnly);
        $this->bet_result = (int)$this->bet_result;
        $this->schedule_id = (int)$this->schedule_id;
        $this->member_id = (int)$this->member_id;
        $this->bet_type = (string)$this->bet_type;
        $this->bet_type_name = (string)$this->bet_type_name;
        $this->bet_no = (string)$this->bet_no;
        $this->bet_content = (string)$this->bet_content;
        $this->multiple = (int)$this->multiple;
        $this->bet_amount = (float)$this->bet_amount;
    }
}
