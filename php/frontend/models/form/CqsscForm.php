<?php


namespace frontend\models\form;


use common\base\BaseForm;

/**
 * Class CqsscForm 重庆时时彩表单数据
 * @package frontend\models\form
 */
class CqsscForm extends BaseForm {
    /**
     * @var int 玩法
     */
    public $playType;
    /**
     * @var int[] 万位选择
     */
    public $wan = [];
    /**
     * @var int[] 千位选择
     */
    public $qian = [];
    /**
     * @var int[] 百位选择
     */
    public $bai = [];
    /**
     * @var int[] 十位选择
     */
    public $shi = [];
    /**
     * @var [] 个位选择
     */
    public $ge = [];
}