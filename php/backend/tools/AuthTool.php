<?php


namespace backend\tools;


use common\constants\A;
use common\constants\C;
use common\constants\M;
use common\models\db\AuthItem;
use common\models\MenuModel;
use common\utils\game\UserUtil;

/**
 * Class AuthTool 权限工具。用于判断当前用户的权限
 * @package backend\tools
 */
class AuthTool {
    /**
     * @var array
     */
    private static $moduleActionMap = [];

    /**
     * 判断是否可以执行某操作
     * @param $module
     * @param $action
     * @return boolean
     */
    public static function canDo($module, $action) {
        AuthTool::checkModuleAction($module, $action);

        if (UserUtil::isAdmin()) {
            return true;
        }
        $userId = UserUtil::getUserId();
        $itemName = AuthTool::makeItemName($module, $action);

        $authItem = AuthItem::find()->select(["auth_item.*"])
            ->leftJoin("auth_role", "auth_role.id = auth_item.role_id")
            ->where([
                "auth_item.item_name" => $itemName,
                "auth_role.bind_id" => $userId,
                "auth_role.type" => C::AUTH_ROLE_TYPE_USER,
            ])->one();
        return $authItem !== null;
    }

    /**
     * 判断是否可以查看
     * @param $module
     * @return bool
     */
    public static function canView($module) {
        return AuthTool::canDo($module, A::VIEW);
    }

    /**
     * 判断是否有添加权限
     * @param $module
     * @return bool
     */
    public static function canAdd($module) {
        return AuthTool::canDo($module, A::ADD);
    }

    /**
     * 判断是否有修改权限
     * @param $module
     * @return bool
     */
    public static function canMod($module) {
        return AuthTool::canDo($module, A::MOD);
    }

    /**
     * 判断是否有删除权限
     * @param $module
     * @return bool
     */
    public static function canDel($module) {
        return AuthTool::canDo($module, A::DEL);
    }

    /**
     * @param $module
     * @param $action
     * @return string
     */
    public static function makeItemName($module, $action) {
        return $module . "-" . $action;
    }

    /**
     * 验证模块下是否有该权限
     * @param $module
     * @param $action
     */
    private static function checkModuleAction($module, $action) {
        $moduleActionMap = AuthTool::getModuleActionMap();

        if (!isset($moduleActionMap[$module][$action])) {
            $moduleLabel = M::getLabel($module);
            $actionLabel = A::getLabel($action);
            die("权限错误：[模块：" . $moduleLabel . "]下没有[动作：" . $actionLabel . "]。请求到 MenuModule::getMenu() 下添加");
        }
    }

    /**
     * 获取 模块 - 动作 的map
     * @return array
     */
    private static function getModuleActionMap() {
        if (!empty(self::$moduleActionMap)) {
            return self::$moduleActionMap;
        }

        self::$moduleActionMap = [];
        $root = MenuModel::getMenu();
        foreach ($root->children as $menu) {
            foreach ($menu->children as $subMenu) {
                $module = $subMenu->module;
                foreach ($subMenu->actions as $action) {
                    self::$moduleActionMap[$module][$action] = true;
                }
            }
        }
        return self::$moduleActionMap;
    }
}