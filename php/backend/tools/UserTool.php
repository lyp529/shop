<?php


namespace backend\tools;


use common\constants\C;
use common\models\UserModel;
use common\utils\game\UserUtil;
use Yii;

/**
 * Class UserTool 用户工具（和业务逻辑有一定关系）。建议只再控制器下使用。不要在 Service 中使用
 * @deprecated 191007 请使用 UserUtil 代替该类
 * @see UserUtil
 * @package backend\tools
 */
class UserTool {
    /**
     * @var null|UserModel 用户数据单次请求的缓存。减少数据库的查询次数
     */
    private static $user = null;

    /**
     * 获取当前登录用户的id
     * @return int
     */
    public static function getId() {
        return (int)Yii::$app->getSession()->get(C::SESSION_KEY_USER_ID);
    }

    /**
     * 获取当前登录用户的数据
     * @return UserModel
     */
    public static function getUser() {
        if (self::$user === null) {
            self::$user = UserModel::initUser(UserUtil::getUserId());
        }
        return self::$user;
    }

    /**
     * 是否是超级管理员
     * @return bool
     */
    public static function isAdmin() {
        $user = UserUtil::getUser();
        return $user->isAdmin();
    }

}