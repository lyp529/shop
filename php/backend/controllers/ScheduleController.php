<?php


namespace backend\controllers;


use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\services\SM;
use common\utils\game\UserUtil;

/**
 * Class ScheduleController 期数控制器
 * @package backend\controllers
 */
class ScheduleController extends BaseBackendController
{
    /**
     * 获取期数表格数据
     */
    public function actionGetScheduleListTable() {
        $page = $this->param("page");
        $size = $this->param("size");
        $kind = $this->param("kind");
        $period = $this->param("period");
        $date = $this->param("date");
        $recently = $this->param("recently");

        $offset = ($page - 1) * $size;
        $tableData = SM::getScheduleService()->getScheduleTable(C::KIND_SSC_BERLIN, $offset, $size, $kind, $period, $date, $recently);

        return $this->done($tableData);
    }

    /**
     * 预设结果
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionPredictScheduleResult() {
        if (!UserUtil::isAdmin()) {
            throw  new ForeseeableException("只有管理员才有权限设置预设结果");
        }
        $scheduleId = $this->param("scheduleId");
        $result = $this->param("result");

        $this->beginTransaction();
        SM::getScheduleService()->predictScheduleResult($scheduleId, $result);

        return $this->done();
    }
}
