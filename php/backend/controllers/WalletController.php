<?php

namespace backend\controllers;

use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\OrderWallet;
use common\models\vo\backend\DepositTypeVo;
use common\models\vo\order\WithdrawVo;
use common\services\SM;
use common\utils\game\UserUtil;


/**
 * Class WalletController  钱包控制器
 * @package backend\WalletController
 */
class WalletController extends BaseBackendController
{
    /**
     * 获取订单列表
     * @return string
     */
    public function actionGetWalletOrderList()
    {
        $page = $this->param("page", 1);
        $rows = $this->param("size", 10);
        $endTime = $this->param("endTime");
        $startTime = $this->param("startTime");
        $orderState = $this->param("orderState");
        $orderNumber = $this->param("orderNo");
        $list = SM::getOrderWalletService()->getWalletOrderList($startTime,$endTime,$orderState,$orderNumber,($page - 1) * $rows, $rows);
        return $this->done($list);
    }

    /**
     * 订单 确认完成/失败
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionUpdateWalletState()
    {
        $id = $this->param("id");
        $this->beginTransaction();
        SM::getOrderWalletService()->updateWalletState($id, UserUtil::getUserId());
        return $this->done();
    }

    /**
     * 获取转账信息（即用户填写的转账银行信息）
     * @throws ForeseeableException
     */
    public function actionGetWithdrawVo() {
        $orderWalletId = $this->param("orderWalletId");

        $orderWallet = OrderWallet::findOne($orderWalletId);
        if ($orderWallet === null) {
            throw new ForeseeableException("记录不存在");
        }
        $withdrawVo = new WithdrawVo();
        $withdrawVo->initByOrderWallet($orderWallet);

        return $this->done(["withdrawVo" => $withdrawVo]);
    }

    /**
     * 获取存款类型开关数据
     */
    public function actionGetDepositTypeVoList() {
        return $this->done([
            "depositTypeVoList" => SM::getSystemService()->getDepositTypeVoList(),
        ]);
    }

    /**
     * 保存 存款类型开关数据
     */
    public function actionSaveDepositTypeVoList() {
        $depositTypeVoListArr = $this->param("depositTypeVoList");
        $depositTypeVoList = DepositTypeVo::initListByArr($depositTypeVoListArr);

        SM::getSystemService()->saveDepositTypeVoList($depositTypeVoList);

        return $this->done();
    }
}
