<?php


namespace backend\controllers;


use common\base\BaseWebController;
use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\services\SM;
use common\utils\ApiUtil;
use common\utils\ArrayUtil;
use yii\db\Exception;

/**
 * Class OpenController 开放控制器。没有权限验证
 * @package backend\controllers
 */
class OpenController extends BaseWebController {

    /**
     * 让支付平台回调的方法
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Exception
     */
    public function actionPayNotify() {
        $hash = $this->param("Hash");
        $params = ApiUtil::decodeHash($hash);
        list($timestamp, $partnerCode, $orderNo, $amount, $notifyType, $remark) = $params;

        $transaction = \Yii::$app->db->beginTransaction();
        $exception = null;
        try {
            $orderWalletService = SM::getOrderWalletService();
            if ($notifyType === C::ORDER_NOTIFY_TYPE_DONE) {
                $orderWalletService->ensurePay($orderNo, $amount);
            } else if ($notifyType === C::ORDER_NOTIFY_TYPE_FAIL) {
                $orderWalletService->cancelPay($orderNo, $remark);
            } else {
                throw new ForeseeableException("未知的通知类型");
            }
        } catch (ForeseeableException $e) {
            $exception = $e;
        } catch (SystemException $e) {
            $message = $e->getMessage();
            if (!ArrayUtil::inArray([$message], ["订单已处理"])) {
                // 消息不属于成功的消息
                $exception = $e;
            }
        } catch (\Exception $e) {
            $exception = $e;
        } finally {
            if ($exception !== null) {
                $transaction->rollBack();
                throw $exception;
            } else {
                $transaction->commit();
            }
        }

        return "SUCCESS";
    }
}