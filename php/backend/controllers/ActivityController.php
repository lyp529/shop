<?php

namespace backend\controllers;


use common\exceptions\SystemException;
use common\services\SM;
use common\utils\game\UserUtil;


/**
 * Class ActivityController 活动控制器
 * @package backend\BaseBackendController
 */
class ActivityController extends BaseBackendController {
    /**
     * 获取兑换奖励列表
     * @return string
     */
    public function actionGetCashRewardsList()
    {
        return $this->done(SM::getActivityService()->getCashRewardsList());
    }

    /**
     * 修改兑换奖励
     * @return string
     * @throws SystemException
     * @throws \Throwable
     * @throws \common\exceptions\ForeseeableException
     * @throws \yii\db\StaleObjectException
     */
    public function actionEditCashRewards(){
        $id = $this->param("id");
        $value = $this->param("value");
        $condition = $this->param("condition");
        $nowIntegral = $this->param("nowIntegral");
        $this->beginTransaction();
        SM::getActivityService()->editCashRewards($id,$value,$condition,$nowIntegral);
        return $this->done();
    }
}
