<?php

namespace backend\controllers;

use common\constants\C;
use common\models\db\UserAccount;
use common\models\db\UserMember;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use Throwable;
use common\services\SM;
use common\exceptions\ForeseeableException;
use yii\web\User;


/**
 * Class ReportController 报表
 * @package backend\BaseBackendController
 */
class ReportController extends BaseBackendController
{

    /**
     * 获取盈亏统计列表
     * @return string
     */
    public function actionGetReportList() {
        $page = $this->param("page", 1);
        $size = $this->param("size", 10);
        $startTime = $this->param("startTime", TimeUtil::today());
        $endTime = $this->param("endTime", TimeUtil::today());
        $username = $this->param("username");
        $userType = $this->param("userType");
        $upType = $this->param("upType");
        $upId = $this->param("upId");


        $queryPlus = [];
        if ($username) {
            if ($userType == C::USER_MEMBER) {
                $queryPlus['username'] = $username;
            } else {
                $account = UserAccount::findOne(['username' => $username]);
                if ($account == null) {
                    return $this->done([]);
                }
                $queryPlus[UserUtil::getIdField($userType)] = $account->id;
            }
        }
        // 查上线
        if ($upType && $upId) {
            $queryPlus[UserUtil::getIdField($upType)] = $upId;
        }

        if ($startTime) {
            $startTime .= ' 00:00:00';
        }
        if ($endTime) {
            $endTime .= ' 23:59:59';
        }

        $list = SM::getLogService()->getReportList($userType, $queryPlus, $startTime, $endTime, ($page - 1) * $size, $size);
        return $this->done($list);
    }

}
