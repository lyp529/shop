<?php

namespace backend\controllers;


use common\constants\C;
use common\constants\L;
use Yii;

/**
 * Class OptionsController 常量、配置控制器。用于前端初始化一些参数
 * @package backend\controllers
 */
class ConstController extends BaseBackendController
{

    /**
     * 获取系统通用的常量列表
     */
    public function actionGet() {
        return $this->done([
            "userTypeLabels" => C::getUserTypeLabel(),
            "noticeTypeLabels" => C::getNoticeTypeLabel(),
            "getWalletOrderTypeLabels" => C::orderWalletTypeLabel(),
            "getWalletOrderStatusLabels" => C::orderWalletStateLabel(),
            "cashLabels" => L::getCashLabels(),
            "kindLabels" => C::getKindLabel(),
            "payPlatUrl" => Yii::$app->params["PAY_PLAT_URL"],
        ]);
    }
}
