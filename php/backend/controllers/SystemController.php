<?php


namespace backend\controllers;


use common\exceptions\ForeseeableException;
use common\models\db\KeyValue;
use common\models\dbVo\config\MemberContactVo;

/**
 * Class SystemController 系统控制器。一些散乱的，没有固定模块的请求就放这里
 * @package backend\controllers
 */
class SystemController extends BaseBackendController {

    /**
     * 获取 联系方式
     */
    public function actionGetMemberContactVo() {
        return $this->done([
            "memberContactVo" => KeyValue::getMemberContactVo(),

        ]);
    }

    /**
     * 保存 联系方式
     * @return string
     * @throws ForeseeableException
     */
    public function actionSaveMemberContactVo() {
        $tmpArr = $this->param("memberContactVo");

        $memberContactVo = new MemberContactVo();
        $memberContactVo->setAttributes($tmpArr);
        KeyValue::saveMemberContactVo($memberContactVo);

        return $this->done();
    }
}