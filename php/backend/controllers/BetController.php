<?php


namespace backend\controllers;


use common\services\SM;
use common\utils\game\UserUtil;

/**
 * Class BetController 注单控制器
 * @package backend\controllers
 */
class BetController extends BaseBackendController {

    /**
     * 获取注单列表表格数据
     */
    public function actionGetBetRowVoTable() {
        $page = $this->param("page");
        $size = $this->param("size");
        $kind = $this->param("kind");
        $period = $this->param("period");
        $startTime = $this->param("startTime");
        $endTime = $this->param("endTime");
        $username = $this->param("username");
        $minBetResult = $this->param("minBetResult");
        $maxBetResult = $this->param("maxBetResult");

        $offset = ($page - 1) * $size;
        $user = UserUtil::getUser();

        $tableData = SM::getBetService()->getBetRowVoTable($offset, $size, $user, $kind, $period, $startTime, $endTime, $username, $minBetResult, $maxBetResult);

        return $this->done($tableData);
    }
}