<?php

namespace backend\controllers;


use backend\models\form\UserForm;
use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\UserAccount;
use common\models\UserModel;
use common\models\vo\user\InfoVo;
use common\models\vo\user\RowVo;
use common\models\UploadModel;
use yii\web\UploadedFile;
use common\utils\game\UserUtil;
use common\utils\ValueUtil;
use common\utils\ArrayUtil;
use Throwable;
use common\services\SM;
use common\models\vo\auth\ModuleVo;


/**
 * Class UserController 用户控制器
 * @package backend\BaseBackendController
 */
class UserController extends BaseBackendController
{
	/**
	 * @return string
	 * @throws SystemException
	 * @throws Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionChangeUser() {
		$user_id = $this->param("user_id");
		$credit = $this->param("credit");
		$integral = $this->param("integral");
		return $this->done(SM::getUserService()->changeUser($user_id,$credit,$integral));
	}
	public function actionGetUserList() {
		$page = $this->param("page");
		$rows = $this->param("size");
		return $this->done(SM::getUserService()->getUserList(($page - 1) * $rows,$rows));
	}
    /**
     * 添加用户
     * @return string
     * @throws ForeseeableException
     * @throws Throwable
     */
    public function actionAdd()
    {
        $this->beginTransaction();

        $form = UserForm::initByParam();
        if (!$form->validate()) {
            throw new ForeseeableException($form->getFirstError());
        }
        SM::getUserService()->addUser($form, UserUtil::getUser());

        return $this->done(["status" => "成功"]);
    }

    /**
     * 修改用户
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionMod()
    {
        $this->beginTransaction();

        $form = UserForm::initByParam();
        if (!$form->validate()) {
            throw new ForeseeableException($form->getFirstError());
        }

        SM::getUserService()->modUser($form, UserUtil::getUser());

        return $this->done();
    }

    /**
     * VIP等级申请
     * @return string
     */
    public function actionVipApplyFor()
    {
        $userId = $this->param("userId");
        SM::getUserService()->vipApplyFor($userId);
        return $this->done();
    }

    /**
     * 修改银行信息
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionBank()
    {

        $form = UserForm::initByParam();
        if (!$form->validate()) {
            throw new ForeseeableException($form->getFirstError());
        }
        SM::getUserService()->adminChangeBank($form);
        return $this->done();
    }

    /**
     * 获取用户表单数据
     * @throws ForeseeableException
     */
    public function actionGetUserForm()
    {
        $userId = $this->param("userId");
        if (empty($userId)) {
            throw new ForeseeableException("请输入用户id");
        }
        $user = UserModel::initUser($userId);
        $userForm = new UserForm();
        $userForm->initByUser($user);
        return $this->done(["userForm" => $userForm]);
    }

    /**
     * 删除用户
     * @return string
     * @throws Throwable
     */
    public function actionDel()
    {
        $userId = $this->param("userId");

        if (empty($userId)) {
            throw new ForeseeableException("请传入需要删除的用户id");
        }
        $user = UserModel::initUser($userId);
        if ($user === null) {
            throw new ForeseeableException("用户不存在");
        }

        $this->beginTransaction();
        SM::getUserService()->delUser($user);

        return $this->done();
    }

    /**
     * 登录系统
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     */
    public function actionLogin()
    {
        $username = $this->param('username');
        $password = $this->param('password');

var_dump($username, $password);die;
        $user = SM::getUserService()->loginCheck($username, $password,true);

        // 设置登录信息
        $this->setSession(C::SESSION_KEY_USERNAME, $user->username);
        $user->login();

        return $this->done(["userId" => $user->userId]);
    }

    /**
     * 初始化管理员
     * @return string
     * @throws ForeseeableException
     * @throws Throwable
     */
    public function actionInitAdmin()
    {
        $this->beginTransaction();

        $username = $this->param("username");
        $password = $this->param("password");

        SM::getUserService()->initAdmin($username, $password);

        return $this->done();
    }

    /**
     * 退出登录
     */
    public function actionLogout()
    {
        if (UserUtil::hasLoginUser()) {
            $user = UserUtil::getUser();
            $user->logout();
            $user->getAccount()->session_id = "";
            $user->save();
        }
        $this->destroySession();
        return $this->done();
    }

    /**
     * 获取菜单
     * @return string
     */
    public function actionGetMenuTree()
    {
        $menuTree = SM::getUserService()->getMenuTree();
        return $this->done(["menuTree" => $menuTree]);
    }

    /**
     * 获取权限列表
     * @return string
     * @throws ForeseeableException
     */
    public function actionGetAuthModuleVoList()
    {
        $userId = $this->param("userId");
        if (empty($userId)) {
            throw new ForeseeableException("用户id不能为空");
        }
        $moduleVoList = SM::getAuthService()->getModuleVoList($userId);
        return $this->done(["moduleVoList" => $moduleVoList]);
    }

    /**
     * 修改用户权限
     * @return string
     * @throws ForeseeableException
     * @throws Throwable
     */
    public function actionModUserAuth()
    {
        $this->beginTransaction();

        $userId = (int)$this->param("userId");
        $moduleVoListData = $this->param("moduleVoList");
        $moduleVoListData = array('name' => 'admin');
        if (empty($userId)) {
            throw new ForeseeableException("用户id不能为空");
        }

        $moduleVoList = ModuleVo::initListByArr($moduleVoListData);

        SM::getAuthService()->modUserAuth($userId, $moduleVoList);

        return $this->done();
    }

    /**
     * 判断系统是否初始化了账户
     */
    public function actionIsInitAdmin()
    {
        $count = (int)UserAccount::find()->count();
        $isInitAdmin = $count !== 0;
        return $this->done(["isInitAdmin" => $isInitAdmin]);
    }

    /**
     * 获取用户数据
     */
    public function actionGetUserInfoVo()
    {
        $userModel = UserUtil::getUser();
        $userInfoVo = new InfoVo();
        $userInfoVo->initByUserModel($userModel);
        return $this->done(["userInfoVo" => $userInfoVo]);
    }

    /**
     * 获取用户表格数据
     * @throws SystemException
     */
    public function actionGetChildRowVoTable()
    {
        $upId = (int)$this->param("upId"); // 父级用户id
        $userType = (int)$this->param("userType");
        $isDirect = (int)$this->param("isDirect"); // 是否直属（直属只能查会员）
        $username = $this->param("username");

        if (empty($upId)) {
            $upId = UserUtil::getUserId();
        }
        $userModel = UserModel::initUser($upId);
        if ($isDirect === C::TRUE) {
            $userModelList = $userModel->getDirectMemberModelList();
        } else {
            if (empty($userType)) {
                throw new SystemException("获取用户类型不能为空");
            }
            $userModelList = $userModel->getChildUserModelList($userType, true, $username);
        }

        $rowVoList = [];
        foreach ($userModelList as $userModel) {
            $rowVo = new RowVo();
            $rowVo->initByUserModel($userModel);
            $rowVoList[] = $rowVo;
        }
        //排序
        $rowVoList = ArrayUtil::arraySort($rowVoList, 'isLogin', SORT_DESC, true);
        return $this->done([
            "rows" => $rowVoList,
            "count" => count($rowVoList),
        ]);
    }

    /**
     * 获取管理员账号
     * @return string
     * @throws SystemException
     */
    public function actionGetUserAdminList()
    {
        $userId = (int)$this->param("userId");
        $username = $this->param("username");
        $userType = UserUtil::getUserType();
        if ($userType !== C::USER_ADMIN){
            throw new SystemException("您不是管理员!");
        }

        $userModel = UserModel::initUser($userId);
        $userModelList = $userModel->getChildUserModelList($userType, true, $username);

        $rowVoList = [];
        foreach ($userModelList as $userModel) {
            $rowVo = new RowVo();
            $rowVo->initByUserModel($userModel);
            $rowVoList[] = $rowVo;
        }
        return $this->done([
            "rows" => $rowVoList,
            "count" => count($rowVoList),
        ]);
    }

    /**
     * 获取用户选项数据
     */
    public function actionGetUserAccountNameMap()
    {
        return $this->done(SM::getUserService()->getUserAccountNameMap(UserUtil::getUser()));
    }

    /**
     * 切换用户激活状态
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionModActive()
    {
        $userId = $this->param("userId");
        $active = $this->param("active");
        $active = ValueUtil::toBool($active);

        $user = UserModel::initUser($userId);
        if ($user === null) {
            throw new ForeseeableException("用户不存在");
        }
        $user->setActive($active);
        if (!$user->save()) {
            throw new SystemException($user->getFirstError());
        }

        return $this->done();
    }

    /**
     * 检测是否登录。用于后台请求，防止登出
     */
    public function actionCheckLogin() {
        return $this->done();
    }

    /**
     * 上传商品图片
     * @return string
     * @throws ForeseeableException
     */
    public function actionUpload(){
        return $this->done(SM::getUserService()->uploadImage());
    }

    /**
     * 编辑商品
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAddShop()
    {
        $id = $this->param("id");
        $name = $this->param("name");
        $credits = $this->param("credits");
        $bonusPoints = $this->param("bonus_points");
        $content = $this->param("content");
        $images = $this->param("images");

        if ($id){
            SM::getUserService()->updateShop($id,$name,$credits,$bonusPoints,$content,$images);
        }else{
            SM::getUserService()->addShop($name,$credits,$bonusPoints,$content,$images);
        }
        return $this->done();
    }

    /**
     * @return string
     * @throws ForeseeableException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteShop(){
        $id = $this->param("id");
        SM::getUserService()->deleteShop($id);
        return $this->done();
    }
    /**
     * 获取商品列表
     * @return string
     */
    public function actionGetShopList(){
        $page = $this->param("page", 1);
        $rows = $this->param("size", 10);
        return $this->done(SM::getUserService()->getShopList(($page - 1) * $rows, $rows));
    }
}
