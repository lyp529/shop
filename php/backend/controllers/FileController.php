<?php


namespace backend\controllers;


use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\services\SM;

/**
 * Class FileController 文件控制器
 * @package backend\controllers
 */
class FileController extends BaseBackendController {
    /**
     * 上传文件
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     */
    public function actionUpload() {
        if (YII_ENV_DEV) {
            // 开发环境下返回允许跨域的请求给客户端
            header("Access-Control-Allow-Origin: *");
        }

        $dirType = (int)$this->param("dirType");
        $name = $this->param("paramName", "file");
        $fileType = (int)$this->param("fileType", C::FILE_TYPE_OTHER);
        $maxByte = (int)$this->param("maxByte", C::_10MB);
        $bindId = (int)$this->param("bindId");
        $table = (int)$this->param("table");
        $bindType = (int)$this->param("bindType");

        $fileVo = SM::getFileService()->uploadFile($dirType, $name, $fileType, $maxByte, $bindId, $table, $bindType);

        return $this->done(["fileVo" => $fileVo]);
    }

    /**
     * 下载文件
     * TODO
     */
    public function actionDownload() {

    }
}