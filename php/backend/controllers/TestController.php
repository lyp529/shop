<?php


namespace backend\controllers;


use common\constants\C;
use common\constants\L;
use common\constants\P;
use common\models\db\Schedule;
use common\models\db\UserMember;
use common\models\UserModel;
use common\services\SM;
use common\utils\ApiUtil;
use common\services\ScheduleService;
use common\services\BetService;
use common\exceptions\SystemException;
use common\utils\game\LogUtil;
use common\utils\game\SscUtil;
use common\utils\game\SscWinUtil;
use common\models\db\EarnRank;


/**
 * Class TestController 测试控制器
 * @package code\controllers
 */
class TestController extends BaseBackendController
{
    /**
     * 测试方法
     * URL: http://127.0.0.1/ga/php/backend/web/index.php/test/test
     */
    public function actionTest() {

        //SM::getPayoutService()->autoPayout();
        //SM::getScheduleService()->openPersonalSchedule(C::KIND_SSC_BERLIN);

        //dv(L::getCashLabels(), 'L::getCashLabels()');
        //self::logUnitTest();
        //self::sscUnitTest();
        //SM::getUserService()->betBackDaily();
        //LogUtil::plog(L::PLOG_VIP_UP, [1, 'aaa']);
        //$schedule=Schedule::findOne(1626);
        //SM::getPayoutService()->payout($schedule);
        // $sccDataVo = ApiUtil::getSscDataVo();


        return "<hr>TestController.actionTest";
    }

    /**
     * 大转盘
     */
    public function actionIntegral() {
        SM::getIntegralService()->getLuckRunResult();
    }

    /**
     * 测试:取网时彩结果
     * @throws \Throwable
     */
    public function actionGetSscResult() {
        $sccDataVo = ApiUtil::getSscDataVo();
        SM::getScheduleService()->updateSSCResult($sccDataVo);
    }

    /**
     * 时时彩各种玩法单元测试
     * @return string
     */
    private function sscUnitTest() {

        var_dump(SscUtil::checkData(P::SC_FIVE_DWD, [[1, 2, 3], [4, 5]]));
        //dv(SscUtil::getGroups(P::SC_FIVE, [[1], [1, 2,], [3], [4], [5]]));
        //dv(SscUtil::getGroups(P::SC_FIVE_120, [[1, 2, 3, 4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FIVE_60, [[1, 2, 3], [4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FIVE_30, [[1, 2, 3], [4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FIVE_20, [[1, 2, 3], [4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FIVE_10, [[1, 2, 3], [4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FIVE_5, [[1, 2, 3], [4, 5, 6, 7]]));

        //dv(SscUtil::getGroups(P::SC_FOUR_Q4, [[1, 2], [4, 5], [5, 6], [7, 8]]));
        //dv(SscUtil::getGroups(P::SC_FOUR_H4, [[1, 2], [4, 5], [5, 6], [7, 8]]));
        //dv(SscUtil::getGroups(P::SC_FOUR_24, [[1, 2, 3, 4, 5]]));

        //dv(SscUtil::getGroups(P::SC_FOUR_12, [[1, 2, 3], [4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FOUR_6, [[1, 2, 3], [4, 5, 6, 7]]));
        //dv(SscUtil::getGroups(P::SC_FOUR_4, [[1, 2, 3], [4, 5, 6, 7]]));


        //dv(SscUtil::getGroups(P::SC_THREE_Q3, [[1, 2], [4, 5], [5, 6]]));
        //dv(SscUtil::getGroups(P::SC_THREE_Z3, [[1, 2], [4, 5], [5, 6]]));
        //dv(SscUtil::getGroups(P::SC_THREE_H3, [[1, 2], [4, 5], [5, 6]]));

        //dv(SscUtil::getGroups(P::SC_THREE_Q3Z6, [[1, 2, 3, 4, 5, 6]]));
        //dv(SscUtil::getGroups(P::SC_THREE_Z3Z6, [[1, 2, 3, 4, 5, 6]]));
        //dv(SscUtil::getGroups(P::SC_THREE_H3Z6, [[1, 2, 3, 4, 5, 6]]));


        //dv(SscUtil::getGroups(P::SC_THREE_Q3Z3, [[1, 2, 3, 4]]));
        //dv(SscUtil::getGroups(P::SC_THREE_Z3Z3, [[1, 2, 3, 4]]));
        //dv(SscUtil::getGroups(P::SC_THREE_H3Z3, [[1, 2, 3, 4]]));

        //dv(SscUtil::getGroups(P::SC_TWO_Q2, [[1, 2], [3, 4]]));
        //dv(SscUtil::getGroups(P::SC_TWO_H2, [[1, 2], [3, 4]]));

        //dv(SscUtil::getGroups(P::SC_TWO_Q2ZX, [[1, 2, 3, 4]]));
        //dv(SscUtil::getGroups(P::SC_TWO_H2ZX, [[1, 2, 3, 4]]));

        //dv(SscUtil::getGroups(P::SC_FIVE_DWD, [[1, 2], [3, 4], [5, 6], [7, 8], [1, 2, 3]]));

        //dv(SscUtil::getGroups(P::SC_BDD_Q31M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_Z31M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_H31M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_Q32M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_H32M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_4X1M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_4X2M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_5X2M, [[1,2,3,4]]));
//        dv(SscUtil::getGroups(P::SC_BDD_5X3M, [[1,2,3,4]]));

        #============================== 组数 ================================
        // echo(SscUtil::getGroupNum(P::SC_FIVE, [[1], [1, 2,], [3], [4], [5]]));
        //echo(SscUtil::getGroupNum(P::SC_FIVE_120, [[1, 2, 3, 4, 5, 6, 7]]));
        // echo(SscUtil::getGroupNum(P::SC_FIVE_60, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscUtil::getGroupNum(P::SC_FIVE_30, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscUtil::getGroupNum(P::SC_FIVE_20, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscUtil::getGroupNum(P::SC_FIVE_10, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscUtil::getGroupNum(P::SC_FIVE_5, [[1, 2, 3], [4, 5, 6, 7]]));
//
//        echo(SscUtil::getGroupNum(P::SC_FOUR_Q4, [[1, 2], [4, 5], [5, 6], [7, 8]]));
//        echo(SscUtil::getGroupNum(P::SC_FOUR_H4, [[1, 2], [4, 5], [5, 6], [7, 8]]));
//        echo(SscUtil::getGroupNum(P::SC_FOUR_24, [[1, 2, 3, 4, 5]]));
//
//        echo(SscUtil::getGroupNum(P::SC_FOUR_12, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscUtil::getGroupNum(P::SC_FOUR_6, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscUtil::getGroupNum(P::SC_FOUR_4, [[1, 2, 3], [4, 5, 6, 7]]));
//
//        echo(SscUtil::getGroupNum(P::SC_THREE_Q3, [[1, 2], [4, 5], [5, 6]]));
//        echo(SscUtil::getGroupNum(P::SC_THREE_Z3, [[1, 2], [4, 5], [5, 6]]));
//        echo(SscUtil::getGroupNum(P::SC_THREE_H3, [[1, 2], [4, 5], [5, 6]]));
//
//        echo(SscUtil::getGroupNum(P::SC_THREE_Q3Z6, [[1, 2, 3, 4, 5, 6]]));
//        echo(SscUtil::getGroupNum(P::SC_THREE_Z3Z6, [[1, 2, 3, 4, 5, 6]]));
//        echo(SscUtil::getGroupNum(P::SC_THREE_H3Z6, [[1, 2, 3, 4, 5, 6]]));
//
//
//        echo(SscUtil::getGroupNum(P::SC_THREE_Q3Z3, [[1, 2, 3, 4]]));
//        echo(SscUtil::getGroupNum(P::SC_THREE_Z3Z3, [[1, 2, 3, 4]]));
//        echo(SscUtil::getGroupNum(P::SC_THREE_H3Z3, [[1, 2, 3, 4]]));
//
//        echo(SscUtil::getGroupNum(P::SC_TWO_Q2, [[1, 2], [3, 4]]));
//        echo(SscUtil::getGroupNum(P::SC_TWO_H2, [[1, 2], [3, 4]]));
//
//        echo(SscUtil::getGroupNum(P::SC_TWO_Q2ZX, [[1, 2, 3, 4]]));
//        echo(SscUtil::getGroupNum(P::SC_TWO_H2ZX, [[1, 2, 3, 4]]));
//
//        echo(SscUtil::getGroupNum(P::SC_FIVE_DWD, [[1, 2], [3, 4], [5, 6], [7, 8], [1, 2, 3]]));
//
//        echo(SscUtil::getGroupNum(P::SC_BDD_Q31M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_Z31M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_H31M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_Q32M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_H32M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_4X1M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_4X2M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_5X2M, [[1,2,3,4]]));
//        echo(SscUtil::getGroupNum(P::SC_BDD_5X3M, [[1,2,3,4]]));
        #============================== 结果 ================================
        $result = [1, 2, 3, 4, 5];
        // echo(SscWinUtil::getGroupWin(P::SC_FIVE, [[1, 2], [1, 2, 3], [3, 2], [4], [5]], $result));
        //echo(SscWinUtil::getGroupWin(P::SC_FIVE_120, [[1, 2, 3, 4, 5, 6, 7]], $result));

        // echo(SscWinUtil::getGroupWin(P::SC_FIVE_60, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscWinUtil::getGroupWin(P::SC_FIVE_30, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscWinUtil::getGroupWin(P::SC_FIVE_20, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscWinUtil::getGroupWin(P::SC_FIVE_10, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscWinUtil::getGroupWin(P::SC_FIVE_5, [[1, 2, 3], [4, 5, 6, 7]]));
//
        //echo(SscWinUtil::getGroupWin(P::SC_FOUR_Q4, [[1, 2], [2, 5], [3, 6], [4, 8]], $result));
        //echo(SscWinUtil::getGroupWin(P::SC_FOUR_H4, [[1, 2], [3, 5], [4, 6], [5, 8]],$result));
//        echo(SscWinUtil::getGroupWin(P::SC_FOUR_24, [[1, 2, 3, 4, 5]], $result));
//
//        echo(SscWinUtil::getGroupWin(P::SC_FOUR_12, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscWinUtil::getGroupWin(P::SC_FOUR_6, [[1, 2, 3], [4, 5, 6, 7]]));
//        echo(SscWinUtil::getGroupWin(P::SC_FOUR_4, [[1, 2, 3], [4, 5, 6, 7]]));
//
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_Q3, [[1, 2], [2, 3], [3, 6]],$result));
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_Z3, [[3, 2], [4, 3], [4, 6]], $result));
        //       echo(SscWinUtil::getGroupWin(P::SC_THREE_H3, [[1, 3], [4, 5], [5, 6]], $result));
//
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_Q3Z6, [[1, 2, 3, 4, 5, 6]]));
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_Z3Z6, [[1, 2, 3, 4, 5, 6]]));
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_H3Z6, [[1, 2, 3, 4, 5, 6]]));
//
//
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_Q3Z3, [[1, 2, 3, 4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_Z3Z3, [[1, 2, 3, 4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_THREE_H3Z3, [[1, 2, 3, 4]]));
//
//        echo(SscWinUtil::getGroupWin(P::SC_TWO_Q2, [[1, 2], [3, 4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_TWO_H2, [[1, 2], [3, 4]]));
//
//        echo(SscWinUtil::getGroupWin(P::SC_TWO_Q2ZX, [[1, 2, 3, 4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_TWO_H2ZX, [[1, 2, 3, 4]]));
//
//        echo(SscWinUtil::getGroupWin(P::SC_FIVE_DWD, [[1, 2], [2, 4], [3, 6], [4, 8], [5, 2, 3]], $result));
//
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_Q31M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_Z31M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_H31M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_Q32M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_H32M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_4X1M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_4X2M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_5X2M, [[1,2,3,4]]));
//        echo(SscWinUtil::getGroupWin(P::SC_BDD_5X3M, [[1,2,3,4]]));

    }


    public function actionVo() {
        return $this->done(["a" => "测试成功"]);
    }

    /**
     * 写入重庆时时彩测试数据
     * @throws SystemException
     */
    private function actionwriteCqsscScheduleTestDate() {
        $scheduleService = SM::getScheduleService();
        $kind = C::KIND_SSC_CHONGQING;
        $statusOpen = C::SCHEDULE_STATUS_OPEN;
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 09:00:00", "2019092501", $statusOpen, $this->getRandomNums(), "2019-09-25 09:10:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 09:30:00", "2019092502", $statusOpen, $this->getRandomNums(), "2019-09-25 09:40:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 10:00:00", "2019092503", $statusOpen, $this->getRandomNums(), "2019-09-25 10:10:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 10:30:00", "2019092504", $statusOpen, $this->getRandomNums(), "2019-09-25 10:40:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 11:00:00", "2019092505", $statusOpen, $this->getRandomNums(), "2019-09-25 11:10:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 11:30:00", "2019092506", $statusOpen, $this->getRandomNums(), "2019-09-25 11:40:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 12:00:00", "2019092507", $statusOpen, $this->getRandomNums(), "2019-09-25 12:10:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 12:30:00", "2019092508", $statusOpen, $this->getRandomNums(), "2019-09-27 12:40:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-25 13:00:00", "2019092509", $statusOpen, $this->getRandomNums(), "2019-09-27 13:10:00");
        $scheduleService->save($kind, "重庆时时彩", "2019-09-26 09:30:00", "2019092610", $statusOpen, $this->getRandomNums(), "2019-09-27 09:40:00");
    }

    /**
     * 订单支付状态查询
     */
    public function actionPayQuery() {
        $isPay = SM::getApiService()->payQuery("DE5D9D52CAE71A5");
        return $isPay ? "pay" : "not pay";
    }

    /**
     * 曼谷时时彩测试
     * @throws SystemException
     */
    public function actionBangkok() {
        $kind = C::KIND_SSC_BERLIN;
        SM::getScheduleService()->generatePersonSchedule($kind);
        SM::getScheduleService()->openPersonalSchedule($kind);
        SM::getPayoutService()->autoPayout();
    }


    /**
     * 初始化盈利排行榜
     */
    public function initializeEarnRank() {
        //生成1000条数据
        for ($i = 0; $i < 1001; ++$i) {
            $this->generatedDataSave();
        }
    }

    /**
     * 生成数据保存
     * @param null $name
     */
    public function generatedDataSave($name = null) {
        //生成中英文名的概率
        $name = ($name == null) ? (mt_rand(1, 30) > 10 ? $this->chineseNames() : $this->englishNames()) : $name;

        // 11种玩法
        $method = [P::SC_FIVE, P::SC_FOUR_Q4, P::SC_FIVE_5, P::SC_FIVE_10, P::SC_FIVE_20, P::SC_FIVE_30, P::SC_FIVE_60, P::SC_FIVE_120, P::SC_THREE_Q3, P::SC_THREE_Q3Z3, P::SC_THREE_Q3Z6];
        $playingMethod = $method[array_rand($method)];

        //随机下注
        $betList = [];
        for ($i = 0; $i <= 500; ++$i) {
            if ($i > 0 && $i % 2 === 0) {
                $betList[] = $i;
            }
        }
        $bet = $betList[array_rand($betList)];

        $sscWinUtil = new SscWinUtil();
        $amount = $sscWinUtil->getOdds($playingMethod) * $bet;

        $earnRank = new EarnRank();
        $earnRank->username = $name;
        $earnRank->er_type = C::KIND_SSC_BANGKOK;  //曼谷时时彩
        $earnRank->amount = (double)$amount;
        $earnRank->save();

        //重复中奖
        mt_rand(1, C::EARN_RANK_REPETITION) == 1 && $this->generatedDataSave($name);
    }

    /**
     * 生成中文名
     * @return string
     */
    private function chineseNames() {
        $lastNameList = [
            '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许', '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '荆',
            '喻', '柏', '水', '窦', '章', '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '任', '袁', '柳', '鲍', '史', '唐', '费', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗',
            '毕', '郝', '安', '常', '傅', '卞', '齐', '元', '顾', '孟', '平', '黄', '穆', '萧', '尹', '姚', '邵', '湛', '汪', '祁', '毛', '狄', '米', '伏', '成', '戴', '谈', '宋', '茅', '庞', '熊', '纪', '舒', '屈', '项', '祝',
            '董', '梁', '杜', '阮', '蓝', '闵', '季', '贾', '路', '娄', '江', '童', '颜', '郭', '梅', '盛', '林', '钟', '徐', '邱', '骆', '高', '夏', '蔡', '田', '樊', '胡', '凌', '霍', '虞', '万', '支', '柯', '管', '卢', '莫',
            '柯', '房', '裘', '缪', '解', '应', '宗', '丁', '宣', '邓', '单', '杭', '洪', '包', '诸', '左', '石', '崔', '吉', '龚', '程', '嵇', '邢', '裴', '陆', '荣', '翁', '荀', '于', '惠', '甄', '曲', '封', '储', '仲', '伊',
            '宁', '仇', '甘', '武', '符', '刘', '景', '詹', '龙', '叶', '幸', '司', '黎', '溥', '印', '怀', '蒲', '邰', '从', '索', '赖', '卓', '屠', '池', '乔', '胥', '闻', '莘', '党', '翟', '谭', '贡', '劳', '逄', '姬', '申',
            '扶', '堵', '冉', '宰', '雍', '桑', '寿', '通', '燕', '浦', '尚', '农', '温', '别', '庄', '晏', '柴', '瞿', '阎', '连', '习', '容', '向', '古', '易', '廖', '庾', '终', '步', '都', '耿', '满', '弘', '匡', '国', '文',
            '寇', '广', '禄', '阙', '东', '欧', '利', '师', '巩', '聂', '关'];

        $firstNameList = [
            '伟', '刚', '勇', '毅', '俊', '峰', '强', '军', '平', '保', '东', '文', '辉', '力', '明', '永', '健', '世', '广', '志', '义', '兴', '良', '海', '山', '仁', '波', '宁', '贵', '福', '生', '龙', '元', '紫琪', '子乔',
            '全', '伟文', '志勇', '文强', '晓君', '晓军', '文辉', '永辉', '广坚', '志明', '依萍', '义良', '文海', '国强', '文祥', '文武', '文彪', '英秀', '春兰', '荣辉', '春颖', '文雅', '秋雅', '裕华', '玉华', '建国', '淑华',
            '翠花', '子熙', '子欣', '子晴', '恣情', '云华', '国', '胜', '学', '祥', '才', '发', '武', '新', '利', '清', '飞', '彬', '富', '顺', '信', '子', '杰', '涛', '昌', '成', '康', '星', '光', '天', '达', '安', '岩', '中',
            '茂', '进', '林', '有', '坚', '和', '彪', '博', '诚', '欧文', '文邹', '文燕', '文艳', '佳音', '文卓', '于芳', '宇文', '家伟', '嘉雯', '嘉伟', '文忠', '伊伊', '萱萱', '问问', '安安', '楠楠', '南南', '凤兰', '玉素',
            '秀政', '文珍', '海燕', '叶叶', '先', '敬', '震', '振', '壮', '会', '思', '群', '豪', '心', '邦', '承', '乐', '绍', '功', '松', '善', '厚', '庆', '磊', '民', '友', '裕', '河', '哲', '江', '超', '浩', '亮', '政',
            '谦', '亨', '奇', '固', '之', '轮', '翰', '晴雯', '晴雯', '玄海', '武广', '梓豪', '华良', '华亮', '天宇', '冬梅', '学海', '午光', '小燕', '小蕊', '小瑞', '子峰', '山川', '礼仪', '立民', '新文', '江丽', '建荣', '魏毅',
            '白茹', '宏建', '洪建', '田野', '辉红', '辰生', '绍旭', '余生', '墨然', '慧丽', '浩然', '昊然', '伶俐', '伟杰', '诗文', '莉莉', '淑萍', '丽颖', '嘉城', '林颖', '金泽', '士斌', '世安', '思禅', '斯文', '思文', '司文',
            '思慧', '斯慧', '卓豫', '丹丹', '朗', '伯', '宏', '言', '若', '鸣', '朋', '斌', '梁', '栋', '维', '启', '克', '伦', '翔', '旭', '鹏', '泽', '晨', '辰', '士', '以', '建', '家', '致', '树', '炎', '德', '行', '时',
            '泰', '盛', '雄', '琛', '钧', '冠', '策', '腾', '楠', '榕', '风', '航', '弘', '秀', '娟', '英', '华', '慧', '巧', '美', '娜', '静', '淑', '惠', '珠', '翠', '雅', '芝', '玉', '萍', '红', '娥', '玲', '芬', '芳',
            '燕', '彩', '春', '菊', '兰', '凤', '洁', '梅', '琳', '素', '云', '莲', '真', '环', '雪', '荣', '爱', '妹', '霞', '香', '月', '莺', '媛', '艳', '瑞', '凡', '佳', '嘉', '琼', '勤', '珍', '贞', '莉', '桂', '娣',
            '叶', '璧', '璐', '娅', '琦', '晶', '妍', '茜', '秋', '珊', '莎', '锦', '黛', '青', '倩', '婷', '姣', '婉', '娴', '瑾', '颖', '露', '瑶', '怡', '婵', '雁', '蓓', '纨', '仪', '荷', '丹', '蓉', '眉', '君', '琴', '蕊',
            '薇', '菁', '梦', '岚', '苑', '婕', '馨', '瑗', '琰', '韵', '融', '园', '诗琪', '琪琪', '琦琦', '易杰', '国立', '佳颖', '佳莹', '卫东', '魏东', '萱萱', '轩轩', '紫萱', '子萱', '萍兰', '姗姗', '珊珊', '建华', '健华',
            '黎明', '魏俊', '海斌', '彬彬', '文敏', '思敏', '斯敏', '才俊', '亚辉', '国韵', '建青', '亚坤', '雅文', '亚文', '荣廷', '向明', '光荣', '光永', '余枫', '宇峰', '正荣', '正云', '宇轩', '雨轩', '雨萱', '玉轩', '正蓉',
            '俞斌', '温雅', '文娅', '璐璐', '小小', '晓晓', '肖肖', '思聪', '斯聪', '露露', '盛文', '胜文', '志涛', '志伟', '佳琪', '嘉琪', '文志', '彭玉', '彭宇', '鹏宇', '魏娟', '席磊', '枫桥', '风桥', '凤娇', '乔乔', '娇娇',
            '姣姣', '莎莎', '庆国', '海伦', '百合', '海峰', '海凤', '海涛', '文丽', '文莉', '科灵', '文玲', '思思', '斯斯', '龙龙', '品如', '品正', '志敏', '智敏', '毅文', '国珍', '苗苗', '淼淼', '金斌', '景斌', '希庾', '艺', '咏',
            '卿', '聪', '澜', '纯', '毓', '悦', '昭', '冰', '爽', '琬', '茗', '羽', '希', '欣', '飘', '育', '滢', '馥', '筠', '柔', '竹', '霭', '凝', '晓', '欢', '霄', '枫', '芸', '菲', '寒', '伊', '亚', '宜', '可', '姬',
            '可可', '科科', '冰冰', '兵兵', '余文', '文羽', '欣欣', '翠儿', '翠兰', '翠花', '春花', '秋华', '秋花', '文举', '文菊', '小昭', '韦伟', '尤文', '有文', '仲文', '润钊', '水清', '喜娜', '舒', '影', '荔', '枝', '丽',
            '阳', '妮', '宝', '贝', '初', '程', '梵', '罡', '恒', '鸿', '桦', '骅', '剑', '娇', '纪', '宽', '苛', '灵', '玛', '媚', '琪', '晴', '容', '睿', '烁', '堂', '唯', '威', '韦', '雯', '苇', '萱', '阅', '彦', '宇',
            '雨', '洋', '忠', '宗', '曼', '紫', '逸', '贤', '蝶', '菡', '绿', '蓝', '儿', '翠', '烟'];

        return $lastNameList[array_rand($lastNameList)] . $firstNameList[array_rand($firstNameList)];
    }

    /**
     * 生成英文名
     * @return string
     */
    private function englishNames() {
        $femaleFirstNameArr = [
            "Mary", "Patricia", "Linda", "Barbara", "Elizabeth", "Jennifer", "Maria", "Susan", "Margaret", "Dorothy",
            "Lisa", "Nancy", "Karen", "Betty", "Helen", "Sandra", "Donna", "Carol", "Ruth", "Sharon", "Michelle",
            "Laura", "Sarah", "Kimberly", "Deborah", "Jessica", "Shirley", "Cynthia", "Angela", "Melissa", "Brenda",
            "Amy", "Anna", "Rebecca", "Virginia", "Kathleen", "Pamela", "Martha", "Debra", "Amanda", "Stephanie",
            "Carolyn", "Christine", "Marie", "Janet", "Catherine", "Frances", "Ann", "Joyce", "Diane", "Alice",
            "Julie", "Heather", "Teresa", "Doris", "Gloria", "Evelyn", "Jean", "Cheryl", "Mildred", "Katherine", "Joan",
            "Ashley", "Judith", "Rose", "Janice", "Kelly", "Nicole", "Judy", "Christina", "Kathy", "Theresa", "Beverly",
            "Denise", "Tammy", "Irene", "Jane", "Lori", "Rachel", "Marilyn", "Andrea", "Kathryn", "Louise", "Sara",
            "Anne", "Jacqueline", "Wanda", "Bonnie", "Julia", "Ruby", "Lois", "Tina", "Phyllis", "Norma", "Paula",
            "Diana", "Annie", "Lillian", "Emily", "Robin", "Peggy", "Crystal", "Gladys", "Rita", "Dawn", "Connie",
            "Florence", "Tracy", "Edna", "Tiffany", "Carmen", "Rosa", "Cindy", "Grace", "Wendy", "Victoria", "Edith",
            "Kim", "Sherry", "Sylvia", "Josephine", "Thelma", "Shannon", "Sheila", "Ethel", "Ellen", "Elaine",
            "Marjorie", "Carrie", "Charlotte", "Monica", "Esther", "Pauline", "Emma", "Juanita", "Anita", "Rhonda",
            "Hazel", "Amber", "Eva", "Debbie", "April", "Leslie", "Clara", "Lucille", "Jamie", "Joanne", "Eleanor",
            "Valerie", "Danielle", "Megan", "Alicia", "Suzanne", "Michele", "Gail", "Bertha", "Darlene", "Veronica",
            "Jill", "Erin", "Geraldine", "Lauren", "Cathy", "Joann", "Lorraine", "Lynn", "Sally", "Regina", "Erica",
            "Beatrice", "Dolores", "Bernice", "Audrey", "Yvonne", "Annette", "June", "Samantha", "Marion", "Dana",
            "Stacy", "Ana", "Renee", "Ida", "Vivian", "Roberta", "Holly", "Brittany", "Melanie", "Loretta", "Yolanda",
            "Jeanette", "Laurie", "Katie", "Kristen", "Vanessa", "Alma", "Sue", "Elsie", "Beth", "Jeanne"
        ];

        $maleFirstNameArr = [
            "James", "John", "Robert", "Michael", "William", "David", "Richard", "Charles", "Joseph", "Thomas",
            "Christopher", "Daniel", "Paul", "Mark", "Donald", "George", "Kenneth", "Steven", "Edward", "Brian",
            "Ronald", "Anthony", "Kevin", "Jason", "Matthew", "Gary", "Timothy", "Jose", "Larry", "Jeffrey", "Frank",
            "Scott", "Eric", "Stephen", "Andrew", "Raymond", "Gregory", "Joshua", "Jerry", "Dennis", "Walter",
            "Patrick", "Peter", "Harold", "Douglas", "Henry", "Carl", "Arthur", "Ryan", "Roger", "Joe", "Juan",
            "Jack", "Albert", "Jonathan", "Justin", "Terry", "Gerald", "Keith", "Samuel", "Willie", "Ralph", "Lawrence",
            "Nicholas", "Roy", "Benjamin", "Bruce", "Brandon", "Adam", "Harry", "Fred", "Wayne", "Billy", "Steve",
            "Louis", "Jeremy", "Aaron", "Randy", "Howard", "Eugene", "Carlos", "Russell", "Bobby", "Victor", "Martin",
            "Ernest", "Phillip", "Todd", "Jesse", "Craig", "Alan", "Shawn", "Clarence", "Sean", "Philip", "Chris",
            "Johnny", "Earl", "Jimmy", "Antonio", "Danny", "Bryan", "Tony", "Luis", "Mike", "Stanley", "Leonard",
            "Nathan", "Dale", "Manuel", "Rodney", "Curtis", "Norman", "Allen", "Marvin", "Vincent", "Glenn", "Jeffery",
            "Travis", "Jeff", "Chad", "Jacob", "Lee", "Melvin", "Alfred", "Kyle", "Francis", "Bradley", "Jesus",
            "Herbert", "Frederick", "Ray", "Joel", "Edwin", "Don", "Eddie", "Ricky", "Troy", "Randall", "Barry",
            "Alexander", "Bernard", "Mario", "Leroy", "Francisco", "Marcus", "Micheal", "Theodore", "Clifford",
            "Miguel", "Oscar", "Jay", "Jim", "Tom", "Calvin", "Alex", "Jon", "Ronnie", "Bill", "Lloyd", "Tommy", "Leon",
            "Derek", "Warren", "Darrell", "Jerome", "Floyd", "Leo", "Alvin", "Tim", "Wesley", "Gordon", "Dean", "Greg",
            "Jorge", "Dustin", "Pedro", "Derrick", "Dan", "Lewis", "Zachary", "Corey", "Herman", "Maurice", "Vernon",
            "Roberto", "Clyde", "Glen", "Hector", "Shane", "Ricardo", "Sam", "Rick", "Lester", "Brent", "Ramon",
            "Charlie", "Tyler", "Gilbert", "Gene"
        ];

        $lastNameArr = [
            "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson",
            "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Garcia", "Martinez", "Robinson", "Clark",
            "Rodriguez", "Lewis", "Lee", "Walker", "Hall", "Allen", "Young", "Hernandez", "King", "Wright", "Lopez",
            "Hill", "Scott", "Green", "Adams", "Baker", "Gonzalez", "Nelson", "Carter", "Mitchell", "Perez", "Roberts",
            "Turner", "Phillips", "Campbell", "Parker", "Evans", "Edwards", "Collins", "Stewart", "Sanchez", "Morris",
            "Rogers", "Reed", "Cook", "Morgan", "Bell", "Murphy", "Bailey", "Rivera", "Cooper", "Richardson", "Cox",
            "Howard", "Ward", "Torres", "Peterson", "Gray", "Ramirez", "James", "Watson", "Brooks", "Kelly", "Sanders",
            "Price", "Bennett", "Wood", "Barnes", "Ross", "Henderson", "Coleman", "Jenkins", "Perry", "Powell", "Long",
            "Patterson", "Hughes", "Flores", "Washington", "Butler", "Simmons", "Foster", "Gonzales", "Bryant",
            "Alexander", "Russell", "Griffin", "Diaz", "Hayes", "Myers", "Ford", "Hamilton", "Graham", "Sullivan", "Wallace",
            "Woods", "Cole", "West", "Jordan", "Owens", "Reynolds", "Fisher", "Ellis", "Harrison", "Gibson", "Mcdonald",
            "Cruz", "Marshall", "Ortiz", "Gomez", "Murray", "Freeman", "Wells", "Webb", "Simpson", "Stevens", "Tucker",
            "Porter", "Hunter", "Hicks", "Crawford", "Henry", "Boyd", "Mason", "Morales", "Kennedy", "Warren", "Dixon",
            "Ramos", "Reyes", "Burns", "Gordon", "Shaw", "Holmes", "Rice", "Robertson", "Hunt", "Black", "Daniels",
            "Palmer", "Mills", "Nichols", "Grant", "Knight", "Ferguson", "Rose", "Stone", "Hawkins", "Dunn", "Perkins",
            "Hudson", "Spencer", "Gardner", "Stephens", "Payne", "Pierce", "Berry", "Matthews", "Arnold", "Wagner",
            "Willis", "Ray", "Watkins", "Olson", "Carroll", "Duncan", "Snyder", "Hart", "Cunningham", "Bradley", "Lane",
            "Andrews", "Ruiz", "Harper", "Fox", "Riley", "Armstrong", "Carpenter", "Weaver", "Greene", "Lawrence",
            "Elliott", "Chavez", "Sims", "Austin", "Peters", "Kelley", "Franklin", "Lawson"
        ];
        $maleFemale = rand(0, 1);//随机男女

        $firstName = $maleFemale ? $femaleFirstNameArr[array_rand($femaleFirstNameArr)] : $maleFirstNameArr[array_rand($maleFirstNameArr)];

        return $firstName . " " . $lastNameArr[array_rand($lastNameArr)];
    }
}
