<?php

namespace backend\controllers;

use common\constants\L;
use common\services\SM;


/**
 * Class LogController  日志控制器
 * @package backend\LogController
 */
class LogController extends BaseBackendController
{
    /**
     * 获取帐变记录列表
     * @return string
     */
    public function actionGetCashLog()
    {
        $page = $this->param("page", 1);
        $size = $this->param("size", 10);
        $endTime = $this->param("endTime");
        $startTime = $this->param("startTime");
        $username = $this->param("username");
        $recordNo = $this->param("recordNo");
        $amount = $this->param("amount");
        $queryPlus = [];
        if ($username) {
            $queryPlus['username'] = $username;
        }
        if ($recordNo) {
            $queryPlus['record_no'] = $recordNo;
        }
        $list = SM::getLogService()->getCashRecordList($queryPlus, $startTime, $endTime, ($page - 1) * $size, $size, $amount);
        return $this->done($list);
    }


    /**
     * 获取盈亏统计列表-(旧)
     * @return string
     */
    public function actionGetReportSummary()
    {
        $page = $this->param("page", 1);
        $size = $this->param("size", 10);
        $endTime = $this->param("endTime");
        $startTime = $this->param("startTime");
        $username = $this->param("username");
        $queryPlus = [];
        if ($username) {
            $queryPlus['username'] = $username;
        }
        if ($startTime) {
            $startTime .= ' 00:00:00';
        }
        if ($endTime) {
            $endTime .= ' 23:59:59';
        }

        $list = SM::getLogService()->getReportSummary($queryPlus, $startTime, $endTime, ($page - 1) * $size, $size);
        return $this->done($list);
    }

    /**
     * 操作日志
     * @return string
     */
    public function actionGetOperateLog()
    {
        $page = $this->param("page", 1);
        $size = $this->param("size", 10);
        $endTime = $this->param("endTime");
        $startTime = $this->param("startTime");
        $username = $this->param("user_name");
        return $this->done(SM::getLogService()->getOperateLog($startTime, $endTime, ($page - 1) * $size, $size, $username));
    }

    /**
     * 登录日志
     * @return string
     */
    public function actionGetLoginLog()
    {
        $page = $this->param("page", 1);
        $size = $this->param("size", 10);
        $endTime = $this->param("endTime");
        $startTime = $this->param("startTime");
        $username = $this->param("username");
        return $this->done(SM::getLogService()->getLoginLog($startTime, $endTime, ($page - 1) * $size, $size, $username));
    }

}
