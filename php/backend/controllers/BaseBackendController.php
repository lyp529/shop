<?php


namespace backend\controllers;


use common\base\BaseWebController;
use common\constants\C;
//use common\services\SM;
use common\utils\game\UserUtil;
use common\utils\TimeUtil;
use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;

/**
 * Class BaseController 后台控制器基类
 * @package backend\controllers
 */
class BaseBackendController extends BaseWebController {
    /**
     * @param Action $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action) {
        $uniqueId = $action->getUniqueId();
        $user = UserUtil::getUser();

        // 判断是否是不用登录也可访问的路由
        if (!$this->isSkipLoginCheckRoute($uniqueId) && !$user->isLoginValid()) {
            $responseCode = C::RESPONSE_CODE_NOT_LOGIN;
            $message = C::getReturnCodeLabel($responseCode);

            // 当前有登录，但是登录无效。原因：被踢出
            if ($user->isLogin()) {
                $message = "您的账号在其他地方被登录";
            }
            Yii::$app->response->data = $this->response($responseCode, $message); // 设置返回数据
            return false;
        } else if ($user->isLogin()) {
            $user->resetSessionDeadTime(300);
        }

        return parent::beforeAction($action);
    }

    /**
     * 是否跳过登录检查的路由
     * @author 190724 by guorz
     *  isset() 查找速度要比 in_array() 查找速度快。所以使用路由作为数组的key字段
     *  isset()     通过二分法查找，减少计算次数
     *  in_array()  通过遍历查找，需要从头开始遍历
     *
     * @param string $route 路由。如：user/login
     * @return bool
     */
    private function isSkipLoginCheckRoute($route) {
        $skipRoute = [
            "base-backend/error" => true,
            "base-backend/home" => true,
            "user/init-admin" => true,
            "user/is-init-admin" => true,
            "user/login" => true,
            "user/logout" => true,
            "user/is-login" => true,
            "file/upload" => true,
            "file/download" => true,
            "test/test" => true,
        ];

        return isset($skipRoute[$route]);
    }

    /**
     * @return string
     */
    public function actionError() {
        return parent::actionError();
    }
}
