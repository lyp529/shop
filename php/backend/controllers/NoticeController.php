<?php

namespace backend\controllers;

use Throwable;
use common\services\SM;
use common\exceptions\ForeseeableException;



/**
 * Class NoticeController 公告控制器
 * @package backend\BaseBackendController
 */
class NoticeController extends BaseBackendController {

    /**
     * 添加公告
     * @return string
     * @throws \common\exceptions\SystemException
     */
    public function actionAddNotice() {
        $title = $this->param("title");
        $content = $this->param("content");
        $noticeType = $this->param("notice_type");
        SM::getNoticeService()->addNotice($title,$content,$noticeType);
        return $this->done();
    }

    /**
     * 编辑公告
     * @return string
     * @throws ForeseeableException
     * @throws Throwable
     * @throws \common\exceptions\SystemException
     * @throws \yii\db\StaleObjectException
     */
    public function actionEditNotice() {
        $id = $this->param("id");
        $title = $this->param("title");
        $content = $this->param("content");
        $noticeType = (int)$this->param("notice_type");
        SM::getNoticeService()->editNotice($id,$title,$content,$noticeType);
        return $this->done();
    }

    /**
     * 获取公告列表
     * @return string
     */
    public function actionGetNoticeList(){
        $page = $this->param("page", 1);
        $rows = $this->param("size", 10);
        return $this->done(SM::getNoticeService()->getNoticeList(($page - 1) * $rows, $rows));
    }

    /**
     * 删除公告
     * @return string
     * @throws ForeseeableException
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelNotice(){
        $noticeId = $this->param("noticeId");
        SM::getNoticeService()->delNotice($noticeId);
        return $this->done();
    }
}
