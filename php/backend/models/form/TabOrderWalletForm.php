<?php


namespace backend\models\form;


use common\base\BaseForm;

/**
 * Class TabOrderWalletForm 钱包订单（充值、提现）表单的模型
 * @package models\form
 */
class TabOrderWalletForm extends BaseForm {
    /**
     * @var int 订单ID
     */
    public $id;
    /**
     * @var int 用户ID
     */
    public $userId;
    /**
     * @var int 操作人员ID
     */
    public $adminId;
    /**
     * @var string 订单编号
     */
    public $orderNumber;
    /**
     * @var int 订单类型（1:存款,2:取款）
     */
    public $orderType;
    /**
     * @var int 订单状态（1:待支付,2:已支付,3:取消支付,4:支付失败）
     */
    public $orderState;
    /**
     * @var string 下单时间
     */
    public $orderTime;
    /**
     * @var string 完成支付时间
     */
    public $finishTime;
    /**
     * @var string 订单金额
     */
    public $amount;
    /**
     * @var int 支付通道
     */
    public $payType;

    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [['userId','orderNumber','orderType', 'orderState', 'orderTime','amount','payType'], 'required'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            "id" => "ID",
            "userId" => "用户",
            "amount" => "订单金额",
            "payType" => "支付通道",
            "adminId" => "操作人员",
            "orderType" => "订单类型",
            "orderTime" => "下单时间",
            "orderState" => "订单状态",
            "orderNumber" => "订单编号"
        ];
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->id = (int)$this->id;
        $this->userId = (int)$this->userId;
        $this->adminId = (int)$this->adminId;
        $this->payType = (int)$this->payType;
        $this->orderType = (int)$this->orderType;
        $this->orderState = (int)$this->orderState;
    }
}
