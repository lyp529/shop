<?php


namespace backend\models\form;


use common\base\BaseForm;
use common\models\UserModel;

/**
 * Class UserForm 新增、修改用户表单的模型
 * @package models\form
 */
class UserForm extends BaseForm {
    /**
     * @var int 用户id
     */
    public $id;
    /**
     * @var string 推荐人
     */
    public $referrer;
    /**
     * @var int 上线类型
     */
    public $upType;
    /**
     * @var int 用户类型
     */
    public $userType;
    /**
     * @var string 登录账号
     */
    public $username;
    /**
     * @var string 登录密码 sha512 后的值
     */
    public $password;
    /**
     * @var string 确认密码 sha512 后的值
     */
    public $ensurePassword;
    /**
     * @var string 用户名
     */
    public $name;
    /**
     * @var int 上级id
     */
    public $upId;
    /**
     * @var string 备注
     */
    public $remark;

    /**
     * @var int 信用额度
     */
    public $credit;
    /**
     * @var int 是否委员会
     */
    public $commissionPlan;
    /**
     * @var int 是否可看报表
     */
    public $companyReport;
    /**
     * @var int 是否包底
     */
    public $includeBelow;
    /**
     * @var double 占成上限
     */
    public $fractionMax;
    /**
     * @var double 大股东占成
     */
    public $fractionSuperHolder;
    /**
     * @var double 股东占成
     */
    public $fractionHolder;
    /**
     * @var double 总代理占成
     */
    public $fractionVendor;
    /**
     * @var int  加/扣款 radio
     */
    public $radioCredit;
    /**
     * @var int  修改的信用额度
     */
    public $changeCredit;
    /**
     * @var string 银行类型
     */
    public $bankName;
    /**
     * @var string 银行账号
     */
    public $bankAccount;
    /**
     * @var string 银行类型
     */
    public $bankUsername;
    /**
     * @var string 绑定邮箱
     */
    public $bankEmail;


    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [["username", "name", "userType"], "required"],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            "id" => "ID",
            "userType" => "用户类型",
            "username" => "登录账号",
            "password" => "登录密码",
            "name" => "用户名"
        ];
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->id = (int)$this->id;
        $this->userType = (int)$this->userType;
        $this->upType = (int)$this->upType;
        $this->upId = (int)$this->upId;
        $this->credit = (int)$this->credit;
        $this->commissionPlan = (int)$this->commissionPlan;
        $this->companyReport = (int)$this->companyReport;
        $this->includeBelow = (int)$this->includeBelow;
        $this->fractionMax = (double)$this->fractionMax;
        $this->fractionSuperHolder = (double)$this->fractionSuperHolder;
        $this->fractionHolder = (double)$this->fractionHolder;
        $this->fractionVendor = (double)$this->fractionVendor;
        $this->radioCredit = (int)$this->radioCredit;
        $this->changeCredit = (int)$this->changeCredit;
    }

    /**
     * @param UserModel $user
     */
    public function initByUser(UserModel $user) {
        $account = $user->getAccount();
        $this->id = $account->id;
        $this->userType = $account->user_type;
        $this->username = $account->username;
        $this->password = "";
        $this->name = $account->name;
        $this->upId = $user->getUpLineId();
        $this->remark = $account->remark;

        if ($user->isSuperHolder()) {
            $superHolder = $user->getSuperHolder();
            $this->credit = (int)$superHolder->credit;
            $this->commissionPlan = (int)$superHolder->commission_plan;
            $this->companyReport = (int)$superHolder->company_report;
            $this->includeBelow = (int)$superHolder->include_below;
        } else if ($user->isHolder()) {
            $holder = $user->getHolder();
            $this->credit = (int)$holder->credit;
            $this->fractionMax = (double)$holder->fraction_max;
            $this->fractionSuperHolder = (double)$holder->fraction_super_holder;
        } else if ($user->isVendor()) {
            $vendor = $user->getVendor();
            $this->credit = (int)$vendor->credit;
            $this->fractionMax = (double)$vendor->fraction_max;
            $this->fractionHolder = (double)$vendor->fraction_holder;
        } else if ($user->isAgent()) {
            $agent = $user->getAgent();
            $this->credit = (int)$agent->credit;
            $this->fractionMax = (double)$agent->fration_max;
            $this->fractionVendor = (double)$agent->fraction_vendor;
        } else if ($user->isMember()) {
            $member = $user->getMember();
            $this->upType = (int)$member->direct_user_type;
            $this->credit = (int)$member->credit;
            $this->bankName = $member->bank_name;
            $this->bankAccount = $member->bank_account;
            $this->bankUsername = $member->bank_username;
            $this->bankEmail = $member->bank_email;
        }
    }
}
