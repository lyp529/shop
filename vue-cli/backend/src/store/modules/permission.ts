import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators'
import {RouteConfig} from 'vue-router'
import {UserStore} from '@/store/modules/UserStore'
import {asyncRoutes, constantRoutes, agentRoutes} from '@/router'
import store from '@/store'

const hasPermission = (roles: string[], route: RouteConfig) => {
    if (route.meta && route.meta.roles) {
        return roles.some(role => route.meta.roles.includes(role))
    } else {
        return true
    }
}

export const filterAsyncRoutes = (routes: RouteConfig[], roles: string[]) => {
    const res: RouteConfig[] = []
    routes.forEach(route => {
        const r = {...route}
        if (hasPermission(roles, r)) {
            if (r.children) {
                r.children = filterAsyncRoutes(r.children, roles)
            }
            res.push(r)
        }
    })
    return res
}

export interface IPermissionState {
    routes: RouteConfig[]
    dynamicRoutes: RouteConfig[]
}

@Module({dynamic: true, store, name: 'permission'})
class Permission extends VuexModule implements IPermissionState {
    public routes: RouteConfig[] = []
    public dynamicRoutes: RouteConfig[] = []

    @Mutation
    private SET_ROUTES(routes: RouteConfig[]) {
        // 排除没有子项的数据
        for (let i in routes) {
            let route = routes[i];
            // @ts-ignore
            if (route.hasOwnProperty("children") && route.children.length === 0) {
                // @ts-ignore
                routes.splice(i, 1);
            }
        }
        this.routes = constantRoutes.concat(routes)
        this.dynamicRoutes = routes
    }

    @Action
    public GenerateRoutes(roles: string[]) {
        let accessedRoutes
        if (roles.includes('admin')) {
            accessedRoutes = asyncRoutes
        } else {
            accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
            // 代理中心
            accessedRoutes = accessedRoutes.concat(agentRoutes);
        }
        this.SET_ROUTES(accessedRoutes)
    }
}

export const PermissionModule = getModule(Permission)
