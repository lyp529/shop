import * as tslib_1 from "tslib";
import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators';
import store from '@/store';
import ConstApi from "@/api/ConstApi";
import OptVo from "@/models/vos/OptVo";
import C from "@/constants/C";
let constStore = class constStore extends VuexModule {
    constructor() {
        super(...arguments);
        /**
         * 获取所有常量配置
         */
        this.constMap = {};
    }
    /**
     * 获取订单状态labels
     */
    get getWalletOrderStatusLabels() {
        return this.constMap.hasOwnProperty("getWalletOrderStatusLabels") ? this.constMap["getWalletOrderStatusLabels"] : {};
    }
    /**
     * 获取钱包订单类型labels
     */
    get getWalletOrderTypeLabels() {
        return this.constMap.hasOwnProperty("getWalletOrderTypeLabels") ? this.constMap["getWalletOrderTypeLabels"] : {};
    }
    /**
     * 获取公告类型labels
     */
    get noticeTypeLabels() {
        return this.constMap.hasOwnProperty("noticeTypeLabels") ? this.constMap["noticeTypeLabels"] : {};
    }
    /**
     * 获取用户类型labels
     */
    get userTypeLabels() {
        return this.constMap.hasOwnProperty("userTypeLabels") ? this.constMap["userTypeLabels"] : {};
    }
    /**
     * 帐变类型labels
     */
    get cashLabels() {
        return this.constMap.hasOwnProperty("cashLabels") ? this.constMap["cashLabels"] : {};
    }
    /**
     * 获取用户类型选项数据（主要是为了可处理数据类型）
     */
    get userTypeOpts() {
        return OptVo.loadListByLabels(this.userTypeLabels, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 管理员
     */
    get manageTypeOpts() {
        return OptVo.loadListByLabels(this.userTypeLabels, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 获取游戏标签列表
     */
    get kindLabels() {
        return this.constMap.hasOwnProperty("kindLabels") ? this.constMap["kindLabels"] : {};
    }
    /**
     * 或有游戏选项
     */
    get kindOpts() {
        return OptVo.loadListByLabels(this.kindLabels, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 支付平台网站地址
     */
    get payPlatUrl() {
        return this.constMap["payPlatUrl"] ? this.constMap["payPlatUrl"] : "";
    }
    /**
     * 管理员加款类型选项
     */
    get adminAddOpts() {
        let map = this.constMap.hasOwnProperty("adminAddLabels") ? this.constMap["adminAddLabels"] : [];
        return OptVo.loadListByLabels(map, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 管理员扣款类型选项
     */
    get adminCutOpts() {
        let map = this.constMap.hasOwnProperty("adminCutLabels") ? this.constMap["adminCutLabels"] : [];
        return OptVo.loadListByLabels(map, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 设置常量map
     * @param constMap
     */
    async setConstMap(constMap) {
        this.constMap = constMap;
    }
    /**
     * 请求
     */
    async requestConstMap() {
        const constMap = await ConstApi.get();
        this.setConstMap(constMap);
    }
};
tslib_1.__decorate([
    Mutation
], constStore.prototype, "setConstMap", null);
tslib_1.__decorate([
    Action
], constStore.prototype, "requestConstMap", null);
constStore = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'const' })
], constStore);
export const ConstStore = getModule(constStore);
//# sourceMappingURL=ConstStore.js.map