import * as tslib_1 from "tslib";
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import store from '@/store';
import elementVariables from '@/styles/element-variables.scss';
import defaultSettings from '@/settings';
let Settings = class Settings extends VuexModule {
    constructor() {
        super(...arguments);
        this.theme = elementVariables.theme;
        this.fixedHeader = defaultSettings.fixedHeader;
        this.showSettings = defaultSettings.showSettings;
        this.showTagsView = defaultSettings.showTagsView;
        this.showSidebarLogo = defaultSettings.showSidebarLogo;
        this.sidebarTextTheme = defaultSettings.sidebarTextTheme;
    }
    CHANGE_SETTING(payload) {
        const { key, value } = payload;
        if (Object.prototype.hasOwnProperty.call(this, key)) {
            this[key] = value;
        }
    }
    ChangeSetting(payload) {
        this.CHANGE_SETTING(payload);
    }
};
tslib_1.__decorate([
    Mutation
], Settings.prototype, "CHANGE_SETTING", null);
tslib_1.__decorate([
    Action
], Settings.prototype, "ChangeSetting", null);
Settings = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'settings' })
], Settings);
export const SettingsModule = getModule(Settings);
//# sourceMappingURL=settings.js.map