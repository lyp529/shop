import {VuexModule, Module, Action, Mutation, getModule} from 'vuex-module-decorators'
import store from '@/store'
import ConstApi from "@/api/ConstApi";
import OptVo from "@/models/vos/OptVo";
import C from "@/constants/C";

export interface IConstState {
    constMap: { [key: string]: any }
}

@Module({dynamic: true, store, name: 'const'})
class constStore extends VuexModule implements IConstState {
    /**
     * 获取所有常量配置
     */
    public constMap: { [key: string]: any } = {};


    /**
     * 获取订单状态labels
     */
    public get getWalletOrderStatusLabels(): { [key: number]: string } {
        return this.constMap.hasOwnProperty("getWalletOrderStatusLabels") ? this.constMap["getWalletOrderStatusLabels"] : {};
    }

    /**
     * 获取钱包订单类型labels
     */
    public get getWalletOrderTypeLabels(): { [key: number]: string } {
        return this.constMap.hasOwnProperty("getWalletOrderTypeLabels") ? this.constMap["getWalletOrderTypeLabels"] : {};
    }

    /**
     * 获取公告类型labels
     */
    public get noticeTypeLabels(): { [key: number]: string } {
        return this.constMap.hasOwnProperty("noticeTypeLabels") ? this.constMap["noticeTypeLabels"] : {};
    }

    /**
     * 获取用户类型labels
     */
    public get userTypeLabels(): { [key: number]: string } {
        return this.constMap.hasOwnProperty("userTypeLabels") ? this.constMap["userTypeLabels"] : {};
    }

    /**
     * 帐变类型labels
     */
    public get cashLabels(): { [key: number]: any } {
        return this.constMap.hasOwnProperty("cashLabels") ? this.constMap["cashLabels"] : {};

    }

    /**
     * 获取用户类型选项数据（主要是为了可处理数据类型）
     */
    public get userTypeOpts(): OptVo[] {
        return OptVo.loadListByLabels(this.userTypeLabels, C.OPT_VALUE_TYPE_NUMBER);
    }

    /**
     * 管理员
     */
    public get manageTypeOpts(): OptVo[] {
        return OptVo.loadListByLabels(this.userTypeLabels, C.OPT_VALUE_TYPE_NUMBER);
    }

    /**
     * 获取游戏标签列表
     */
    public get kindLabels(): { [key: string]: string } {
        return this.constMap.hasOwnProperty("kindLabels") ? this.constMap["kindLabels"] : {};
    }

    /**
     * 或有游戏选项
     */
    public get kindOpts(): OptVo[] {
        return OptVo.loadListByLabels(this.kindLabels, C.OPT_VALUE_TYPE_NUMBER);
    }

    /**
     * 支付平台网站地址
     */
    public get payPlatUrl(): string {
        return this.constMap["payPlatUrl"] ? this.constMap["payPlatUrl"] : "";
    }

    /**
     * 管理员加款类型选项
     */
    public get adminAddOpts(): OptVo[] {
        let map = this.constMap.hasOwnProperty("adminAddLabels") ? this.constMap["adminAddLabels"] : [];
        return OptVo.loadListByLabels(map, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 管理员扣款类型选项
     */
    public get adminCutOpts(): OptVo[] {
        let map = this.constMap.hasOwnProperty("adminCutLabels") ? this.constMap["adminCutLabels"] : [];
        return OptVo.loadListByLabels(map, C.OPT_VALUE_TYPE_NUMBER);
    }


    /**
     * 设置常量map
     * @param constMap
     */
    @Mutation
    private async setConstMap(constMap: { [key: string]: any }) {
        this.constMap = constMap;
    }

    /**
     * 请求
     */
    @Action
    public async requestConstMap() {
        const constMap = await ConstApi.get()
        this.setConstMap(constMap)
    }
}

export const ConstStore = getModule(constStore)
