import * as tslib_1 from "tslib";
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import { asyncRoutes, constantRoutes, agentRoutes } from '@/router';
import store from '@/store';
const hasPermission = (roles, route) => {
    if (route.meta && route.meta.roles) {
        return roles.some(role => route.meta.roles.includes(role));
    }
    else {
        return true;
    }
};
export const filterAsyncRoutes = (routes, roles) => {
    const res = [];
    routes.forEach(route => {
        const r = { ...route };
        if (hasPermission(roles, r)) {
            if (r.children) {
                r.children = filterAsyncRoutes(r.children, roles);
            }
            res.push(r);
        }
    });
    return res;
};
let Permission = class Permission extends VuexModule {
    constructor() {
        super(...arguments);
        this.routes = [];
        this.dynamicRoutes = [];
    }
    SET_ROUTES(routes) {
        // 排除没有子项的数据
        for (let i in routes) {
            let route = routes[i];
            // @ts-ignore
            if (route.hasOwnProperty("children") && route.children.length === 0) {
                // @ts-ignore
                routes.splice(i, 1);
            }
        }
        this.routes = constantRoutes.concat(routes);
        this.dynamicRoutes = routes;
    }
    GenerateRoutes(roles) {
        let accessedRoutes;
        if (roles.includes('admin')) {
            accessedRoutes = asyncRoutes;
        }
        else {
            accessedRoutes = filterAsyncRoutes(asyncRoutes, roles);
            // 代理中心
            accessedRoutes = accessedRoutes.concat(agentRoutes);
        }
        this.SET_ROUTES(accessedRoutes);
    }
};
tslib_1.__decorate([
    Mutation
], Permission.prototype, "SET_ROUTES", null);
tslib_1.__decorate([
    Action
], Permission.prototype, "GenerateRoutes", null);
Permission = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'permission' })
], Permission);
export const PermissionModule = getModule(Permission);
//# sourceMappingURL=permission.js.map