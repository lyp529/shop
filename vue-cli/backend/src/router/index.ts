import Vue from 'vue'
import Router, {RouteConfig} from 'vue-router'
import C from "@/constants/C";

/* Layout */
import Layout from '@/layout/index.vue'

/* Router modules */
// import componentsRouter from './modules/demo/components'
// import chartsRouter from './modules/demo/charts'
// import tableRouter from './modules/demo/table'
// import nestedRouter from './modules/demo/nested'
// import {testerRouter} from "@/router/modules/developer";
import DebugUtil from "@/utils/DebugUtil";

Vue.use(Router)

/*
  Note: sub-menu only appear when children.length>=1
  Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
*/

/*
  name:'router-name'             the name field is required when using <keep-alive>, it should also match its component's name property
                                 detail see : https://vuejs.org/v2/guide/components-dynamic-async.html#keep-alive-with-Dynamic-Components
  redirect:                      if set to 'noredirect', no redirect action will be trigger when clicking the breadcrumb
  meta: {
    roles: ['admin', 'editor']   will control the page roles (allow setting multiple roles)
    title: 'title'               the name showed in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon showed in the sidebar
    hidden: true                 if true, this route will not show in the sidebar (default is false)
    alwaysShow: true             if true, will always show the root menu (default is false)
                                 if false, hide the root menu when has less or equal than one children route
    breadcrumb: false            if false, the item will be hidden in breadcrumb (default is true)
    noCache: true                if true, the page will not be cached (default is false)
    affix: true                  if true, the tag will affix in the tags-view
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
*/

/**
 ConstantRoutes
 a base page that does not have permission requirements
 all roles can be accessed
 */
export const constantRoutes: RouteConfig[] = [
    {
        path: '/redirect',
        component: Layout,
        meta: {hidden: true},
        children: [
            {
                path: '/redirect/:path*',
                component: () => import(/* webpackChunkName: "redirect" */ '@/views/redirect/index.vue')
            }
        ]
    },
    {
        path: '/login',
        component: () => import(/* webpackChunkName: "login" */ '@/views/login/index.vue'),
        meta: {hidden: true}
    },
    {
        path: '/auth-redirect',
        component: () => import(/* webpackChunkName: "auth-redirect" */ '@/views/login/auth-redirect.vue'),
        meta: {hidden: true}
    },
    {
        path: '/404',
        component: () => import(/* webpackChunkName: "404" */ '@/views/error-page/404.vue'),
        meta: {hidden: true}
    },
    {
        path: '/401',
        component: () => import(/* webpackChunkName: "401" */ '@/views/error-page/401.vue'),
        meta: {hidden: true}
    },
    // {
    //     path: '/',
    //     component: Layout,
    //     redirect: '/dashboard',
    //     children: [
    //         {
    //             path: 'dashboard',
    //             component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/index.vue'),
    //             name: 'Dashboard',
    //             meta: {
    //                 title: 'dashboard',
    //                 icon: 'dashboard',
    //                 affix: true
    //             }
    //         }
    //     ]
    // },
    {
        path: '/',
        component: Layout,
        redirect: '/home',
        children: [
            {
                path: 'home',
                component: () => import(/* webpackChunkName: "home" */ '@/views/home/Home.vue'),
                name: 'Dashboard',
                meta: {
                    title: 'dashboard',
                    icon: 'dashboard',
                    affix: true
                }
            }
        ]
    }
]

let asyncRoutesConfig: RouteConfig[] = []

let asyncRoutesCommon = [
    {
        path: '/account',
        component: Layout,
        redirect: '/account',
        meta: {
            roles: ['admin'],
            icon: 'tree',
            title: 'accountManage'
        },
        children: [
            {
                path: 'manage',
                component: () => import(/* webpackChunkName: "dashboard" */ '@/views/account-manage/AccountManage.vue'),
                name: 'manage',
                meta: {
                    title: 'userManage',
                    icon: 'tree',
                    roles: ['admin'] // or you can only set roles in sub nav
                }
            },
            {
                path: 'member',
                component: () => import(/* webpackChunkName: "dashboard" */ '@/views/account-manage/AccountUserAdmin.vue'),
                name: 'member',
                meta: {
                    title: 'userAdmin',
                    icon: 'tree',
                    roles: ['admin'] // or you can only set roles in sub nav
                }
            }
        ]
    },
    {
        path: '/commodity',
        component: Layout,
        redirect: '/log/index',
        name: 'logManage',
        meta: {
            title: 'logManage',
            icon: 'documentation',
            roles: ['admin'], // you can set roles in root nav
            alwaysShow: true // will always show the root menu
        },
        children: [
            {
                path: 'commodity',
                component: () => import(/* webpackChunkName: "dashboard" */ '@/views/log/operate_log.vue'),
                name: 'operateLog',
                meta: {
                    title: 'operateLog',
                    icon: 'documentation',
                    noCache: true
                }
            }
        ]
    },
    {
        path: '/orderManage',
        component: Layout,
        redirect: '/orderManage/index',
        name: 'orderManage',
        meta: {
            title: 'orderManage',
            icon: 'documentation',
            roles: ['admin'], // you can set roles in root nav
            alwaysShow: true // will always show the root menu
        },
        children: [
            {
                path: 'order',
                component: () => import(/* webpackChunkName: "dashboard" */ '@/views/order/order.vue'),
                name: 'orderList',
                meta: {
                    title: 'orderList',
                    icon: 'documentation',
                    noCache: true
                }
            }
        ]
    },
    {
        path: '/otherSettings',
        component: Layout,
        redirect: '/otherSettings/index',
        name: 'otherSettings',
        meta: {
            title: 'otherSettings',
            icon: 'form',
            roles: ['admin'], // you can set roles in root nav
            alwaysShow: true // will always show the root menu
        },
        children: [
            {
                path: 'LuckRun',
                component: () => import(/* webpackChunkName: "luck-run" */ '@/views/luck/LuckRun.vue'),
                name: 'luckRun',
                meta: {
                    title: 'luckRun',
                    icon: 'like',
                    noCache: true
                }
            }
        ]
    },
]


asyncRoutesConfig = asyncRoutesConfig.concat(asyncRoutesCommon);
/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes: RouteConfig[] = asyncRoutesConfig;

/** 只有代理才有的路由,管理员也没有
 * agentRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const agentRoutes: RouteConfig[] = [
    {
        path: '/system',
        component: Layout,
        redirect: '/system/notice',
        children: [
            {
                path: 'notice',
                component: () => import(/* webpackChunkName: "dashboard" */ '@/views/system/SystemNotice.vue'),
                name: 'systemNotice',
                meta: {
                    title: 'systemNotice',
                    icon: 'dashboard',
                    affix: true
                }
            }
        ]
    },
    //代理中心
    {
        path: '/agentCenter',
        component: Layout,
        redirect: '/agentCenter/index',
        name: 'agentCenter',
        meta: {
            title: 'agentCenter',
            icon: 'list',
            roles: ['agent'], // you can set roles in root nav
            alwaysShow: true // will always show the root menu
        },
        children: [
            {
                path: 'memberManager',
                component: () => import(/* webpackChunkName: "systemNotice" */ '@/views/account-manage/AccountManage.vue'),
                name: 'memberManager',
                meta: {
                    title: 'memberManager',
                    icon: 'list',
                    roles: ['agent'], // you can set roles in root nav
                    noCache: true,
                }
            },
            {
                path: 'reportSummary',
                component: () => import(/* webpackChunkName: "systemNotice" */ '@/views/account-report/Report.vue'),
                name: 'reportSummary',
                meta: {
                    title: 'reportSummary',
                    icon: 'list',
                    roles: ['agent'], // you can set roles in root nav
                    noCache: true
                }
            },
            {
                path: 'order-list',
                component: () => import(/* webpackChunkName: "systemNotice" */ '@/views/wallet/OrderList.vue'),
                name: 'transactionRecord',
                meta: {
                    title: 'transactionRecord',
                    icon: 'list',
                    noCache: true,
                }
            },
            {
                path: 'list',
                component: () => import(/* webpackChunkName: "systemNotice" */ '@/views/bet/BetList.vue'),
                name: 'betList',
                meta: {
                    title: 'betList',
                    icon: 'list',
                    noCache: true
                }
            },
            {
                path: 'cashRecord',
                component: () => import(/* webpackChunkName: "systemNotice" */ '@/views/agent-center/cash-log.vue'),
                name: 'cashRecord',
                meta: {
                    title: 'cashRecord',
                    icon: 'list',
                    roles: ['agent'], // you can set roles in root nav
                    noCache: true
                }
            },
            {
                path: 'promote',
                component: () => import(/* webpackChunkName: "systemNotice" */ '@/views/agent-center/spread.vue'),
                name: 'promote',
                meta: {
                    title: 'promote',
                    icon: 'list',
                    roles: ['agent'], // you can set roles in root nav
                    noCache: true
                }
            },

        ]
    },
]

const createRouter = () => new Router({
    // mode: 'history',  // Disabled due to Github Pages doesn't support this, enable this if you need.
    scrollBehavior: (to, from, savedPosition) => {
        if (savedPosition) {
            return savedPosition
        } else {
            return {x: 0, y: 0}
        }
    },
    base: process.env.BASE_URL,
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter();
    (router as any).matcher = (newRouter as any).matcher // reset router
}

export default router
