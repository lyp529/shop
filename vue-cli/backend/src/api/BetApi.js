import TableDataVo from "@/models/vos/TableDataVo";
import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
/**
 * 注单api
 */
export default class BetApi {
    /**
     * 请求注单表格数据
     * @param params
     */
    static async getBetRowVoTable(params) {
        let vo = await HttpUtil.post("bet/getBetRowVoTable", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
}
//# sourceMappingURL=BetApi.js.map