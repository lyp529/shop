/**
 * 钱包API
 * */
import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";
export default class LogApi {
    /**
     * 获取表单数据
     * @param params
     */
    static async getCashRecord(params) {
        let vo = await HttpUtil.post("log/getCashLog", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取表单数据
     * @param params
     */
    static async getReportSummary(params) {
        let vo = await HttpUtil.post("log/getReportSummary", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取操作日志
     * @param params
     */
    static async getOperateLog(params) {
        let vo = await HttpUtil.post("log/get-operate-log", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取登录日志
     * @param params
     */
    static async getLoginLog(params) {
        let vo = await HttpUtil.post("log/get-login-log", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
}
//# sourceMappingURL=LogApi.js.map