/**
 * 用户api
 */
import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";


export default class ActivityApi {
      /**
     * 推广链接
     * @param params
     */
    public static async getUserLinks(params: any) : Promise<any> {
        let vo = await HttpUtil.post("activity/spreadLinks",params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return {superHolderNameMap: {}, holderNameMap: {}, vendorNameMap: {}, agentNameMap: {}};
        }
        return vo.d;
    }
}
