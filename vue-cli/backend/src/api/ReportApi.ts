/**
 * 报表API
 * */
import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";

export default class ReportApi {

    /**
     * 获取表单数据
     * @param params
     */
    public static async getReportSummary(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("report/getReportList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 获取明细内容
     */
    public static async getReportDetail(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("report/getReportDetail", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

}
