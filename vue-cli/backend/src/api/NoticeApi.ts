/**
 * 公告API
 * */
import ResponseVo from "@/models/vos/ResponseVo";
import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";

export default class NoticeApi {

    /**
     * 获取公告表数据
     * @param params
     */
    public static async getTable(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("notice/getNoticeList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 添加公告
     * @param params
     */
    public static async addNotice(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("notice/addNotice", params);
        return vo.commonHandler();
    }

    /**
     * 编辑公告
     * @param params
     * @param url
     */
    public static async editNotice(params: any,url: string): Promise<boolean> {
        let vo = await HttpUtil.post(url, params);
        return vo.commonHandler();
    }
}
