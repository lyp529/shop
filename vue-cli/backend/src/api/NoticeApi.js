import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";
export default class NoticeApi {
    /**
     * 获取公告表数据
     * @param params
     */
    static async getTable(params) {
        let vo = await HttpUtil.post("notice/getNoticeList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 添加公告
     * @param params
     */
    static async addNotice(params) {
        let vo = await HttpUtil.post("notice/addNotice", params);
        return vo.commonHandler();
    }
    /**
     * 编辑公告
     * @param params
     * @param url
     */
    static async editNotice(params, url) {
        let vo = await HttpUtil.post(url, params);
        return vo.commonHandler();
    }
}
//# sourceMappingURL=NoticeApi.js.map