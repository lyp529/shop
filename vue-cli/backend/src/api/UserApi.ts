/**
 * 用户api
 */
import ResponseVo from "@/models/vos/ResponseVo";
import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
import UserInfoVo from "@/models/vos/UserInfoVo";
import TableDataVo from "@/models/vos/TableDataVo";
import {emptyTypeAnnotation} from "@babel/types";

export default class UserApi {
    public static async changeUser(params: any) {
        let vo = await HttpUtil.post("user/changeUser", params);
        return vo.commonHandler();
    }
    public static async getUserList(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("user/getUserList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 用户登录
     * @param params
     */
    public static async login(params: {username: string, password: string}): Promise<ResponseVo> {
        return await HttpUtil.post("user/login", params);
    }

    /**
     * 是否初始化管理员
     */
    public static async isInitAdmin(): Promise<boolean> {
        let vo = await HttpUtil.post("user/isInitAdmin")
        if (vo.isDone()) {
            return vo.data("isInitAdmin");
        } else {
            WindowUtil.messageError(vo.message);
        }
        return true;
    }

    /**
     * 初始化管理员
     * @param params
     */
    public static async initAdmin(params: {username: string, password: string}): Promise<boolean> {
        let vo = await HttpUtil.post("user/initAdmin", params);
        return vo.commonHandler();
    }

    /**
     * 登出接口
     */
    public static async logout(): Promise<boolean> {
        let vo = await HttpUtil.post("user/logout");
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return false;
        }
        return true;
    }

    /**
     * 获取验证码
     */
    public static async getCode(): Promise<any> {
        let vo = await HttpUtil.post("user/getCode");
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
        }
        return vo.data('code');
    }

    /**
     * 添加用户
     */
    public static async add(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("user/add", params);
        return vo.commonHandler();
    }

    /**
     * 获取当前登录账号的的信息
     */
    public static async getGetUserInfoVo(): Promise<UserInfoVo|null> {
        let vo = await HttpUtil.post("user/getUserInfoVo");
        if (!vo.isDone()) {
            return null;
        }
        return new UserInfoVo().load(vo.data("userInfoVo"));
    }

    /**
     * 获取直属下线表格数据
     */
    public static async getChildRowVoTable(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("user/getChildRowVoTable", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 获取管理员账号数据
     * @param params
     */
    public static async  getMemberManageList(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("user/get-user-admin-list",params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取用户表单数据
     * @param params
     */
    public static async getUserForm(params: {userId: number}): Promise<any> {
        let vo = await HttpUtil.post("user/getUserForm", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return null;
        }
        return vo.data("userForm");
    }

    /**
     * 修改用户数据
     * @param params
     */
    public static async mod(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("user/mod", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }

    public static async vipApplyFor(userId: number) {
        let vo = await HttpUtil.post("user/vipApplyFor", {userId: userId});
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 修改银行信息
     */
    public static async bank(params: any): Promise<boolean> {
        let vo =await HttpUtil.post("user/bank",params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 获取用户表格数据
     */
    public static async getUserAccountNameMap(): Promise<{superHolderNameMap: {}, holderNameMap: {}, vendorNameMap: {}, agentNameMap: {}}> {
        let vo = await HttpUtil.post("user/getUserAccountNameMap");
        if (!vo.isDone()) {
            WindowUtil.messageError("获取用户选项数据失败");
            return {superHolderNameMap: {}, holderNameMap: {}, vendorNameMap: {}, agentNameMap: {}};
        }
        return vo.d;
    }

    /**
     * 删除用户
     * @param userId
     */
    public static async del(userId: number): Promise<boolean> {
        let vo = await HttpUtil.post("user/del", {userId});
        return vo.commonHandler();
    }

    /**
     *
     * @param userId
     * @param active
     */
    public static async modActive(userId: number, active: boolean): Promise<boolean> {
        let vo = await HttpUtil.post("user/modActive", {userId, active});
        return vo.commonHandler();
    }

    /**
     * 检测是否登录
     */
    public static async checkLogin(): Promise<boolean> {
        let vo = await HttpUtil.post("user/checkLogin");
        return vo.isDone();
    }

    /**
     * 上传商品图片
     */
    public static async upload(params: any): Promise<boolean> {
        let vo =await HttpUtil.post("user/upload",params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 添加商品
     */
    public static async addShop(params: any): Promise<boolean> {
        let vo =await HttpUtil.post("user/addShop",params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 获取商品表数据
     * @param params
     */
    public static async getTable(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("user/getShopList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
}
