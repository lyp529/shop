import TableDataVo from "@/models/vos/TableDataVo";
import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";

/**
 * 期数api
 */
export default class ScheduleApi {
    /**
     * 获取开奖数据
     * @param params
     */
    public static async getScheduleListTable(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("schedule/getScheduleListTable", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 预设结果
     * @param params
     */
    public static async predictScheduleResult(params: any) {
        let vo = await HttpUtil.post("schedule/predictScheduleResult", params);
        return vo.commonHandler();
    }
}
