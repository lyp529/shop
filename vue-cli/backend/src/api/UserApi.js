import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
import UserInfoVo from "@/models/vos/UserInfoVo";
import TableDataVo from "@/models/vos/TableDataVo";
export default class UserApi {
    static async changeUser(params) {
        let vo = await HttpUtil.post("user/changeUser", params);
        return vo.commonHandler();
    }
    static async getUserList(params) {
        let vo = await HttpUtil.post("user/getUserList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 用户登录
     * @param params
     */
    static async login(params) {
        return await HttpUtil.post("user/login", params);
    }
    /**
     * 是否初始化管理员
     */
    static async isInitAdmin() {
        let vo = await HttpUtil.post("user/isInitAdmin");
        if (vo.isDone()) {
            return vo.data("isInitAdmin");
        }
        else {
            WindowUtil.messageError(vo.message);
        }
        return true;
    }
    /**
     * 初始化管理员
     * @param params
     */
    static async initAdmin(params) {
        let vo = await HttpUtil.post("user/initAdmin", params);
        return vo.commonHandler();
    }
    /**
     * 登出接口
     */
    static async logout() {
        let vo = await HttpUtil.post("user/logout");
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return false;
        }
        return true;
    }
    /**
     * 获取验证码
     */
    static async getCode() {
        let vo = await HttpUtil.post("user/getCode");
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
        }
        return vo.data('code');
    }
    /**
     * 添加用户
     */
    static async add(params) {
        let vo = await HttpUtil.post("user/add", params);
        return vo.commonHandler();
    }
    /**
     * 获取当前登录账号的的信息
     */
    static async getGetUserInfoVo() {
        let vo = await HttpUtil.post("user/getUserInfoVo");
        if (!vo.isDone()) {
            return null;
        }
        return new UserInfoVo().load(vo.data("userInfoVo"));
    }
    /**
     * 获取直属下线表格数据
     */
    static async getChildRowVoTable(params) {
        let vo = await HttpUtil.post("user/getChildRowVoTable", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取管理员账号数据
     * @param params
     */
    static async getMemberManageList(params) {
        let vo = await HttpUtil.post("user/get-user-admin-list", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取用户表单数据
     * @param params
     */
    static async getUserForm(params) {
        let vo = await HttpUtil.post("user/getUserForm", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return null;
        }
        return vo.data("userForm");
    }
    /**
     * 修改用户数据
     * @param params
     */
    static async mod(params) {
        let vo = await HttpUtil.post("user/mod", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    static async vipApplyFor(userId) {
        let vo = await HttpUtil.post("user/vipApplyFor", { userId: userId });
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 修改银行信息
     */
    static async bank(params) {
        let vo = await HttpUtil.post("user/bank", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 获取用户表格数据
     */
    static async getUserAccountNameMap() {
        let vo = await HttpUtil.post("user/getUserAccountNameMap");
        if (!vo.isDone()) {
            WindowUtil.messageError("获取用户选项数据失败");
            return { superHolderNameMap: {}, holderNameMap: {}, vendorNameMap: {}, agentNameMap: {} };
        }
        return vo.d;
    }
    /**
     * 删除用户
     * @param userId
     */
    static async del(userId) {
        let vo = await HttpUtil.post("user/del", { userId });
        return vo.commonHandler();
    }
    /**
     *
     * @param userId
     * @param active
     */
    static async modActive(userId, active) {
        let vo = await HttpUtil.post("user/modActive", { userId, active });
        return vo.commonHandler();
    }
    /**
     * 检测是否登录
     */
    static async checkLogin() {
        let vo = await HttpUtil.post("user/checkLogin");
        return vo.isDone();
    }
    /**
     * 上传商品图片
     */
    static async upload(params) {
        let vo = await HttpUtil.post("user/upload", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 添加商品
     */
    static async addShop(params) {
        let vo = await HttpUtil.post("user/addShop", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.m);
            return false;
        }
        return vo.commonHandler();
    }
    /**
     * 获取商品表数据
     * @param params
     */
    static async getTable(params) {
        let vo = await HttpUtil.post("user/getShopList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
}
//# sourceMappingURL=UserApi.js.map