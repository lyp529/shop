import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";
import WindowUtil from "@/utils/WindowUtil";
/**
 * 钱包API
 * */
export default class WalletApi {
    /**
     * 获取表单数据
     * @param params
     */
    static async getWalletOrderList(params) {
        let vo = await HttpUtil.post("wallet/getWalletOrderList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 修改表单数据状态
     * @param params
     */
    static async updateWalletState(params) {
        let vo = await HttpUtil.post("wallet/updateWalletState", params);
        return vo.commonHandler();
    }
    /**
     * 获取提款信息
     * @param params
     */
    static async getWithdrawVo(params) {
        let vo = await HttpUtil.post("wallet/getWithdrawVo", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return null;
        }
        return vo.data("withdrawVo");
    }
    /**
     * 获取存款类型开关数据
     */
    static async getDepositTypeVoList() {
        let vo = await HttpUtil.post("wallet/getDepositTypeVoList");
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return [];
        }
        return vo.data("depositTypeVoList");
    }
    /**
     * 保存存款类型开关数据
     */
    static async saveDepositTypeVoList(params) {
        let vo = await HttpUtil.post("wallet/saveDepositTypeVoList", params);
        return vo.commonHandler();
    }
}
//# sourceMappingURL=WalletApi.js.map