/**
 * 钱包API
 * */
import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";

export default class LogApi {

    /**
     * 获取表单数据
     * @param params
     */
    public static async getCashRecord(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("log/getCashLog", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 获取表单数据
     * @param params
     */
    public static async getReportSummary(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("log/getReportSummary", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 获取操作日志
     * @param params
     */
    public static async getOperateLog(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("log/get-operate-log", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }

    /**
     * 获取登录日志
     * @param params
     */
    public static async getLoginLog(params: any): Promise<TableDataVo> {
        let vo = await HttpUtil.post("log/get-login-log", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
}
