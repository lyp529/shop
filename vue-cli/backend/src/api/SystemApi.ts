import HttpUtil from "@/utils/HttpUtil";

/**
 * 系统 api
 */
export default class SystemApi {
    /**
     * 获取 联系方式
     */
    public static async getMemberContactVo(): Promise<any> {
        let vo = await HttpUtil.post("system/getMemberContactVo");
        if (!vo.isDone()) {
            return {};
        }
        return vo.data("memberContactVo");
    }

    /**
     * 保存 联系方式
     */
    public static async saveMemberContactVo(memberContactVo: any): Promise<boolean> {
        let vo = await HttpUtil.post("system/saveMemberContactVo", {memberContactVo: memberContactVo});
        return vo.commonHandler();
    }
}
