/**
 * 常量相关的api
 */
import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
export default class ConstApi {
    /**
     * 获取常量参数
     */
    static async get() {
        let vo = await HttpUtil.post("const/get");
        if (!vo.isDone()) {
            WindowUtil.messageError("获取系统常量数据失败");
            return {};
        }
        return vo.d;
    }
}
//# sourceMappingURL=ConstApi.js.map