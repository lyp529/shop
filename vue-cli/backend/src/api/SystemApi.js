import HttpUtil from "@/utils/HttpUtil";
/**
 * 系统 api
 */
export default class SystemApi {
    /**
     * 获取 联系方式
     */
    static async getMemberContactVo() {
        let vo = await HttpUtil.post("system/getMemberContactVo");
        if (!vo.isDone()) {
            return {};
        }
        return vo.data("memberContactVo");
    }
    /**
     * 保存 联系方式
     */
    static async saveMemberContactVo(memberContactVo) {
        let vo = await HttpUtil.post("system/saveMemberContactVo", { memberContactVo: memberContactVo });
        return vo.commonHandler();
    }
}
//# sourceMappingURL=SystemApi.js.map