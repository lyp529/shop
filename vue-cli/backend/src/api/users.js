import request from '@/utils/request';
export const getUsers = (params) => request({
    url: '/users',
    method: 'get',
    params
});
export const getUserInfo = (data) => request({
    url: '/users/info',
    method: 'post',
    data
});
export const getUserByName = (username) => request({
    url: `/users/${username}`,
    method: 'get'
});
export const updateUser = (username, data) => request({
    url: `/users/${username}`,
    method: 'put',
    data
});
export const deleteUser = (username) => request({
    url: `/users/${username}`,
    method: 'delete'
});
export const login = (data) => request({
    url: '/users/login',
    method: 'post',
    data
});
export const logout = () => request({
    url: '/users/logout',
    method: 'post'
});
export const register = (data) => request({
    url: '/users/register',
    method: 'post',
    data
});
//# sourceMappingURL=users.js.map