/**
 * 报表API
 * */
import HttpUtil from "@/utils/HttpUtil";
import TableDataVo from "@/models/vos/TableDataVo";
export default class ReportApi {
    /**
     * 获取表单数据
     * @param params
     */
    static async getReportSummary(params) {
        let vo = await HttpUtil.post("report/getReportList", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
    /**
     * 获取明细内容
     */
    static async getReportDetail(params) {
        let vo = await HttpUtil.post("report/getReportDetail", params);
        if (!vo.isDone()) {
            return new TableDataVo();
        }
        return new TableDataVo().load(vo.d);
    }
}
//# sourceMappingURL=ReportApi.js.map