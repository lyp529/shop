import request from '@/utils/request';
export const getRoles = (params) => request({
    url: '/roles',
    method: 'get',
    params
});
export const createRole = (data) => request({
    url: '/roles',
    method: 'post',
    data
});
export const updateRole = (id, data) => request({
    url: `/roles/${id}`,
    method: 'put',
    data
});
export const deleteRole = (id) => request({
    url: `/roles/${id}`,
    method: 'delete'
});
export const getRoutes = (params) => request({
    url: '/routes',
    method: 'get',
    params
});
//# sourceMappingURL=roles.js.map