/**
 * 用户api
 */
import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
export default class ActivityApi {
    /**
   * 推广链接
   * @param params
   */
    static async getUserLinks(params) {
        let vo = await HttpUtil.post("activity/spreadLinks", params);
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return { superHolderNameMap: {}, holderNameMap: {}, vendorNameMap: {}, agentNameMap: {} };
        }
        return vo.d;
    }
}
//# sourceMappingURL=ActivityApi.js.map