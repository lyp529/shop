/**
 * 获取
 */
import C from "@/constants/C";
/**
 * 用户工具
 */
export default class UserUtil {
    /**
     * 获取子账号类型
     * @param userType
     */
    static getChildType(userType) {
        let childType = C.USER_TYPE_MEMBER;
        if (userType === C.USER_TYPE_SOLDER) {
            childType = C.USER_TYPE_HOLDER;
        }
        else if (userType === C.USER_TYPE_HOLDER) {
            childType = C.USER_TYPE_VENDOR;
        }
        else if (userType === C.USER_TYPE_VENDOR) {
            childType = C.USER_TYPE_AGENT;
        }
        else if (userType === C.USER_TYPE_AGENT) {
            childType = C.USER_TYPE_MEMBER;
        }
        return childType;
    }
}
//# sourceMappingURL=UserUtil.js.map