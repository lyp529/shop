import { UserStore } from '@/store/modules/UserStore';
export const checkPermission = (value) => {
    if (value && value instanceof Array && value.length > 0) {
        const roles = UserStore.roles;
        const permissionRoles = value;
        const hasPermission = roles.some(role => {
            return permissionRoles.includes(role);
        });
        return hasPermission;
    }
    else {
        console.error(`need roles! Like v-permission="['admin','editor']"`);
        return false;
    }
};
//# sourceMappingURL=permission.js.map