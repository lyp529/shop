import ResponseVo from "../models/vos/ResponseVo";
import { UserStore } from "@/store/modules/UserStore";
import WindowUtil from "@/utils/WindowUtil";
import C from "@/constants/C";
const qs = require('qs');
const axios = require('axios');
/**
 * http工具
 */
export default class HttpUtil {
    /**
     * 将字符串中的大写字母转为 -加小写字母  e.g. schedule/getOldScheduleVoList  > schedule/get-old-schedule-vo-list 更方便跟后台对应
     * @param txt 字符串
     */
    static upperSplit(txt) {
        if (txt.indexOf('-') == -1) {
            let str = "";
            for (let i = 0; i < txt.length; i++) { //小写字符asc-ii码为
                if (txt.charCodeAt(i) >= 65 && txt.charCodeAt(i) <= 90) {
                    str += "-" + txt[i].toLowerCase();
                }
                else {
                    str += txt[i];
                }
            }
            // console.log(txt+" >> "+str);
            return str;
        }
        else {
            return txt;
        }
    }
    /**
     * 发送post请求
     * @param route 请求地址
     * @param params 请求参数
     */
    static async post(route, params = {}) {
        return new Promise((resolve) => {
            let requestUrl = HttpUtil.getUrl(HttpUtil.upperSplit(route));
            axios.post(requestUrl, qs.stringify(params)).then(async (response) => {
                let data = response.data;
                let responseVo = new ResponseVo();
                responseVo.c = data.c;
                responseVo.m = data.m;
                responseVo.d = data.d;
                if (responseVo.c === C.RESPONSE_CODE_NOT_LOGIN) {
                    await UserStore.LogOut();
                    await WindowUtil.dialog(responseVo.m);
                }
                else {
                    resolve(responseVo);
                }
            }).catch((error) => {
                let responseVo = new ResponseVo();
                responseVo.c = -99;
                responseVo.m = "系统出错";
                responseVo.d = error;
                resolve(responseVo);
            });
        });
    }
    /**
     * 获取基本的请求地址
     * 用于测试环境获取
     */
    static getUrl(route) {
        let requestUrl = "index.php/" + route;
        if (process.env.NODE_ENV !== 'production') {
            requestUrl = "backend/" + requestUrl;
        }
        return requestUrl;
    }
}
//# sourceMappingURL=HttpUtil.js.map