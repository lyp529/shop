/**
 * 通用工具封装
 */
export default class CommonUtil {
    /**
     * 深拷贝数据
     * @param data
     */
    static copy(data) {
        return JSON.parse(JSON.stringify(data));
    }
    /**
     * 驼峰命名转中横线。
     *  如：输入： UserList 返回 user-list
     * @param str
     */
    static humpToHyphen(str) {
        let hyphen = str.replace(/([A-Z])/g, "-$1").toLowerCase();
        hyphen = hyphen.substr(1, hyphen.length - 1);
        return hyphen;
    }
    /**
     * 判断是否为空
     * @param value
     */
    static isEmpty(value) {
        return value === "" || value === 0 || value === undefined || value === null || value === false || CommonUtil.size(value) === 0;
    }
    /**
     * 获取数据的长度
     * @param value
     */
    static size(value) {
        let typeName = (typeof value).toLowerCase();
        if (typeName === 'string' || typeName === 'array') {
            return value.length;
        }
        else if (typeName === 'object') {
            if (value === null) {
                return 0;
            }
            else {
                return Object.keys(value).length;
            }
        }
        else { //number 或 其他
            return -1;
        }
    }
    /**
     * 等待
     * @param {Number} ms 等待毫秒数
     * @return Promise
     */
    static wait(ms = 100) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, ms);
        });
    }
    ;
    /**
     * 交换数组下标位置
     * @param arr
     * @param index1
     * @param index2
     */
    static arraySwap(arr, index1, index2) {
        if (index1 < 0 || index1 >= arr.length) {
            return arr;
        }
        if (index2 < 0 || index2 >= arr.length) {
            return arr;
        }
        if (index1 === index2) {
            return arr;
        }
        arr[index1] = arr.splice(index2, 1, arr[index1])[0];
        return arr;
    }
    /**
     * 获取当天时间
     * @return {String} yyyy-MM-dd
     */
    static getTodayDates() {
        let date = new Date();
        let yyyy = date.getFullYear();
        let MM = date.getMonth() + 1;
        let dd = date.getDate();
        if (MM < 10) {
            MM = "0" + MM;
        }
        if (dd < 10) {
            dd = "0" + dd;
        }
        return yyyy + "-" + MM + "-" + dd;
    }
    /**
     * * 获取上一月时间
     */
    static getNextDate() {
        let date = new Date();
        let yyyy = date.getFullYear();
        let MM = date.getMonth();
        let dd = date.getDate();
        if (MM < 10) {
            MM = "0" + MM;
        }
        if (dd < 10) {
            dd = "0" + dd;
        }
        return yyyy + "-" + MM + "-" + dd;
    }
    /**
     * 在数字前面补0
     * @param num
     * @param digits
     */
    static fillZero(num, digits = 4) {
        // 兼容数字0
        if (this.isEmpty(num)) {
            num = "";
        }
        for (let i = 1; i <= digits; i++) {
            if (num < Math.pow(10, i - 1)) {
                num = "0" + num;
            }
        }
        return num;
    }
    /**
     * 货币格式化
     */
    static moneyFormat(num) {
        // 整数部分
        let integer = Number(num);
        let decimals = Number((Number(num) - integer) * 100);
        decimals = CommonUtil.fillZero(decimals, 2);
        let str = (integer + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return str + "." + decimals;
    }
    ;
    /**
     * 将数组顺序反转
     */
    static reverse(a) {
        let len = a.length; //获取数组的长度
        if (len === 0) {
            return [];
        }
        let mid = Number(len / 2); //获取数组长度的中间值，由于有可能是不是整数，将得到的数字转为整数
        for (let i = 0; i <= mid; i++) {
            let temp = a[i];
            a[i] = a[len - 1 - i];
            a[len - 1 - i] = temp;
        }
        return a;
    }
}
//# sourceMappingURL=CommonUtil.js.map