import CommonUtil from "@/utils/CommonUtil";
/**
 * 时间工具
 */
export default class TimeUtil {
    /**
     * 获取当前时间戳。单位：秒
     */
    static timestamp() {
        return Math.floor(new Date().getTime() / 1000);
    }
    /**
     * 将字符串、时间戳等转成日期对象（兼容个浏览器）
     * @param datetime 字符串
     */
    static getDate(datetime = undefined) {
        if (datetime === undefined) {
            return new Date();
        }
        return new Date(datetime.replace(/-/g, "/"));
    }
    /**
     * 获取当前时间
     * @param date
     */
    static dateObjToDate(date) {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        let day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        return year + "-" + month + "-" + day;
    }
    /**
     * 获取多少天之前的日期
     * @param date 原日期
     * @param days date 多少天之前
     */
    static getDateBeforeDays(date, days = 1) {
        let timestamp = TimeUtil.datetimeToTimestamp(date) - days * 86400;
        return TimeUtil.datetimeToDate(TimeUtil.timestampToTime(timestamp));
    }
    /**
     * 获取多少天之后的日期
     * @param date 原日期
     * @param days date 多少天之后
     */
    static getDateAfterDays(date, days = 1) {
        let timestamp = TimeUtil.datetimeToTimestamp(date) + days * 86400;
        return TimeUtil.datetimeToDate(TimeUtil.timestampToTime(timestamp));
    }
    /**
     * 当前时间
     * @param isMin 是否整分钟
     */
    static now(isMin = false) {
        let timestamp = TimeUtil.timestamp();
        if (isMin) {
            timestamp = timestamp - timestamp % 60;
        }
        return TimeUtil.timestampToTime(timestamp);
    }
    /**
     * 今天
     */
    static today() {
        return TimeUtil.dateObjToDate(TimeUtil.getDate());
    }
    /**
     * 昨天
     */
    static yesterday() {
        return TimeUtil.getDateBeforeDays(TimeUtil.today(), 1);
    }
    /**
     * 时间戳转时间字符串
     * @param timestamp 时间戳（单位：秒）
     */
    static timestampToTime(timestamp) {
        let date = new Date(timestamp * 1000);
        let yyyy = date.getFullYear();
        let MM = CommonUtil.fillZero(date.getMonth() + 1, 2);
        let dd = CommonUtil.fillZero(date.getDate(), 2);
        let HH = CommonUtil.fillZero(date.getHours(), 2);
        let mm = CommonUtil.fillZero(date.getMinutes(), 2);
        let ss = CommonUtil.fillZero(date.getSeconds(), 2);
        return yyyy + "-" + MM + "-" + dd + " " + HH + ":" + mm + ":" + ss;
    }
    /**
     * 时间转日期（去除时分秒部分）
     * @param datetime
     */
    static datetimeToDate(datetime) {
        let tmpArr = datetime.split(" ");
        return tmpArr[0];
    }
    /**
     * 日期字符串转时间戳（秒）
     * @param datetime （支持日期）
     */
    static datetimeToTimestamp(datetime) {
        return Math.floor(this.getDate(datetime).getTime() / 1000);
    }
}
//# sourceMappingURL=TimeUtil.js.map