import * as tslib_1 from "tslib";
import { Component, Vue } from 'vue-property-decorator';
import C from "@/constants/C";
import OptVo from "@/models/vos/OptVo";
import TableVo from "@/models/vos/TableVo";
import { ConstStore } from "@/store/modules/ConstStore";
import WalletApi from "@/api/WalletApi";
import GaCountdown from "@/components/Ga/GaCountdown.vue";
/**
 * 账号表格数据封装
 */
let WalletTable = class WalletTable extends Vue {
    /**
     * 账号表格数据封装
     */
    constructor() {
        super(...arguments);
        /**
         * 表格数据
         */
        this.table = new TableVo({
            orderNo: "",
            orderType: "",
            orderState: 1,
            isShowEnter: false,
        });
        /**
         * 提款信息是否加载中
         */
        this.withdrawVoLoading = false;
        /**
         * 提款信息是否显示
         */
        this.withdrawVoVisible = false;
        /**
         * 提款信息详细内容
         */
        this.withdrawVo = {
            orderWalletId: 0,
            orderNo: "",
            bankNo: "",
            bankName: "",
        };
    }
    isShowFH(state) {
        console.log(state);
        return state;
    }
    /**
     *获取订单类型
     */
    get walletOrderTypeLabels() {
        return OptVo.loadListByLabels(ConstStore.getWalletOrderTypeLabels, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     *获取订单状态
     */
    get walletOrderStatusLabels() {
        return OptVo.loadListByLabels(ConstStore.getWalletOrderStatusLabels, C.OPT_VALUE_TYPE_NUMBER);
    }
    /**
     * 渲染后执行的方法
     */
    mounted() {
        this.table.params.order_state = { 1: "待发货" };
        this.requestWalletTable();
    }
    /**
     * 请求表格数据
     */
    async requestWalletTable(resetPage = false) {
        this.table.loading = true;
        if (resetPage) {
            this.table.page = 1;
        }
        let vo = await WalletApi.getWalletOrderList(this.table.getParams());
        console.log(vo);
        this.table.setByTableDataVo(vo);
        this.table.loading = false;
    }
    onSizeChange(size) {
        this.table.size = size;
        this.requestWalletTable();
    }
    onPageChange(page) {
        this.table.page = page;
        this.requestWalletTable();
    }
    /**
     * 确认完成
     */
    async onClickFH(id) {
        if (!id) {
            return;
        }
        let isDone = await WalletApi.updateWalletState({ 'id': id });
        this.table.loading = true;
        if (isDone) {
            await this.requestWalletTable();
        }
        this.table.loading = false;
    }
};
WalletTable = tslib_1.__decorate([
    Component({
        components: { GaCountdown }
    })
], WalletTable);
export default WalletTable;
//# sourceMappingURL=order.vue.js.map