/**
 * 前端通用常量定义
 */
import {MessageType} from "element-ui/types/message";
import {NotificationPosition} from "element-ui/types/notification";
import HttpUtil from "../utils/HttpUtil";
import CommonUtil from "../utils/CommonUtil";
import OptVo from "@/models/vos/OptVo";

export default class C {
    /**
     * 表单类型：添加
     */
    public static readonly FORM_TYPE_ADD: number = 1;
    /**
     * 表单类型：修改
     */
    public static readonly FORM_TYPE_MOD: number = 2;
    /**
     * 表单类型：初始化
     */
    public static readonly FORM_TYPE_INIT: number = 3;
    /**
     * 表单类型：登录
     */
    public static readonly FORM_TYPE_LOGIN: number = 4;
    /**
     * 表单类型：保存
     */
    public static readonly FORM_TYPE_SAVE: number = 5;
    /**
     * 表单类型：提交（确认提交数据）
     */
    public static readonly FORM_TYPE_SUBMIT: number = 6;
    /**
     * 表单类型：加/扣款
     */
    public static readonly FORM_TYPE_CREDIT: number = 7;

    /**
     * 表单类型：修改银行信息
     */
    public static readonly FORM_TYPE_BANK: number = 9;
    /**
     * 修改类型：信用额增加
     */
    public static readonly UPDATE_CREDIT_TYPE_ADD: number = 1;
    /**
     * 修改类型：信用额减少
     */
    public static readonly UPDATE_CREDIT_TYPE_REDUCE: number = 2;

    /**
     * 消息类型：成功
     */
    public static readonly MESSAGE_TYPE_SUCCESS: MessageType = "success";
    /**
     * 消息类型：警告
     */
    public static readonly MESSAGE_TYPE_WARNING: MessageType = "warning";
    /**
     * 消息类型：信息（默认）
     */
    public static readonly MESSAGE_TYPE_INFO: MessageType = "info";
    /**
     * 消息类型：错误
     */
    public static readonly MESSAGE_TYPE_ERROR: MessageType = "error";

    /**
     * 通知位置：上右
     */
    public static readonly NOTIFICATION_POSITION_TOP_RIGHT: NotificationPosition = "top-right";
    /**
     * 通知位置：上左
     */
    public static readonly NOTIFICATION_POSITION_TOP_LEFT: NotificationPosition = "top-left";
    /**
     * 通知位置：下右
     */
    public static readonly NOTIFICATION_POSITION_BOTTOM_RIGHT: NotificationPosition = "bottom-right";
    /**
     * 通知位置：下左
     */
    public static readonly NOTIFICATION_POSITION_BOTTOM_LEFT: NotificationPosition = "bottom-left";

    /**
     * 用户类型：会员
     */
    public static readonly USER_TYPE_MEMBER = 1;
    /**
     * 用户类型：代理
     */
    public static readonly USER_TYPE_AGENT = 2;
    /**
     * 用户类型：总代理
     */
    public static readonly USER_TYPE_VENDOR = 3;
    /**
     * 用户类型：股东
     */
    public static readonly USER_TYPE_HOLDER = 4;
    /**
     * 用户类型：大股东
     */
    public static readonly USER_TYPE_SOLDER = 5;
    /**
     * 用户类型：管理员
     */
    public static readonly USER_TYPE_ADMIN = 99;

    /**
     * 选项数据类型：数字
     */
    public static readonly OPT_VALUE_TYPE_NUMBER = 1;
    /**
     * 选项数据类型：字符串
     */
    public static readonly OPT_VALUE_TYPE_STRING = 2;
    /**
     * 选项数据类型：默认（原来是什么就是什么）
     */
    public static readonly OPT_VALUE_TYPE_DEFAULT = 3;

    /**
     * 响应代码：未登录
     */
    public static readonly RESPONSE_CODE_NOT_LOGIN = -3;

    /**
     * 平台类型：微信
     */
    public static readonly PLAT_TYPE_WX = 1;

    /**
     * 支付类型：微信-个人商业版扫码
     */
    public static readonly PAY_TYPE_SY_MANUAL = 102;

    /**
     * 文件夹类型：商户二维码
     */
    public static readonly DIR_TYPE_MEMBER_QR_CODE = 1;

    /**
     * 是否常量：是
     */
    public static readonly TRUE = 1;
    /**
     * 是否常量：否
     */
    public static readonly FALSE = 0;

    /**
     * 钱包存提款状态：待处理（默认状态）
     */
    public static readonly ORDER_WALLET_STATE_WAIT = 1;
    /**
     * 钱包存提款状态：已完成（管理员确认完成）
     */
    public static readonly ORDER_WALLET_STATE_DONE = 2;
    /**
     * 钱包存提款状态：已取消（用户点击取消）
     */
    public static readonly ORDER_WALLET_STATE_CANCEL = 3;
    /**
     * 钱包存提款状态：已失败（管理员点击失败）
     */
    public static readonly ORDER_WALLET_STATE_FAIL = 4;

    /**
     * 标签数据map
     */
    private static constantMap: { [key: string]: any } | null = null;

    /**
     * 注单状态：中奖
     */
    public static readonly BET_STATUS_LABEL_WIN = "已派奖";
    /**
     * 注单状态：未中奖
     */
    public static readonly BET_STATUS_LABEL_LOSE = "未中奖";

    /**
     * 报表类型1:盈亏报表
     */
    public static readonly REPORT_TYPE_INOUT = "1";

    /**
     * 报表类型2:分类报表
     */
    public static readonly REPORT_TYPE_CATEGORY = "2";

    /**
     * 初始化系统常量
     */
    public static initConstantMap() {
        HttpUtil.post("const/get").then((vo) => {
            if (vo.isDone()) {
                C.constantMap = vo.d;
            } else {
                console.error(vo.message);
            }
        }).catch((e) => {
            C.constantMap = {};
            console.error("系统常量初始化失败");
            console.error(e);
        });
    }

    /**
     * 获取常量
     * @param prop
     */
    public static getConstant(prop: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            // 如果数据还没获取，则等待一段时间后再次获取
            if (C.constantMap === null) {
                CommonUtil.wait().then(() => {
                    C.getConstant(prop).then(resolve).then(reject);
                });
                return;
            }

            let labels = C.constantMap.hasOwnProperty(prop) ? C.constantMap[prop] : [];
            resolve(CommonUtil.copy(labels));
        });
    }

    /**
     * 获取是否的选项数据
     */
    public static getIsNotOpts() {
        let labels: {[key: number]: string} = {};
        labels[C.TRUE] = "是";
        labels[C.FALSE] = "否";
        let opts = OptVo.loadListByLabels(labels, C.OPT_VALUE_TYPE_NUMBER);
        CommonUtil.arraySwap(opts, 0, 1);
        return opts;
    }
}
