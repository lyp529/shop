import HttpUtil from "../utils/HttpUtil";
import CommonUtil from "../utils/CommonUtil";
import OptVo from "@/models/vos/OptVo";
export default class C {
    /**
     * 初始化系统常量
     */
    static initConstantMap() {
        HttpUtil.post("const/get").then((vo) => {
            if (vo.isDone()) {
                C.constantMap = vo.d;
            }
            else {
                console.error(vo.message);
            }
        }).catch((e) => {
            C.constantMap = {};
            console.error("系统常量初始化失败");
            console.error(e);
        });
    }
    /**
     * 获取常量
     * @param prop
     */
    static getConstant(prop) {
        return new Promise((resolve, reject) => {
            // 如果数据还没获取，则等待一段时间后再次获取
            if (C.constantMap === null) {
                CommonUtil.wait().then(() => {
                    C.getConstant(prop).then(resolve).then(reject);
                });
                return;
            }
            let labels = C.constantMap.hasOwnProperty(prop) ? C.constantMap[prop] : [];
            resolve(CommonUtil.copy(labels));
        });
    }
    /**
     * 获取是否的选项数据
     */
    static getIsNotOpts() {
        let labels = {};
        labels[C.TRUE] = "是";
        labels[C.FALSE] = "否";
        let opts = OptVo.loadListByLabels(labels, C.OPT_VALUE_TYPE_NUMBER);
        CommonUtil.arraySwap(opts, 0, 1);
        return opts;
    }
}
/**
 * 表单类型：添加
 */
C.FORM_TYPE_ADD = 1;
/**
 * 表单类型：修改
 */
C.FORM_TYPE_MOD = 2;
/**
 * 表单类型：初始化
 */
C.FORM_TYPE_INIT = 3;
/**
 * 表单类型：登录
 */
C.FORM_TYPE_LOGIN = 4;
/**
 * 表单类型：保存
 */
C.FORM_TYPE_SAVE = 5;
/**
 * 表单类型：提交（确认提交数据）
 */
C.FORM_TYPE_SUBMIT = 6;
/**
 * 表单类型：加/扣款
 */
C.FORM_TYPE_CREDIT = 7;
/**
 * 表单类型：修改银行信息
 */
C.FORM_TYPE_BANK = 9;
/**
 * 修改类型：信用额增加
 */
C.UPDATE_CREDIT_TYPE_ADD = 1;
/**
 * 修改类型：信用额减少
 */
C.UPDATE_CREDIT_TYPE_REDUCE = 2;
/**
 * 消息类型：成功
 */
C.MESSAGE_TYPE_SUCCESS = "success";
/**
 * 消息类型：警告
 */
C.MESSAGE_TYPE_WARNING = "warning";
/**
 * 消息类型：信息（默认）
 */
C.MESSAGE_TYPE_INFO = "info";
/**
 * 消息类型：错误
 */
C.MESSAGE_TYPE_ERROR = "error";
/**
 * 通知位置：上右
 */
C.NOTIFICATION_POSITION_TOP_RIGHT = "top-right";
/**
 * 通知位置：上左
 */
C.NOTIFICATION_POSITION_TOP_LEFT = "top-left";
/**
 * 通知位置：下右
 */
C.NOTIFICATION_POSITION_BOTTOM_RIGHT = "bottom-right";
/**
 * 通知位置：下左
 */
C.NOTIFICATION_POSITION_BOTTOM_LEFT = "bottom-left";
/**
 * 用户类型：会员
 */
C.USER_TYPE_MEMBER = 1;
/**
 * 用户类型：代理
 */
C.USER_TYPE_AGENT = 2;
/**
 * 用户类型：总代理
 */
C.USER_TYPE_VENDOR = 3;
/**
 * 用户类型：股东
 */
C.USER_TYPE_HOLDER = 4;
/**
 * 用户类型：大股东
 */
C.USER_TYPE_SOLDER = 5;
/**
 * 用户类型：管理员
 */
C.USER_TYPE_ADMIN = 99;
/**
 * 选项数据类型：数字
 */
C.OPT_VALUE_TYPE_NUMBER = 1;
/**
 * 选项数据类型：字符串
 */
C.OPT_VALUE_TYPE_STRING = 2;
/**
 * 选项数据类型：默认（原来是什么就是什么）
 */
C.OPT_VALUE_TYPE_DEFAULT = 3;
/**
 * 响应代码：未登录
 */
C.RESPONSE_CODE_NOT_LOGIN = -3;
/**
 * 平台类型：微信
 */
C.PLAT_TYPE_WX = 1;
/**
 * 支付类型：微信-个人商业版扫码
 */
C.PAY_TYPE_SY_MANUAL = 102;
/**
 * 文件夹类型：商户二维码
 */
C.DIR_TYPE_MEMBER_QR_CODE = 1;
/**
 * 是否常量：是
 */
C.TRUE = 1;
/**
 * 是否常量：否
 */
C.FALSE = 0;
/**
 * 钱包存提款状态：待处理（默认状态）
 */
C.ORDER_WALLET_STATE_WAIT = 1;
/**
 * 钱包存提款状态：已完成（管理员确认完成）
 */
C.ORDER_WALLET_STATE_DONE = 2;
/**
 * 钱包存提款状态：已取消（用户点击取消）
 */
C.ORDER_WALLET_STATE_CANCEL = 3;
/**
 * 钱包存提款状态：已失败（管理员点击失败）
 */
C.ORDER_WALLET_STATE_FAIL = 4;
/**
 * 标签数据map
 */
C.constantMap = null;
/**
 * 注单状态：中奖
 */
C.BET_STATUS_LABEL_WIN = "已派奖";
/**
 * 注单状态：未中奖
 */
C.BET_STATUS_LABEL_LOSE = "未中奖";
/**
 * 报表类型1:盈亏报表
 */
C.REPORT_TYPE_INOUT = "1";
/**
 * 报表类型2:分类报表
 */
C.REPORT_TYPE_CATEGORY = "2";
//# sourceMappingURL=C.js.map