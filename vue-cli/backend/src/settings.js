// You can customize below settings :)
const settings = {
    title: '易排健康管理系统',
    showSettings: true,
    showTagsView: true,
    fixedHeader: false,
    showSidebarLogo: false,
    errorLog: ['production'],
    sidebarTextTheme: true,
    devServerPort: 9527,
    mockServerPort: 9528
};
export default settings;
//# sourceMappingURL=settings.js.map