/**
 * 可json实例化
 */
export default interface JsonInstantiable<T> {
    load(values: {[key: string]: any}): T;
}
