import BaseVo from "@/models/vos/BaseVo";
import JsonInstantiable from "@/models/JsonInitabled";

/**
 * 首页数据
 */
export default class HomeVo extends BaseVo implements JsonInstantiable<HomeVo> {
    /**
     * 本次登录时间
     */
    public loginTime: string = "";
    /**
     * 本次登录IP
     */
    public loginIp: string = "";
    /**
     * 上次登录时间
     */
    public lastLoginTime: string = "";
    /**
     * 上次登录IP
     */
    public lastLoginIp: string = "";


    /**
     * json 加载对象数据
     * @param values
     */
    load(values: { [p: string]: any }): HomeVo {
        this.loginTime = values.loginTime;
        this.loginIp = values.loginIp;
        this.lastLoginTime = values.lastLoginTime;
        this.lastLoginIp = values.lastLoginIp;
        return this;
    }

}
