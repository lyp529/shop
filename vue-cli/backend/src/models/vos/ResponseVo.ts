/**
 * 返回参数数据封装
 */
import WindowUtil from "@/utils/WindowUtil";

export default class ResponseVo {
    /**
     * 返回代码
     */
    public c: number = 0;
    /**
     * 返回消息
     */
    public m: string = "";
    /**
     * 返回数据
     */
    public d: any = {};

    /**
     * 使用json数据初始化对象
     * @param params
     */
    public constructor(params: {[key: string]: any} = {}) {
        if (params.hasOwnProperty("c")) {
            this.c = params["c"];
        }
        if (params.hasOwnProperty("m")) {
            this.m = params["m"];
        }
        if (params.hasOwnProperty("d")) {
            this.d = params["d"];
        }
    }

    /**
     * 是否处理成功
     */
    public isDone(): boolean {
        return this.c > 0;
    }

    /**
     * 获取消息
     */
    get message(): string {
        return this.m;
    }

    /**
     * 获取数据
     * @param prop
     */
    public data(prop: string): any {
        if (this.d.hasOwnProperty(prop)) {
            return this.d[prop];
        }
        return null;
    }

    /**
     * 通用结果处理
     */
    public commonHandler(): boolean {
        if (this.isDone()) {
            WindowUtil.notifySuccess(this.message);
            return true;
        }else {
            WindowUtil.messageError(this.message);
            return false;
        }
    }
}
