import BaseVo from "./BaseVo";
import C from "../../constants/C";
import CommonUtil from "@/utils/CommonUtil";

/**
 * 基础表单
 */
export default class FormVo extends BaseVo {
    /**
     * 是否加载中
     */
    public loading: boolean = false;
    /**
     * 是否可见
     */
    public visible: boolean = false;
    /**
     * 第二弹窗是否可见
     */
    public secondVisible: boolean = false;
    /**
     * 表单类型
     */
    public type: number = C.FORM_TYPE_ADD;
    /**
     * 表格需要提交的参数封装
     */
    public params: {[key: string]: any} = {};
    /**
     * 空参数。用于清空参数
     */
    public emptyParams: {[key: string]: any} = {};

    /**
     * 初始化对象
     */
    public constructor(params: {[key: string]: any}) {
        super();
        this.params = params;
        this.emptyParams = CommonUtil.copy(params);
    }

    /**
     * 设置表单类型
     * @param type
     */
    public setType(type: number): FormVo {
        this.type = type;
        return this;
    }

    /**
     * 重置表单数据
     */
    public reset() {
        this.params = CommonUtil.copy(this.emptyParams);
    }
}
