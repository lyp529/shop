/**
 * 表格数据封装
 */
import BaseVo from "@/models/vos/BaseVo";
import CommonUtil from "@/utils/CommonUtil";
import ResponseVo from "@/models/vos/ResponseVo";
import TableDataVo from "@/models/vos/TableDataVo";


export default class TableVo extends BaseVo {
    /**
     * 是否加载中
     */
    public loading: boolean = false;
    /**
     * 表格数据
     */
    public rows: any[] = [];
    /**
     * 表格总数据条数
     */
    public count: number = 0;
    /**
     * 页码
     */
    public page: number = 1;
    /**
     * 每页大小
     */
    public size: number = 10;
    /**
     * 表格需要提交的参数封装
     */
    public params: {[key: string]: any} = {};

    /**
     * 构造函数
     * @param params
     */
    public constructor(params: {[key: string]: any} = {}) {
        super();
        this.params = params;
    }

    /**
     * 获取参数（包含页码数据）
     */
    public getParams(): {[key: string]: any} {
        let params: {[key: string]: any} = CommonUtil.copy(this.params);
        params.page = this.page;
        params.size = this.size;
        return params;
    }

    /**
     * 使用返回数据快速赋值。
     * 要求：responseVo 中数据必须有 rows 和 count 字段
     * @param vo
     */
    public resetByResponseVo(vo: ResponseVo) {
        this.rows = vo.data("rows");
        this.count = Number(vo.data("count"));
    }

    /**
     * 使用表格数据初始化表格
     * @param tableDataVo
     */
    public setByTableDataVo(tableDataVo: TableDataVo) {
        this.rows = tableDataVo.rows;
        this.count = tableDataVo.count;
    }

    /**
     * 清空表格数据
     */
    public clearRows(){
        this.rows = [];
    }
}
