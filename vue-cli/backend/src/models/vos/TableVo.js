/**
 * 表格数据封装
 */
import BaseVo from "@/models/vos/BaseVo";
import CommonUtil from "@/utils/CommonUtil";
export default class TableVo extends BaseVo {
    /**
     * 构造函数
     * @param params
     */
    constructor(params = {}) {
        super();
        /**
         * 是否加载中
         */
        this.loading = false;
        /**
         * 表格数据
         */
        this.rows = [];
        /**
         * 表格总数据条数
         */
        this.count = 0;
        /**
         * 页码
         */
        this.page = 1;
        /**
         * 每页大小
         */
        this.size = 10;
        /**
         * 表格需要提交的参数封装
         */
        this.params = {};
        this.params = params;
    }
    /**
     * 获取参数（包含页码数据）
     */
    getParams() {
        let params = CommonUtil.copy(this.params);
        params.page = this.page;
        params.size = this.size;
        return params;
    }
    /**
     * 使用返回数据快速赋值。
     * 要求：responseVo 中数据必须有 rows 和 count 字段
     * @param vo
     */
    resetByResponseVo(vo) {
        this.rows = vo.data("rows");
        this.count = Number(vo.data("count"));
    }
    /**
     * 使用表格数据初始化表格
     * @param tableDataVo
     */
    setByTableDataVo(tableDataVo) {
        this.rows = tableDataVo.rows;
        this.count = tableDataVo.count;
    }
    /**
     * 清空表格数据
     */
    clearRows() {
        this.rows = [];
    }
}
//# sourceMappingURL=TableVo.js.map