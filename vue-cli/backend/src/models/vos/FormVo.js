import BaseVo from "./BaseVo";
import C from "../../constants/C";
import CommonUtil from "@/utils/CommonUtil";
/**
 * 基础表单
 */
export default class FormVo extends BaseVo {
    /**
     * 初始化对象
     */
    constructor(params) {
        super();
        /**
         * 是否加载中
         */
        this.loading = false;
        /**
         * 是否可见
         */
        this.visible = false;
        /**
         * 第二弹窗是否可见
         */
        this.secondVisible = false;
        /**
         * 表单类型
         */
        this.type = C.FORM_TYPE_ADD;
        /**
         * 表格需要提交的参数封装
         */
        this.params = {};
        /**
         * 空参数。用于清空参数
         */
        this.emptyParams = {};
        this.params = params;
        this.emptyParams = CommonUtil.copy(params);
    }
    /**
     * 设置表单类型
     * @param type
     */
    setType(type) {
        this.type = type;
        return this;
    }
    /**
     * 重置表单数据
     */
    reset() {
        this.params = CommonUtil.copy(this.emptyParams);
    }
}
//# sourceMappingURL=FormVo.js.map