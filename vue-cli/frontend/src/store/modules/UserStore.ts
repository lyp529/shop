import {VuexModule, Module, Action, Mutation, getModule} from 'vuex-module-decorators'
import store from '@/store/index'

export interface IUserState {

}

@Module({dynamic: true, store, name: 'user'})
class userStore extends VuexModule implements IUserState {

}

export const UserStore = getModule(userStore)
