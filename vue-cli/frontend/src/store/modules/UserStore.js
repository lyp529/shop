import * as tslib_1 from "tslib";
import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators';
import store from '@/store/index';
import UserApi from "@/api/UserApi";
import CookieUtil from "@/utils/CookieUtil";
import C from "@/constants/C";
import CommonUtil from "@/utils/CommonUtil";
import WindowUtil from "@/utils/WindowUtil";
import A from "@/constants/A";
import ActivityApi from "@/api/ActivityApi";
import SystemVo from "@/models/vos/SystemVo";
let userStore = class userStore extends VuexModule {
    constructor() {
        super(...arguments);
        /**
         * 用户id
         */
        this.userId = 0;
        /**
         * 登录账号
         */
        this.username = "";
        /**
         * 用户名
         */
        this.name = "";
        /**
         * 账户余额（信用额度）
         */
        this.credit = 0;
        /**
         * 积分
         */
        this.integral = 0;
        /**
         * vip等级
         */
        this.vipLevel = 0;
        /**
         * 前一天的vip等级
         */
        this.beforeVipLevel = 0;
        /**
         * 前一天零点账户余额
         */
        this.beforeCredit = 0;
        /**
         * 每天下单金额返回百分比
         */
        this.backPercent = "";
        /**
         * 昨天下单返利
         */
        this.placeOrderRebated = "";
        /**
         * 今天预计下单返利
         */
        this.todayPredictRebate = "";
        /**
         * vip等级名称
         */
        this.vipName = "";
        /**
         * @var int 提现剩余次数
         */
        this.withdrawRestTimes = 0;
        /**
         * 登录后自动跳转的路由
         */
        this.afterLoginPath = "/";
        /**
         * 是否正在登录
         */
        this.logging = false;
        /**
         * 是否已经设置了资金密码
         */
        this.hasCashPassword = false;
        /**
         *是否签到
         */
        this.isSign = false;
        /**
         * 设置签到金额
         */
        this.signAmount = "";
        this.bankId = 0;
        this.bankName = "";
        this.bankAccount = "";
        this.bankUsername = "";
        this.bankEmail = "";
        /**
         * 系统中需要动态刷新的数据
         */
        this.systemVo = new SystemVo();
    }
    /**
     * 用户是否登录
     */
    get isLogin() {
        return this.userId !== undefined && this.userId > 0;
    }
    /**
     * vip图片路径
     */
    get vipImg() {
        if (this.vipLevel == 1) {
            return A.IMG_VIP1;
        }
        else if (this.vipLevel == 2) {
            return A.IMG_VIP2;
        }
        else if (this.vipLevel == 3) {
            return A.IMG_VIP3;
        }
        else if (this.vipLevel == 4) {
            return A.IMG_VIP4;
        }
        else if (this.vipLevel == 5) {
            return A.IMG_VIP5;
        }
        else {
            return A.IMG_VIP0;
        }
    }
    /**
     * 设置用户id
     * @param userId
     */
    setUserId(userId) {
        this.userId = userId;
    }
    /**
     * 设置是否有签到
     * @param signDate
     */
    setIsSign(signDate) {
        this.isSign = signDate;
    }
    /**
     * 设置签到获得金额
     * @param signAmount
     */
    setSignAmount(signAmount) {
        this.signAmount = signAmount;
    }
    /**
     * 设置登录账号
     * @param username
     */
    setUsername(username) {
        this.username = username;
    }
    /**
     * 设置用户名
     * @param name
     */
    setName(name) {
        this.name = name;
    }
    /**
     * 设置信用额度
     * @param credit
     */
    setCredit(credit) {
        this.credit = credit;
    }
    /**
     * 设置信用额度
     * @param integral
     */
    setIntegral(integral) {
        this.integral = integral;
    }
    /**
     * 设置VIP等级
     * @param vipLevel
     */
    setVipLevel(vipLevel) {
        this.vipLevel = vipLevel;
    }
    /**
     * 设置前一天的VIP等级
     * @param beforeVipLevel
     */
    setBeforeVipLevel(beforeVipLevel) {
        this.beforeVipLevel = beforeVipLevel;
    }
    /**
     * 设置今日下单返利比例
     * @param backPercent
     */
    setBackPercent(backPercent) {
        this.backPercent = backPercent;
    }
    /**
     * 设置昨天下单返利
     * @param placeOrderRebated
     */
    setPlaceOrderRebated(placeOrderRebated) {
        this.placeOrderRebated = placeOrderRebated;
    }
    /**
     * 设置今天预计下单返利
     * @param todayPredictRebate
     */
    setTodayPredictRebate(todayPredictRebate) {
        this.todayPredictRebate = todayPredictRebate;
    }
    /**
     * 设置昨日零点前账户余额
     * @param beforeCredit
     */
    seBeforeCredit(beforeCredit) {
        this.beforeCredit = beforeCredit;
    }
    /**
     * 设置帐户VIP名称
     * @param vipName
     */
    setVipName(vipName) {
        this.vipName = vipName;
    }
    /**
     * 设置登录后自动跳转的路径
     * @param path
     */
    setAfterLoginPath(path) {
        this.afterLoginPath = path;
    }
    setBankId(bankId) {
        this.bankId = bankId;
    }
    setBankName(bankName) {
        this.bankName = bankName;
    }
    setBankAccount(bankAccount) {
        this.bankAccount = bankAccount;
    }
    setBankUsername(bankUsername) {
        this.bankUsername = bankUsername;
    }
    setBankEmail(bankEmail) {
        this.bankEmail = bankEmail;
    }
    /**
     * 设置是否正在登录
     * @param logging
     */
    setLogging(logging) {
        this.logging = logging;
        if (this.logging) {
            WindowUtil.toastLoading("登录中...");
        }
        else {
            WindowUtil.toastClear();
        }
    }
    /**
     * 设置是否正在登录
     * @param hasCashPassword
     */
    setHasCashPassword(hasCashPassword) {
        this.hasCashPassword = hasCashPassword;
    }
    /**
     * 设置提现剩余次数
     * @param withdrawRestTimes
     */
    setWithdrawRestTimes(withdrawRestTimes) {
        this.withdrawRestTimes = withdrawRestTimes;
    }
    /**
     * 设置系统数据
     * @param systemVo
     */
    setSystemVo(systemVo) {
        this.systemVo = systemVo;
    }
    /**
     * 登录
     * @param params
     */
    async login(params) {
        let sessionId = await UserApi.login(params);
        if (CommonUtil.isEmpty(sessionId)) {
            return;
        }
        CookieUtil.set(C.COOKIE_KEY_SESSION_ID, sessionId);
        await this.requestUserVo();
    }
    /**
     * 登出
     */
    async logout() {
        let done = await UserApi.logout();
        if (!done) {
            return;
        }
        this.setUserId(0);
        CookieUtil.set(C.COOKIE_KEY_SESSION_ID, "");
    }
    /**
     * 获取用户数据
     */
    async requestUserVo() {
        let userVo = await UserApi.getUserVo();
        this.setUserId(userVo.userId);
        this.setUsername(userVo.username);
        this.setName(userVo.name);
        this.setCredit(userVo.credit);
        this.setVipLevel(userVo.vipLevel);
        this.setIntegral(userVo.integral);
        this.setHasCashPassword(userVo.hasCashPassword);
        this.setWithdrawRestTimes(userVo.withdrawRestTimes);
        this.setVipName(userVo.vipName);
        this.setIsSign(userVo.isSign);
        this.setBankId(userVo.bankId);
        this.setBankName(userVo.bankName);
        this.setBankUsername(userVo.bankUsername);
        this.setBankAccount(userVo.bankAccount);
        this.setBankEmail(userVo.bankEmail);
        this.setBeforeVipLevel(userVo.beforeVipLevel);
        this.setBackPercent(userVo.backPercent);
        this.setPlaceOrderRebated(userVo.placeOrderRebated);
        this.setTodayPredictRebate(userVo.todayPredictRebate);
        this.seBeforeCredit(userVo.beforeCredit);
    }
    /**
     * 使用 session id 登录
     */
    async loginBySessionId() {
        this.setUserId(0);
        let sessionId = CookieUtil.get(C.COOKIE_KEY_SESSION_ID);
        if (CommonUtil.isEmpty(sessionId)) {
            CookieUtil.set(C.COOKIE_KEY_SESSION_ID, sessionId);
            return;
        }
        let done = await UserApi.loginBySessionId({ sessionId: sessionId });
        if (!done) {
            CookieUtil.set(C.COOKIE_KEY_SESSION_ID, "");
            return;
        }
        CookieUtil.set(C.COOKIE_KEY_SESSION_ID, sessionId);
        await this.requestUserVo();
    }
    /**
     * 签到
     */
    async signIn() {
        let signAmount = await ActivityApi.SignIn();
        if (parseFloat(signAmount) > 0) {
            this.setIsSign(true);
            this.setSignAmount(signAmount);
        }
        await this.requestUserVo();
    }
    /**
     * 请求系统中定时更新的数据
     */
    async requestSystemVo() {
        if (!this.isLogin) {
            return;
        }
        let json = await UserApi.getSystemVo();
        if (json === null) {
            return;
        }
        this.setCredit(json.credit);
        this.setIntegral(json.integral);
        let systemVo = new SystemVo().load(json);
        this.setSystemVo(systemVo);
    }
};
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setUserId", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setIsSign", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setSignAmount", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setUsername", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setName", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setCredit", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setIntegral", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setVipLevel", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBeforeVipLevel", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBackPercent", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setPlaceOrderRebated", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setTodayPredictRebate", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "seBeforeCredit", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setVipName", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setAfterLoginPath", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBankId", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBankName", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBankAccount", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBankUsername", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setBankEmail", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setLogging", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setHasCashPassword", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setWithdrawRestTimes", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setSystemVo", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "login", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "logout", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "requestUserVo", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "loginBySessionId", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "signIn", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "requestSystemVo", null);
userStore = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'user' })
], userStore);
export const UserStore = getModule(userStore);
//# sourceMappingURL=UserStore.js.map