
interface ICommonScss {
    MaxWidth: string;
    ColorBgDark: string;
    ColorBg: string;
    ColorBgLight: string;
    ColorTextCommon: string;
    ColorTextLight: string;
    ColorTextLight2: string;
    ColorPrimary: string;
    ColorSuccess: string;
    ColorWarning: string;
    ColorDanger: string;
}

export const CommonScss: ICommonScss;
export default CommonScss;