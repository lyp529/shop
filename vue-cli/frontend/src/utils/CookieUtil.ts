/**
 * cookie 工具
 */
import * as Cookies from "js-cookie";

export default class CookieUtil {
    /**
     * 获取值
     * @param key
     */
    public static get(key: string): string {
        let value = Cookies.get(key);
        if (value === undefined) {
            value = "";
        }
        return value;
    }

    /**
     * 设置值
     * @param key
     * @param value
     */
    public static set(key: string, value: string) {
        Cookies.set(key, value);
    }
}