import {Dialog, Notify, Toast} from "vant";
import {VanToast} from "vant/types/toast";

/**
 * 窗口工具。用于弹窗、消息提醒、通知等
 */
export default class WindowUtil {
    /**
     * 当前 toast 实例
     */
    private static toastIns: VanToast | null = null;


    /**
     * 基础消息提醒
     * @param message 通知消息内容
     * @param type 类型
     * @param durationSecond 显示市场
     */
    public static notify(message: string, type: 'primary' | 'success' | 'danger' | 'warning' = 'primary', durationSecond: number = 3) {
        Notify.clear();
        // 关闭后延时调用。使 notify 有刷新的效果，给用户感觉是新的消息
        setTimeout(() => {
            Notify({
                message: message,
                type: type,
                duration: durationSecond * 1000
            });
        }, 100);
    }

    /**
     * 基础消息
     * @param message
     * @param durationSecond
     */
    public static notifyPrimary(message: string, durationSecond: number = 3) {
        this.notify(message, 'primary', durationSecond);
    }

    /**
     * 成功消息
     * @param message
     * @param durationSecond
     */
    public static notifySuccess(message: string, durationSecond: number = 3) {
        this.notify(message, 'success', durationSecond);
    }

    /**
     * 警告消息
     * @param message
     * @param durationSecond
     */
    public static notifyWarning(message: string, durationSecond: number = 3) {
        this.notify(message, 'warning', durationSecond);
    }

    /**
     * 等待警告消息。一般用于请求中的提示
     * @param durationSecond
     */
    public static notifyWarningWait(durationSecond: number = 3) {
        this.notifyWarning("正在处理，请稍后...", durationSecond);
    }

    /**
     * 失败（危险）消息
     * @param message
     * @param durationSecond
     */
    public static notifyError(message: string, durationSecond: number = 3) {
        this.notify(message, 'danger', durationSecond);
    }


    /**
     * 弹窗确认提示
     * @param message
     * @param title
     * @param overlay
     * @param options
     */
    public static async confirm(message: string, title: string = "请确认", overlay: boolean = true, options: any = {}): Promise<boolean> {
        options.message = message;
        options.title = title;
        options.overlay = overlay;
        try {
            let action = await Dialog.confirm(options);
            return action === 'confirm';
        } catch (e) {
            return false;
        }
    }


    /**
     * 弹窗提示
     * @param message 提示内容
     * @param title 提示标题
     * @param confirmButtonText 确认按钮文字
     */
    public static async dialog(message: string, title: string = "提示", confirmButtonText: string = "确认") {
        return Dialog.alert({
            message: message,
            title: title,
            confirmButtonText: confirmButtonText,
        });
    }


    /**
     * 轻提示
     * @return toastIns 实例
     */
    public static toast(message: string, second: number = 3): VanToast {
        return WindowUtil.toastIns = Toast(message, {
            duration: second * 1000
        });
    }

    /**
     * 成功轻提示
     * @param message
     * @return toastIns 实例
     */
    public static toastSuccess(message: string): VanToast {
        return WindowUtil.toastIns = Toast.success(message);
    }

    /**
     * 失败轻提示
     * @param message
     * @return toastIns 实例
     */
    public static toastError(message: string): VanToast {
        return WindowUtil.toastIns = Toast.fail(message);
    }

    /**
     * 轻提示加载
     * @param message
     * @return toastIns 实例
     */
    public static toastLoading(message: string = "加载中"): VanToast {
        return WindowUtil.toastIns = Toast.loading({message, duration: 0})
    }

    /**
     * 轻提示加载(全屏遮罩)
     * @param message
     * @return toastIns 实例
     */
    public static toastLoadingMask(message: string = "加载中"): VanToast {
        return WindowUtil.toastIns = Toast.loading({message: message, duration: 0, mask: true})
    }

    /**
     * 清除当前轻提示
     */
    public static toastClear() {
        if (WindowUtil.toastIns !== null) {
            WindowUtil.toastIns.clear();
            WindowUtil.toastIns = null;
        }
    }
}
