/**
 * cookie 工具
 */
import * as Cookies from "js-cookie";
export default class CookieUtil {
    /**
     * 获取值
     * @param key
     */
    static get(key) {
        let value = Cookies.get(key);
        if (value === undefined) {
            value = "";
        }
        return value;
    }
    /**
     * 设置值
     * @param key
     * @param value
     */
    static set(key, value) {
        Cookies.set(key, value);
    }
}
//# sourceMappingURL=CookieUtil.js.map