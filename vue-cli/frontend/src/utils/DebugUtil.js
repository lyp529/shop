/**
 * 调试工具
 */
export default class DebugUtil {
    /**
     * 当前环境是否是开发环境
     */
    static isDebug() {
        return process.env.NODE_ENV !== 'production';
    }
}
//# sourceMappingURL=DebugUtil.js.map