import C from "@/constants/C";
import { Dialog, Notify, Toast } from "vant";
/**
 * 窗口工具。用于弹窗、消息提醒、通知等
 */
export default class WindowUtil {
    /**
     * 基础消息提醒
     * @param message 通知消息内容
     * @param type 类型
     * @param durationSecond 显示市场
     */
    static notify(message, type = C.NOTIFY_TYPE_PRIMARY, durationSecond = 3) {
        Notify.clear();
        // 关闭后延时调用。使 notify 有刷新的效果，给用户感觉是新的消息
        setTimeout(() => {
            Notify({
                message: message,
                type: type,
                duration: durationSecond * 1000
            });
        }, 100);
    }
    /**
     * 基础消息
     * @param message
     * @param durationSecond
     */
    static notifyPrimary(message, durationSecond = 3) {
        this.notify(message, C.NOTIFY_TYPE_PRIMARY, durationSecond);
    }
    /**
     * 成功消息
     * @param message
     * @param durationSecond
     */
    static notifySuccess(message, durationSecond = 3) {
        this.notify(message, C.NOTIFY_TYPE_SUCCESS, durationSecond);
    }
    /**
     * 警告消息
     * @param message
     * @param durationSecond
     */
    static notifyWarning(message, durationSecond = 3) {
        this.notify(message, C.NOTIFY_TYPE_WARNING, durationSecond);
    }
    /**
     * 等待警告消息。一般用于请求中的提示
     * @param durationSecond
     */
    static notifyWarningWait(durationSecond = 3) {
        this.notifyWarning("正在处理，请稍后...", durationSecond);
    }
    /**
     * 失败（危险）消息
     * @param message
     * @param durationSecond
     */
    static notifyError(message, durationSecond = 3) {
        this.notify(message, C.NOTIFY_TYPE_DANGER, durationSecond);
    }
    /**
     * 弹窗确认提示
     * @param message 确认消息
     * @param title 确认标题
     * @param overlay 是否展示遮罩层
     * @param options 详细选项数据。请参看下方文档
     * @see https://youzan.github.io/vant/#/zh-CN/dialog
     */
    static async confirm(message, title = "请确认", overlay = true, options = {}) {
        options.message = message;
        options.title = title;
        options.overlay = overlay;
        try {
            let action = await Dialog.confirm(options);
            return action === C.CONFIRM_ACTION_CONFIRM;
        }
        catch (e) {
            return false;
        }
    }
    /**
     * 弹窗提示
     * @param message 提示内容
     * @param title 提示标题
     * @param confirmButtonText 确认按钮文字
     */
    static async dialog(message, title = "提示", confirmButtonText = "确认") {
        return Dialog.alert({
            message: message,
            title: title,
            confirmButtonText: confirmButtonText,
        });
    }
    /**
     * 轻提示
     * @return toastIns 实例
     */
    static toast(message, second = 3) {
        return WindowUtil.toastIns = Toast(message, {
            duration: second * 1000
        });
    }
    /**
     * 成功轻提示
     * @param message
     * @return toastIns 实例
     */
    static toastSuccess(message) {
        return WindowUtil.toastIns = Toast.success(message);
    }
    /**
     * 失败轻提示
     * @param message
     * @return toastIns 实例
     */
    static toastError(message) {
        return WindowUtil.toastIns = Toast.fail(message);
    }
    /**
     * 轻提示加载
     * @param message
     * @return toastIns 实例
     */
    static toastLoading(message = "加载中") {
        return WindowUtil.toastIns = Toast.loading({ message, duration: 0 });
    }
    /**
     * 轻提示加载(全屏遮罩)
     * @param message
     * @return toastIns 实例
     */
    static toastLoadingMask(message = "加载中") {
        return WindowUtil.toastIns = Toast.loading({ message: message, duration: 0, mask: true });
    }
    /**
     * 清除当前轻提示
     */
    static toastClear() {
        if (WindowUtil.toastIns !== null) {
            WindowUtil.toastIns.clear();
            WindowUtil.toastIns = null;
        }
    }
}
/**
 * 当前 toast 实例
 */
WindowUtil.toastIns = null;
//# sourceMappingURL=WindowUtil.js.map