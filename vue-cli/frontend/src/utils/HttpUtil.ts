import DebugUtil from "@/utils/DebugUtil";
import ResponseVo from "@/models/vos/ResponseVo";
import WindowUtil from "@/utils/WindowUtil";
import {UserStore} from "@/store/modules/UserStore";

const qs = require('qs');
const axios = require('axios');

/**
 * http工具
 */
export default class HttpUtil {

    /**
     * 将字符串中的大写字母转为 -加小写字母  e.g. schedule/getOldScheduleVoList  > schedule/get-old-schedule-vo-list 更方便跟后台对应
     * @param txt 字符串
     */
    public static upperSplit(txt: string): string {

        if (txt.indexOf('-') == -1) {
            let str = "";
            for (let i = 0; i < txt.length; i++) { //小写字符asc-ii码为
                if (txt.charCodeAt(i) >= 65 && txt.charCodeAt(i) <= 90) {
                    str += "-" + txt[i].toLowerCase();
                } else {
                    str += txt[i]
                }
            }
            // console.log(txt+" >> "+str);
            return str;
        } else {
            return txt;
        }
    }

    /**
     * 发送post请求
     * @param route 请求地址
     * @param params 请求参数
     */
    public static post(route: string, params: { [key: string]: any } = {}): Promise<ResponseVo> {
        return new Promise<ResponseVo>((resolve, reject) => {
            let requestUrl = HttpUtil.getUrl(HttpUtil.upperSplit(route));
            axios.post(requestUrl, qs.stringify(params)).then(async (response: any) => {
                let data = response.data;
                let responseVo = new ResponseVo();
                responseVo.c = data.c;
                responseVo.m = data.m;
                responseVo.d = data.d;
                resolve(responseVo);
            }).catch((error: any) => {
                let responseVo = new ResponseVo();
                responseVo.c = -99;
                responseVo.m = "系统出错";
                responseVo.d = error;
                resolve(responseVo);
            });
        });
    }

    /**
     * 发送get请求
     * @param route 请求地址
     * @param params 请求参数
     */
    // public static get(route: string, params: { [key: string]: any } = {}): Promise<ResponseVo> {
    //     return new Promise<ResponseVo>((resolve, reject) => {
    //         let requestUrl = HttpUtil.getUrl(HttpUtil.upperSplit(route));
    //         axios.get(requestUrl, qs.stringify(params)).then(async (response: any) => {
    //             let data = response.data;
    //             let responseVo = new ResponseVo();
    //             responseVo.c = data.c;
    //             responseVo.m = data.m;
    //             responseVo.d = data.d;
    //             if (responseVo.c === C.RESPONSE_CODE_NOT_LOGIN) {
    //                 await WindowUtil.dialog(responseVo.m);
    //                 await UserStore.logout();
    //             } else {
    //                 resolve(responseVo);
    //             }
    //         }).catch((error: any) => {
    //             let responseVo = new ResponseVo();
    //             responseVo.c = -99;
    //             responseVo.m = "系统出错";
    //             responseVo.d = error;
    //             resolve(responseVo);
    //         });
    //     });
    // }


    /**
     * 获取基本的请求地址
     * @param route
     */
    public static getUrl(route: string): string {
        let requestUrl = "index.php/" + route;
        if (DebugUtil.isDebug()) {
            requestUrl = "api/" + requestUrl;
        }
        return requestUrl;
    }
}
