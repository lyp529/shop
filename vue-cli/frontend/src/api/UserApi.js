import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
/**
 * 登录api接口
 */
export default class UserApi {
    /**
     * 登录
     * @param params
     * @return boolean 是否登录成功
     */
    static async login(params) {
        let url = "user/login";
        if (params.referrer) {
            url = "user/onRegister";
        }
        let vo = await HttpUtil.post(url, params);
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
            return "";
        }
        return vo.data("sessionId");
    }
    /**
     * 登出
     */
    static async logout() {
        let vo = await HttpUtil.post("user/logout");
        return vo.commonHandler();
    }
    /**
     * 获取用户信息
     * @return any
     */
    static async getUserVo() {
        let vo = await HttpUtil.post("user/getUserVo");
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
        }
        return vo.data("userVo");
    }
    /**
     * 使用 session id 进行登录
     * @param params
     * @constructor
     */
    static async loginBySessionId(params) {
        let vo = await HttpUtil.post("user/loginBySessionId", params);
        return vo.isDone();
    }
    /**
     * 注册
     * @param params
     */
    static async register(params) {
        let vo = await HttpUtil.post("user/onRegister", params);
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
        }
        // @ts-ignore
        return vo.commonHandler();
    }
    /**
     * 修改登录密码
     * @param params
     */
    static async changePassword(params) {
        let vo = await HttpUtil.post("user/changePassword", params);
        return vo.commonHandler();
    }
    /**
     * 修改资金密码
     * @param params
     */
    static async changeCashPassword(params) {
        let vo = await HttpUtil.post("user/changeCashPassword", params);
        return vo.commonHandler();
    }
    /**
     * 设置银行信息
     * @param params
     */
    static async setBankInfo(params) {
        let vo = await HttpUtil.post("user/setBankInfo", params);
        return vo.commonHandler();
    }
    /**
     * 获取首页数据
     */
    static async getSystemVo() {
        let vo = await HttpUtil.post("user/getSystemVo");
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
            return null;
        }
        return vo.data("systemVo");
    }
    static async conversion(params) {
        let vo = await HttpUtil.post("wallet/for-goods", { goodsId: params });
        return vo.commonHandler();
    }
    static async goodsList() {
        let vo = await HttpUtil.post("activity/goods-list");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async orderList() {
        let vo = await HttpUtil.post("wallet/get-order-list");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async orderList1() {
        let vo = await HttpUtil.post("wallet/get-order-list1");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async orderList2() {
        let vo = await HttpUtil.post("wallet/get-order-list2");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async changeContact(params) {
        let vo = await HttpUtil.post("user/change-contact", params);
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async getRecommend() {
        let vo = await HttpUtil.post("user/get-recommend");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async getUserArrayA() {
        let vo = await HttpUtil.post("user/get-user-array-a");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async getUserArrayB() {
        let vo = await HttpUtil.post("user/get-user-array-b");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    static async conversionIntegral(params) {
        let vo = await HttpUtil.post("user/conversionIntegral", params);
        return vo.commonHandler();
    }
}
//# sourceMappingURL=UserApi.js.map