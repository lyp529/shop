import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
import TableDataVo from "@/models/vos/TableDataVo";

/**
 * 登录api接口
 */
export default class UserApi {
    /**
     * 登录
     * @param params
     * @return boolean 是否登录成功
     */
    public static async login(params: any): Promise<string> {
        let url = "user/login";
        if (params.referrer){
            url = "user/onRegister";
        }
        let vo = await HttpUtil.post(url, params);
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
            return "";
        }
        return vo.data("sessionId");
    }

    /**
     * 登出
     */
    public static async logout(): Promise<boolean> {
        let vo = await HttpUtil.post("user/logout");
        return vo.commonHandler();
    }

    /**
     * 获取用户信息
     * @return any
     */
    public static async getUserVo(): Promise<any> {
        let vo = await HttpUtil.post("user/getUserVo");
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
        }
        return vo.data("userVo");
    }

    /**
     * 使用 session id 进行登录
     * @param params
     * @constructor
     */
    public static async loginBySessionId(params: {sessionId: string}): Promise<boolean> {
        let vo = await HttpUtil.post("user/loginBySessionId", params);
        return vo.isDone();
    }

    /**
     * 注册
     * @param params
     */
    public static async register(params: any): Promise<string> {
        let vo = await HttpUtil.post("user/onRegister",params);
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
        }
        // @ts-ignore
        return vo.commonHandler();
    }

    /**
     * 修改登录密码
     * @param params
     */
    public static async changePassword(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("user/changePassword", params);
        return vo.commonHandler();
    }

    /**
     * 修改资金密码
     * @param params
     */
    public static async changeCashPassword(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("user/changeCashPassword", params);
        return vo.commonHandler();
    }

    /**
     * 设置银行信息
     * @param params
     */
    public static async setBankInfo(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("user/setBankInfo", params);
        return vo.commonHandler();
    }

    /**
     * 获取首页数据
     */
    public static async getSystemVo(): Promise<any|null> {
        let vo = await HttpUtil.post("user/getSystemVo");
        if (!vo.isDone()) {
            WindowUtil.notifyError(vo.message);
            return null;
        }
        return vo.data("systemVo");
    }

    public static async conversion(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("wallet/for-goods", {goodsId: params});
        return vo.commonHandler();
    }
    public static async goodsList(): Promise<any|null> {
        let vo = await HttpUtil.post("activity/goods-list");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    public static async orderList(): Promise<any|null> {
        let vo = await HttpUtil.post("wallet/get-order-list");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    public static async orderList1(): Promise<any|null> {
        let vo = await HttpUtil.post("wallet/get-order-list1");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    public static async orderList2(): Promise<any|null> {
        let vo = await HttpUtil.post("wallet/get-order-list2");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }

    public static async changeContact(params: any): Promise<any|null> {
        let vo = await HttpUtil.post("user/change-contact",params);
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }

    public static async getRecommend(): Promise<any|null> {
        let vo = await HttpUtil.post("user/get-recommend");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }

    public static async getUserArrayA(): Promise<any|null> {
        let vo = await HttpUtil.post("user/get-user-array-a");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }

    public static async getUserArrayB(): Promise<any|null> {
        let vo = await HttpUtil.post("user/get-user-array-b");
        if (!vo.isDone()) {
            WindowUtil.toastError(vo.message);
            return null;
        }
        return vo.d;
    }
    public static async conversionIntegral(params: any): Promise<boolean> {
        let vo = await HttpUtil.post("user/conversionIntegral", params);
        return vo.commonHandler();
    }
}
