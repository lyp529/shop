/**
 * 返回参数数据封装
 */
import WindowUtil from "@/utils/WindowUtil";
export default class ResponseVo {
    /**
     * 使用json数据初始化对象
     * @param params
     */
    constructor(params = {}) {
        /**
         * 返回代码
         */
        this.c = 0;
        /**
         * 返回消息
         */
        this.m = "";
        /**
         * 返回数据
         */
        this.d = {};
        if (params.hasOwnProperty("c")) {
            this.c = params["c"];
        }
        if (params.hasOwnProperty("m")) {
            this.m = params["m"];
        }
        if (params.hasOwnProperty("d")) {
            this.d = params["d"];
        }
    }
    /**
     * 是否处理成功
     */
    isDone() {
        return this.c > 0;
    }
    /**
     * 获取消息
     */
    get message() {
        return this.m;
    }
    /**
     * 获取数据
     * @param prop
     */
    data(prop) {
        if (this.d.hasOwnProperty(prop)) {
            return this.d[prop];
        }
        return null;
    }
    /**
     * 通用结果处理
     */
    commonHandler() {
        if (this.isDone()) {
            WindowUtil.notifySuccess(this.message);
            return true;
        }
        else {
            WindowUtil.notifyError(this.message);
            return false;
        }
    }
}
//# sourceMappingURL=ResponseVo.js.map