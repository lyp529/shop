import JsonInstantiable from "@/models/JsonInitabled";

/**
 * 所有vo的基类
 */
export default class BaseVo {
    /**
     * 使用数组初始化对象数据
     * @param type
     * @param jsonArr
     */
    static loadListByArr<T extends BaseVo>(type: {new(): T}, jsonArr: any[]): T[] {
        let vos: T[] = [];
        jsonArr.forEach((values) => {
            let vo = new type();
            // @ts-ignore
            vos.push((vo as JsonInstantiable<type>).load(values));
        });
        return vos;
    }
}
