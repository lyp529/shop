import BaseVo from "@/models/vos/BaseVo";
import JsonInstantiable from "@/models/JsonInitabled";

/**
 * 用户vo
 */
export default class UserInfoVo extends BaseVo implements JsonInstantiable<UserInfoVo>{

    /**
     * @var int 用户id
     */
    public id: number = 0;
    /**
     * @var string 用户名字
     */
    public name: string = "";
    /**
     * @var string 头像图片地址
     */
    public avatar: string = "";
    /**
     * @var string[] 角色权限
     */
    public roles: string[] = [];
    /**
     * @var int 用户类型
     */
    public userType: number = 0;

    /**
     *
     * @param values
     */
    public load(values: { [key: string]: any }): UserInfoVo {
        this.id = Number(values.id);
        this.name = values.name;
        this.avatar = values.avatar;
        this.roles = values.roles;
        this.userType = Number(values.userType);
        return this;
    }
}
