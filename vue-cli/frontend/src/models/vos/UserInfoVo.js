import BaseVo from "@/models/vos/BaseVo";
/**
 * 用户vo
 */
export default class UserInfoVo extends BaseVo {
    constructor() {
        super(...arguments);
        /**
         * @var int 用户id
         */
        this.id = 0;
        /**
         * @var string 用户名字
         */
        this.name = "";
        /**
         * @var string 头像图片地址
         */
        this.avatar = "";
        /**
         * @var string[] 角色权限
         */
        this.roles = [];
        /**
         * @var int 用户类型
         */
        this.userType = 0;
    }
    /**
     *
     * @param values
     */
    load(values) {
        this.id = Number(values.id);
        this.name = values.name;
        this.avatar = values.avatar;
        this.roles = values.roles;
        this.userType = Number(values.userType);
        return this;
    }
}
//# sourceMappingURL=UserInfoVo.js.map