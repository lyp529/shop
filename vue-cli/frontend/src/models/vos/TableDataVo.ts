/**
 * 表格数据
 */
import JsonInstantiable from "@/models/JsonInitabled";

export default class TableDataVo implements JsonInstantiable<TableDataVo>{
    public rows: Array<any> = [];
    public type: Array<any> = [];
    public count: number = 0;

    /**
     * 使用json数据加载对象
     * @param values
     */
    load(values: { [p: string]: any }): TableDataVo {
        this.rows = values.rows;
        this.type = values.type;
        this.count = Number(values.count);
        return this;
    }
}
