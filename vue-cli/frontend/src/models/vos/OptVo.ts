import JsonInstantiable from "@/models/JsonInitabled";

/**
 * 选项数据（可以用 labels 初始化）
 */
export default class OptVo implements JsonInstantiable<OptVo>{
    /**
     * 选项数据
     */
    public value: number|string = "";
    /**
     * 标签数据
     */
    public label: string = "";

    /**
     * 使用数据初始化
     * @param values
     */
    load(values: { [p: string]: any }): OptVo {
        this.value = values.value;
        this.label = values.label;
        return this
    }

    /**
     * 使用 labels 初始化一个列表
     * @param labels
     * @param valueType
     */
    public static loadListByLabels(labels: any, valueType: number = 3): OptVo[] {
        let list: OptVo[] = [];
        for (let i in labels) {
            let label = labels[i];
            let value: string|number = "";
            if (valueType === 1) {
                value = Number(i);
            } else if (valueType === 2) {
                value = i + "";
            } else {
                value = i;
            }
            const optVo = new OptVo().load({value, label});
            list.push(optVo);
        }
        return list;
    }

    /**
     * 使用 labels 初始化一个map
     * @param labels
     * @param valueType
     */
    public static loadMapByLabels(labels: any, valueType: number = 3): {[key: number]: OptVo}|{[key: string]: OptVo} {
        let list = OptVo.loadListByLabels(labels, valueType);
        let map: any = {};
        list.forEach((optVo, index) => {
            map[optVo.value] = optVo;
        });
        return map;
    }
}
