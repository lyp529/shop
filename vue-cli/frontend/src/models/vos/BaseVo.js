/**
 * 所有vo的基类
 */
export default class BaseVo {
    /**
     * 使用数组初始化对象数据
     * @param type
     * @param jsonArr
     */
    static loadListByArr(type, jsonArr) {
        let vos = [];
        jsonArr.forEach((values) => {
            let vo = new type();
            // @ts-ignore
            vos.push(vo.load(values));
        });
        return vos;
    }
}
//# sourceMappingURL=BaseVo.js.map