import BaseVo from "@/models/vos/BaseVo";
import JsonInstantiable from "@/models/JsonInitabled";

/**
 * 路由数据封装
 */
export default class RouteVo extends BaseVo implements JsonInstantiable<RouteVo>{
    /**
     * 路由
     */
    public path = "";
    /**
     * 名字
     */
    public name = "";
    /**
     * 组件
     */
    public component: any;
    /**
     * 路由属性。如果要给单个路由设置属性，必须设置在这里
     */
    public props: any = {};


    /**
     * 初始化一个基础对象
     */
    public static newIns(path: string, name: string, component: any, title: string = ""): RouteVo {
        let vo = new RouteVo();
        vo.path = path;
        vo.name = name;
        vo.component = component;
        vo.props = {title: title};
        return vo;
    }

    /**
     * 使用json数据初始化
     * @param values
     */
    load(values: { [p: string]: any }): RouteVo {
        this.path = values.path;
        this.name = values.name;
        this.component = values.component;
        this.props = values.props;
        return this;
    }

    /**
     * 使用路由数据初始化
     * @param route
     */
    loadByRoute(route: any): RouteVo {
        route.matched.forEach((obj: any) => {
            this.path = obj.path;
            this.name = obj.name;
            if (obj.props.hasOwnProperty("default")) {
                this.props = obj.props.default;
            }
        });
        return this;
    }

    /**
     * 标题
     */
    get title(): string {
        return this.props.hasOwnProperty("title") ? this.props["title"] : "";
    }
}