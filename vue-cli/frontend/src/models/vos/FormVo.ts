import BaseVo from "./BaseVo";

/**
 * 基础表单
 */
export default class FormVo extends BaseVo {
    /**
     * 是否加载中
     */
    public loading: boolean = false;
    /**
     * 是否可见
     */
    public visible: boolean = false;
    /**
     * 第二弹窗是否可见
     */
    public secondVisible: boolean = false;
    /**
     * 表单类型
     */
    public type: number = 1;
    /**
     * 表格需要提交的参数封装
     */
    public params: {[key: string]: any} = {};
    /**
     * 空参数。用于清空参数
     */
    public emptyParams: {[key: string]: any} = {};

    /**
     * 初始化对象
     */
    public constructor(params: {[key: string]: any}) {
        super();
        this.params = params;
        this.emptyParams = JSON.parse(JSON.stringify(params));
    }

    /**
     * 设置表单类型
     * @param type
     */
    public setType(type: number): FormVo {
        this.type = type;
        return this;
    }

}
