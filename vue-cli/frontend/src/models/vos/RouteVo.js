import BaseVo from "@/models/vos/BaseVo";
/**
 * 路由数据封装
 */
export default class RouteVo extends BaseVo {
    constructor() {
        super(...arguments);
        /**
         * 路由
         */
        this.path = "";
        /**
         * 名字
         */
        this.name = "";
        /**
         * 路由属性。如果要给单个路由设置属性，必须设置在这里
         */
        this.props = {};
    }
    /**
     * 初始化一个基础对象
     */
    static newIns(path, name, component, title = "") {
        let vo = new RouteVo();
        vo.path = path;
        vo.name = name;
        vo.component = component;
        vo.props = { title: title };
        return vo;
    }
    /**
     * 使用json数据初始化
     * @param values
     */
    load(values) {
        this.path = values.path;
        this.name = values.name;
        this.component = values.component;
        this.props = values.props;
        return this;
    }
    /**
     * 使用路由数据初始化
     * @param route
     */
    loadByRoute(route) {
        route.matched.forEach((obj) => {
            this.path = obj.path;
            this.name = obj.name;
            if (obj.props.hasOwnProperty("default")) {
                this.props = obj.props.default;
            }
        });
        return this;
    }
    /**
     * 标题
     */
    get title() {
        return this.props.hasOwnProperty("title") ? this.props["title"] : "";
    }
}
//# sourceMappingURL=RouteVo.js.map