/**
 * 页面配置。
 */
exports.pages = function () {
    return {
        // 主页
        index: {
            entry: 'src/pages/index/main.ts', // js 入口文件
            template: 'public/index.html', // html 入口文件
            chunks: ['chunk-libs', 'chunk-vant', 'runtime', 'index'] // runtime: 运行时模块， index: 页面模块
        }
    };
};