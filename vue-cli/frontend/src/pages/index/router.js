import Vue from 'vue';
import Router from 'vue-router';
import RouteVo from "@/models/vos/RouteVo";
Vue.use(Router);
/**
 * 获取路由
 */
const routes = () => {
    let routes = [];
    // ------------------- 特殊路由
    // 首页
    routes.push(RouteVo.newIns("/", "home", () => import(/* webpackChunkName: "view-index" */ '@/pages/index/view/Home.vue')));
    return routes;
};
export default new Router({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes: routes()
});
//# sourceMappingURL=router.js.map