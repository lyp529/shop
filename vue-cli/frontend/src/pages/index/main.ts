import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from '@/store/index';
import Vant from 'vant';
import 'vant/lib/index.css';
// @ts-ignore 二维码组件
import VueQriously from 'vue-qriously';
import "@/assets/css/common.scss";
import "@/assets/css/vant-modify.scss";

Vue.config.productionTip = false;
Vue.use(VueQriously);
Vue.use(Vant);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
