const path = require('path')
const DllReferencePlugin = require('webpack/lib/DllReferencePlugin');
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin')
const pages = require('./src/pages/pages.config');

// 项目名字。如： ga、ga_dev
const projectName = path.basename(path.dirname(path.dirname(__dirname)));

module.exports = {
    publicPath: './',
    productionSourceMap: false,
    pages: pages.pages(),
    devServer: {
        port: 10002,
        proxy: {
            '/api': {
                target: `http://127.0.0.1/${projectName}/php/frontend/web/`,
                secure: false,  // 如果是https接口，需要配置这个参数
                changeOrigin: true,  //是否跨域
                pathRewrite: {
                    '^/api': ''
                }
            }

        }
    },
    // configureWebpack: {
    //     plugins: [
    //         new DllReferencePlugin({
    //             context: process.cwd(),
    //             manifest: require('./public/vendor/vendor-manifest.json')
    //         }),
    //         new DllReferencePlugin({
    //             context: process.cwd(),
    //             manifest: require('./public/vendor/common-manifest.json')
    //         }),
    //         // 将 dll 注入到 生成的 html 模板中
    //         new AddAssetHtmlPlugin({
    //             // dll文件位置
    //             filepath: path.resolve(__dirname, './public/vendor/*.js'),
    //             // dll 引用路径
    //             publicPath: './vendor',
    //             // dll最终输出的目录
    //             outputPath: './vendor'
    //         })
    //     ]
    // },

    chainWebpack(config) {
        config.when(process.env.NODE_ENV !== 'development',
            config => {
                config.optimization.splitChunks({
                    chunks: 'all',
                    cacheGroups: {
                        libs: {
                            name: 'chunk-libs',
                            test: /[\\/]node_modules[\\/]/,
                            priority: 10,
                            chunks: 'initial' // only package third parties that are initially dependent
                        },
                        vant: {
                            name: 'chunk-vant', // split elementUI into a single package
                            priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                            test: /[\\/]node_modules[\\/]_?vant(.*)/ // in order to adapt to cnpm
                        },
                    }
                })
                config.optimization.runtimeChunk('single')
            })
    },


    pluginOptions: {
        // scss 加载。加载后 TypeScript 可以读取到 scss 的数据
        'style-resources-loader': {
            preProcessor: 'scss',
            patterns: [
                path.resolve(__dirname, 'src/assets/css/common.scss'),
            ]
        },

        // 参考: https://www.npmjs.com/package/vue-cli-plugin-dll
        // dll: {
        //     // 入口配置
        //     entry: ['vue','axios'],
        //     // 输入目录
        //     output: path.join(__dirname, './public/dll'),
        //
        //     // 是否开启 DllReferencePlugin,
        //
        //     // 1. 如果你需要在开发环境中不采用开启分包模式，你可以配置open为false。
        //     // 例如，我们入口配置为 entry: ['vue']， 我们执行npm run dll 构建了vendor包。
        //     // 在npm run serve的时候，如果默认open开启的情况下，其实开发环境采用的vue是生产环境的包，因为我们dll命令构建的就是生产环境的包。
        //     // 这会让我们在开发环境中无法看到vue给我们带来的友好提示建议。
        //     // 我们可以配置open : process.env.NODE_ENV === 'production'，只在生产环境开启DllReferencePlugin
        //
        //     // 2. 参数会自动识别是否有先执行npm run dll生产分包，如果没有的情况下则不开启dll。
        //     open: true,
        //
        //
        //
        //     // 自动注入到index.html
        //     // 在构建其他命令的时候，如果开启了自动注入。程序会手动将output中生成的*.dll.js 文件自动注入到index.html中。
        //     inject: true,
        // }
    }
};
