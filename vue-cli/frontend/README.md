# frontend

## 目录结构：

```
/frontend
    /dist                       代码编译后生成的文件
    /node_modules               nodejs依赖库
    /public                     编译后的web访问路径
    /src                        主要代码目录
        /api                    api访问路径
        /assets                 资源路径
        /css                    css路径
            common.scss         通用全局配置
            keyframes.scss      动画配置
            vant-modify.scss    vant样式的修改
        /components             通用组件
        /layout                 通用布局
        /pages                  页面（用于用户分页加载。不需要加载过多无用的资源）
            /__template          页面模板
                /view           页面
                main.ts         页面入口（编译入口）
                router.ts       路由
            /index              首页
                ...
            /...
        /store                  vuex 全局状态管理
            /modules            模块
                AppStore.ts     应用模块
                UserStore.ts    用户模块
            index.ts            store引入入口
        /utils                  系统通用工具
        /vo                     数据对象（ValueObject）
    .browserslistrc
    .gitignore
    babel.config.js
    frontend.iml                idea模块配置文件
    package.json                nodejs项目配置文件
    package-lock.json           nodejs依赖库锁文件（可以确保 npm insall 下载的资源一致）
    postcss.config.js           
    README.md                   说明文档
    tsconfig.json               typescript 配置
    tslint.json                 tslint配置
    vue.config.js               vue配置
    webpack.dll.config.js       webpack dll 配置（目前没用）
```

## 添加新页面应用所需操作
>（以创建彩票为例）

1.创建页面文件夹：

>复制`frontend/src/pages/__template` => `frontend/src/pages/lottery`
    
2.添加vue配置：
>编辑文件 `src/pages/pages.config.js`
>
- 添加内容
```
    lottery: addConfig("lottery"),
```