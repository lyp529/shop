/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.13 : Database - shop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shop` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci */;

USE `shop`;

/*Table structure for table `_debug_log` */

DROP TABLE IF EXISTS `_debug_log`;

CREATE TABLE `_debug_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_type` varchar(20) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `record_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `log_type` (`log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据库日志表';

/*Data for the table `_debug_log` */

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `item_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限项目名字',
  `record_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `item_name` (`item_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='权限表';

/*Data for the table `auth_item` */

/*Table structure for table `auth_role` */

DROP TABLE IF EXISTS `auth_role`;

CREATE TABLE `auth_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '角色类型。1用户 2分组',
  `bind_id` int(11) NOT NULL COMMENT '绑定的数据表id。如用户id、分组id',
  `record_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `bind_id` (`bind_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='权限角色';

/*Data for the table `auth_role` */

/*Table structure for table `cfg_earn_rank_offset` */

DROP TABLE IF EXISTS `cfg_earn_rank_offset`;

CREATE TABLE `cfg_earn_rank_offset` (
  `er_offset` int(11) NOT NULL COMMENT '保存每次查询tab_earnings_ranking的offset',
  `record_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='盈利排行榜配置表';

/*Data for the table `cfg_earn_rank_offset` */

/*Table structure for table `cfg_kind` */

DROP TABLE IF EXISTS `cfg_kind`;

CREATE TABLE `cfg_kind` (
  `kind` int(11) NOT NULL,
  `kind_name` varchar(100) NOT NULL,
  `start_time` time NOT NULL COMMENT '每天开始时间',
  `end_time` time NOT NULL COMMENT '每天结束时间',
  `minute` int(11) NOT NULL COMMENT '每期多少分钟',
  `periods` int(11) NOT NULL COMMENT '每天总期数',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT '有效无效',
  PRIMARY KEY (`kind`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='彩票类型配置表';

/*Data for the table `cfg_kind` */

insert  into `cfg_kind`(`kind`,`kind_name`,`start_time`,`end_time`,`minute`,`periods`,`active`) values (101,'重庆时时彩','00:30:00','23:50:00',20,60,1),(102,'曼谷时时彩','00:00:00','23:55:00',5,288,1),(103,'柏林五分彩','00:00:00','23:55:00',5,288,1),(201,'广东11选5','09:30:00','23:50:00',20,142,1),(202,'江西11选5','09:30:00','23:50:00',20,142,1),(301,'江苏快3','08:50:00','22:10:00',20,41,1),(302,'北京快3','09:20:00','23:40:00',20,44,1),(303,'曼谷快3','09:00:00','22:00:00',5,157,1);

/*Table structure for table `cfg_luck_run` */

DROP TABLE IF EXISTS `cfg_luck_run`;

CREATE TABLE `cfg_luck_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_type` int(11) NOT NULL,
  `info` varchar(100) NOT NULL,
  `rate` int(11) NOT NULL,
  `reward_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='积分轮盘配置表';

/*Data for the table `cfg_luck_run` */

insert  into `cfg_luck_run`(`id`,`reward_type`,`info`,`rate`,`reward_value`) values (1,1,'谢谢参与',72,0),(2,2,'现金',7,5),(3,2,'现金',6,10),(4,2,'现金',5,18),(5,2,'现金',4,28),(6,2,'现金',3,38),(7,2,'现金',2,88),(8,2,'现金',1,8888);

/*Table structure for table `cfg_vip` */

DROP TABLE IF EXISTS `cfg_vip`;

CREATE TABLE `cfg_vip` (
  `level` int(11) NOT NULL COMMENT 'VIP等级',
  `name` varchar(50) NOT NULL COMMENT 'VIP名称',
  `amount` int(11) NOT NULL COMMENT '金额',
  `amount_desc` varchar(100) NOT NULL COMMENT '金额描述',
  `withdraw_times` int(11) NOT NULL COMMENT '每日可提现次数',
  `single_limit` int(11) NOT NULL COMMENT '单笔限额',
  `back_percent` int(11) NOT NULL COMMENT '每天下单金额返回百分比',
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='VIP等级配置';

/*Data for the table `cfg_vip` */

insert  into `cfg_vip`(`level`,`name`,`amount`,`amount_desc`,`withdraw_times`,`single_limit`,`back_percent`) values (0,'青铜会员',0,'5万以下',2,5000,0),(1,'白银会员',50000,'5万',2,5000,0),(2,'黄金会员',150000,'15万',2,10000,1),(3,'铂金会员',300000,'30万',2,20000,2),(4,'钻石会员',500000,'50万',2,40000,4),(5,'皇冠会员',1000000,'100万',2,100000,8);

/*Table structure for table `log_cron_record` */

DROP TABLE IF EXISTS `log_cron_record`;

CREATE TABLE `log_cron_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cron_id` int(11) NOT NULL COMMENT '任务id',
  `reason` tinyint(3) NOT NULL COMMENT '运行结果。1成功 -1出错',
  `use_second` decimal(10,4) DEFAULT NULL COMMENT '用时秒数',
  `record_time` int(11) NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='记录定时任务运行的状态';

/*Data for the table `log_cron_record` */

/*Table structure for table `log_general` */

DROP TABLE IF EXISTS `log_general`;

CREATE TABLE `log_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_type` int(11) NOT NULL COMMENT '日志类型',
  `log_label` varchar(200) NOT NULL COMMENT '日志类型名称',
  `params` varchar(1000) NOT NULL COMMENT '参数',
  `remark` varchar(1000) NOT NULL COMMENT '备注',
  `record_time` datetime NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='一般日志表';

/*Data for the table `log_general` */

/*Table structure for table `log_login` */

DROP TABLE IF EXISTS `log_login`;

CREATE TABLE `log_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(1) NOT NULL COMMENT '用户类型',
  `user_id` int(11) NOT NULL COMMENT '登录人id',
  `ip` varchar(20) NOT NULL COMMENT '登陆ip',
  `login_type` int(1) NOT NULL COMMENT '1登入 2登出',
  `record_time` datetime NOT NULL COMMENT '登陆或登出时间',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8mb4 COMMENT='登录日志';

/*Data for the table `log_login` */

insert  into `log_login`(`id`,`user_type`,`user_id`,`ip`,`login_type`,`record_time`,`username`) values (1,99,1,'127.0.0.1',1,'2019-11-23 15:38:59','admin'),(2,1,2,'127.0.0.1',1,'2019-11-23 16:21:06','member'),(3,1,2,'127.0.0.1',1,'2019-11-23 16:21:12','member'),(4,1,2,'127.0.0.1',2,'2019-11-23 16:21:27','member'),(5,1,2,'127.0.0.1',1,'2019-11-23 16:21:34','member'),(6,1,2,'127.0.0.1',1,'2019-11-23 16:29:42','member'),(7,1,2,'127.0.0.1',1,'2019-11-23 16:37:25','member'),(8,1,2,'127.0.0.1',1,'2019-11-23 16:51:40','member'),(9,1,2,'127.0.0.1',1,'2019-11-23 16:51:40','member'),(10,1,2,'127.0.0.1',1,'2019-11-23 16:51:51','member'),(11,1,2,'127.0.0.1',1,'2019-11-23 16:51:56','member'),(12,1,2,'127.0.0.1',1,'2019-11-23 16:51:56','member'),(13,1,2,'127.0.0.1',1,'2019-11-23 16:52:02','member'),(14,1,2,'127.0.0.1',1,'2019-11-23 16:52:02','member'),(15,1,2,'127.0.0.1',1,'2019-11-23 16:52:04','member'),(16,1,2,'127.0.0.1',1,'2019-11-23 16:52:24','member'),(17,1,2,'127.0.0.1',1,'2019-11-23 16:52:24','member'),(18,1,2,'127.0.0.1',1,'2019-11-23 16:52:24','member'),(19,1,2,'127.0.0.1',1,'2019-11-23 16:52:24','member'),(20,1,2,'127.0.0.1',1,'2019-11-23 16:52:35','member'),(21,1,2,'127.0.0.1',1,'2019-11-23 16:52:35','member'),(22,1,2,'127.0.0.1',1,'2019-11-23 16:52:35','member'),(23,1,2,'127.0.0.1',1,'2019-11-23 16:52:36','member'),(24,1,2,'127.0.0.1',1,'2019-11-23 16:52:36','member'),(25,1,2,'127.0.0.1',1,'2019-11-23 16:52:49','member'),(26,1,2,'127.0.0.1',1,'2019-11-23 16:52:49','member'),(27,1,2,'127.0.0.1',1,'2019-11-23 16:52:49','member'),(28,1,2,'127.0.0.1',1,'2019-11-23 16:52:50','member'),(29,1,2,'127.0.0.1',1,'2019-11-23 16:52:50','member'),(30,1,2,'127.0.0.1',1,'2019-11-23 16:52:50','member'),(31,1,2,'127.0.0.1',1,'2019-11-23 16:54:54','member'),(32,1,2,'127.0.0.1',1,'2019-11-23 16:54:55','member'),(33,1,2,'127.0.0.1',1,'2019-11-23 16:54:55','member'),(34,1,2,'127.0.0.1',1,'2019-11-23 16:54:57','member'),(35,1,2,'127.0.0.1',1,'2019-11-23 16:54:57','member'),(36,1,2,'127.0.0.1',1,'2019-11-23 16:54:57','member'),(37,1,2,'127.0.0.1',1,'2019-11-23 16:54:57','member'),(38,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(39,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(40,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(41,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(42,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(43,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(44,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(45,1,2,'127.0.0.1',1,'2019-11-23 16:55:23','member'),(46,1,2,'127.0.0.1',1,'2019-11-23 16:56:47','member'),(47,1,2,'127.0.0.1',1,'2019-11-23 16:56:47','member'),(48,1,2,'127.0.0.1',1,'2019-11-23 16:56:47','member'),(49,1,2,'127.0.0.1',1,'2019-11-23 16:56:47','member'),(50,1,2,'127.0.0.1',1,'2019-11-23 16:56:47','member'),(51,1,2,'127.0.0.1',1,'2019-11-23 16:56:47','member'),(52,1,2,'127.0.0.1',1,'2019-11-23 16:56:50','member'),(53,1,2,'127.0.0.1',1,'2019-11-23 16:56:50','member'),(54,1,2,'127.0.0.1',1,'2019-11-23 16:56:50','member'),(55,1,2,'127.0.0.1',1,'2019-11-23 16:56:50','member'),(56,1,2,'127.0.0.1',1,'2019-11-23 16:57:06','member'),(57,1,2,'127.0.0.1',1,'2019-11-23 16:57:07','member'),(58,1,2,'127.0.0.1',1,'2019-11-23 16:57:26','member'),(59,1,2,'127.0.0.1',1,'2019-11-23 16:57:44','member'),(60,1,2,'127.0.0.1',1,'2019-11-23 16:57:44','member'),(61,1,2,'127.0.0.1',1,'2019-11-23 16:57:55','member'),(62,1,2,'127.0.0.1',1,'2019-11-23 16:57:56','member'),(63,1,2,'127.0.0.1',1,'2019-11-23 16:57:56','member'),(64,1,2,'127.0.0.1',1,'2019-11-23 17:02:51','member'),(65,1,2,'127.0.0.1',1,'2019-11-23 17:02:51','member'),(66,1,2,'127.0.0.1',1,'2019-11-23 17:02:52','member'),(67,1,2,'127.0.0.1',1,'2019-11-23 17:02:52','member'),(68,1,2,'127.0.0.1',1,'2019-11-23 17:17:07','member'),(69,1,2,'127.0.0.1',1,'2019-11-23 17:17:07','member'),(70,1,2,'127.0.0.1',1,'2019-11-23 17:17:07','member'),(71,1,2,'127.0.0.1',1,'2019-11-23 17:17:07','member'),(72,1,2,'127.0.0.1',1,'2019-11-23 17:17:07','member'),(73,1,2,'127.0.0.1',1,'2019-11-23 17:17:40','member'),(74,1,2,'127.0.0.1',1,'2019-11-23 17:17:40','member'),(75,1,2,'127.0.0.1',1,'2019-11-23 17:17:40','member'),(76,1,2,'127.0.0.1',1,'2019-11-23 17:17:41','member'),(77,1,2,'127.0.0.1',1,'2019-11-23 17:17:41','member'),(78,1,2,'127.0.0.1',1,'2019-11-23 17:17:41','member'),(79,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(80,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(81,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(82,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(83,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(84,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(85,1,2,'127.0.0.1',1,'2019-11-23 17:18:39','member'),(86,1,2,'127.0.0.1',1,'2019-11-23 17:24:04','member'),(87,1,2,'127.0.0.1',1,'2019-11-23 17:25:20','member'),(88,1,2,'127.0.0.1',1,'2019-11-23 18:39:43','member'),(89,1,2,'127.0.0.1',2,'2019-11-23 18:39:49','member'),(90,1,2,'127.0.0.1',1,'2019-11-23 18:39:55','member'),(91,1,2,'127.0.0.1',1,'2019-11-23 18:46:21','member'),(92,1,2,'127.0.0.1',1,'2019-11-23 18:46:44','member'),(93,1,2,'127.0.0.1',1,'2019-11-23 18:46:45','member'),(94,1,2,'127.0.0.1',1,'2019-11-23 19:01:08','member'),(95,1,2,'127.0.0.1',1,'2019-11-23 19:01:13','member'),(96,1,2,'127.0.0.1',1,'2019-11-23 19:01:21','member'),(97,1,2,'127.0.0.1',1,'2019-11-23 19:01:47','member'),(98,1,2,'127.0.0.1',1,'2019-11-23 19:02:00','member'),(99,1,2,'127.0.0.1',1,'2019-11-23 19:02:08','member'),(100,1,2,'127.0.0.1',1,'2019-11-23 19:02:54','member'),(101,1,2,'127.0.0.1',1,'2019-11-23 19:03:18','member'),(102,1,2,'127.0.0.1',1,'2019-11-23 19:12:24','member'),(103,1,2,'127.0.0.1',1,'2019-11-23 19:17:20','member'),(104,1,2,'127.0.0.1',1,'2019-11-23 19:19:47','member'),(105,1,2,'127.0.0.1',1,'2019-11-23 19:21:41','member'),(106,1,2,'127.0.0.1',1,'2019-11-23 20:02:27','member'),(107,1,2,'127.0.0.1',1,'2019-11-23 20:14:48','member'),(108,1,2,'127.0.0.1',1,'2019-11-23 20:14:48','member'),(109,1,2,'127.0.0.1',1,'2019-11-23 20:14:55','member'),(110,1,2,'127.0.0.1',1,'2019-11-23 20:14:55','member'),(111,1,2,'127.0.0.1',1,'2019-11-23 20:14:55','member'),(112,1,2,'127.0.0.1',1,'2019-11-23 20:15:34','member'),(113,1,2,'127.0.0.1',1,'2019-11-23 20:15:34','member'),(114,1,2,'127.0.0.1',1,'2019-11-23 20:15:34','member'),(115,1,2,'127.0.0.1',1,'2019-11-23 20:15:35','member'),(116,1,2,'127.0.0.1',1,'2019-11-23 20:15:45','member'),(117,1,2,'127.0.0.1',1,'2019-11-23 20:15:47','member'),(118,1,2,'127.0.0.1',1,'2019-11-23 20:15:47','member'),(119,1,2,'127.0.0.1',1,'2019-11-23 20:15:47','member'),(120,1,2,'127.0.0.1',1,'2019-11-23 20:15:47','member'),(121,1,2,'127.0.0.1',1,'2019-11-23 20:15:59','member'),(122,1,2,'127.0.0.1',1,'2019-11-23 20:15:59','member'),(123,1,2,'127.0.0.1',1,'2019-11-23 20:15:59','member'),(124,1,2,'127.0.0.1',1,'2019-11-23 20:15:59','member'),(125,1,2,'127.0.0.1',1,'2019-11-23 20:15:59','member'),(126,1,2,'127.0.0.1',1,'2019-11-23 20:15:59','member'),(127,1,2,'127.0.0.1',1,'2019-11-23 20:16:14','member'),(128,1,2,'127.0.0.1',1,'2019-11-23 20:16:15','member'),(129,1,2,'127.0.0.1',1,'2019-11-23 20:16:15','member'),(130,1,2,'127.0.0.1',1,'2019-11-23 20:16:15','member'),(131,1,2,'127.0.0.1',1,'2019-11-23 20:16:15','member'),(132,1,2,'127.0.0.1',1,'2019-11-23 20:16:15','member'),(133,1,2,'127.0.0.1',1,'2019-11-23 20:16:17','member'),(134,1,2,'127.0.0.1',1,'2019-11-23 20:18:41','member'),(135,1,2,'127.0.0.1',1,'2019-11-23 20:27:04','member'),(136,1,2,'127.0.0.1',1,'2019-11-23 20:51:06','member'),(137,1,2,'127.0.0.1',1,'2019-11-23 20:54:26','member'),(138,1,2,'127.0.0.1',1,'2019-11-23 20:55:30','member');

/*Table structure for table `log_operate` */

DROP TABLE IF EXISTS `log_operate`;

CREATE TABLE `log_operate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL COMMENT '操作用户类型',
  `user_id` int(11) NOT NULL COMMENT '操作用户',
  `ip` varchar(20) NOT NULL COMMENT '用户IP',
  `user_name` varchar(20) NOT NULL COMMENT '用户姓名(方便查看)',
  `log_type` int(11) NOT NULL COMMENT '日志类型C.LOG*',
  `log_label` varchar(100) DEFAULT NULL COMMENT '日志的标注(方便查看,不用于程序)',
  `params` text COMMENT '自定义参数枚举|隔开',
  `old_data` text COMMENT 'model.toArray的数据',
  `new_data` text COMMENT 'model.load可以将数据载入',
  `data_class` varchar(100) DEFAULT NULL COMMENT '数据的类名(主要用于查看)',
  `record_time` datetime NOT NULL COMMENT '操作日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='操作日志表';

/*Data for the table `log_operate` */

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8mb4_german2_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1574480052),('m190903_113257_create_user',1574480054),('m190906_020641_create_auth',1574480055),('m190907_093734_change_tab_user',1574480055),('m190910_065650_user',1574480055),('m190911_083642_delete_tab_user',1574480055),('m190911_100559_change_user_account',1574480055),('m190912_100715_create_notice',1574480055),('m190918_012257_create_tab_cron',1574480055),('m190919_041201_tab_signin',1574480055),('m190919_115152_create_order_wallet',1574480055),('m190920_064956_betting_record',1574480055),('m190920_123452_change_tab_order_wallet',1574480055),('m190923_025107_tab_change_tab_order_wallet',1574480056),('m190923_095838_change_notice_type_table_name',1574480056),('m190924_161243_table',1574480056),('m190925_022916_change_tab_schedule',1574480056),('m190926_023840_schedule',1574480056),('m190926_070610_del_tab_bet_lottery',1574480056),('m190927_064231_cfg_vip',1574480056),('m190927_064610_user_member',1574480056),('m190927_072158_change_tab_bet',1574480056),('m190927_080708_change_del_bet_lottery',1574480056),('m190928_013457_cash_record',1574480056),('m190928_040611_update_tab_schedule_field_NULL',1574480056),('m190929_030047_add_member_integral',1574480056),('m190929_114221__debug_log',1574480056),('m190929_114310_log',1574480056),('m190929_130629_create_cfg_luck_run',1574480056),('m190930_032055_cash_record',1574480056),('m191004_035159_cash_record',1574480056),('m191004_041209_bet',1574480056),('m191007_072600_change_luck_run',1574480056),('m191007_072635_bet',1574480056),('m191008_071744_change_tab_signs',1574480056),('m191008_084354_user_account',1574480057),('m191009_035144_user_member',1574480057),('m191009_041127_change_tab_cash_record',1574480057),('m191009_073707_user_member',1574480057),('m191009_073756_change_tab_cash_record',1574480057),('m191010_015010_change_user_agent',1574480057),('m191010_021510_change_tab_order_wallet',1574480057),('m191010_031435_change_field_credit',1574480057),('m191010_033327_change_cfg_luck_run',1574480057),('m191010_071641_user_member',1574480057),('m191011_081338_create_tab_key_value',1574480057),('m191011_101415_change_member_vipdate',1574480057),('m191011_102424_user_member',1574480057),('m191014_032354_change_member_before',1574480057),('m191014_065746_create_tab_earnings_ranking',1574480057),('m191014_074641_change_tab_earnings_ranking',1574480057),('m191014_093734_tab_schedule',1574480057),('m191014_094908_tab_schedule_data',1574480057),('m191014_101916_create_cfg_earnings_ranking',1574480057),('m191014_113918_create_earn_rank',1574480057),('m191014_121449_user_member',1574480057),('m191014_133226_decimal',1574480057),('m191015_063801_insert_cfg_luck_run',1574480057),('m191015_075147_change_user_account',1574480058),('m191015_113819_insert_cfg_luck_run',1574480058),('m191015_114402_change_cfg_earn_rank_offset',1574480058),('m191015_142130_change_tab_earn_rank',1574480058),('m191016_085523_change_tab_earn_rank',1574480058),('m191017_093425_create_tab_earn_rank_real',1574480058),('m191017_112930_create_tab_earn_rank_real_offset',1574480058),('m191017_113648_create_cfg_earn_rank_real_offset',1574480058),('m191017_132218_tab_order_wallet',1574480058),('m191018_034136_change_cfg_earn_rank_real_offset',1574480058),('m191019_013004_log_operate',1574480058),('m191019_094323_rename_log_operate',1574480058),('m191021_025757_change_user_account',1574480058),('m191021_034247_order_wallet',1574480058),('m191021_090057_change_user_member',1574480058),('m191021_101350_change_cfg_luck_run',1574480058),('m191023_130732_chang_log_login',1574480058),('m191113_050135_tab_award',1574480058),('m191113_051830_tab_award',1574495031),('m191113_052043_tab_award',1574495031),('m191113_121247_tab_goods',1574495031),('m191114_063606_change_tab_goods',1574495031),('m191114_071901_change_user_member',1574495031),('m191114_072345_create_cash_rewards',1574495031),('m191114_080254_create_order',1574495031),('m191114_100105_change_order',1574495031),('m191117_030242_change_tab_award',1574495031),('m191117_030524_change_tab_award',1574495101),('m191123_080242_create_tab_cash_rewards',1574496274),('m191123_082422_create_tab_award',1574497530),('m191123_103042_create_tab_award',1574505119),('m191123_112932_change_tab_award',1574509292),('m191123_114332_create_tab_award',1574509428),('m191123_123628_create_tab_order',1574512939);

/*Table structure for table `tab_award` */

DROP TABLE IF EXISTS `tab_award`;

CREATE TABLE `tab_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='奖励中心';

/*Data for the table `tab_award` */

insert  into `tab_award`(`id`,`value`) values (1,'{\"team\":800,\"logistics\":1200,\"director\":700,\"manager\":600,\"majordomo\":400,\"chairman\":300,\"garage\":1200}');

/*Table structure for table `tab_bet` */

DROP TABLE IF EXISTS `tab_bet`;

CREATE TABLE `tab_bet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '注单ID',
  `bet_no` varchar(100) NOT NULL COMMENT '注单编号-uniqid',
  `schedule_id` int(11) NOT NULL COMMENT '场次ID',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `member_id` int(11) NOT NULL COMMENT '会员ID',
  `agent_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `bet_type` varchar(100) NOT NULL COMMENT '下注类型',
  `bet_type_name` varchar(200) NOT NULL COMMENT '下注类型名称',
  `bet_content` varchar(1000) NOT NULL COMMENT '下注内容',
  `multiple` int(11) NOT NULL COMMENT '倍数',
  `unit` tinyint(1) NOT NULL COMMENT '单位 1元 2角',
  `group_num` int(11) NOT NULL COMMENT '分组的数量',
  `bet_amount` decimal(12,2) DEFAULT NULL COMMENT '用户下注金额',
  `bet_result` decimal(12,2) NOT NULL COMMENT '派彩结果',
  `bet_date` date DEFAULT NULL COMMENT '下注日期',
  `bet_balance` decimal(12,2) NOT NULL COMMENT '投注后的余额',
  `update_time` datetime DEFAULT NULL COMMENT '数据更新时间',
  `record_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`),
  KEY `bet_no` (`bet_no`,`schedule_id`,`member_id`),
  KEY `bet_date` (`bet_date`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='注单表';

/*Data for the table `tab_bet` */

insert  into `tab_bet`(`id`,`bet_no`,`schedule_id`,`username`,`member_id`,`agent_id`,`vendor_id`,`holder_id`,`super_holder_id`,`bet_type`,`bet_type_name`,`bet_content`,`multiple`,`unit`,`group_num`,`bet_amount`,`bet_result`,`bet_date`,`bet_balance`,`update_time`,`record_time`) values (1,'\'{\"team\":0,\"logistics\":0,\"director\":0,\"manager\":0,\"majordomo\":0,\"chairman\":0,\"garage\":0}\'',0,'',0,0,0,0,0,'','','',0,0,0,NULL,'0.00',NULL,'0.00',NULL,NULL);

/*Table structure for table `tab_cash_record` */

DROP TABLE IF EXISTS `tab_cash_record`;

CREATE TABLE `tab_cash_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `record_no` varchar(100) DEFAULT NULL COMMENT '单号-不同功能代表不同单呈',
  `clog_type` int(11) NOT NULL COMMENT '帐变类型',
  `clog_type_label` varchar(200) NOT NULL COMMENT '帐户日志类型名称',
  `params` varchar(200) NOT NULL COMMENT '抽象类型',
  `amount` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '备注(部分备注由类型,注单编号,内容自动生成)',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `record_time` datetime NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`),
  KEY `user_type` (`member_id`,`clog_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='现金帐变记录表';

/*Data for the table `tab_cash_record` */

/*Table structure for table `tab_cash_rewards` */

DROP TABLE IF EXISTS `tab_cash_rewards`;

CREATE TABLE `tab_cash_rewards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类型',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '值',
  `condition` int(11) NOT NULL DEFAULT '0' COMMENT '条件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='兑换奖励表';

/*Data for the table `tab_cash_rewards` */

insert  into `tab_cash_rewards`(`id`,`title`,`key`,`value`,`condition`) values (1,'团队奖','TEAM','0.02',0),(2,'物流中心','LOGISTICS','0.03',0),(3,'主管','DIRECTOR','0.35',300000),(4,'经理','MANAGER','0.30',600000),(5,'总监','MAJORDOMO','0.20',1200000),(6,'董事','CHAIRMAN','0.15',2400000),(7,'车房','GARAGE','0.03',4800000);

/*Table structure for table `tab_cron` */

DROP TABLE IF EXISTS `tab_cron`;

CREATE TABLE `tab_cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '定时任务名称',
  `route` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '定时任务路由',
  `cron_time` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '运行时间。crontab格式',
  `status` tinyint(1) NOT NULL COMMENT '任务状态。1运行中 2运行结束',
  `last_run_time` datetime NOT NULL COMMENT '上次运行时间',
  `next_run_time` datetime NOT NULL COMMENT '下次运行时间',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '启用状态。1启用 0关闭',
  `update_time` datetime NOT NULL COMMENT '任务配置更新时间',
  `record_time` datetime NOT NULL COMMENT '任务配置创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='定时任务配置';

/*Data for the table `tab_cron` */

insert  into `tab_cron`(`id`,`name`,`route`,`cron_time`,`status`,`last_run_time`,`next_run_time`,`active`,`update_time`,`record_time`) values (1,'记录玩家0点时的信息','cron-do/mark-user-data','0 0 * * *',1,'2019-11-23 14:24:49','2019-11-23 14:24:49',1,'2019-11-23 15:23:59','2019-11-23 14:24:49'),(2,'自动退水','cron-do/bet-back-daily','1 0 * * *',1,'2019-11-23 14:24:49','2019-11-23 14:24:49',1,'2019-11-23 15:23:59','2019-11-23 14:24:49'),(3,'清理垃圾数据','cron-do/clear-trash','0 5 * * *',1,'2019-11-23 14:24:49','2019-11-23 14:24:49',1,'2019-11-23 15:23:59','2019-11-23 14:24:49'),(4,'自动处理私彩数据','cron-do/ssc-personal','* * * * *',1,'2019-11-23 14:24:49','2019-11-23 14:24:49',1,'2019-11-23 15:23:59','2019-11-23 14:24:49'),(5,'自动处理重庆时时彩','cron-do/ssc-cq','* * * * *',1,'2019-11-23 14:24:49','2019-11-23 14:24:49',1,'2019-11-23 15:23:59','2019-11-23 14:24:49'),(6,'刷新在线人数','cron-do/refresh-online-num','* * * * *',1,'2019-11-23 14:24:49','2019-11-23 14:24:49',1,'2019-11-23 15:23:59','2019-11-23 14:24:49');

/*Table structure for table `tab_earn_rank` */

DROP TABLE IF EXISTS `tab_earn_rank`;

CREATE TABLE `tab_earn_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '中奖用户名',
  `er_type` int(11) NOT NULL COMMENT '中奖玩法',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '中奖金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='盈利排行榜数据表';

/*Data for the table `tab_earn_rank` */

/*Table structure for table `tab_earn_rank_real` */

DROP TABLE IF EXISTS `tab_earn_rank_real`;

CREATE TABLE `tab_earn_rank_real` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '中奖用户名',
  `er_type` int(11) NOT NULL COMMENT '中奖玩法',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '中奖金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='盈利排行榜数据表（真实数据）';

/*Data for the table `tab_earn_rank_real` */

/*Table structure for table `tab_goods` */

DROP TABLE IF EXISTS `tab_goods`;

CREATE TABLE `tab_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credits` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '兑换积分',
  `bonus_points` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '奖励分',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品名称',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片',
  `record_date` datetime NOT NULL COMMENT '添加时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='商品表';

/*Data for the table `tab_goods` */

insert  into `tab_goods`(`id`,`credits`,`bonus_points`,`name`,`content`,`images`,`record_date`,`update_date`) values (1,'10000.00','10000.00','橙子','橙子麻阳冰糖橙新鲜孕妇水果包邮批发甜橙手剥橙薄皮整箱10斤','shop_1574497234.jpg','2019-11-23 16:20:36','2019-11-23 16:20:36');

/*Table structure for table `tab_key_value` */

DROP TABLE IF EXISTS `tab_key_value`;

CREATE TABLE `tab_key_value` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '存储数据的key',
  `value` text COLLATE utf8mb4_unicode_ci COMMENT '存储内容',
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='key value 存储引擎';

/*Data for the table `tab_key_value` */

insert  into `tab_key_value`(`key`,`value`) values ('MEMBER_CONTACT_VO','{\"qqs\":[\"1302278079\",\"2483303495\"]}'),('DEPOSIT_TYPE_MAP','{\"2\":\"银行汇款\",\"3\":\"微信二维码\",\"4\":\"支付宝二维码\",\"5\":\"云闪付二维码\"}');

/*Table structure for table `tab_notice` */

DROP TABLE IF EXISTS `tab_notice`;

CREATE TABLE `tab_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL COMMENT '操作人员',
  `notice_type` tinyint(1) NOT NULL COMMENT '类型',
  `record_date` datetime NOT NULL COMMENT '添加时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='公告表';

/*Data for the table `tab_notice` */

/*Table structure for table `tab_order` */

DROP TABLE IF EXISTS `tab_order`;

CREATE TABLE `tab_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `admin_id` int(11) DEFAULT NULL COMMENT '操作人员ID',
  `goods_id` int(11) NOT NULL COMMENT '商品ID',
  `order_number` char(33) COLLATE utf8_german2_ci NOT NULL COMMENT '订单编号',
  `express_no` char(33) COLLATE utf8_german2_ci DEFAULT NULL,
  `order_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:待发货,2:已发货',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `integral` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单所需积分',
  `record_date` datetime NOT NULL COMMENT '添加时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `goods_name` varchar(255) COLLATE utf8_german2_ci NOT NULL COMMENT '商品名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `订单编号` (`order_number`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci COMMENT='订单表';

/*Data for the table `tab_order` */

insert  into `tab_order`(`id`,`user_id`,`admin_id`,`goods_id`,`order_number`,`express_no`,`order_state`,`order_time`,`integral`,`record_date`,`update_date`,`goods_name`) values (17,2,NULL,1,'15745130284907',NULL,1,'2019-11-23 20:43:48','10000.00','2019-11-23 20:43:48',NULL,'橙子'),(18,2,NULL,1,'15745130669990',NULL,1,'2019-11-23 20:44:26','10000.00','2019-11-23 20:44:26',NULL,'橙子');

/*Table structure for table `tab_order_wallet` */

DROP TABLE IF EXISTS `tab_order_wallet`;

CREATE TABLE `tab_order_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `agent_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL COMMENT '操作人员ID',
  `order_no` char(33) COLLATE utf8_german2_ci NOT NULL COMMENT '订单编号',
  `order_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:存款,2:取款',
  `order_sub_type` int(11) NOT NULL COMMENT '存取款方式',
  `order_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:待处理,2:已完成,3:已取消,4:已失败',
  `order_time` datetime NOT NULL COMMENT '下单时间',
  `finish_time` datetime DEFAULT NULL COMMENT '完成支付时间',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `pay_type` smallint(2) NOT NULL COMMENT '支付通道',
  `pay_username` varchar(200) COLLATE utf8_german2_ci DEFAULT NULL COMMENT '支付用户信息',
  `bank_name` varchar(50) COLLATE utf8_german2_ci DEFAULT NULL COMMENT '银行名字。如：中国工商银行',
  `bank_account` varchar(255) COLLATE utf8_german2_ci DEFAULT NULL COMMENT '银行卡编号',
  `bank_username` varchar(255) COLLATE utf8_german2_ci DEFAULT NULL COMMENT '开户姓名',
  `remark` varchar(500) COLLATE utf8_german2_ci DEFAULT NULL COMMENT '备注',
  `update_time` datetime DEFAULT NULL,
  `record_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `订单编号` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci COMMENT='钱包订单表';

/*Data for the table `tab_order_wallet` */

/*Table structure for table `tab_schedule` */

DROP TABLE IF EXISTS `tab_schedule`;

CREATE TABLE `tab_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(2) NOT NULL COMMENT '彩票类型',
  `kind_name` varchar(100) DEFAULT NULL COMMENT '类型名称-重复数据方便查看',
  `open_time` datetime NOT NULL COMMENT '开赛时间',
  `period` varchar(20) DEFAULT NULL COMMENT '期数',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0未开奖 1已开奖',
  `active` int(1) NOT NULL COMMENT '是否开启',
  `record_time` datetime NOT NULL COMMENT '新增时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `result_nums` varchar(10) DEFAULT NULL COMMENT '中奖号码1@2@3',
  `predict_nums` varchar(100) DEFAULT NULL COMMENT '预设结果',
  `payout_time` datetime DEFAULT NULL COMMENT '派彩时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kind` (`kind`,`period`),
  KEY `active` (`active`),
  KEY `game_type` (`kind`),
  KEY `open_time` (`open_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='彩票赛事表';

/*Data for the table `tab_schedule` */

/*Table structure for table `user_account` */

DROP TABLE IF EXISTS `user_account`;

CREATE TABLE `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(1) NOT NULL COMMENT '用户类型',
  `user_type_name` varchar(20) NOT NULL COMMENT '用户类型名称',
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `password2` varchar(500) DEFAULT NULL COMMENT '资金密码',
  `name` varchar(100) NOT NULL,
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `record_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `modify_user_type` int(11) DEFAULT NULL,
  `modify_user_id` int(11) NOT NULL,
  `modify_username` varchar(100) DEFAULT NULL,
  `modify_ip` varchar(30) DEFAULT NULL,
  `session_id` varchar(100) DEFAULT NULL,
  `session_dead_time` datetime DEFAULT NULL COMMENT 'session有效截止时间。用于判断会员是否登录',
  `login_fail_times` int(11) NOT NULL COMMENT '登录失败次数',
  `lock` int(11) NOT NULL COMMENT '是否锁定',
  `login_time` datetime DEFAULT NULL COMMENT '最近登录时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最近登录ip',
  `last_login_time` datetime DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(100) DEFAULT NULL COMMENT '上次登录IP',
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`modify_user_type`,`modify_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户帐号表';

/*Data for the table `user_account` */

insert  into `user_account`(`id`,`user_type`,`user_type_name`,`username`,`password`,`password2`,`name`,`remark`,`record_time`,`update_time`,`modify_user_type`,`modify_user_id`,`modify_username`,`modify_ip`,`session_id`,`session_dead_time`,`login_fail_times`,`lock`,`login_time`,`login_ip`,`last_login_time`,`last_login_ip`) values (1,99,'管理员','admin','lsrjXOipsCRBeL8o5JZsLOG4OFcjqWprg4hYzdbKCh4=',NULL,'系统管理员',NULL,'2019-11-08 09:31:38','2019-11-23 21:03:12',99,0,NULL,'127.0.0.1','SID5dd8e2139b66c','2019-11-23 21:08:12',0,0,'2019-11-23 15:38:59','127.0.0.1','2019-11-12 18:19:16','127.0.0.1'),(2,1,'会员','member','lsrjXOipsCRBeL8o5JZsLOG4OFcjqWprg4hYzdbKCh4=','lsrjXOipsCRBeL8o5JZsLOG4OFcjqWprg4hYzdbKCh4=','会员','','2019-11-08 09:34:27','2019-11-23 21:03:10',99,1,'admin','127.0.0.1','SID5dd90c7bdbf9c','2019-11-23 21:03:40',0,0,'2019-11-23 18:39:55','127.0.0.1','2019-11-23 16:21:34','127.0.0.1');

/*Table structure for table `user_admin` */

DROP TABLE IF EXISTS `user_admin`;

CREATE TABLE `user_admin` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '重复数据-便于查看',
  `name` varchar(100) NOT NULL COMMENT '重复数据-便于查看',
  `is_admin` int(11) NOT NULL COMMENT '是否超级管理员',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';

/*Data for the table `user_admin` */

insert  into `user_admin`(`user_id`,`username`,`name`,`is_admin`) values (1,'admin','系统管理员',1);

/*Table structure for table `user_agent` */

DROP TABLE IF EXISTS `user_agent`;

CREATE TABLE `user_agent` (
  `user_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` decimal(10,2) NOT NULL,
  `fraction_vendor` decimal(3,2) NOT NULL,
  `fration_max` decimal(3,2) NOT NULL,
  `invite_code` varchar(50) DEFAULT NULL COMMENT '邀请码',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理表';

/*Data for the table `user_agent` */

/*Table structure for table `user_holder` */

DROP TABLE IF EXISTS `user_holder`;

CREATE TABLE `user_holder` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` decimal(10,2) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `fraction_max` double NOT NULL COMMENT '股东占成上限',
  `fraction_super_holder` double NOT NULL COMMENT '大股东占成',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='股东表';

/*Data for the table `user_holder` */

/*Table structure for table `user_member` */

DROP TABLE IF EXISTS `user_member`;

CREATE TABLE `user_member` (
  `user_id` int(11) NOT NULL,
  `is_direct` int(11) NOT NULL COMMENT '是否直属会员',
  `direct_user_type` int(11) NOT NULL COMMENT '直属上线类型',
  `direct_user_id` int(11) NOT NULL COMMENT '直属上线的ID',
  `super_holder_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `vip_level` int(1) NOT NULL DEFAULT '0' COMMENT 'VIP等级',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` decimal(12,2) NOT NULL,
  `last_back_date` date DEFAULT NULL COMMENT '最近返还下单金额日',
  `integral` decimal(12,2) DEFAULT NULL,
  `last_sign_date` date DEFAULT NULL COMMENT '签到时间',
  `last_withdraw_date` date DEFAULT NULL COMMENT '最近提款时间',
  `last_withdraw_times` int(11) NOT NULL DEFAULT '0' COMMENT '最近提款次数',
  `bank_id` int(11) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `bank_account` varchar(50) DEFAULT NULL,
  `bank_username` varchar(50) DEFAULT NULL,
  `bank_email` varchar(100) DEFAULT NULL,
  `before_vip_level` int(1) DEFAULT NULL COMMENT '昨日vip等级',
  `total_pay` int(11) NOT NULL DEFAULT '0' COMMENT '累计充值',
  `total_bet` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '累计下注(消费)',
  `before_credit` decimal(12,2) DEFAULT NULL COMMENT '昨日账户余额',
  `before_date` datetime DEFAULT NULL COMMENT '上次记录之前信息的时间',
  `place_order_rebated` decimal(12,2) DEFAULT NULL COMMENT '昨天下单返利',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员表';

/*Data for the table `user_member` */

insert  into `user_member`(`user_id`,`is_direct`,`direct_user_type`,`direct_user_id`,`super_holder_id`,`holder_id`,`vendor_id`,`agent_id`,`username`,`name`,`vip_level`,`active`,`is_delete`,`credit`,`last_back_date`,`integral`,`last_sign_date`,`last_withdraw_date`,`last_withdraw_times`,`bank_id`,`bank_name`,`bank_account`,`bank_username`,`bank_email`,`before_vip_level`,`total_pay`,`total_bet`,`before_credit`,`before_date`,`place_order_rebated`) values (2,0,2,5,2,3,4,5,'member','会员',0,1,0,'205400.00',NULL,'55000.00','2019-11-12','2019-11-13',2,6,'平安银行','6222000011112222123','发叫撒','48456@qq.com',NULL,0,'0.00',NULL,NULL,NULL);

/*Table structure for table `user_super_holder` */

DROP TABLE IF EXISTS `user_super_holder`;

CREATE TABLE `user_super_holder` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` decimal(10,2) NOT NULL,
  `commission_plan` int(11) NOT NULL,
  `company_report` int(11) NOT NULL COMMENT '是否可查看公司报表',
  `include_below` int(11) NOT NULL COMMENT '是否包底',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='大股东表';

/*Data for the table `user_super_holder` */

/*Table structure for table `user_vendor` */

DROP TABLE IF EXISTS `user_vendor`;

CREATE TABLE `user_vendor` (
  `user_id` int(11) NOT NULL,
  `holder_id` int(11) NOT NULL,
  `super_holder_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名-重复数据用于查看',
  `name` varchar(100) NOT NULL COMMENT '呢称-重复数据用于查看',
  `active` int(1) NOT NULL DEFAULT '1',
  `is_delete` int(1) NOT NULL DEFAULT '0',
  `credit` decimal(10,2) NOT NULL,
  `fraction_holder` decimal(3,2) NOT NULL COMMENT '股东占成',
  `fraction_max` double(3,2) NOT NULL COMMENT '总代理占成上限',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='总代理表';

/*Data for the table `user_vendor` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
